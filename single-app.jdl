
application {
  config {
    baseName teleconsultation,
    applicationType monolith,
    packageName com.pascob.teleconsultation,
    authenticationType jwt,
    devDatabaseType postgresql,
    prodDatabaseType postgresql,
    clientFramework angularX
  }
  entities *
}

entity FormationSanitaire {
	designation String required
    sigle String
    telephone String
    fixe String
    bp String
    email String required pattern(/^[^@\s]+@[^@\s]+\.[^@\s]+$/)
    logo ImageBlob
} 

entity Specialite {
	designation String required
    frais Double min(0) 
}

entity Medecin {
	userId Long required
	matricule String
    signature ImageBlob
}

entity Patient {
	userId Long required
	dateNaissance LocalDate
    genre Genre
}

enum Genre {
    M, F
}

entity DossierMedical {
	numero String required
    groupeSanguin GroupeSanguin
}

enum GroupeSanguin {
    O_PLUS, AB_PLUS, A_PLUS, B_PLUS, O_MOINS, AB_MOINS, A_MOINS, B_MOINS
}

entity DemandeConsultation {
	dateHeureDemande ZonedDateTime
    ordre Integer
    statut StatutDemandeConsultation
    codeOtp String
    telephonePaiement String
    montant Double min(0)
}

enum StatutDemandeConsultation {
    EN_COURS, PAYE, EN_CONSULTATION, DELAIS_EXPIRE, CONSULTE
}

entity Consultation {
	poids Integer min(0)
    temperature Float min(0)
    pouls Integer min(0)
    bpm Integer min(0)
    age Float min(0)
    taille Integer min(0)
    dateDebutConsulation ZonedDateTime
    isSuivi Boolean
    isClose Boolean
}

entity FicheConsultation {
	dateFiche LocalDate required
    numero String
    observation String
    diagnostic String
} 

entity Produit {
	designation String required
} 
entity Ordonnance {
	dateOrdonnance LocalDate required
    numero String
} 

@skipClient
entity PrescriptionProduit {
	posologie String
    observation String
}

entity ExamenMedical {
	designation String required
    code String
} 

entity Bulletin {
	dateBulletin LocalDate required
    numero String
}

@skipClient
entity PrescriptionExamen {
	description String
    resultat String
} 

entity Symptome {
	designation String required
    code String
}

@skipClient
entity FicheSymptome {
	observation String
}

entity Message {
	dateMessage ZonedDateTime
	message String
    userIdFrom Long
    userIdTo Long
    readed Boolean
}

entity Notification {
	dateNotification ZonedDateTime
    objet String
    corpus String
    sender String
    receiver String
    typeNotification TypeNotification
}

enum TypeNotification {
	EMAIL, ALERT, SMS
}

relationship OneToMany {
  FormationSanitaire to Medecin,
  Medecin to DossierMedical,
  Medecin to Consultation,
  Medecin to FicheConsultation,
  Specialite to Medecin,
  Specialite to DemandeConsultation,
  DossierMedical to DemandeConsultation,
  DemandeConsultation to Consultation,
  Consultation to FicheConsultation,
  FicheConsultation to Ordonnance,
  FicheConsultation to Bulletin,
  Produit to PrescriptionProduit,
  Ordonnance to PrescriptionProduit,
  Bulletin to PrescriptionExamen,
  ExamenMedical to PrescriptionExamen,
  FicheConsultation to FicheSymptome,
  Symptome to FicheSymptome
}

relationship OneToOne {
	Patient to DossierMedical
}

paginate Message, Notification with infinite-scroll
paginate all with pagination except Message, Notification
dto * with mapstruct
service * with serviceClass
filter *

    