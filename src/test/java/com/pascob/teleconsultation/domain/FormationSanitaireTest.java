package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FormationSanitaireTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FormationSanitaire.class);
        FormationSanitaire formationSanitaire1 = new FormationSanitaire();
        formationSanitaire1.setId(1L);
        FormationSanitaire formationSanitaire2 = new FormationSanitaire();
        formationSanitaire2.setId(formationSanitaire1.getId());
        assertThat(formationSanitaire1).isEqualTo(formationSanitaire2);
        formationSanitaire2.setId(2L);
        assertThat(formationSanitaire1).isNotEqualTo(formationSanitaire2);
        formationSanitaire1.setId(null);
        assertThat(formationSanitaire1).isNotEqualTo(formationSanitaire2);
    }
}
