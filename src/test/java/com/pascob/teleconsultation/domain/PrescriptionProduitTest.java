package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PrescriptionProduitTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrescriptionProduit.class);
        PrescriptionProduit prescriptionProduit1 = new PrescriptionProduit();
        prescriptionProduit1.setId(1L);
        PrescriptionProduit prescriptionProduit2 = new PrescriptionProduit();
        prescriptionProduit2.setId(prescriptionProduit1.getId());
        assertThat(prescriptionProduit1).isEqualTo(prescriptionProduit2);
        prescriptionProduit2.setId(2L);
        assertThat(prescriptionProduit1).isNotEqualTo(prescriptionProduit2);
        prescriptionProduit1.setId(null);
        assertThat(prescriptionProduit1).isNotEqualTo(prescriptionProduit2);
    }
}
