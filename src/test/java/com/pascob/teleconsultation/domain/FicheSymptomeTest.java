package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FicheSymptomeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FicheSymptome.class);
        FicheSymptome ficheSymptome1 = new FicheSymptome();
        ficheSymptome1.setId(1L);
        FicheSymptome ficheSymptome2 = new FicheSymptome();
        ficheSymptome2.setId(ficheSymptome1.getId());
        assertThat(ficheSymptome1).isEqualTo(ficheSymptome2);
        ficheSymptome2.setId(2L);
        assertThat(ficheSymptome1).isNotEqualTo(ficheSymptome2);
        ficheSymptome1.setId(null);
        assertThat(ficheSymptome1).isNotEqualTo(ficheSymptome2);
    }
}
