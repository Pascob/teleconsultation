package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SymptomeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Symptome.class);
        Symptome symptome1 = new Symptome();
        symptome1.setId(1L);
        Symptome symptome2 = new Symptome();
        symptome2.setId(symptome1.getId());
        assertThat(symptome1).isEqualTo(symptome2);
        symptome2.setId(2L);
        assertThat(symptome1).isNotEqualTo(symptome2);
        symptome1.setId(null);
        assertThat(symptome1).isNotEqualTo(symptome2);
    }
}
