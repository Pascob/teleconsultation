package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PrescriptionExamenTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrescriptionExamen.class);
        PrescriptionExamen prescriptionExamen1 = new PrescriptionExamen();
        prescriptionExamen1.setId(1L);
        PrescriptionExamen prescriptionExamen2 = new PrescriptionExamen();
        prescriptionExamen2.setId(prescriptionExamen1.getId());
        assertThat(prescriptionExamen1).isEqualTo(prescriptionExamen2);
        prescriptionExamen2.setId(2L);
        assertThat(prescriptionExamen1).isNotEqualTo(prescriptionExamen2);
        prescriptionExamen1.setId(null);
        assertThat(prescriptionExamen1).isNotEqualTo(prescriptionExamen2);
    }
}
