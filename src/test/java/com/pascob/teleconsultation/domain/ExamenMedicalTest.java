package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExamenMedicalTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExamenMedical.class);
        ExamenMedical examenMedical1 = new ExamenMedical();
        examenMedical1.setId(1L);
        ExamenMedical examenMedical2 = new ExamenMedical();
        examenMedical2.setId(examenMedical1.getId());
        assertThat(examenMedical1).isEqualTo(examenMedical2);
        examenMedical2.setId(2L);
        assertThat(examenMedical1).isNotEqualTo(examenMedical2);
        examenMedical1.setId(null);
        assertThat(examenMedical1).isNotEqualTo(examenMedical2);
    }
}
