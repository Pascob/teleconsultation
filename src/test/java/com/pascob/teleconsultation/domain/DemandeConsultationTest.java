package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DemandeConsultationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DemandeConsultation.class);
        DemandeConsultation demandeConsultation1 = new DemandeConsultation();
        demandeConsultation1.setId(1L);
        DemandeConsultation demandeConsultation2 = new DemandeConsultation();
        demandeConsultation2.setId(demandeConsultation1.getId());
        assertThat(demandeConsultation1).isEqualTo(demandeConsultation2);
        demandeConsultation2.setId(2L);
        assertThat(demandeConsultation1).isNotEqualTo(demandeConsultation2);
        demandeConsultation1.setId(null);
        assertThat(demandeConsultation1).isNotEqualTo(demandeConsultation2);
    }
}
