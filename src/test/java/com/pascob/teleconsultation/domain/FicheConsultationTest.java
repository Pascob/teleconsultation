package com.pascob.teleconsultation.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FicheConsultationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FicheConsultation.class);
        FicheConsultation ficheConsultation1 = new FicheConsultation();
        ficheConsultation1.setId(1L);
        FicheConsultation ficheConsultation2 = new FicheConsultation();
        ficheConsultation2.setId(ficheConsultation1.getId());
        assertThat(ficheConsultation1).isEqualTo(ficheConsultation2);
        ficheConsultation2.setId(2L);
        assertThat(ficheConsultation1).isNotEqualTo(ficheConsultation2);
        ficheConsultation1.setId(null);
        assertThat(ficheConsultation1).isNotEqualTo(ficheConsultation2);
    }
}
