package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Bulletin;
import com.pascob.teleconsultation.domain.Consultation;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.FicheSymptome;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.domain.Ordonnance;
import com.pascob.teleconsultation.repository.FicheConsultationRepository;
import com.pascob.teleconsultation.service.criteria.FicheConsultationCriteria;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import com.pascob.teleconsultation.service.mapper.FicheConsultationMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FicheConsultationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FicheConsultationResourceIT {

    private static final LocalDate DEFAULT_DATE_FICHE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FICHE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_FICHE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final String DEFAULT_OBSERVATION = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVATION = "BBBBBBBBBB";

    private static final String DEFAULT_DIAGNOSTIC = "AAAAAAAAAA";
    private static final String UPDATED_DIAGNOSTIC = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/fiche-consultations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FicheConsultationRepository ficheConsultationRepository;

    @Autowired
    private FicheConsultationMapper ficheConsultationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFicheConsultationMockMvc;

    private FicheConsultation ficheConsultation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FicheConsultation createEntity(EntityManager em) {
        FicheConsultation ficheConsultation = new FicheConsultation()
            .dateFiche(DEFAULT_DATE_FICHE)
            .numero(DEFAULT_NUMERO)
            .observation(DEFAULT_OBSERVATION)
            .diagnostic(DEFAULT_DIAGNOSTIC);
        return ficheConsultation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FicheConsultation createUpdatedEntity(EntityManager em) {
        FicheConsultation ficheConsultation = new FicheConsultation()
            .dateFiche(UPDATED_DATE_FICHE)
            .numero(UPDATED_NUMERO)
            .observation(UPDATED_OBSERVATION)
            .diagnostic(UPDATED_DIAGNOSTIC);
        return ficheConsultation;
    }

    @BeforeEach
    public void initTest() {
        ficheConsultation = createEntity(em);
    }

    @Test
    @Transactional
    void createFicheConsultation() throws Exception {
        int databaseSizeBeforeCreate = ficheConsultationRepository.findAll().size();
        // Create the FicheConsultation
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);
        restFicheConsultationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeCreate + 1);
        FicheConsultation testFicheConsultation = ficheConsultationList.get(ficheConsultationList.size() - 1);
        assertThat(testFicheConsultation.getDateFiche()).isEqualTo(DEFAULT_DATE_FICHE);
        assertThat(testFicheConsultation.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testFicheConsultation.getObservation()).isEqualTo(DEFAULT_OBSERVATION);
        assertThat(testFicheConsultation.getDiagnostic()).isEqualTo(DEFAULT_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void createFicheConsultationWithExistingId() throws Exception {
        // Create the FicheConsultation with an existing ID
        ficheConsultation.setId(1L);
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        int databaseSizeBeforeCreate = ficheConsultationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFicheConsultationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDateFicheIsRequired() throws Exception {
        int databaseSizeBeforeTest = ficheConsultationRepository.findAll().size();
        // set the field null
        ficheConsultation.setDateFiche(null);

        // Create the FicheConsultation, which fails.
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        restFicheConsultationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllFicheConsultations() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList
        restFicheConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ficheConsultation.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateFiche").value(hasItem(DEFAULT_DATE_FICHE.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].observation").value(hasItem(DEFAULT_OBSERVATION)))
            .andExpect(jsonPath("$.[*].diagnostic").value(hasItem(DEFAULT_DIAGNOSTIC)));
    }

    @Test
    @Transactional
    void getFicheConsultation() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get the ficheConsultation
        restFicheConsultationMockMvc
            .perform(get(ENTITY_API_URL_ID, ficheConsultation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ficheConsultation.getId().intValue()))
            .andExpect(jsonPath("$.dateFiche").value(DEFAULT_DATE_FICHE.toString()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO))
            .andExpect(jsonPath("$.observation").value(DEFAULT_OBSERVATION))
            .andExpect(jsonPath("$.diagnostic").value(DEFAULT_DIAGNOSTIC));
    }

    @Test
    @Transactional
    void getFicheConsultationsByIdFiltering() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        Long id = ficheConsultation.getId();

        defaultFicheConsultationShouldBeFound("id.equals=" + id);
        defaultFicheConsultationShouldNotBeFound("id.notEquals=" + id);

        defaultFicheConsultationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFicheConsultationShouldNotBeFound("id.greaterThan=" + id);

        defaultFicheConsultationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFicheConsultationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDateFicheIsEqualToSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where dateFiche equals to DEFAULT_DATE_FICHE
        defaultFicheConsultationShouldBeFound("dateFiche.equals=" + DEFAULT_DATE_FICHE);

        // Get all the ficheConsultationList where dateFiche equals to UPDATED_DATE_FICHE
        defaultFicheConsultationShouldNotBeFound("dateFiche.equals=" + UPDATED_DATE_FICHE);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDateFicheIsInShouldWork() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where dateFiche in DEFAULT_DATE_FICHE or UPDATED_DATE_FICHE
        defaultFicheConsultationShouldBeFound("dateFiche.in=" + DEFAULT_DATE_FICHE + "," + UPDATED_DATE_FICHE);

        // Get all the ficheConsultationList where dateFiche equals to UPDATED_DATE_FICHE
        defaultFicheConsultationShouldNotBeFound("dateFiche.in=" + UPDATED_DATE_FICHE);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDateFicheIsNullOrNotNull() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where dateFiche is not null
        defaultFicheConsultationShouldBeFound("dateFiche.specified=true");

        // Get all the ficheConsultationList where dateFiche is null
        defaultFicheConsultationShouldNotBeFound("dateFiche.specified=false");
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDateFicheIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where dateFiche is greater than or equal to DEFAULT_DATE_FICHE
        defaultFicheConsultationShouldBeFound("dateFiche.greaterThanOrEqual=" + DEFAULT_DATE_FICHE);

        // Get all the ficheConsultationList where dateFiche is greater than or equal to UPDATED_DATE_FICHE
        defaultFicheConsultationShouldNotBeFound("dateFiche.greaterThanOrEqual=" + UPDATED_DATE_FICHE);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDateFicheIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where dateFiche is less than or equal to DEFAULT_DATE_FICHE
        defaultFicheConsultationShouldBeFound("dateFiche.lessThanOrEqual=" + DEFAULT_DATE_FICHE);

        // Get all the ficheConsultationList where dateFiche is less than or equal to SMALLER_DATE_FICHE
        defaultFicheConsultationShouldNotBeFound("dateFiche.lessThanOrEqual=" + SMALLER_DATE_FICHE);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDateFicheIsLessThanSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where dateFiche is less than DEFAULT_DATE_FICHE
        defaultFicheConsultationShouldNotBeFound("dateFiche.lessThan=" + DEFAULT_DATE_FICHE);

        // Get all the ficheConsultationList where dateFiche is less than UPDATED_DATE_FICHE
        defaultFicheConsultationShouldBeFound("dateFiche.lessThan=" + UPDATED_DATE_FICHE);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDateFicheIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where dateFiche is greater than DEFAULT_DATE_FICHE
        defaultFicheConsultationShouldNotBeFound("dateFiche.greaterThan=" + DEFAULT_DATE_FICHE);

        // Get all the ficheConsultationList where dateFiche is greater than SMALLER_DATE_FICHE
        defaultFicheConsultationShouldBeFound("dateFiche.greaterThan=" + SMALLER_DATE_FICHE);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByNumeroIsEqualToSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where numero equals to DEFAULT_NUMERO
        defaultFicheConsultationShouldBeFound("numero.equals=" + DEFAULT_NUMERO);

        // Get all the ficheConsultationList where numero equals to UPDATED_NUMERO
        defaultFicheConsultationShouldNotBeFound("numero.equals=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByNumeroIsInShouldWork() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where numero in DEFAULT_NUMERO or UPDATED_NUMERO
        defaultFicheConsultationShouldBeFound("numero.in=" + DEFAULT_NUMERO + "," + UPDATED_NUMERO);

        // Get all the ficheConsultationList where numero equals to UPDATED_NUMERO
        defaultFicheConsultationShouldNotBeFound("numero.in=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByNumeroIsNullOrNotNull() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where numero is not null
        defaultFicheConsultationShouldBeFound("numero.specified=true");

        // Get all the ficheConsultationList where numero is null
        defaultFicheConsultationShouldNotBeFound("numero.specified=false");
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByNumeroContainsSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where numero contains DEFAULT_NUMERO
        defaultFicheConsultationShouldBeFound("numero.contains=" + DEFAULT_NUMERO);

        // Get all the ficheConsultationList where numero contains UPDATED_NUMERO
        defaultFicheConsultationShouldNotBeFound("numero.contains=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByNumeroNotContainsSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where numero does not contain DEFAULT_NUMERO
        defaultFicheConsultationShouldNotBeFound("numero.doesNotContain=" + DEFAULT_NUMERO);

        // Get all the ficheConsultationList where numero does not contain UPDATED_NUMERO
        defaultFicheConsultationShouldBeFound("numero.doesNotContain=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByObservationIsEqualToSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where observation equals to DEFAULT_OBSERVATION
        defaultFicheConsultationShouldBeFound("observation.equals=" + DEFAULT_OBSERVATION);

        // Get all the ficheConsultationList where observation equals to UPDATED_OBSERVATION
        defaultFicheConsultationShouldNotBeFound("observation.equals=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByObservationIsInShouldWork() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where observation in DEFAULT_OBSERVATION or UPDATED_OBSERVATION
        defaultFicheConsultationShouldBeFound("observation.in=" + DEFAULT_OBSERVATION + "," + UPDATED_OBSERVATION);

        // Get all the ficheConsultationList where observation equals to UPDATED_OBSERVATION
        defaultFicheConsultationShouldNotBeFound("observation.in=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByObservationIsNullOrNotNull() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where observation is not null
        defaultFicheConsultationShouldBeFound("observation.specified=true");

        // Get all the ficheConsultationList where observation is null
        defaultFicheConsultationShouldNotBeFound("observation.specified=false");
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByObservationContainsSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where observation contains DEFAULT_OBSERVATION
        defaultFicheConsultationShouldBeFound("observation.contains=" + DEFAULT_OBSERVATION);

        // Get all the ficheConsultationList where observation contains UPDATED_OBSERVATION
        defaultFicheConsultationShouldNotBeFound("observation.contains=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByObservationNotContainsSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where observation does not contain DEFAULT_OBSERVATION
        defaultFicheConsultationShouldNotBeFound("observation.doesNotContain=" + DEFAULT_OBSERVATION);

        // Get all the ficheConsultationList where observation does not contain UPDATED_OBSERVATION
        defaultFicheConsultationShouldBeFound("observation.doesNotContain=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDiagnosticIsEqualToSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where diagnostic equals to DEFAULT_DIAGNOSTIC
        defaultFicheConsultationShouldBeFound("diagnostic.equals=" + DEFAULT_DIAGNOSTIC);

        // Get all the ficheConsultationList where diagnostic equals to UPDATED_DIAGNOSTIC
        defaultFicheConsultationShouldNotBeFound("diagnostic.equals=" + UPDATED_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDiagnosticIsInShouldWork() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where diagnostic in DEFAULT_DIAGNOSTIC or UPDATED_DIAGNOSTIC
        defaultFicheConsultationShouldBeFound("diagnostic.in=" + DEFAULT_DIAGNOSTIC + "," + UPDATED_DIAGNOSTIC);

        // Get all the ficheConsultationList where diagnostic equals to UPDATED_DIAGNOSTIC
        defaultFicheConsultationShouldNotBeFound("diagnostic.in=" + UPDATED_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDiagnosticIsNullOrNotNull() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where diagnostic is not null
        defaultFicheConsultationShouldBeFound("diagnostic.specified=true");

        // Get all the ficheConsultationList where diagnostic is null
        defaultFicheConsultationShouldNotBeFound("diagnostic.specified=false");
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDiagnosticContainsSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where diagnostic contains DEFAULT_DIAGNOSTIC
        defaultFicheConsultationShouldBeFound("diagnostic.contains=" + DEFAULT_DIAGNOSTIC);

        // Get all the ficheConsultationList where diagnostic contains UPDATED_DIAGNOSTIC
        defaultFicheConsultationShouldNotBeFound("diagnostic.contains=" + UPDATED_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByDiagnosticNotContainsSomething() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        // Get all the ficheConsultationList where diagnostic does not contain DEFAULT_DIAGNOSTIC
        defaultFicheConsultationShouldNotBeFound("diagnostic.doesNotContain=" + DEFAULT_DIAGNOSTIC);

        // Get all the ficheConsultationList where diagnostic does not contain UPDATED_DIAGNOSTIC
        defaultFicheConsultationShouldBeFound("diagnostic.doesNotContain=" + UPDATED_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByOrdonnanceIsEqualToSomething() throws Exception {
        Ordonnance ordonnance;
        if (TestUtil.findAll(em, Ordonnance.class).isEmpty()) {
            ficheConsultationRepository.saveAndFlush(ficheConsultation);
            ordonnance = OrdonnanceResourceIT.createEntity(em);
        } else {
            ordonnance = TestUtil.findAll(em, Ordonnance.class).get(0);
        }
        em.persist(ordonnance);
        em.flush();
        ficheConsultation.addOrdonnance(ordonnance);
        ficheConsultationRepository.saveAndFlush(ficheConsultation);
        Long ordonnanceId = ordonnance.getId();

        // Get all the ficheConsultationList where ordonnance equals to ordonnanceId
        defaultFicheConsultationShouldBeFound("ordonnanceId.equals=" + ordonnanceId);

        // Get all the ficheConsultationList where ordonnance equals to (ordonnanceId + 1)
        defaultFicheConsultationShouldNotBeFound("ordonnanceId.equals=" + (ordonnanceId + 1));
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByBulletinIsEqualToSomething() throws Exception {
        Bulletin bulletin;
        if (TestUtil.findAll(em, Bulletin.class).isEmpty()) {
            ficheConsultationRepository.saveAndFlush(ficheConsultation);
            bulletin = BulletinResourceIT.createEntity(em);
        } else {
            bulletin = TestUtil.findAll(em, Bulletin.class).get(0);
        }
        em.persist(bulletin);
        em.flush();
        ficheConsultation.addBulletin(bulletin);
        ficheConsultationRepository.saveAndFlush(ficheConsultation);
        Long bulletinId = bulletin.getId();

        // Get all the ficheConsultationList where bulletin equals to bulletinId
        defaultFicheConsultationShouldBeFound("bulletinId.equals=" + bulletinId);

        // Get all the ficheConsultationList where bulletin equals to (bulletinId + 1)
        defaultFicheConsultationShouldNotBeFound("bulletinId.equals=" + (bulletinId + 1));
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByFicheSymptomeIsEqualToSomething() throws Exception {
        FicheSymptome ficheSymptome;
        if (TestUtil.findAll(em, FicheSymptome.class).isEmpty()) {
            ficheConsultationRepository.saveAndFlush(ficheConsultation);
            ficheSymptome = FicheSymptomeResourceIT.createEntity(em);
        } else {
            ficheSymptome = TestUtil.findAll(em, FicheSymptome.class).get(0);
        }
        em.persist(ficheSymptome);
        em.flush();
        ficheConsultation.addFicheSymptome(ficheSymptome);
        ficheConsultationRepository.saveAndFlush(ficheConsultation);
        Long ficheSymptomeId = ficheSymptome.getId();

        // Get all the ficheConsultationList where ficheSymptome equals to ficheSymptomeId
        defaultFicheConsultationShouldBeFound("ficheSymptomeId.equals=" + ficheSymptomeId);

        // Get all the ficheConsultationList where ficheSymptome equals to (ficheSymptomeId + 1)
        defaultFicheConsultationShouldNotBeFound("ficheSymptomeId.equals=" + (ficheSymptomeId + 1));
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByMedecinIsEqualToSomething() throws Exception {
        Medecin medecin;
        if (TestUtil.findAll(em, Medecin.class).isEmpty()) {
            ficheConsultationRepository.saveAndFlush(ficheConsultation);
            medecin = MedecinResourceIT.createEntity(em);
        } else {
            medecin = TestUtil.findAll(em, Medecin.class).get(0);
        }
        em.persist(medecin);
        em.flush();
        ficheConsultation.setMedecin(medecin);
        ficheConsultationRepository.saveAndFlush(ficheConsultation);
        Long medecinId = medecin.getId();

        // Get all the ficheConsultationList where medecin equals to medecinId
        defaultFicheConsultationShouldBeFound("medecinId.equals=" + medecinId);

        // Get all the ficheConsultationList where medecin equals to (medecinId + 1)
        defaultFicheConsultationShouldNotBeFound("medecinId.equals=" + (medecinId + 1));
    }

    @Test
    @Transactional
    void getAllFicheConsultationsByConsultationIsEqualToSomething() throws Exception {
        Consultation consultation;
        if (TestUtil.findAll(em, Consultation.class).isEmpty()) {
            ficheConsultationRepository.saveAndFlush(ficheConsultation);
            consultation = ConsultationResourceIT.createEntity(em);
        } else {
            consultation = TestUtil.findAll(em, Consultation.class).get(0);
        }
        em.persist(consultation);
        em.flush();
        ficheConsultation.setConsultation(consultation);
        ficheConsultationRepository.saveAndFlush(ficheConsultation);
        Long consultationId = consultation.getId();

        // Get all the ficheConsultationList where consultation equals to consultationId
        defaultFicheConsultationShouldBeFound("consultationId.equals=" + consultationId);

        // Get all the ficheConsultationList where consultation equals to (consultationId + 1)
        defaultFicheConsultationShouldNotBeFound("consultationId.equals=" + (consultationId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFicheConsultationShouldBeFound(String filter) throws Exception {
        restFicheConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ficheConsultation.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateFiche").value(hasItem(DEFAULT_DATE_FICHE.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].observation").value(hasItem(DEFAULT_OBSERVATION)))
            .andExpect(jsonPath("$.[*].diagnostic").value(hasItem(DEFAULT_DIAGNOSTIC)));

        // Check, that the count call also returns 1
        restFicheConsultationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFicheConsultationShouldNotBeFound(String filter) throws Exception {
        restFicheConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFicheConsultationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingFicheConsultation() throws Exception {
        // Get the ficheConsultation
        restFicheConsultationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingFicheConsultation() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();

        // Update the ficheConsultation
        FicheConsultation updatedFicheConsultation = ficheConsultationRepository.findById(ficheConsultation.getId()).get();
        // Disconnect from session so that the updates on updatedFicheConsultation are not directly saved in db
        em.detach(updatedFicheConsultation);
        updatedFicheConsultation
            .dateFiche(UPDATED_DATE_FICHE)
            .numero(UPDATED_NUMERO)
            .observation(UPDATED_OBSERVATION)
            .diagnostic(UPDATED_DIAGNOSTIC);
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(updatedFicheConsultation);

        restFicheConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ficheConsultationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isOk());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
        FicheConsultation testFicheConsultation = ficheConsultationList.get(ficheConsultationList.size() - 1);
        assertThat(testFicheConsultation.getDateFiche()).isEqualTo(UPDATED_DATE_FICHE);
        assertThat(testFicheConsultation.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testFicheConsultation.getObservation()).isEqualTo(UPDATED_OBSERVATION);
        assertThat(testFicheConsultation.getDiagnostic()).isEqualTo(UPDATED_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void putNonExistingFicheConsultation() throws Exception {
        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();
        ficheConsultation.setId(count.incrementAndGet());

        // Create the FicheConsultation
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFicheConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ficheConsultationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFicheConsultation() throws Exception {
        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();
        ficheConsultation.setId(count.incrementAndGet());

        // Create the FicheConsultation
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFicheConsultation() throws Exception {
        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();
        ficheConsultation.setId(count.incrementAndGet());

        // Create the FicheConsultation
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheConsultationMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFicheConsultationWithPatch() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();

        // Update the ficheConsultation using partial update
        FicheConsultation partialUpdatedFicheConsultation = new FicheConsultation();
        partialUpdatedFicheConsultation.setId(ficheConsultation.getId());

        partialUpdatedFicheConsultation.dateFiche(UPDATED_DATE_FICHE).observation(UPDATED_OBSERVATION);

        restFicheConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFicheConsultation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFicheConsultation))
            )
            .andExpect(status().isOk());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
        FicheConsultation testFicheConsultation = ficheConsultationList.get(ficheConsultationList.size() - 1);
        assertThat(testFicheConsultation.getDateFiche()).isEqualTo(UPDATED_DATE_FICHE);
        assertThat(testFicheConsultation.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testFicheConsultation.getObservation()).isEqualTo(UPDATED_OBSERVATION);
        assertThat(testFicheConsultation.getDiagnostic()).isEqualTo(DEFAULT_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void fullUpdateFicheConsultationWithPatch() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();

        // Update the ficheConsultation using partial update
        FicheConsultation partialUpdatedFicheConsultation = new FicheConsultation();
        partialUpdatedFicheConsultation.setId(ficheConsultation.getId());

        partialUpdatedFicheConsultation
            .dateFiche(UPDATED_DATE_FICHE)
            .numero(UPDATED_NUMERO)
            .observation(UPDATED_OBSERVATION)
            .diagnostic(UPDATED_DIAGNOSTIC);

        restFicheConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFicheConsultation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFicheConsultation))
            )
            .andExpect(status().isOk());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
        FicheConsultation testFicheConsultation = ficheConsultationList.get(ficheConsultationList.size() - 1);
        assertThat(testFicheConsultation.getDateFiche()).isEqualTo(UPDATED_DATE_FICHE);
        assertThat(testFicheConsultation.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testFicheConsultation.getObservation()).isEqualTo(UPDATED_OBSERVATION);
        assertThat(testFicheConsultation.getDiagnostic()).isEqualTo(UPDATED_DIAGNOSTIC);
    }

    @Test
    @Transactional
    void patchNonExistingFicheConsultation() throws Exception {
        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();
        ficheConsultation.setId(count.incrementAndGet());

        // Create the FicheConsultation
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFicheConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, ficheConsultationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFicheConsultation() throws Exception {
        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();
        ficheConsultation.setId(count.incrementAndGet());

        // Create the FicheConsultation
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFicheConsultation() throws Exception {
        int databaseSizeBeforeUpdate = ficheConsultationRepository.findAll().size();
        ficheConsultation.setId(count.incrementAndGet());

        // Create the FicheConsultation
        FicheConsultationDTO ficheConsultationDTO = ficheConsultationMapper.toDto(ficheConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ficheConsultationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FicheConsultation in the database
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFicheConsultation() throws Exception {
        // Initialize the database
        ficheConsultationRepository.saveAndFlush(ficheConsultation);

        int databaseSizeBeforeDelete = ficheConsultationRepository.findAll().size();

        // Delete the ficheConsultation
        restFicheConsultationMockMvc
            .perform(delete(ENTITY_API_URL_ID, ficheConsultation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FicheConsultation> ficheConsultationList = ficheConsultationRepository.findAll();
        assertThat(ficheConsultationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
