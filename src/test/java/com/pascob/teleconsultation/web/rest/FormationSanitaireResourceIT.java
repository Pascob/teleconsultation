package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.FormationSanitaire;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.repository.FormationSanitaireRepository;
import com.pascob.teleconsultation.service.criteria.FormationSanitaireCriteria;
import com.pascob.teleconsultation.service.dto.FormationSanitaireDTO;
import com.pascob.teleconsultation.service.mapper.FormationSanitaireMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link FormationSanitaireResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FormationSanitaireResourceIT {

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_SIGLE = "AAAAAAAAAA";
    private static final String UPDATED_SIGLE = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_FIXE = "AAAAAAAAAA";
    private static final String UPDATED_FIXE = "BBBBBBBBBB";

    private static final String DEFAULT_BP = "AAAAAAAAAA";
    private static final String UPDATED_BP = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "%1=3R.@hm*.c=FL6i";
    private static final String UPDATED_EMAIL = "+Gp\\Q@J.L";

    private static final byte[] DEFAULT_LOGO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_LOGO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_LOGO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_LOGO_CONTENT_TYPE = "image/png";

    private static final String ENTITY_API_URL = "/api/formation-sanitaires";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FormationSanitaireRepository formationSanitaireRepository;

    @Autowired
    private FormationSanitaireMapper formationSanitaireMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFormationSanitaireMockMvc;

    private FormationSanitaire formationSanitaire;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FormationSanitaire createEntity(EntityManager em) {
        FormationSanitaire formationSanitaire = new FormationSanitaire()
            .designation(DEFAULT_DESIGNATION)
            .sigle(DEFAULT_SIGLE)
            .telephone(DEFAULT_TELEPHONE)
            .fixe(DEFAULT_FIXE)
            .bp(DEFAULT_BP)
            .email(DEFAULT_EMAIL)
            .logo(DEFAULT_LOGO)
            .logoContentType(DEFAULT_LOGO_CONTENT_TYPE);
        return formationSanitaire;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FormationSanitaire createUpdatedEntity(EntityManager em) {
        FormationSanitaire formationSanitaire = new FormationSanitaire()
            .designation(UPDATED_DESIGNATION)
            .sigle(UPDATED_SIGLE)
            .telephone(UPDATED_TELEPHONE)
            .fixe(UPDATED_FIXE)
            .bp(UPDATED_BP)
            .email(UPDATED_EMAIL)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE);
        return formationSanitaire;
    }

    @BeforeEach
    public void initTest() {
        formationSanitaire = createEntity(em);
    }

    @Test
    @Transactional
    void createFormationSanitaire() throws Exception {
        int databaseSizeBeforeCreate = formationSanitaireRepository.findAll().size();
        // Create the FormationSanitaire
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);
        restFormationSanitaireMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isCreated());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeCreate + 1);
        FormationSanitaire testFormationSanitaire = formationSanitaireList.get(formationSanitaireList.size() - 1);
        assertThat(testFormationSanitaire.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testFormationSanitaire.getSigle()).isEqualTo(DEFAULT_SIGLE);
        assertThat(testFormationSanitaire.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testFormationSanitaire.getFixe()).isEqualTo(DEFAULT_FIXE);
        assertThat(testFormationSanitaire.getBp()).isEqualTo(DEFAULT_BP);
        assertThat(testFormationSanitaire.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testFormationSanitaire.getLogo()).isEqualTo(DEFAULT_LOGO);
        assertThat(testFormationSanitaire.getLogoContentType()).isEqualTo(DEFAULT_LOGO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void createFormationSanitaireWithExistingId() throws Exception {
        // Create the FormationSanitaire with an existing ID
        formationSanitaire.setId(1L);
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        int databaseSizeBeforeCreate = formationSanitaireRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFormationSanitaireMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDesignationIsRequired() throws Exception {
        int databaseSizeBeforeTest = formationSanitaireRepository.findAll().size();
        // set the field null
        formationSanitaire.setDesignation(null);

        // Create the FormationSanitaire, which fails.
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        restFormationSanitaireMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isBadRequest());

        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = formationSanitaireRepository.findAll().size();
        // set the field null
        formationSanitaire.setEmail(null);

        // Create the FormationSanitaire, which fails.
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        restFormationSanitaireMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isBadRequest());

        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllFormationSanitaires() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList
        restFormationSanitaireMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(formationSanitaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].sigle").value(hasItem(DEFAULT_SIGLE)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].fixe").value(hasItem(DEFAULT_FIXE)))
            .andExpect(jsonPath("$.[*].bp").value(hasItem(DEFAULT_BP)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].logoContentType").value(hasItem(DEFAULT_LOGO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].logo").value(hasItem(Base64Utils.encodeToString(DEFAULT_LOGO))));
    }

    @Test
    @Transactional
    void getFormationSanitaire() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get the formationSanitaire
        restFormationSanitaireMockMvc
            .perform(get(ENTITY_API_URL_ID, formationSanitaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(formationSanitaire.getId().intValue()))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.sigle").value(DEFAULT_SIGLE))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
            .andExpect(jsonPath("$.fixe").value(DEFAULT_FIXE))
            .andExpect(jsonPath("$.bp").value(DEFAULT_BP))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.logoContentType").value(DEFAULT_LOGO_CONTENT_TYPE))
            .andExpect(jsonPath("$.logo").value(Base64Utils.encodeToString(DEFAULT_LOGO)));
    }

    @Test
    @Transactional
    void getFormationSanitairesByIdFiltering() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        Long id = formationSanitaire.getId();

        defaultFormationSanitaireShouldBeFound("id.equals=" + id);
        defaultFormationSanitaireShouldNotBeFound("id.notEquals=" + id);

        defaultFormationSanitaireShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFormationSanitaireShouldNotBeFound("id.greaterThan=" + id);

        defaultFormationSanitaireShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFormationSanitaireShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where designation equals to DEFAULT_DESIGNATION
        defaultFormationSanitaireShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the formationSanitaireList where designation equals to UPDATED_DESIGNATION
        defaultFormationSanitaireShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultFormationSanitaireShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the formationSanitaireList where designation equals to UPDATED_DESIGNATION
        defaultFormationSanitaireShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where designation is not null
        defaultFormationSanitaireShouldBeFound("designation.specified=true");

        // Get all the formationSanitaireList where designation is null
        defaultFormationSanitaireShouldNotBeFound("designation.specified=false");
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByDesignationContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where designation contains DEFAULT_DESIGNATION
        defaultFormationSanitaireShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the formationSanitaireList where designation contains UPDATED_DESIGNATION
        defaultFormationSanitaireShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where designation does not contain DEFAULT_DESIGNATION
        defaultFormationSanitaireShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the formationSanitaireList where designation does not contain UPDATED_DESIGNATION
        defaultFormationSanitaireShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesBySigleIsEqualToSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where sigle equals to DEFAULT_SIGLE
        defaultFormationSanitaireShouldBeFound("sigle.equals=" + DEFAULT_SIGLE);

        // Get all the formationSanitaireList where sigle equals to UPDATED_SIGLE
        defaultFormationSanitaireShouldNotBeFound("sigle.equals=" + UPDATED_SIGLE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesBySigleIsInShouldWork() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where sigle in DEFAULT_SIGLE or UPDATED_SIGLE
        defaultFormationSanitaireShouldBeFound("sigle.in=" + DEFAULT_SIGLE + "," + UPDATED_SIGLE);

        // Get all the formationSanitaireList where sigle equals to UPDATED_SIGLE
        defaultFormationSanitaireShouldNotBeFound("sigle.in=" + UPDATED_SIGLE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesBySigleIsNullOrNotNull() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where sigle is not null
        defaultFormationSanitaireShouldBeFound("sigle.specified=true");

        // Get all the formationSanitaireList where sigle is null
        defaultFormationSanitaireShouldNotBeFound("sigle.specified=false");
    }

    @Test
    @Transactional
    void getAllFormationSanitairesBySigleContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where sigle contains DEFAULT_SIGLE
        defaultFormationSanitaireShouldBeFound("sigle.contains=" + DEFAULT_SIGLE);

        // Get all the formationSanitaireList where sigle contains UPDATED_SIGLE
        defaultFormationSanitaireShouldNotBeFound("sigle.contains=" + UPDATED_SIGLE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesBySigleNotContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where sigle does not contain DEFAULT_SIGLE
        defaultFormationSanitaireShouldNotBeFound("sigle.doesNotContain=" + DEFAULT_SIGLE);

        // Get all the formationSanitaireList where sigle does not contain UPDATED_SIGLE
        defaultFormationSanitaireShouldBeFound("sigle.doesNotContain=" + UPDATED_SIGLE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByTelephoneIsEqualToSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where telephone equals to DEFAULT_TELEPHONE
        defaultFormationSanitaireShouldBeFound("telephone.equals=" + DEFAULT_TELEPHONE);

        // Get all the formationSanitaireList where telephone equals to UPDATED_TELEPHONE
        defaultFormationSanitaireShouldNotBeFound("telephone.equals=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByTelephoneIsInShouldWork() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where telephone in DEFAULT_TELEPHONE or UPDATED_TELEPHONE
        defaultFormationSanitaireShouldBeFound("telephone.in=" + DEFAULT_TELEPHONE + "," + UPDATED_TELEPHONE);

        // Get all the formationSanitaireList where telephone equals to UPDATED_TELEPHONE
        defaultFormationSanitaireShouldNotBeFound("telephone.in=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByTelephoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where telephone is not null
        defaultFormationSanitaireShouldBeFound("telephone.specified=true");

        // Get all the formationSanitaireList where telephone is null
        defaultFormationSanitaireShouldNotBeFound("telephone.specified=false");
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByTelephoneContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where telephone contains DEFAULT_TELEPHONE
        defaultFormationSanitaireShouldBeFound("telephone.contains=" + DEFAULT_TELEPHONE);

        // Get all the formationSanitaireList where telephone contains UPDATED_TELEPHONE
        defaultFormationSanitaireShouldNotBeFound("telephone.contains=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByTelephoneNotContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where telephone does not contain DEFAULT_TELEPHONE
        defaultFormationSanitaireShouldNotBeFound("telephone.doesNotContain=" + DEFAULT_TELEPHONE);

        // Get all the formationSanitaireList where telephone does not contain UPDATED_TELEPHONE
        defaultFormationSanitaireShouldBeFound("telephone.doesNotContain=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByFixeIsEqualToSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where fixe equals to DEFAULT_FIXE
        defaultFormationSanitaireShouldBeFound("fixe.equals=" + DEFAULT_FIXE);

        // Get all the formationSanitaireList where fixe equals to UPDATED_FIXE
        defaultFormationSanitaireShouldNotBeFound("fixe.equals=" + UPDATED_FIXE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByFixeIsInShouldWork() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where fixe in DEFAULT_FIXE or UPDATED_FIXE
        defaultFormationSanitaireShouldBeFound("fixe.in=" + DEFAULT_FIXE + "," + UPDATED_FIXE);

        // Get all the formationSanitaireList where fixe equals to UPDATED_FIXE
        defaultFormationSanitaireShouldNotBeFound("fixe.in=" + UPDATED_FIXE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByFixeIsNullOrNotNull() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where fixe is not null
        defaultFormationSanitaireShouldBeFound("fixe.specified=true");

        // Get all the formationSanitaireList where fixe is null
        defaultFormationSanitaireShouldNotBeFound("fixe.specified=false");
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByFixeContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where fixe contains DEFAULT_FIXE
        defaultFormationSanitaireShouldBeFound("fixe.contains=" + DEFAULT_FIXE);

        // Get all the formationSanitaireList where fixe contains UPDATED_FIXE
        defaultFormationSanitaireShouldNotBeFound("fixe.contains=" + UPDATED_FIXE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByFixeNotContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where fixe does not contain DEFAULT_FIXE
        defaultFormationSanitaireShouldNotBeFound("fixe.doesNotContain=" + DEFAULT_FIXE);

        // Get all the formationSanitaireList where fixe does not contain UPDATED_FIXE
        defaultFormationSanitaireShouldBeFound("fixe.doesNotContain=" + UPDATED_FIXE);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByBpIsEqualToSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where bp equals to DEFAULT_BP
        defaultFormationSanitaireShouldBeFound("bp.equals=" + DEFAULT_BP);

        // Get all the formationSanitaireList where bp equals to UPDATED_BP
        defaultFormationSanitaireShouldNotBeFound("bp.equals=" + UPDATED_BP);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByBpIsInShouldWork() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where bp in DEFAULT_BP or UPDATED_BP
        defaultFormationSanitaireShouldBeFound("bp.in=" + DEFAULT_BP + "," + UPDATED_BP);

        // Get all the formationSanitaireList where bp equals to UPDATED_BP
        defaultFormationSanitaireShouldNotBeFound("bp.in=" + UPDATED_BP);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByBpIsNullOrNotNull() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where bp is not null
        defaultFormationSanitaireShouldBeFound("bp.specified=true");

        // Get all the formationSanitaireList where bp is null
        defaultFormationSanitaireShouldNotBeFound("bp.specified=false");
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByBpContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where bp contains DEFAULT_BP
        defaultFormationSanitaireShouldBeFound("bp.contains=" + DEFAULT_BP);

        // Get all the formationSanitaireList where bp contains UPDATED_BP
        defaultFormationSanitaireShouldNotBeFound("bp.contains=" + UPDATED_BP);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByBpNotContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where bp does not contain DEFAULT_BP
        defaultFormationSanitaireShouldNotBeFound("bp.doesNotContain=" + DEFAULT_BP);

        // Get all the formationSanitaireList where bp does not contain UPDATED_BP
        defaultFormationSanitaireShouldBeFound("bp.doesNotContain=" + UPDATED_BP);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where email equals to DEFAULT_EMAIL
        defaultFormationSanitaireShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the formationSanitaireList where email equals to UPDATED_EMAIL
        defaultFormationSanitaireShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultFormationSanitaireShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the formationSanitaireList where email equals to UPDATED_EMAIL
        defaultFormationSanitaireShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where email is not null
        defaultFormationSanitaireShouldBeFound("email.specified=true");

        // Get all the formationSanitaireList where email is null
        defaultFormationSanitaireShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByEmailContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where email contains DEFAULT_EMAIL
        defaultFormationSanitaireShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the formationSanitaireList where email contains UPDATED_EMAIL
        defaultFormationSanitaireShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        // Get all the formationSanitaireList where email does not contain DEFAULT_EMAIL
        defaultFormationSanitaireShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the formationSanitaireList where email does not contain UPDATED_EMAIL
        defaultFormationSanitaireShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllFormationSanitairesByMedecinIsEqualToSomething() throws Exception {
        Medecin medecin;
        if (TestUtil.findAll(em, Medecin.class).isEmpty()) {
            formationSanitaireRepository.saveAndFlush(formationSanitaire);
            medecin = MedecinResourceIT.createEntity(em);
        } else {
            medecin = TestUtil.findAll(em, Medecin.class).get(0);
        }
        em.persist(medecin);
        em.flush();
        formationSanitaire.addMedecin(medecin);
        formationSanitaireRepository.saveAndFlush(formationSanitaire);
        Long medecinId = medecin.getId();

        // Get all the formationSanitaireList where medecin equals to medecinId
        defaultFormationSanitaireShouldBeFound("medecinId.equals=" + medecinId);

        // Get all the formationSanitaireList where medecin equals to (medecinId + 1)
        defaultFormationSanitaireShouldNotBeFound("medecinId.equals=" + (medecinId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFormationSanitaireShouldBeFound(String filter) throws Exception {
        restFormationSanitaireMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(formationSanitaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].sigle").value(hasItem(DEFAULT_SIGLE)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].fixe").value(hasItem(DEFAULT_FIXE)))
            .andExpect(jsonPath("$.[*].bp").value(hasItem(DEFAULT_BP)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].logoContentType").value(hasItem(DEFAULT_LOGO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].logo").value(hasItem(Base64Utils.encodeToString(DEFAULT_LOGO))));

        // Check, that the count call also returns 1
        restFormationSanitaireMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFormationSanitaireShouldNotBeFound(String filter) throws Exception {
        restFormationSanitaireMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFormationSanitaireMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingFormationSanitaire() throws Exception {
        // Get the formationSanitaire
        restFormationSanitaireMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingFormationSanitaire() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();

        // Update the formationSanitaire
        FormationSanitaire updatedFormationSanitaire = formationSanitaireRepository.findById(formationSanitaire.getId()).get();
        // Disconnect from session so that the updates on updatedFormationSanitaire are not directly saved in db
        em.detach(updatedFormationSanitaire);
        updatedFormationSanitaire
            .designation(UPDATED_DESIGNATION)
            .sigle(UPDATED_SIGLE)
            .telephone(UPDATED_TELEPHONE)
            .fixe(UPDATED_FIXE)
            .bp(UPDATED_BP)
            .email(UPDATED_EMAIL)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE);
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(updatedFormationSanitaire);

        restFormationSanitaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, formationSanitaireDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isOk());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
        FormationSanitaire testFormationSanitaire = formationSanitaireList.get(formationSanitaireList.size() - 1);
        assertThat(testFormationSanitaire.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testFormationSanitaire.getSigle()).isEqualTo(UPDATED_SIGLE);
        assertThat(testFormationSanitaire.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testFormationSanitaire.getFixe()).isEqualTo(UPDATED_FIXE);
        assertThat(testFormationSanitaire.getBp()).isEqualTo(UPDATED_BP);
        assertThat(testFormationSanitaire.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testFormationSanitaire.getLogo()).isEqualTo(UPDATED_LOGO);
        assertThat(testFormationSanitaire.getLogoContentType()).isEqualTo(UPDATED_LOGO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingFormationSanitaire() throws Exception {
        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();
        formationSanitaire.setId(count.incrementAndGet());

        // Create the FormationSanitaire
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFormationSanitaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, formationSanitaireDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFormationSanitaire() throws Exception {
        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();
        formationSanitaire.setId(count.incrementAndGet());

        // Create the FormationSanitaire
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFormationSanitaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFormationSanitaire() throws Exception {
        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();
        formationSanitaire.setId(count.incrementAndGet());

        // Create the FormationSanitaire
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFormationSanitaireMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFormationSanitaireWithPatch() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();

        // Update the formationSanitaire using partial update
        FormationSanitaire partialUpdatedFormationSanitaire = new FormationSanitaire();
        partialUpdatedFormationSanitaire.setId(formationSanitaire.getId());

        partialUpdatedFormationSanitaire.telephone(UPDATED_TELEPHONE).bp(UPDATED_BP);

        restFormationSanitaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFormationSanitaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFormationSanitaire))
            )
            .andExpect(status().isOk());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
        FormationSanitaire testFormationSanitaire = formationSanitaireList.get(formationSanitaireList.size() - 1);
        assertThat(testFormationSanitaire.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testFormationSanitaire.getSigle()).isEqualTo(DEFAULT_SIGLE);
        assertThat(testFormationSanitaire.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testFormationSanitaire.getFixe()).isEqualTo(DEFAULT_FIXE);
        assertThat(testFormationSanitaire.getBp()).isEqualTo(UPDATED_BP);
        assertThat(testFormationSanitaire.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testFormationSanitaire.getLogo()).isEqualTo(DEFAULT_LOGO);
        assertThat(testFormationSanitaire.getLogoContentType()).isEqualTo(DEFAULT_LOGO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateFormationSanitaireWithPatch() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();

        // Update the formationSanitaire using partial update
        FormationSanitaire partialUpdatedFormationSanitaire = new FormationSanitaire();
        partialUpdatedFormationSanitaire.setId(formationSanitaire.getId());

        partialUpdatedFormationSanitaire
            .designation(UPDATED_DESIGNATION)
            .sigle(UPDATED_SIGLE)
            .telephone(UPDATED_TELEPHONE)
            .fixe(UPDATED_FIXE)
            .bp(UPDATED_BP)
            .email(UPDATED_EMAIL)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE);

        restFormationSanitaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFormationSanitaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFormationSanitaire))
            )
            .andExpect(status().isOk());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
        FormationSanitaire testFormationSanitaire = formationSanitaireList.get(formationSanitaireList.size() - 1);
        assertThat(testFormationSanitaire.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testFormationSanitaire.getSigle()).isEqualTo(UPDATED_SIGLE);
        assertThat(testFormationSanitaire.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testFormationSanitaire.getFixe()).isEqualTo(UPDATED_FIXE);
        assertThat(testFormationSanitaire.getBp()).isEqualTo(UPDATED_BP);
        assertThat(testFormationSanitaire.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testFormationSanitaire.getLogo()).isEqualTo(UPDATED_LOGO);
        assertThat(testFormationSanitaire.getLogoContentType()).isEqualTo(UPDATED_LOGO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingFormationSanitaire() throws Exception {
        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();
        formationSanitaire.setId(count.incrementAndGet());

        // Create the FormationSanitaire
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFormationSanitaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, formationSanitaireDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFormationSanitaire() throws Exception {
        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();
        formationSanitaire.setId(count.incrementAndGet());

        // Create the FormationSanitaire
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFormationSanitaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFormationSanitaire() throws Exception {
        int databaseSizeBeforeUpdate = formationSanitaireRepository.findAll().size();
        formationSanitaire.setId(count.incrementAndGet());

        // Create the FormationSanitaire
        FormationSanitaireDTO formationSanitaireDTO = formationSanitaireMapper.toDto(formationSanitaire);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFormationSanitaireMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(formationSanitaireDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FormationSanitaire in the database
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFormationSanitaire() throws Exception {
        // Initialize the database
        formationSanitaireRepository.saveAndFlush(formationSanitaire);

        int databaseSizeBeforeDelete = formationSanitaireRepository.findAll().size();

        // Delete the formationSanitaire
        restFormationSanitaireMockMvc
            .perform(delete(ENTITY_API_URL_ID, formationSanitaire.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FormationSanitaire> formationSanitaireList = formationSanitaireRepository.findAll();
        assertThat(formationSanitaireList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
