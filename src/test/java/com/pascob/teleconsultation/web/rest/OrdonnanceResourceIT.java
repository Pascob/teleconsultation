package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.Ordonnance;
import com.pascob.teleconsultation.domain.PrescriptionProduit;
import com.pascob.teleconsultation.repository.OrdonnanceRepository;
import com.pascob.teleconsultation.service.criteria.OrdonnanceCriteria;
import com.pascob.teleconsultation.service.dto.OrdonnanceDTO;
import com.pascob.teleconsultation.service.mapper.OrdonnanceMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OrdonnanceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrdonnanceResourceIT {

    private static final LocalDate DEFAULT_DATE_ORDONNANCE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_ORDONNANCE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_ORDONNANCE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ordonnances";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrdonnanceRepository ordonnanceRepository;

    @Autowired
    private OrdonnanceMapper ordonnanceMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrdonnanceMockMvc;

    private Ordonnance ordonnance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ordonnance createEntity(EntityManager em) {
        Ordonnance ordonnance = new Ordonnance().dateOrdonnance(DEFAULT_DATE_ORDONNANCE).numero(DEFAULT_NUMERO);
        return ordonnance;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ordonnance createUpdatedEntity(EntityManager em) {
        Ordonnance ordonnance = new Ordonnance().dateOrdonnance(UPDATED_DATE_ORDONNANCE).numero(UPDATED_NUMERO);
        return ordonnance;
    }

    @BeforeEach
    public void initTest() {
        ordonnance = createEntity(em);
    }

    @Test
    @Transactional
    void createOrdonnance() throws Exception {
        int databaseSizeBeforeCreate = ordonnanceRepository.findAll().size();
        // Create the Ordonnance
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);
        restOrdonnanceMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO)))
            .andExpect(status().isCreated());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeCreate + 1);
        Ordonnance testOrdonnance = ordonnanceList.get(ordonnanceList.size() - 1);
        assertThat(testOrdonnance.getDateOrdonnance()).isEqualTo(DEFAULT_DATE_ORDONNANCE);
        assertThat(testOrdonnance.getNumero()).isEqualTo(DEFAULT_NUMERO);
    }

    @Test
    @Transactional
    void createOrdonnanceWithExistingId() throws Exception {
        // Create the Ordonnance with an existing ID
        ordonnance.setId(1L);
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        int databaseSizeBeforeCreate = ordonnanceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrdonnanceMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDateOrdonnanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = ordonnanceRepository.findAll().size();
        // set the field null
        ordonnance.setDateOrdonnance(null);

        // Create the Ordonnance, which fails.
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        restOrdonnanceMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO)))
            .andExpect(status().isBadRequest());

        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllOrdonnances() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList
        restOrdonnanceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ordonnance.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateOrdonnance").value(hasItem(DEFAULT_DATE_ORDONNANCE.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)));
    }

    @Test
    @Transactional
    void getOrdonnance() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get the ordonnance
        restOrdonnanceMockMvc
            .perform(get(ENTITY_API_URL_ID, ordonnance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ordonnance.getId().intValue()))
            .andExpect(jsonPath("$.dateOrdonnance").value(DEFAULT_DATE_ORDONNANCE.toString()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO));
    }

    @Test
    @Transactional
    void getOrdonnancesByIdFiltering() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        Long id = ordonnance.getId();

        defaultOrdonnanceShouldBeFound("id.equals=" + id);
        defaultOrdonnanceShouldNotBeFound("id.notEquals=" + id);

        defaultOrdonnanceShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultOrdonnanceShouldNotBeFound("id.greaterThan=" + id);

        defaultOrdonnanceShouldBeFound("id.lessThanOrEqual=" + id);
        defaultOrdonnanceShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByDateOrdonnanceIsEqualToSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where dateOrdonnance equals to DEFAULT_DATE_ORDONNANCE
        defaultOrdonnanceShouldBeFound("dateOrdonnance.equals=" + DEFAULT_DATE_ORDONNANCE);

        // Get all the ordonnanceList where dateOrdonnance equals to UPDATED_DATE_ORDONNANCE
        defaultOrdonnanceShouldNotBeFound("dateOrdonnance.equals=" + UPDATED_DATE_ORDONNANCE);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByDateOrdonnanceIsInShouldWork() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where dateOrdonnance in DEFAULT_DATE_ORDONNANCE or UPDATED_DATE_ORDONNANCE
        defaultOrdonnanceShouldBeFound("dateOrdonnance.in=" + DEFAULT_DATE_ORDONNANCE + "," + UPDATED_DATE_ORDONNANCE);

        // Get all the ordonnanceList where dateOrdonnance equals to UPDATED_DATE_ORDONNANCE
        defaultOrdonnanceShouldNotBeFound("dateOrdonnance.in=" + UPDATED_DATE_ORDONNANCE);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByDateOrdonnanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where dateOrdonnance is not null
        defaultOrdonnanceShouldBeFound("dateOrdonnance.specified=true");

        // Get all the ordonnanceList where dateOrdonnance is null
        defaultOrdonnanceShouldNotBeFound("dateOrdonnance.specified=false");
    }

    @Test
    @Transactional
    void getAllOrdonnancesByDateOrdonnanceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where dateOrdonnance is greater than or equal to DEFAULT_DATE_ORDONNANCE
        defaultOrdonnanceShouldBeFound("dateOrdonnance.greaterThanOrEqual=" + DEFAULT_DATE_ORDONNANCE);

        // Get all the ordonnanceList where dateOrdonnance is greater than or equal to UPDATED_DATE_ORDONNANCE
        defaultOrdonnanceShouldNotBeFound("dateOrdonnance.greaterThanOrEqual=" + UPDATED_DATE_ORDONNANCE);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByDateOrdonnanceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where dateOrdonnance is less than or equal to DEFAULT_DATE_ORDONNANCE
        defaultOrdonnanceShouldBeFound("dateOrdonnance.lessThanOrEqual=" + DEFAULT_DATE_ORDONNANCE);

        // Get all the ordonnanceList where dateOrdonnance is less than or equal to SMALLER_DATE_ORDONNANCE
        defaultOrdonnanceShouldNotBeFound("dateOrdonnance.lessThanOrEqual=" + SMALLER_DATE_ORDONNANCE);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByDateOrdonnanceIsLessThanSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where dateOrdonnance is less than DEFAULT_DATE_ORDONNANCE
        defaultOrdonnanceShouldNotBeFound("dateOrdonnance.lessThan=" + DEFAULT_DATE_ORDONNANCE);

        // Get all the ordonnanceList where dateOrdonnance is less than UPDATED_DATE_ORDONNANCE
        defaultOrdonnanceShouldBeFound("dateOrdonnance.lessThan=" + UPDATED_DATE_ORDONNANCE);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByDateOrdonnanceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where dateOrdonnance is greater than DEFAULT_DATE_ORDONNANCE
        defaultOrdonnanceShouldNotBeFound("dateOrdonnance.greaterThan=" + DEFAULT_DATE_ORDONNANCE);

        // Get all the ordonnanceList where dateOrdonnance is greater than SMALLER_DATE_ORDONNANCE
        defaultOrdonnanceShouldBeFound("dateOrdonnance.greaterThan=" + SMALLER_DATE_ORDONNANCE);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByNumeroIsEqualToSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where numero equals to DEFAULT_NUMERO
        defaultOrdonnanceShouldBeFound("numero.equals=" + DEFAULT_NUMERO);

        // Get all the ordonnanceList where numero equals to UPDATED_NUMERO
        defaultOrdonnanceShouldNotBeFound("numero.equals=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByNumeroIsInShouldWork() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where numero in DEFAULT_NUMERO or UPDATED_NUMERO
        defaultOrdonnanceShouldBeFound("numero.in=" + DEFAULT_NUMERO + "," + UPDATED_NUMERO);

        // Get all the ordonnanceList where numero equals to UPDATED_NUMERO
        defaultOrdonnanceShouldNotBeFound("numero.in=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByNumeroIsNullOrNotNull() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where numero is not null
        defaultOrdonnanceShouldBeFound("numero.specified=true");

        // Get all the ordonnanceList where numero is null
        defaultOrdonnanceShouldNotBeFound("numero.specified=false");
    }

    @Test
    @Transactional
    void getAllOrdonnancesByNumeroContainsSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where numero contains DEFAULT_NUMERO
        defaultOrdonnanceShouldBeFound("numero.contains=" + DEFAULT_NUMERO);

        // Get all the ordonnanceList where numero contains UPDATED_NUMERO
        defaultOrdonnanceShouldNotBeFound("numero.contains=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByNumeroNotContainsSomething() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        // Get all the ordonnanceList where numero does not contain DEFAULT_NUMERO
        defaultOrdonnanceShouldNotBeFound("numero.doesNotContain=" + DEFAULT_NUMERO);

        // Get all the ordonnanceList where numero does not contain UPDATED_NUMERO
        defaultOrdonnanceShouldBeFound("numero.doesNotContain=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllOrdonnancesByPrescriptionProduitIsEqualToSomething() throws Exception {
        PrescriptionProduit prescriptionProduit;
        if (TestUtil.findAll(em, PrescriptionProduit.class).isEmpty()) {
            ordonnanceRepository.saveAndFlush(ordonnance);
            prescriptionProduit = PrescriptionProduitResourceIT.createEntity(em);
        } else {
            prescriptionProduit = TestUtil.findAll(em, PrescriptionProduit.class).get(0);
        }
        em.persist(prescriptionProduit);
        em.flush();
        ordonnance.addPrescriptionProduit(prescriptionProduit);
        ordonnanceRepository.saveAndFlush(ordonnance);
        Long prescriptionProduitId = prescriptionProduit.getId();

        // Get all the ordonnanceList where prescriptionProduit equals to prescriptionProduitId
        defaultOrdonnanceShouldBeFound("prescriptionProduitId.equals=" + prescriptionProduitId);

        // Get all the ordonnanceList where prescriptionProduit equals to (prescriptionProduitId + 1)
        defaultOrdonnanceShouldNotBeFound("prescriptionProduitId.equals=" + (prescriptionProduitId + 1));
    }

    @Test
    @Transactional
    void getAllOrdonnancesByFicheConsultationIsEqualToSomething() throws Exception {
        FicheConsultation ficheConsultation;
        if (TestUtil.findAll(em, FicheConsultation.class).isEmpty()) {
            ordonnanceRepository.saveAndFlush(ordonnance);
            ficheConsultation = FicheConsultationResourceIT.createEntity(em);
        } else {
            ficheConsultation = TestUtil.findAll(em, FicheConsultation.class).get(0);
        }
        em.persist(ficheConsultation);
        em.flush();
        ordonnance.setFicheConsultation(ficheConsultation);
        ordonnanceRepository.saveAndFlush(ordonnance);
        Long ficheConsultationId = ficheConsultation.getId();

        // Get all the ordonnanceList where ficheConsultation equals to ficheConsultationId
        defaultOrdonnanceShouldBeFound("ficheConsultationId.equals=" + ficheConsultationId);

        // Get all the ordonnanceList where ficheConsultation equals to (ficheConsultationId + 1)
        defaultOrdonnanceShouldNotBeFound("ficheConsultationId.equals=" + (ficheConsultationId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOrdonnanceShouldBeFound(String filter) throws Exception {
        restOrdonnanceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ordonnance.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateOrdonnance").value(hasItem(DEFAULT_DATE_ORDONNANCE.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)));

        // Check, that the count call also returns 1
        restOrdonnanceMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOrdonnanceShouldNotBeFound(String filter) throws Exception {
        restOrdonnanceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOrdonnanceMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingOrdonnance() throws Exception {
        // Get the ordonnance
        restOrdonnanceMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingOrdonnance() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();

        // Update the ordonnance
        Ordonnance updatedOrdonnance = ordonnanceRepository.findById(ordonnance.getId()).get();
        // Disconnect from session so that the updates on updatedOrdonnance are not directly saved in db
        em.detach(updatedOrdonnance);
        updatedOrdonnance.dateOrdonnance(UPDATED_DATE_ORDONNANCE).numero(UPDATED_NUMERO);
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(updatedOrdonnance);

        restOrdonnanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ordonnanceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO))
            )
            .andExpect(status().isOk());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
        Ordonnance testOrdonnance = ordonnanceList.get(ordonnanceList.size() - 1);
        assertThat(testOrdonnance.getDateOrdonnance()).isEqualTo(UPDATED_DATE_ORDONNANCE);
        assertThat(testOrdonnance.getNumero()).isEqualTo(UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void putNonExistingOrdonnance() throws Exception {
        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();
        ordonnance.setId(count.incrementAndGet());

        // Create the Ordonnance
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrdonnanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ordonnanceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrdonnance() throws Exception {
        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();
        ordonnance.setId(count.incrementAndGet());

        // Create the Ordonnance
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdonnanceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrdonnance() throws Exception {
        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();
        ordonnance.setId(count.incrementAndGet());

        // Create the Ordonnance
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdonnanceMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrdonnanceWithPatch() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();

        // Update the ordonnance using partial update
        Ordonnance partialUpdatedOrdonnance = new Ordonnance();
        partialUpdatedOrdonnance.setId(ordonnance.getId());

        restOrdonnanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrdonnance.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrdonnance))
            )
            .andExpect(status().isOk());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
        Ordonnance testOrdonnance = ordonnanceList.get(ordonnanceList.size() - 1);
        assertThat(testOrdonnance.getDateOrdonnance()).isEqualTo(DEFAULT_DATE_ORDONNANCE);
        assertThat(testOrdonnance.getNumero()).isEqualTo(DEFAULT_NUMERO);
    }

    @Test
    @Transactional
    void fullUpdateOrdonnanceWithPatch() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();

        // Update the ordonnance using partial update
        Ordonnance partialUpdatedOrdonnance = new Ordonnance();
        partialUpdatedOrdonnance.setId(ordonnance.getId());

        partialUpdatedOrdonnance.dateOrdonnance(UPDATED_DATE_ORDONNANCE).numero(UPDATED_NUMERO);

        restOrdonnanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrdonnance.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrdonnance))
            )
            .andExpect(status().isOk());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
        Ordonnance testOrdonnance = ordonnanceList.get(ordonnanceList.size() - 1);
        assertThat(testOrdonnance.getDateOrdonnance()).isEqualTo(UPDATED_DATE_ORDONNANCE);
        assertThat(testOrdonnance.getNumero()).isEqualTo(UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void patchNonExistingOrdonnance() throws Exception {
        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();
        ordonnance.setId(count.incrementAndGet());

        // Create the Ordonnance
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrdonnanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, ordonnanceDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrdonnance() throws Exception {
        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();
        ordonnance.setId(count.incrementAndGet());

        // Create the Ordonnance
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdonnanceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrdonnance() throws Exception {
        int databaseSizeBeforeUpdate = ordonnanceRepository.findAll().size();
        ordonnance.setId(count.incrementAndGet());

        // Create the Ordonnance
        OrdonnanceDTO ordonnanceDTO = ordonnanceMapper.toDto(ordonnance);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdonnanceMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(ordonnanceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Ordonnance in the database
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrdonnance() throws Exception {
        // Initialize the database
        ordonnanceRepository.saveAndFlush(ordonnance);

        int databaseSizeBeforeDelete = ordonnanceRepository.findAll().size();

        // Delete the ordonnance
        restOrdonnanceMockMvc
            .perform(delete(ENTITY_API_URL_ID, ordonnance.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Ordonnance> ordonnanceList = ordonnanceRepository.findAll();
        assertThat(ordonnanceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
