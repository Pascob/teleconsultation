package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.FicheSymptome;
import com.pascob.teleconsultation.domain.Symptome;
import com.pascob.teleconsultation.repository.FicheSymptomeRepository;
import com.pascob.teleconsultation.service.criteria.FicheSymptomeCriteria;
import com.pascob.teleconsultation.service.dto.FicheSymptomeDTO;
import com.pascob.teleconsultation.service.mapper.FicheSymptomeMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FicheSymptomeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FicheSymptomeResourceIT {

    private static final String DEFAULT_OBSERVATION = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVATION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/fiche-symptomes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FicheSymptomeRepository ficheSymptomeRepository;

    @Autowired
    private FicheSymptomeMapper ficheSymptomeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFicheSymptomeMockMvc;

    private FicheSymptome ficheSymptome;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FicheSymptome createEntity(EntityManager em) {
        FicheSymptome ficheSymptome = new FicheSymptome().observation(DEFAULT_OBSERVATION);
        return ficheSymptome;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FicheSymptome createUpdatedEntity(EntityManager em) {
        FicheSymptome ficheSymptome = new FicheSymptome().observation(UPDATED_OBSERVATION);
        return ficheSymptome;
    }

    @BeforeEach
    public void initTest() {
        ficheSymptome = createEntity(em);
    }

    @Test
    @Transactional
    void createFicheSymptome() throws Exception {
        int databaseSizeBeforeCreate = ficheSymptomeRepository.findAll().size();
        // Create the FicheSymptome
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);
        restFicheSymptomeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeCreate + 1);
        FicheSymptome testFicheSymptome = ficheSymptomeList.get(ficheSymptomeList.size() - 1);
        assertThat(testFicheSymptome.getObservation()).isEqualTo(DEFAULT_OBSERVATION);
    }

    @Test
    @Transactional
    void createFicheSymptomeWithExistingId() throws Exception {
        // Create the FicheSymptome with an existing ID
        ficheSymptome.setId(1L);
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);

        int databaseSizeBeforeCreate = ficheSymptomeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFicheSymptomeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllFicheSymptomes() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        // Get all the ficheSymptomeList
        restFicheSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ficheSymptome.getId().intValue())))
            .andExpect(jsonPath("$.[*].observation").value(hasItem(DEFAULT_OBSERVATION)));
    }

    @Test
    @Transactional
    void getFicheSymptome() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        // Get the ficheSymptome
        restFicheSymptomeMockMvc
            .perform(get(ENTITY_API_URL_ID, ficheSymptome.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ficheSymptome.getId().intValue()))
            .andExpect(jsonPath("$.observation").value(DEFAULT_OBSERVATION));
    }

    @Test
    @Transactional
    void getFicheSymptomesByIdFiltering() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        Long id = ficheSymptome.getId();

        defaultFicheSymptomeShouldBeFound("id.equals=" + id);
        defaultFicheSymptomeShouldNotBeFound("id.notEquals=" + id);

        defaultFicheSymptomeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFicheSymptomeShouldNotBeFound("id.greaterThan=" + id);

        defaultFicheSymptomeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFicheSymptomeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllFicheSymptomesByObservationIsEqualToSomething() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        // Get all the ficheSymptomeList where observation equals to DEFAULT_OBSERVATION
        defaultFicheSymptomeShouldBeFound("observation.equals=" + DEFAULT_OBSERVATION);

        // Get all the ficheSymptomeList where observation equals to UPDATED_OBSERVATION
        defaultFicheSymptomeShouldNotBeFound("observation.equals=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheSymptomesByObservationIsInShouldWork() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        // Get all the ficheSymptomeList where observation in DEFAULT_OBSERVATION or UPDATED_OBSERVATION
        defaultFicheSymptomeShouldBeFound("observation.in=" + DEFAULT_OBSERVATION + "," + UPDATED_OBSERVATION);

        // Get all the ficheSymptomeList where observation equals to UPDATED_OBSERVATION
        defaultFicheSymptomeShouldNotBeFound("observation.in=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheSymptomesByObservationIsNullOrNotNull() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        // Get all the ficheSymptomeList where observation is not null
        defaultFicheSymptomeShouldBeFound("observation.specified=true");

        // Get all the ficheSymptomeList where observation is null
        defaultFicheSymptomeShouldNotBeFound("observation.specified=false");
    }

    @Test
    @Transactional
    void getAllFicheSymptomesByObservationContainsSomething() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        // Get all the ficheSymptomeList where observation contains DEFAULT_OBSERVATION
        defaultFicheSymptomeShouldBeFound("observation.contains=" + DEFAULT_OBSERVATION);

        // Get all the ficheSymptomeList where observation contains UPDATED_OBSERVATION
        defaultFicheSymptomeShouldNotBeFound("observation.contains=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheSymptomesByObservationNotContainsSomething() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        // Get all the ficheSymptomeList where observation does not contain DEFAULT_OBSERVATION
        defaultFicheSymptomeShouldNotBeFound("observation.doesNotContain=" + DEFAULT_OBSERVATION);

        // Get all the ficheSymptomeList where observation does not contain UPDATED_OBSERVATION
        defaultFicheSymptomeShouldBeFound("observation.doesNotContain=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllFicheSymptomesByFicheConsultationIsEqualToSomething() throws Exception {
        FicheConsultation ficheConsultation;
        if (TestUtil.findAll(em, FicheConsultation.class).isEmpty()) {
            ficheSymptomeRepository.saveAndFlush(ficheSymptome);
            ficheConsultation = FicheConsultationResourceIT.createEntity(em);
        } else {
            ficheConsultation = TestUtil.findAll(em, FicheConsultation.class).get(0);
        }
        em.persist(ficheConsultation);
        em.flush();
        ficheSymptome.setFicheConsultation(ficheConsultation);
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);
        Long ficheConsultationId = ficheConsultation.getId();

        // Get all the ficheSymptomeList where ficheConsultation equals to ficheConsultationId
        defaultFicheSymptomeShouldBeFound("ficheConsultationId.equals=" + ficheConsultationId);

        // Get all the ficheSymptomeList where ficheConsultation equals to (ficheConsultationId + 1)
        defaultFicheSymptomeShouldNotBeFound("ficheConsultationId.equals=" + (ficheConsultationId + 1));
    }

    @Test
    @Transactional
    void getAllFicheSymptomesBySymptomeIsEqualToSomething() throws Exception {
        Symptome symptome;
        if (TestUtil.findAll(em, Symptome.class).isEmpty()) {
            ficheSymptomeRepository.saveAndFlush(ficheSymptome);
            symptome = SymptomeResourceIT.createEntity(em);
        } else {
            symptome = TestUtil.findAll(em, Symptome.class).get(0);
        }
        em.persist(symptome);
        em.flush();
        ficheSymptome.setSymptome(symptome);
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);
        Long symptomeId = symptome.getId();

        // Get all the ficheSymptomeList where symptome equals to symptomeId
        defaultFicheSymptomeShouldBeFound("symptomeId.equals=" + symptomeId);

        // Get all the ficheSymptomeList where symptome equals to (symptomeId + 1)
        defaultFicheSymptomeShouldNotBeFound("symptomeId.equals=" + (symptomeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFicheSymptomeShouldBeFound(String filter) throws Exception {
        restFicheSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ficheSymptome.getId().intValue())))
            .andExpect(jsonPath("$.[*].observation").value(hasItem(DEFAULT_OBSERVATION)));

        // Check, that the count call also returns 1
        restFicheSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFicheSymptomeShouldNotBeFound(String filter) throws Exception {
        restFicheSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFicheSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingFicheSymptome() throws Exception {
        // Get the ficheSymptome
        restFicheSymptomeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingFicheSymptome() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();

        // Update the ficheSymptome
        FicheSymptome updatedFicheSymptome = ficheSymptomeRepository.findById(ficheSymptome.getId()).get();
        // Disconnect from session so that the updates on updatedFicheSymptome are not directly saved in db
        em.detach(updatedFicheSymptome);
        updatedFicheSymptome.observation(UPDATED_OBSERVATION);
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(updatedFicheSymptome);

        restFicheSymptomeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ficheSymptomeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isOk());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
        FicheSymptome testFicheSymptome = ficheSymptomeList.get(ficheSymptomeList.size() - 1);
        assertThat(testFicheSymptome.getObservation()).isEqualTo(UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void putNonExistingFicheSymptome() throws Exception {
        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();
        ficheSymptome.setId(count.incrementAndGet());

        // Create the FicheSymptome
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFicheSymptomeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ficheSymptomeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFicheSymptome() throws Exception {
        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();
        ficheSymptome.setId(count.incrementAndGet());

        // Create the FicheSymptome
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheSymptomeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFicheSymptome() throws Exception {
        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();
        ficheSymptome.setId(count.incrementAndGet());

        // Create the FicheSymptome
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheSymptomeMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFicheSymptomeWithPatch() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();

        // Update the ficheSymptome using partial update
        FicheSymptome partialUpdatedFicheSymptome = new FicheSymptome();
        partialUpdatedFicheSymptome.setId(ficheSymptome.getId());

        restFicheSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFicheSymptome.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFicheSymptome))
            )
            .andExpect(status().isOk());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
        FicheSymptome testFicheSymptome = ficheSymptomeList.get(ficheSymptomeList.size() - 1);
        assertThat(testFicheSymptome.getObservation()).isEqualTo(DEFAULT_OBSERVATION);
    }

    @Test
    @Transactional
    void fullUpdateFicheSymptomeWithPatch() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();

        // Update the ficheSymptome using partial update
        FicheSymptome partialUpdatedFicheSymptome = new FicheSymptome();
        partialUpdatedFicheSymptome.setId(ficheSymptome.getId());

        partialUpdatedFicheSymptome.observation(UPDATED_OBSERVATION);

        restFicheSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFicheSymptome.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFicheSymptome))
            )
            .andExpect(status().isOk());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
        FicheSymptome testFicheSymptome = ficheSymptomeList.get(ficheSymptomeList.size() - 1);
        assertThat(testFicheSymptome.getObservation()).isEqualTo(UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void patchNonExistingFicheSymptome() throws Exception {
        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();
        ficheSymptome.setId(count.incrementAndGet());

        // Create the FicheSymptome
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFicheSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, ficheSymptomeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFicheSymptome() throws Exception {
        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();
        ficheSymptome.setId(count.incrementAndGet());

        // Create the FicheSymptome
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFicheSymptome() throws Exception {
        int databaseSizeBeforeUpdate = ficheSymptomeRepository.findAll().size();
        ficheSymptome.setId(count.incrementAndGet());

        // Create the FicheSymptome
        FicheSymptomeDTO ficheSymptomeDTO = ficheSymptomeMapper.toDto(ficheSymptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFicheSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ficheSymptomeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FicheSymptome in the database
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFicheSymptome() throws Exception {
        // Initialize the database
        ficheSymptomeRepository.saveAndFlush(ficheSymptome);

        int databaseSizeBeforeDelete = ficheSymptomeRepository.findAll().size();

        // Delete the ficheSymptome
        restFicheSymptomeMockMvc
            .perform(delete(ENTITY_API_URL_ID, ficheSymptome.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FicheSymptome> ficheSymptomeList = ficheSymptomeRepository.findAll();
        assertThat(ficheSymptomeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
