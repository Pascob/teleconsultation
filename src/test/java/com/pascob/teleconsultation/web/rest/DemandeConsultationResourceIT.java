package com.pascob.teleconsultation.web.rest;

import static com.pascob.teleconsultation.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Consultation;
import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.domain.Specialite;
import com.pascob.teleconsultation.domain.enumeration.StatutDemandeConsultation;
import com.pascob.teleconsultation.repository.DemandeConsultationRepository;
import com.pascob.teleconsultation.service.criteria.DemandeConsultationCriteria;
import com.pascob.teleconsultation.service.dto.DemandeConsultationDTO;
import com.pascob.teleconsultation.service.mapper.DemandeConsultationMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DemandeConsultationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DemandeConsultationResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_HEURE_DEMANDE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_HEURE_DEMANDE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_HEURE_DEMANDE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Integer DEFAULT_ORDRE = 1;
    private static final Integer UPDATED_ORDRE = 2;
    private static final Integer SMALLER_ORDRE = 1 - 1;

    private static final StatutDemandeConsultation DEFAULT_STATUT = StatutDemandeConsultation.EN_COURS;
    private static final StatutDemandeConsultation UPDATED_STATUT = StatutDemandeConsultation.PAYE;

    private static final String DEFAULT_CODE_OTP = "AAAAAAAAAA";
    private static final String UPDATED_CODE_OTP = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE_PAIEMENT = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE_PAIEMENT = "BBBBBBBBBB";

    private static final Double DEFAULT_MONTANT = 0D;
    private static final Double UPDATED_MONTANT = 1D;
    private static final Double SMALLER_MONTANT = 0D - 1D;

    private static final String ENTITY_API_URL = "/api/demande-consultations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DemandeConsultationRepository demandeConsultationRepository;

    @Autowired
    private DemandeConsultationMapper demandeConsultationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDemandeConsultationMockMvc;

    private DemandeConsultation demandeConsultation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DemandeConsultation createEntity(EntityManager em) {
        DemandeConsultation demandeConsultation = new DemandeConsultation()
            .dateHeureDemande(DEFAULT_DATE_HEURE_DEMANDE)
            .ordre(DEFAULT_ORDRE)
            .statut(DEFAULT_STATUT)
            .codeOtp(DEFAULT_CODE_OTP)
            .telephonePaiement(DEFAULT_TELEPHONE_PAIEMENT)
            .montant(DEFAULT_MONTANT);
        return demandeConsultation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DemandeConsultation createUpdatedEntity(EntityManager em) {
        DemandeConsultation demandeConsultation = new DemandeConsultation()
            .dateHeureDemande(UPDATED_DATE_HEURE_DEMANDE)
            .ordre(UPDATED_ORDRE)
            .statut(UPDATED_STATUT)
            .codeOtp(UPDATED_CODE_OTP)
            .telephonePaiement(UPDATED_TELEPHONE_PAIEMENT)
            .montant(UPDATED_MONTANT);
        return demandeConsultation;
    }

    @BeforeEach
    public void initTest() {
        demandeConsultation = createEntity(em);
    }

    @Test
    @Transactional
    void createDemandeConsultation() throws Exception {
        int databaseSizeBeforeCreate = demandeConsultationRepository.findAll().size();
        // Create the DemandeConsultation
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);
        restDemandeConsultationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeCreate + 1);
        DemandeConsultation testDemandeConsultation = demandeConsultationList.get(demandeConsultationList.size() - 1);
        assertThat(testDemandeConsultation.getDateHeureDemande()).isEqualTo(DEFAULT_DATE_HEURE_DEMANDE);
        assertThat(testDemandeConsultation.getOrdre()).isEqualTo(DEFAULT_ORDRE);
        assertThat(testDemandeConsultation.getStatut()).isEqualTo(DEFAULT_STATUT);
        assertThat(testDemandeConsultation.getCodeOtp()).isEqualTo(DEFAULT_CODE_OTP);
        assertThat(testDemandeConsultation.getTelephonePaiement()).isEqualTo(DEFAULT_TELEPHONE_PAIEMENT);
        assertThat(testDemandeConsultation.getMontant()).isEqualTo(DEFAULT_MONTANT);
    }

    @Test
    @Transactional
    void createDemandeConsultationWithExistingId() throws Exception {
        // Create the DemandeConsultation with an existing ID
        demandeConsultation.setId(1L);
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);

        int databaseSizeBeforeCreate = demandeConsultationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDemandeConsultationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDemandeConsultations() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList
        restDemandeConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(demandeConsultation.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateHeureDemande").value(hasItem(sameInstant(DEFAULT_DATE_HEURE_DEMANDE))))
            .andExpect(jsonPath("$.[*].ordre").value(hasItem(DEFAULT_ORDRE)))
            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
            .andExpect(jsonPath("$.[*].codeOtp").value(hasItem(DEFAULT_CODE_OTP)))
            .andExpect(jsonPath("$.[*].telephonePaiement").value(hasItem(DEFAULT_TELEPHONE_PAIEMENT)))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.doubleValue())));
    }

    @Test
    @Transactional
    void getDemandeConsultation() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get the demandeConsultation
        restDemandeConsultationMockMvc
            .perform(get(ENTITY_API_URL_ID, demandeConsultation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(demandeConsultation.getId().intValue()))
            .andExpect(jsonPath("$.dateHeureDemande").value(sameInstant(DEFAULT_DATE_HEURE_DEMANDE)))
            .andExpect(jsonPath("$.ordre").value(DEFAULT_ORDRE))
            .andExpect(jsonPath("$.statut").value(DEFAULT_STATUT.toString()))
            .andExpect(jsonPath("$.codeOtp").value(DEFAULT_CODE_OTP))
            .andExpect(jsonPath("$.telephonePaiement").value(DEFAULT_TELEPHONE_PAIEMENT))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT.doubleValue()));
    }

    @Test
    @Transactional
    void getDemandeConsultationsByIdFiltering() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        Long id = demandeConsultation.getId();

        defaultDemandeConsultationShouldBeFound("id.equals=" + id);
        defaultDemandeConsultationShouldNotBeFound("id.notEquals=" + id);

        defaultDemandeConsultationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDemandeConsultationShouldNotBeFound("id.greaterThan=" + id);

        defaultDemandeConsultationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDemandeConsultationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDateHeureDemandeIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where dateHeureDemande equals to DEFAULT_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldBeFound("dateHeureDemande.equals=" + DEFAULT_DATE_HEURE_DEMANDE);

        // Get all the demandeConsultationList where dateHeureDemande equals to UPDATED_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldNotBeFound("dateHeureDemande.equals=" + UPDATED_DATE_HEURE_DEMANDE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDateHeureDemandeIsInShouldWork() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where dateHeureDemande in DEFAULT_DATE_HEURE_DEMANDE or UPDATED_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldBeFound("dateHeureDemande.in=" + DEFAULT_DATE_HEURE_DEMANDE + "," + UPDATED_DATE_HEURE_DEMANDE);

        // Get all the demandeConsultationList where dateHeureDemande equals to UPDATED_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldNotBeFound("dateHeureDemande.in=" + UPDATED_DATE_HEURE_DEMANDE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDateHeureDemandeIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where dateHeureDemande is not null
        defaultDemandeConsultationShouldBeFound("dateHeureDemande.specified=true");

        // Get all the demandeConsultationList where dateHeureDemande is null
        defaultDemandeConsultationShouldNotBeFound("dateHeureDemande.specified=false");
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDateHeureDemandeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where dateHeureDemande is greater than or equal to DEFAULT_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldBeFound("dateHeureDemande.greaterThanOrEqual=" + DEFAULT_DATE_HEURE_DEMANDE);

        // Get all the demandeConsultationList where dateHeureDemande is greater than or equal to UPDATED_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldNotBeFound("dateHeureDemande.greaterThanOrEqual=" + UPDATED_DATE_HEURE_DEMANDE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDateHeureDemandeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where dateHeureDemande is less than or equal to DEFAULT_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldBeFound("dateHeureDemande.lessThanOrEqual=" + DEFAULT_DATE_HEURE_DEMANDE);

        // Get all the demandeConsultationList where dateHeureDemande is less than or equal to SMALLER_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldNotBeFound("dateHeureDemande.lessThanOrEqual=" + SMALLER_DATE_HEURE_DEMANDE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDateHeureDemandeIsLessThanSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where dateHeureDemande is less than DEFAULT_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldNotBeFound("dateHeureDemande.lessThan=" + DEFAULT_DATE_HEURE_DEMANDE);

        // Get all the demandeConsultationList where dateHeureDemande is less than UPDATED_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldBeFound("dateHeureDemande.lessThan=" + UPDATED_DATE_HEURE_DEMANDE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDateHeureDemandeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where dateHeureDemande is greater than DEFAULT_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldNotBeFound("dateHeureDemande.greaterThan=" + DEFAULT_DATE_HEURE_DEMANDE);

        // Get all the demandeConsultationList where dateHeureDemande is greater than SMALLER_DATE_HEURE_DEMANDE
        defaultDemandeConsultationShouldBeFound("dateHeureDemande.greaterThan=" + SMALLER_DATE_HEURE_DEMANDE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByOrdreIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where ordre equals to DEFAULT_ORDRE
        defaultDemandeConsultationShouldBeFound("ordre.equals=" + DEFAULT_ORDRE);

        // Get all the demandeConsultationList where ordre equals to UPDATED_ORDRE
        defaultDemandeConsultationShouldNotBeFound("ordre.equals=" + UPDATED_ORDRE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByOrdreIsInShouldWork() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where ordre in DEFAULT_ORDRE or UPDATED_ORDRE
        defaultDemandeConsultationShouldBeFound("ordre.in=" + DEFAULT_ORDRE + "," + UPDATED_ORDRE);

        // Get all the demandeConsultationList where ordre equals to UPDATED_ORDRE
        defaultDemandeConsultationShouldNotBeFound("ordre.in=" + UPDATED_ORDRE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByOrdreIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where ordre is not null
        defaultDemandeConsultationShouldBeFound("ordre.specified=true");

        // Get all the demandeConsultationList where ordre is null
        defaultDemandeConsultationShouldNotBeFound("ordre.specified=false");
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByOrdreIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where ordre is greater than or equal to DEFAULT_ORDRE
        defaultDemandeConsultationShouldBeFound("ordre.greaterThanOrEqual=" + DEFAULT_ORDRE);

        // Get all the demandeConsultationList where ordre is greater than or equal to UPDATED_ORDRE
        defaultDemandeConsultationShouldNotBeFound("ordre.greaterThanOrEqual=" + UPDATED_ORDRE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByOrdreIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where ordre is less than or equal to DEFAULT_ORDRE
        defaultDemandeConsultationShouldBeFound("ordre.lessThanOrEqual=" + DEFAULT_ORDRE);

        // Get all the demandeConsultationList where ordre is less than or equal to SMALLER_ORDRE
        defaultDemandeConsultationShouldNotBeFound("ordre.lessThanOrEqual=" + SMALLER_ORDRE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByOrdreIsLessThanSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where ordre is less than DEFAULT_ORDRE
        defaultDemandeConsultationShouldNotBeFound("ordre.lessThan=" + DEFAULT_ORDRE);

        // Get all the demandeConsultationList where ordre is less than UPDATED_ORDRE
        defaultDemandeConsultationShouldBeFound("ordre.lessThan=" + UPDATED_ORDRE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByOrdreIsGreaterThanSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where ordre is greater than DEFAULT_ORDRE
        defaultDemandeConsultationShouldNotBeFound("ordre.greaterThan=" + DEFAULT_ORDRE);

        // Get all the demandeConsultationList where ordre is greater than SMALLER_ORDRE
        defaultDemandeConsultationShouldBeFound("ordre.greaterThan=" + SMALLER_ORDRE);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByStatutIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where statut equals to DEFAULT_STATUT
        defaultDemandeConsultationShouldBeFound("statut.equals=" + DEFAULT_STATUT);

        // Get all the demandeConsultationList where statut equals to UPDATED_STATUT
        defaultDemandeConsultationShouldNotBeFound("statut.equals=" + UPDATED_STATUT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByStatutIsInShouldWork() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where statut in DEFAULT_STATUT or UPDATED_STATUT
        defaultDemandeConsultationShouldBeFound("statut.in=" + DEFAULT_STATUT + "," + UPDATED_STATUT);

        // Get all the demandeConsultationList where statut equals to UPDATED_STATUT
        defaultDemandeConsultationShouldNotBeFound("statut.in=" + UPDATED_STATUT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByStatutIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where statut is not null
        defaultDemandeConsultationShouldBeFound("statut.specified=true");

        // Get all the demandeConsultationList where statut is null
        defaultDemandeConsultationShouldNotBeFound("statut.specified=false");
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByCodeOtpIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where codeOtp equals to DEFAULT_CODE_OTP
        defaultDemandeConsultationShouldBeFound("codeOtp.equals=" + DEFAULT_CODE_OTP);

        // Get all the demandeConsultationList where codeOtp equals to UPDATED_CODE_OTP
        defaultDemandeConsultationShouldNotBeFound("codeOtp.equals=" + UPDATED_CODE_OTP);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByCodeOtpIsInShouldWork() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where codeOtp in DEFAULT_CODE_OTP or UPDATED_CODE_OTP
        defaultDemandeConsultationShouldBeFound("codeOtp.in=" + DEFAULT_CODE_OTP + "," + UPDATED_CODE_OTP);

        // Get all the demandeConsultationList where codeOtp equals to UPDATED_CODE_OTP
        defaultDemandeConsultationShouldNotBeFound("codeOtp.in=" + UPDATED_CODE_OTP);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByCodeOtpIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where codeOtp is not null
        defaultDemandeConsultationShouldBeFound("codeOtp.specified=true");

        // Get all the demandeConsultationList where codeOtp is null
        defaultDemandeConsultationShouldNotBeFound("codeOtp.specified=false");
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByCodeOtpContainsSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where codeOtp contains DEFAULT_CODE_OTP
        defaultDemandeConsultationShouldBeFound("codeOtp.contains=" + DEFAULT_CODE_OTP);

        // Get all the demandeConsultationList where codeOtp contains UPDATED_CODE_OTP
        defaultDemandeConsultationShouldNotBeFound("codeOtp.contains=" + UPDATED_CODE_OTP);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByCodeOtpNotContainsSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where codeOtp does not contain DEFAULT_CODE_OTP
        defaultDemandeConsultationShouldNotBeFound("codeOtp.doesNotContain=" + DEFAULT_CODE_OTP);

        // Get all the demandeConsultationList where codeOtp does not contain UPDATED_CODE_OTP
        defaultDemandeConsultationShouldBeFound("codeOtp.doesNotContain=" + UPDATED_CODE_OTP);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByTelephonePaiementIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where telephonePaiement equals to DEFAULT_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldBeFound("telephonePaiement.equals=" + DEFAULT_TELEPHONE_PAIEMENT);

        // Get all the demandeConsultationList where telephonePaiement equals to UPDATED_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldNotBeFound("telephonePaiement.equals=" + UPDATED_TELEPHONE_PAIEMENT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByTelephonePaiementIsInShouldWork() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where telephonePaiement in DEFAULT_TELEPHONE_PAIEMENT or UPDATED_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldBeFound("telephonePaiement.in=" + DEFAULT_TELEPHONE_PAIEMENT + "," + UPDATED_TELEPHONE_PAIEMENT);

        // Get all the demandeConsultationList where telephonePaiement equals to UPDATED_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldNotBeFound("telephonePaiement.in=" + UPDATED_TELEPHONE_PAIEMENT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByTelephonePaiementIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where telephonePaiement is not null
        defaultDemandeConsultationShouldBeFound("telephonePaiement.specified=true");

        // Get all the demandeConsultationList where telephonePaiement is null
        defaultDemandeConsultationShouldNotBeFound("telephonePaiement.specified=false");
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByTelephonePaiementContainsSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where telephonePaiement contains DEFAULT_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldBeFound("telephonePaiement.contains=" + DEFAULT_TELEPHONE_PAIEMENT);

        // Get all the demandeConsultationList where telephonePaiement contains UPDATED_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldNotBeFound("telephonePaiement.contains=" + UPDATED_TELEPHONE_PAIEMENT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByTelephonePaiementNotContainsSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where telephonePaiement does not contain DEFAULT_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldNotBeFound("telephonePaiement.doesNotContain=" + DEFAULT_TELEPHONE_PAIEMENT);

        // Get all the demandeConsultationList where telephonePaiement does not contain UPDATED_TELEPHONE_PAIEMENT
        defaultDemandeConsultationShouldBeFound("telephonePaiement.doesNotContain=" + UPDATED_TELEPHONE_PAIEMENT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByMontantIsEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where montant equals to DEFAULT_MONTANT
        defaultDemandeConsultationShouldBeFound("montant.equals=" + DEFAULT_MONTANT);

        // Get all the demandeConsultationList where montant equals to UPDATED_MONTANT
        defaultDemandeConsultationShouldNotBeFound("montant.equals=" + UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByMontantIsInShouldWork() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where montant in DEFAULT_MONTANT or UPDATED_MONTANT
        defaultDemandeConsultationShouldBeFound("montant.in=" + DEFAULT_MONTANT + "," + UPDATED_MONTANT);

        // Get all the demandeConsultationList where montant equals to UPDATED_MONTANT
        defaultDemandeConsultationShouldNotBeFound("montant.in=" + UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByMontantIsNullOrNotNull() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where montant is not null
        defaultDemandeConsultationShouldBeFound("montant.specified=true");

        // Get all the demandeConsultationList where montant is null
        defaultDemandeConsultationShouldNotBeFound("montant.specified=false");
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByMontantIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where montant is greater than or equal to DEFAULT_MONTANT
        defaultDemandeConsultationShouldBeFound("montant.greaterThanOrEqual=" + DEFAULT_MONTANT);

        // Get all the demandeConsultationList where montant is greater than or equal to UPDATED_MONTANT
        defaultDemandeConsultationShouldNotBeFound("montant.greaterThanOrEqual=" + UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByMontantIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where montant is less than or equal to DEFAULT_MONTANT
        defaultDemandeConsultationShouldBeFound("montant.lessThanOrEqual=" + DEFAULT_MONTANT);

        // Get all the demandeConsultationList where montant is less than or equal to SMALLER_MONTANT
        defaultDemandeConsultationShouldNotBeFound("montant.lessThanOrEqual=" + SMALLER_MONTANT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByMontantIsLessThanSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where montant is less than DEFAULT_MONTANT
        defaultDemandeConsultationShouldNotBeFound("montant.lessThan=" + DEFAULT_MONTANT);

        // Get all the demandeConsultationList where montant is less than UPDATED_MONTANT
        defaultDemandeConsultationShouldBeFound("montant.lessThan=" + UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByMontantIsGreaterThanSomething() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        // Get all the demandeConsultationList where montant is greater than DEFAULT_MONTANT
        defaultDemandeConsultationShouldNotBeFound("montant.greaterThan=" + DEFAULT_MONTANT);

        // Get all the demandeConsultationList where montant is greater than SMALLER_MONTANT
        defaultDemandeConsultationShouldBeFound("montant.greaterThan=" + SMALLER_MONTANT);
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByConsultationIsEqualToSomething() throws Exception {
        Consultation consultation;
        if (TestUtil.findAll(em, Consultation.class).isEmpty()) {
            demandeConsultationRepository.saveAndFlush(demandeConsultation);
            consultation = ConsultationResourceIT.createEntity(em);
        } else {
            consultation = TestUtil.findAll(em, Consultation.class).get(0);
        }
        em.persist(consultation);
        em.flush();
        demandeConsultation.addConsultation(consultation);
        demandeConsultationRepository.saveAndFlush(demandeConsultation);
        Long consultationId = consultation.getId();

        // Get all the demandeConsultationList where consultation equals to consultationId
        defaultDemandeConsultationShouldBeFound("consultationId.equals=" + consultationId);

        // Get all the demandeConsultationList where consultation equals to (consultationId + 1)
        defaultDemandeConsultationShouldNotBeFound("consultationId.equals=" + (consultationId + 1));
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsBySpecialiteIsEqualToSomething() throws Exception {
        Specialite specialite;
        if (TestUtil.findAll(em, Specialite.class).isEmpty()) {
            demandeConsultationRepository.saveAndFlush(demandeConsultation);
            specialite = SpecialiteResourceIT.createEntity(em);
        } else {
            specialite = TestUtil.findAll(em, Specialite.class).get(0);
        }
        em.persist(specialite);
        em.flush();
        demandeConsultation.setSpecialite(specialite);
        demandeConsultationRepository.saveAndFlush(demandeConsultation);
        Long specialiteId = specialite.getId();

        // Get all the demandeConsultationList where specialite equals to specialiteId
        defaultDemandeConsultationShouldBeFound("specialiteId.equals=" + specialiteId);

        // Get all the demandeConsultationList where specialite equals to (specialiteId + 1)
        defaultDemandeConsultationShouldNotBeFound("specialiteId.equals=" + (specialiteId + 1));
    }

    @Test
    @Transactional
    void getAllDemandeConsultationsByDossierMedicalIsEqualToSomething() throws Exception {
        DossierMedical dossierMedical;
        if (TestUtil.findAll(em, DossierMedical.class).isEmpty()) {
            demandeConsultationRepository.saveAndFlush(demandeConsultation);
            dossierMedical = DossierMedicalResourceIT.createEntity(em);
        } else {
            dossierMedical = TestUtil.findAll(em, DossierMedical.class).get(0);
        }
        em.persist(dossierMedical);
        em.flush();
        demandeConsultation.setDossierMedical(dossierMedical);
        demandeConsultationRepository.saveAndFlush(demandeConsultation);
        Long dossierMedicalId = dossierMedical.getId();

        // Get all the demandeConsultationList where dossierMedical equals to dossierMedicalId
        defaultDemandeConsultationShouldBeFound("dossierMedicalId.equals=" + dossierMedicalId);

        // Get all the demandeConsultationList where dossierMedical equals to (dossierMedicalId + 1)
        defaultDemandeConsultationShouldNotBeFound("dossierMedicalId.equals=" + (dossierMedicalId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDemandeConsultationShouldBeFound(String filter) throws Exception {
        restDemandeConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(demandeConsultation.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateHeureDemande").value(hasItem(sameInstant(DEFAULT_DATE_HEURE_DEMANDE))))
            .andExpect(jsonPath("$.[*].ordre").value(hasItem(DEFAULT_ORDRE)))
            .andExpect(jsonPath("$.[*].statut").value(hasItem(DEFAULT_STATUT.toString())))
            .andExpect(jsonPath("$.[*].codeOtp").value(hasItem(DEFAULT_CODE_OTP)))
            .andExpect(jsonPath("$.[*].telephonePaiement").value(hasItem(DEFAULT_TELEPHONE_PAIEMENT)))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.doubleValue())));

        // Check, that the count call also returns 1
        restDemandeConsultationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDemandeConsultationShouldNotBeFound(String filter) throws Exception {
        restDemandeConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDemandeConsultationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingDemandeConsultation() throws Exception {
        // Get the demandeConsultation
        restDemandeConsultationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDemandeConsultation() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();

        // Update the demandeConsultation
        DemandeConsultation updatedDemandeConsultation = demandeConsultationRepository.findById(demandeConsultation.getId()).get();
        // Disconnect from session so that the updates on updatedDemandeConsultation are not directly saved in db
        em.detach(updatedDemandeConsultation);
        updatedDemandeConsultation
            .dateHeureDemande(UPDATED_DATE_HEURE_DEMANDE)
            .ordre(UPDATED_ORDRE)
            .statut(UPDATED_STATUT)
            .codeOtp(UPDATED_CODE_OTP)
            .telephonePaiement(UPDATED_TELEPHONE_PAIEMENT)
            .montant(UPDATED_MONTANT);
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(updatedDemandeConsultation);

        restDemandeConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, demandeConsultationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isOk());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
        DemandeConsultation testDemandeConsultation = demandeConsultationList.get(demandeConsultationList.size() - 1);
        assertThat(testDemandeConsultation.getDateHeureDemande()).isEqualTo(UPDATED_DATE_HEURE_DEMANDE);
        assertThat(testDemandeConsultation.getOrdre()).isEqualTo(UPDATED_ORDRE);
        assertThat(testDemandeConsultation.getStatut()).isEqualTo(UPDATED_STATUT);
        assertThat(testDemandeConsultation.getCodeOtp()).isEqualTo(UPDATED_CODE_OTP);
        assertThat(testDemandeConsultation.getTelephonePaiement()).isEqualTo(UPDATED_TELEPHONE_PAIEMENT);
        assertThat(testDemandeConsultation.getMontant()).isEqualTo(UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void putNonExistingDemandeConsultation() throws Exception {
        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();
        demandeConsultation.setId(count.incrementAndGet());

        // Create the DemandeConsultation
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDemandeConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, demandeConsultationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDemandeConsultation() throws Exception {
        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();
        demandeConsultation.setId(count.incrementAndGet());

        // Create the DemandeConsultation
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandeConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDemandeConsultation() throws Exception {
        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();
        demandeConsultation.setId(count.incrementAndGet());

        // Create the DemandeConsultation
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandeConsultationMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDemandeConsultationWithPatch() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();

        // Update the demandeConsultation using partial update
        DemandeConsultation partialUpdatedDemandeConsultation = new DemandeConsultation();
        partialUpdatedDemandeConsultation.setId(demandeConsultation.getId());

        partialUpdatedDemandeConsultation.ordre(UPDATED_ORDRE).statut(UPDATED_STATUT);

        restDemandeConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDemandeConsultation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDemandeConsultation))
            )
            .andExpect(status().isOk());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
        DemandeConsultation testDemandeConsultation = demandeConsultationList.get(demandeConsultationList.size() - 1);
        assertThat(testDemandeConsultation.getDateHeureDemande()).isEqualTo(DEFAULT_DATE_HEURE_DEMANDE);
        assertThat(testDemandeConsultation.getOrdre()).isEqualTo(UPDATED_ORDRE);
        assertThat(testDemandeConsultation.getStatut()).isEqualTo(UPDATED_STATUT);
        assertThat(testDemandeConsultation.getCodeOtp()).isEqualTo(DEFAULT_CODE_OTP);
        assertThat(testDemandeConsultation.getTelephonePaiement()).isEqualTo(DEFAULT_TELEPHONE_PAIEMENT);
        assertThat(testDemandeConsultation.getMontant()).isEqualTo(DEFAULT_MONTANT);
    }

    @Test
    @Transactional
    void fullUpdateDemandeConsultationWithPatch() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();

        // Update the demandeConsultation using partial update
        DemandeConsultation partialUpdatedDemandeConsultation = new DemandeConsultation();
        partialUpdatedDemandeConsultation.setId(demandeConsultation.getId());

        partialUpdatedDemandeConsultation
            .dateHeureDemande(UPDATED_DATE_HEURE_DEMANDE)
            .ordre(UPDATED_ORDRE)
            .statut(UPDATED_STATUT)
            .codeOtp(UPDATED_CODE_OTP)
            .telephonePaiement(UPDATED_TELEPHONE_PAIEMENT)
            .montant(UPDATED_MONTANT);

        restDemandeConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDemandeConsultation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDemandeConsultation))
            )
            .andExpect(status().isOk());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
        DemandeConsultation testDemandeConsultation = demandeConsultationList.get(demandeConsultationList.size() - 1);
        assertThat(testDemandeConsultation.getDateHeureDemande()).isEqualTo(UPDATED_DATE_HEURE_DEMANDE);
        assertThat(testDemandeConsultation.getOrdre()).isEqualTo(UPDATED_ORDRE);
        assertThat(testDemandeConsultation.getStatut()).isEqualTo(UPDATED_STATUT);
        assertThat(testDemandeConsultation.getCodeOtp()).isEqualTo(UPDATED_CODE_OTP);
        assertThat(testDemandeConsultation.getTelephonePaiement()).isEqualTo(UPDATED_TELEPHONE_PAIEMENT);
        assertThat(testDemandeConsultation.getMontant()).isEqualTo(UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void patchNonExistingDemandeConsultation() throws Exception {
        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();
        demandeConsultation.setId(count.incrementAndGet());

        // Create the DemandeConsultation
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDemandeConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, demandeConsultationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDemandeConsultation() throws Exception {
        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();
        demandeConsultation.setId(count.incrementAndGet());

        // Create the DemandeConsultation
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandeConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDemandeConsultation() throws Exception {
        int databaseSizeBeforeUpdate = demandeConsultationRepository.findAll().size();
        demandeConsultation.setId(count.incrementAndGet());

        // Create the DemandeConsultation
        DemandeConsultationDTO demandeConsultationDTO = demandeConsultationMapper.toDto(demandeConsultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDemandeConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(demandeConsultationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DemandeConsultation in the database
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDemandeConsultation() throws Exception {
        // Initialize the database
        demandeConsultationRepository.saveAndFlush(demandeConsultation);

        int databaseSizeBeforeDelete = demandeConsultationRepository.findAll().size();

        // Delete the demandeConsultation
        restDemandeConsultationMockMvc
            .perform(delete(ENTITY_API_URL_ID, demandeConsultation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DemandeConsultation> demandeConsultationList = demandeConsultationRepository.findAll();
        assertThat(demandeConsultationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
