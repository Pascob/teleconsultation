package com.pascob.teleconsultation.web.rest;

import static com.pascob.teleconsultation.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Notification;
import com.pascob.teleconsultation.domain.enumeration.TypeNotification;
import com.pascob.teleconsultation.repository.NotificationRepository;
import com.pascob.teleconsultation.service.criteria.NotificationCriteria;
import com.pascob.teleconsultation.service.dto.NotificationDTO;
import com.pascob.teleconsultation.service.mapper.NotificationMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NotificationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class NotificationResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_NOTIFICATION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_NOTIFICATION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_NOTIFICATION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_OBJET = "AAAAAAAAAA";
    private static final String UPDATED_OBJET = "BBBBBBBBBB";

    private static final String DEFAULT_CORPUS = "AAAAAAAAAA";
    private static final String UPDATED_CORPUS = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER = "AAAAAAAAAA";
    private static final String UPDATED_SENDER = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVER = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVER = "BBBBBBBBBB";

    private static final TypeNotification DEFAULT_TYPE_NOTIFICATION = TypeNotification.EMAIL;
    private static final TypeNotification UPDATED_TYPE_NOTIFICATION = TypeNotification.ALERT;

    private static final String ENTITY_API_URL = "/api/notifications";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private NotificationMapper notificationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNotificationMockMvc;

    private Notification notification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notification createEntity(EntityManager em) {
        Notification notification = new Notification()
            .dateNotification(DEFAULT_DATE_NOTIFICATION)
            .objet(DEFAULT_OBJET)
            .corpus(DEFAULT_CORPUS)
            .sender(DEFAULT_SENDER)
            .receiver(DEFAULT_RECEIVER)
            .typeNotification(DEFAULT_TYPE_NOTIFICATION);
        return notification;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notification createUpdatedEntity(EntityManager em) {
        Notification notification = new Notification()
            .dateNotification(UPDATED_DATE_NOTIFICATION)
            .objet(UPDATED_OBJET)
            .corpus(UPDATED_CORPUS)
            .sender(UPDATED_SENDER)
            .receiver(UPDATED_RECEIVER)
            .typeNotification(UPDATED_TYPE_NOTIFICATION);
        return notification;
    }

    @BeforeEach
    public void initTest() {
        notification = createEntity(em);
    }

    @Test
    @Transactional
    void createNotification() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();
        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);
        restNotificationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeCreate + 1);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getDateNotification()).isEqualTo(DEFAULT_DATE_NOTIFICATION);
        assertThat(testNotification.getObjet()).isEqualTo(DEFAULT_OBJET);
        assertThat(testNotification.getCorpus()).isEqualTo(DEFAULT_CORPUS);
        assertThat(testNotification.getSender()).isEqualTo(DEFAULT_SENDER);
        assertThat(testNotification.getReceiver()).isEqualTo(DEFAULT_RECEIVER);
        assertThat(testNotification.getTypeNotification()).isEqualTo(DEFAULT_TYPE_NOTIFICATION);
    }

    @Test
    @Transactional
    void createNotificationWithExistingId() throws Exception {
        // Create the Notification with an existing ID
        notification.setId(1L);
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        int databaseSizeBeforeCreate = notificationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllNotifications() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList
        restNotificationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateNotification").value(hasItem(sameInstant(DEFAULT_DATE_NOTIFICATION))))
            .andExpect(jsonPath("$.[*].objet").value(hasItem(DEFAULT_OBJET)))
            .andExpect(jsonPath("$.[*].corpus").value(hasItem(DEFAULT_CORPUS)))
            .andExpect(jsonPath("$.[*].sender").value(hasItem(DEFAULT_SENDER)))
            .andExpect(jsonPath("$.[*].receiver").value(hasItem(DEFAULT_RECEIVER)))
            .andExpect(jsonPath("$.[*].typeNotification").value(hasItem(DEFAULT_TYPE_NOTIFICATION.toString())));
    }

    @Test
    @Transactional
    void getNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get the notification
        restNotificationMockMvc
            .perform(get(ENTITY_API_URL_ID, notification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(notification.getId().intValue()))
            .andExpect(jsonPath("$.dateNotification").value(sameInstant(DEFAULT_DATE_NOTIFICATION)))
            .andExpect(jsonPath("$.objet").value(DEFAULT_OBJET))
            .andExpect(jsonPath("$.corpus").value(DEFAULT_CORPUS))
            .andExpect(jsonPath("$.sender").value(DEFAULT_SENDER))
            .andExpect(jsonPath("$.receiver").value(DEFAULT_RECEIVER))
            .andExpect(jsonPath("$.typeNotification").value(DEFAULT_TYPE_NOTIFICATION.toString()));
    }

    @Test
    @Transactional
    void getNotificationsByIdFiltering() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        Long id = notification.getId();

        defaultNotificationShouldBeFound("id.equals=" + id);
        defaultNotificationShouldNotBeFound("id.notEquals=" + id);

        defaultNotificationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultNotificationShouldNotBeFound("id.greaterThan=" + id);

        defaultNotificationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultNotificationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllNotificationsByDateNotificationIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateNotification equals to DEFAULT_DATE_NOTIFICATION
        defaultNotificationShouldBeFound("dateNotification.equals=" + DEFAULT_DATE_NOTIFICATION);

        // Get all the notificationList where dateNotification equals to UPDATED_DATE_NOTIFICATION
        defaultNotificationShouldNotBeFound("dateNotification.equals=" + UPDATED_DATE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByDateNotificationIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateNotification in DEFAULT_DATE_NOTIFICATION or UPDATED_DATE_NOTIFICATION
        defaultNotificationShouldBeFound("dateNotification.in=" + DEFAULT_DATE_NOTIFICATION + "," + UPDATED_DATE_NOTIFICATION);

        // Get all the notificationList where dateNotification equals to UPDATED_DATE_NOTIFICATION
        defaultNotificationShouldNotBeFound("dateNotification.in=" + UPDATED_DATE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByDateNotificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateNotification is not null
        defaultNotificationShouldBeFound("dateNotification.specified=true");

        // Get all the notificationList where dateNotification is null
        defaultNotificationShouldNotBeFound("dateNotification.specified=false");
    }

    @Test
    @Transactional
    void getAllNotificationsByDateNotificationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateNotification is greater than or equal to DEFAULT_DATE_NOTIFICATION
        defaultNotificationShouldBeFound("dateNotification.greaterThanOrEqual=" + DEFAULT_DATE_NOTIFICATION);

        // Get all the notificationList where dateNotification is greater than or equal to UPDATED_DATE_NOTIFICATION
        defaultNotificationShouldNotBeFound("dateNotification.greaterThanOrEqual=" + UPDATED_DATE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByDateNotificationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateNotification is less than or equal to DEFAULT_DATE_NOTIFICATION
        defaultNotificationShouldBeFound("dateNotification.lessThanOrEqual=" + DEFAULT_DATE_NOTIFICATION);

        // Get all the notificationList where dateNotification is less than or equal to SMALLER_DATE_NOTIFICATION
        defaultNotificationShouldNotBeFound("dateNotification.lessThanOrEqual=" + SMALLER_DATE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByDateNotificationIsLessThanSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateNotification is less than DEFAULT_DATE_NOTIFICATION
        defaultNotificationShouldNotBeFound("dateNotification.lessThan=" + DEFAULT_DATE_NOTIFICATION);

        // Get all the notificationList where dateNotification is less than UPDATED_DATE_NOTIFICATION
        defaultNotificationShouldBeFound("dateNotification.lessThan=" + UPDATED_DATE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByDateNotificationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where dateNotification is greater than DEFAULT_DATE_NOTIFICATION
        defaultNotificationShouldNotBeFound("dateNotification.greaterThan=" + DEFAULT_DATE_NOTIFICATION);

        // Get all the notificationList where dateNotification is greater than SMALLER_DATE_NOTIFICATION
        defaultNotificationShouldBeFound("dateNotification.greaterThan=" + SMALLER_DATE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByObjetIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where objet equals to DEFAULT_OBJET
        defaultNotificationShouldBeFound("objet.equals=" + DEFAULT_OBJET);

        // Get all the notificationList where objet equals to UPDATED_OBJET
        defaultNotificationShouldNotBeFound("objet.equals=" + UPDATED_OBJET);
    }

    @Test
    @Transactional
    void getAllNotificationsByObjetIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where objet in DEFAULT_OBJET or UPDATED_OBJET
        defaultNotificationShouldBeFound("objet.in=" + DEFAULT_OBJET + "," + UPDATED_OBJET);

        // Get all the notificationList where objet equals to UPDATED_OBJET
        defaultNotificationShouldNotBeFound("objet.in=" + UPDATED_OBJET);
    }

    @Test
    @Transactional
    void getAllNotificationsByObjetIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where objet is not null
        defaultNotificationShouldBeFound("objet.specified=true");

        // Get all the notificationList where objet is null
        defaultNotificationShouldNotBeFound("objet.specified=false");
    }

    @Test
    @Transactional
    void getAllNotificationsByObjetContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where objet contains DEFAULT_OBJET
        defaultNotificationShouldBeFound("objet.contains=" + DEFAULT_OBJET);

        // Get all the notificationList where objet contains UPDATED_OBJET
        defaultNotificationShouldNotBeFound("objet.contains=" + UPDATED_OBJET);
    }

    @Test
    @Transactional
    void getAllNotificationsByObjetNotContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where objet does not contain DEFAULT_OBJET
        defaultNotificationShouldNotBeFound("objet.doesNotContain=" + DEFAULT_OBJET);

        // Get all the notificationList where objet does not contain UPDATED_OBJET
        defaultNotificationShouldBeFound("objet.doesNotContain=" + UPDATED_OBJET);
    }

    @Test
    @Transactional
    void getAllNotificationsByCorpusIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where corpus equals to DEFAULT_CORPUS
        defaultNotificationShouldBeFound("corpus.equals=" + DEFAULT_CORPUS);

        // Get all the notificationList where corpus equals to UPDATED_CORPUS
        defaultNotificationShouldNotBeFound("corpus.equals=" + UPDATED_CORPUS);
    }

    @Test
    @Transactional
    void getAllNotificationsByCorpusIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where corpus in DEFAULT_CORPUS or UPDATED_CORPUS
        defaultNotificationShouldBeFound("corpus.in=" + DEFAULT_CORPUS + "," + UPDATED_CORPUS);

        // Get all the notificationList where corpus equals to UPDATED_CORPUS
        defaultNotificationShouldNotBeFound("corpus.in=" + UPDATED_CORPUS);
    }

    @Test
    @Transactional
    void getAllNotificationsByCorpusIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where corpus is not null
        defaultNotificationShouldBeFound("corpus.specified=true");

        // Get all the notificationList where corpus is null
        defaultNotificationShouldNotBeFound("corpus.specified=false");
    }

    @Test
    @Transactional
    void getAllNotificationsByCorpusContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where corpus contains DEFAULT_CORPUS
        defaultNotificationShouldBeFound("corpus.contains=" + DEFAULT_CORPUS);

        // Get all the notificationList where corpus contains UPDATED_CORPUS
        defaultNotificationShouldNotBeFound("corpus.contains=" + UPDATED_CORPUS);
    }

    @Test
    @Transactional
    void getAllNotificationsByCorpusNotContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where corpus does not contain DEFAULT_CORPUS
        defaultNotificationShouldNotBeFound("corpus.doesNotContain=" + DEFAULT_CORPUS);

        // Get all the notificationList where corpus does not contain UPDATED_CORPUS
        defaultNotificationShouldBeFound("corpus.doesNotContain=" + UPDATED_CORPUS);
    }

    @Test
    @Transactional
    void getAllNotificationsBySenderIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where sender equals to DEFAULT_SENDER
        defaultNotificationShouldBeFound("sender.equals=" + DEFAULT_SENDER);

        // Get all the notificationList where sender equals to UPDATED_SENDER
        defaultNotificationShouldNotBeFound("sender.equals=" + UPDATED_SENDER);
    }

    @Test
    @Transactional
    void getAllNotificationsBySenderIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where sender in DEFAULT_SENDER or UPDATED_SENDER
        defaultNotificationShouldBeFound("sender.in=" + DEFAULT_SENDER + "," + UPDATED_SENDER);

        // Get all the notificationList where sender equals to UPDATED_SENDER
        defaultNotificationShouldNotBeFound("sender.in=" + UPDATED_SENDER);
    }

    @Test
    @Transactional
    void getAllNotificationsBySenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where sender is not null
        defaultNotificationShouldBeFound("sender.specified=true");

        // Get all the notificationList where sender is null
        defaultNotificationShouldNotBeFound("sender.specified=false");
    }

    @Test
    @Transactional
    void getAllNotificationsBySenderContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where sender contains DEFAULT_SENDER
        defaultNotificationShouldBeFound("sender.contains=" + DEFAULT_SENDER);

        // Get all the notificationList where sender contains UPDATED_SENDER
        defaultNotificationShouldNotBeFound("sender.contains=" + UPDATED_SENDER);
    }

    @Test
    @Transactional
    void getAllNotificationsBySenderNotContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where sender does not contain DEFAULT_SENDER
        defaultNotificationShouldNotBeFound("sender.doesNotContain=" + DEFAULT_SENDER);

        // Get all the notificationList where sender does not contain UPDATED_SENDER
        defaultNotificationShouldBeFound("sender.doesNotContain=" + UPDATED_SENDER);
    }

    @Test
    @Transactional
    void getAllNotificationsByReceiverIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where receiver equals to DEFAULT_RECEIVER
        defaultNotificationShouldBeFound("receiver.equals=" + DEFAULT_RECEIVER);

        // Get all the notificationList where receiver equals to UPDATED_RECEIVER
        defaultNotificationShouldNotBeFound("receiver.equals=" + UPDATED_RECEIVER);
    }

    @Test
    @Transactional
    void getAllNotificationsByReceiverIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where receiver in DEFAULT_RECEIVER or UPDATED_RECEIVER
        defaultNotificationShouldBeFound("receiver.in=" + DEFAULT_RECEIVER + "," + UPDATED_RECEIVER);

        // Get all the notificationList where receiver equals to UPDATED_RECEIVER
        defaultNotificationShouldNotBeFound("receiver.in=" + UPDATED_RECEIVER);
    }

    @Test
    @Transactional
    void getAllNotificationsByReceiverIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where receiver is not null
        defaultNotificationShouldBeFound("receiver.specified=true");

        // Get all the notificationList where receiver is null
        defaultNotificationShouldNotBeFound("receiver.specified=false");
    }

    @Test
    @Transactional
    void getAllNotificationsByReceiverContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where receiver contains DEFAULT_RECEIVER
        defaultNotificationShouldBeFound("receiver.contains=" + DEFAULT_RECEIVER);

        // Get all the notificationList where receiver contains UPDATED_RECEIVER
        defaultNotificationShouldNotBeFound("receiver.contains=" + UPDATED_RECEIVER);
    }

    @Test
    @Transactional
    void getAllNotificationsByReceiverNotContainsSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where receiver does not contain DEFAULT_RECEIVER
        defaultNotificationShouldNotBeFound("receiver.doesNotContain=" + DEFAULT_RECEIVER);

        // Get all the notificationList where receiver does not contain UPDATED_RECEIVER
        defaultNotificationShouldBeFound("receiver.doesNotContain=" + UPDATED_RECEIVER);
    }

    @Test
    @Transactional
    void getAllNotificationsByTypeNotificationIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where typeNotification equals to DEFAULT_TYPE_NOTIFICATION
        defaultNotificationShouldBeFound("typeNotification.equals=" + DEFAULT_TYPE_NOTIFICATION);

        // Get all the notificationList where typeNotification equals to UPDATED_TYPE_NOTIFICATION
        defaultNotificationShouldNotBeFound("typeNotification.equals=" + UPDATED_TYPE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByTypeNotificationIsInShouldWork() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where typeNotification in DEFAULT_TYPE_NOTIFICATION or UPDATED_TYPE_NOTIFICATION
        defaultNotificationShouldBeFound("typeNotification.in=" + DEFAULT_TYPE_NOTIFICATION + "," + UPDATED_TYPE_NOTIFICATION);

        // Get all the notificationList where typeNotification equals to UPDATED_TYPE_NOTIFICATION
        defaultNotificationShouldNotBeFound("typeNotification.in=" + UPDATED_TYPE_NOTIFICATION);
    }

    @Test
    @Transactional
    void getAllNotificationsByTypeNotificationIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        // Get all the notificationList where typeNotification is not null
        defaultNotificationShouldBeFound("typeNotification.specified=true");

        // Get all the notificationList where typeNotification is null
        defaultNotificationShouldNotBeFound("typeNotification.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNotificationShouldBeFound(String filter) throws Exception {
        restNotificationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notification.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateNotification").value(hasItem(sameInstant(DEFAULT_DATE_NOTIFICATION))))
            .andExpect(jsonPath("$.[*].objet").value(hasItem(DEFAULT_OBJET)))
            .andExpect(jsonPath("$.[*].corpus").value(hasItem(DEFAULT_CORPUS)))
            .andExpect(jsonPath("$.[*].sender").value(hasItem(DEFAULT_SENDER)))
            .andExpect(jsonPath("$.[*].receiver").value(hasItem(DEFAULT_RECEIVER)))
            .andExpect(jsonPath("$.[*].typeNotification").value(hasItem(DEFAULT_TYPE_NOTIFICATION.toString())));

        // Check, that the count call also returns 1
        restNotificationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNotificationShouldNotBeFound(String filter) throws Exception {
        restNotificationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNotificationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingNotification() throws Exception {
        // Get the notification
        restNotificationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Update the notification
        Notification updatedNotification = notificationRepository.findById(notification.getId()).get();
        // Disconnect from session so that the updates on updatedNotification are not directly saved in db
        em.detach(updatedNotification);
        updatedNotification
            .dateNotification(UPDATED_DATE_NOTIFICATION)
            .objet(UPDATED_OBJET)
            .corpus(UPDATED_CORPUS)
            .sender(UPDATED_SENDER)
            .receiver(UPDATED_RECEIVER)
            .typeNotification(UPDATED_TYPE_NOTIFICATION);
        NotificationDTO notificationDTO = notificationMapper.toDto(updatedNotification);

        restNotificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, notificationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isOk());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getDateNotification()).isEqualTo(UPDATED_DATE_NOTIFICATION);
        assertThat(testNotification.getObjet()).isEqualTo(UPDATED_OBJET);
        assertThat(testNotification.getCorpus()).isEqualTo(UPDATED_CORPUS);
        assertThat(testNotification.getSender()).isEqualTo(UPDATED_SENDER);
        assertThat(testNotification.getReceiver()).isEqualTo(UPDATED_RECEIVER);
        assertThat(testNotification.getTypeNotification()).isEqualTo(UPDATED_TYPE_NOTIFICATION);
    }

    @Test
    @Transactional
    void putNonExistingNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();
        notification.setId(count.incrementAndGet());

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, notificationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();
        notification.setId(count.incrementAndGet());

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();
        notification.setId(count.incrementAndGet());

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotificationMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateNotificationWithPatch() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Update the notification using partial update
        Notification partialUpdatedNotification = new Notification();
        partialUpdatedNotification.setId(notification.getId());

        partialUpdatedNotification
            .corpus(UPDATED_CORPUS)
            .sender(UPDATED_SENDER)
            .receiver(UPDATED_RECEIVER)
            .typeNotification(UPDATED_TYPE_NOTIFICATION);

        restNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNotification.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNotification))
            )
            .andExpect(status().isOk());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getDateNotification()).isEqualTo(DEFAULT_DATE_NOTIFICATION);
        assertThat(testNotification.getObjet()).isEqualTo(DEFAULT_OBJET);
        assertThat(testNotification.getCorpus()).isEqualTo(UPDATED_CORPUS);
        assertThat(testNotification.getSender()).isEqualTo(UPDATED_SENDER);
        assertThat(testNotification.getReceiver()).isEqualTo(UPDATED_RECEIVER);
        assertThat(testNotification.getTypeNotification()).isEqualTo(UPDATED_TYPE_NOTIFICATION);
    }

    @Test
    @Transactional
    void fullUpdateNotificationWithPatch() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Update the notification using partial update
        Notification partialUpdatedNotification = new Notification();
        partialUpdatedNotification.setId(notification.getId());

        partialUpdatedNotification
            .dateNotification(UPDATED_DATE_NOTIFICATION)
            .objet(UPDATED_OBJET)
            .corpus(UPDATED_CORPUS)
            .sender(UPDATED_SENDER)
            .receiver(UPDATED_RECEIVER)
            .typeNotification(UPDATED_TYPE_NOTIFICATION);

        restNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNotification.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNotification))
            )
            .andExpect(status().isOk());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
        Notification testNotification = notificationList.get(notificationList.size() - 1);
        assertThat(testNotification.getDateNotification()).isEqualTo(UPDATED_DATE_NOTIFICATION);
        assertThat(testNotification.getObjet()).isEqualTo(UPDATED_OBJET);
        assertThat(testNotification.getCorpus()).isEqualTo(UPDATED_CORPUS);
        assertThat(testNotification.getSender()).isEqualTo(UPDATED_SENDER);
        assertThat(testNotification.getReceiver()).isEqualTo(UPDATED_RECEIVER);
        assertThat(testNotification.getTypeNotification()).isEqualTo(UPDATED_TYPE_NOTIFICATION);
    }

    @Test
    @Transactional
    void patchNonExistingNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();
        notification.setId(count.incrementAndGet());

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, notificationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();
        notification.setId(count.incrementAndGet());

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();
        notification.setId(count.incrementAndGet());

        // Create the Notification
        NotificationDTO notificationDTO = notificationMapper.toDto(notification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(notificationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Notification in the database
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteNotification() throws Exception {
        // Initialize the database
        notificationRepository.saveAndFlush(notification);

        int databaseSizeBeforeDelete = notificationRepository.findAll().size();

        // Delete the notification
        restNotificationMockMvc
            .perform(delete(ENTITY_API_URL_ID, notification.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Notification> notificationList = notificationRepository.findAll();
        assertThat(notificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
