package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.domain.Specialite;
import com.pascob.teleconsultation.repository.SpecialiteRepository;
import com.pascob.teleconsultation.service.criteria.SpecialiteCriteria;
import com.pascob.teleconsultation.service.dto.SpecialiteDTO;
import com.pascob.teleconsultation.service.mapper.SpecialiteMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SpecialiteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SpecialiteResourceIT {

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final Double DEFAULT_FRAIS = 0D;
    private static final Double UPDATED_FRAIS = 1D;
    private static final Double SMALLER_FRAIS = 0D - 1D;

    private static final String ENTITY_API_URL = "/api/specialites";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SpecialiteRepository specialiteRepository;

    @Autowired
    private SpecialiteMapper specialiteMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSpecialiteMockMvc;

    private Specialite specialite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Specialite createEntity(EntityManager em) {
        Specialite specialite = new Specialite().designation(DEFAULT_DESIGNATION).frais(DEFAULT_FRAIS);
        return specialite;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Specialite createUpdatedEntity(EntityManager em) {
        Specialite specialite = new Specialite().designation(UPDATED_DESIGNATION).frais(UPDATED_FRAIS);
        return specialite;
    }

    @BeforeEach
    public void initTest() {
        specialite = createEntity(em);
    }

    @Test
    @Transactional
    void createSpecialite() throws Exception {
        int databaseSizeBeforeCreate = specialiteRepository.findAll().size();
        // Create the Specialite
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);
        restSpecialiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(specialiteDTO)))
            .andExpect(status().isCreated());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeCreate + 1);
        Specialite testSpecialite = specialiteList.get(specialiteList.size() - 1);
        assertThat(testSpecialite.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testSpecialite.getFrais()).isEqualTo(DEFAULT_FRAIS);
    }

    @Test
    @Transactional
    void createSpecialiteWithExistingId() throws Exception {
        // Create the Specialite with an existing ID
        specialite.setId(1L);
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        int databaseSizeBeforeCreate = specialiteRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpecialiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(specialiteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDesignationIsRequired() throws Exception {
        int databaseSizeBeforeTest = specialiteRepository.findAll().size();
        // set the field null
        specialite.setDesignation(null);

        // Create the Specialite, which fails.
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        restSpecialiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(specialiteDTO)))
            .andExpect(status().isBadRequest());

        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllSpecialites() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList
        restSpecialiteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specialite.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].frais").value(hasItem(DEFAULT_FRAIS.doubleValue())));
    }

    @Test
    @Transactional
    void getSpecialite() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get the specialite
        restSpecialiteMockMvc
            .perform(get(ENTITY_API_URL_ID, specialite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(specialite.getId().intValue()))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.frais").value(DEFAULT_FRAIS.doubleValue()));
    }

    @Test
    @Transactional
    void getSpecialitesByIdFiltering() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        Long id = specialite.getId();

        defaultSpecialiteShouldBeFound("id.equals=" + id);
        defaultSpecialiteShouldNotBeFound("id.notEquals=" + id);

        defaultSpecialiteShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSpecialiteShouldNotBeFound("id.greaterThan=" + id);

        defaultSpecialiteShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSpecialiteShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllSpecialitesByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where designation equals to DEFAULT_DESIGNATION
        defaultSpecialiteShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the specialiteList where designation equals to UPDATED_DESIGNATION
        defaultSpecialiteShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSpecialitesByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultSpecialiteShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the specialiteList where designation equals to UPDATED_DESIGNATION
        defaultSpecialiteShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSpecialitesByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where designation is not null
        defaultSpecialiteShouldBeFound("designation.specified=true");

        // Get all the specialiteList where designation is null
        defaultSpecialiteShouldNotBeFound("designation.specified=false");
    }

    @Test
    @Transactional
    void getAllSpecialitesByDesignationContainsSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where designation contains DEFAULT_DESIGNATION
        defaultSpecialiteShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the specialiteList where designation contains UPDATED_DESIGNATION
        defaultSpecialiteShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSpecialitesByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where designation does not contain DEFAULT_DESIGNATION
        defaultSpecialiteShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the specialiteList where designation does not contain UPDATED_DESIGNATION
        defaultSpecialiteShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSpecialitesByFraisIsEqualToSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where frais equals to DEFAULT_FRAIS
        defaultSpecialiteShouldBeFound("frais.equals=" + DEFAULT_FRAIS);

        // Get all the specialiteList where frais equals to UPDATED_FRAIS
        defaultSpecialiteShouldNotBeFound("frais.equals=" + UPDATED_FRAIS);
    }

    @Test
    @Transactional
    void getAllSpecialitesByFraisIsInShouldWork() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where frais in DEFAULT_FRAIS or UPDATED_FRAIS
        defaultSpecialiteShouldBeFound("frais.in=" + DEFAULT_FRAIS + "," + UPDATED_FRAIS);

        // Get all the specialiteList where frais equals to UPDATED_FRAIS
        defaultSpecialiteShouldNotBeFound("frais.in=" + UPDATED_FRAIS);
    }

    @Test
    @Transactional
    void getAllSpecialitesByFraisIsNullOrNotNull() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where frais is not null
        defaultSpecialiteShouldBeFound("frais.specified=true");

        // Get all the specialiteList where frais is null
        defaultSpecialiteShouldNotBeFound("frais.specified=false");
    }

    @Test
    @Transactional
    void getAllSpecialitesByFraisIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where frais is greater than or equal to DEFAULT_FRAIS
        defaultSpecialiteShouldBeFound("frais.greaterThanOrEqual=" + DEFAULT_FRAIS);

        // Get all the specialiteList where frais is greater than or equal to UPDATED_FRAIS
        defaultSpecialiteShouldNotBeFound("frais.greaterThanOrEqual=" + UPDATED_FRAIS);
    }

    @Test
    @Transactional
    void getAllSpecialitesByFraisIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where frais is less than or equal to DEFAULT_FRAIS
        defaultSpecialiteShouldBeFound("frais.lessThanOrEqual=" + DEFAULT_FRAIS);

        // Get all the specialiteList where frais is less than or equal to SMALLER_FRAIS
        defaultSpecialiteShouldNotBeFound("frais.lessThanOrEqual=" + SMALLER_FRAIS);
    }

    @Test
    @Transactional
    void getAllSpecialitesByFraisIsLessThanSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where frais is less than DEFAULT_FRAIS
        defaultSpecialiteShouldNotBeFound("frais.lessThan=" + DEFAULT_FRAIS);

        // Get all the specialiteList where frais is less than UPDATED_FRAIS
        defaultSpecialiteShouldBeFound("frais.lessThan=" + UPDATED_FRAIS);
    }

    @Test
    @Transactional
    void getAllSpecialitesByFraisIsGreaterThanSomething() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        // Get all the specialiteList where frais is greater than DEFAULT_FRAIS
        defaultSpecialiteShouldNotBeFound("frais.greaterThan=" + DEFAULT_FRAIS);

        // Get all the specialiteList where frais is greater than SMALLER_FRAIS
        defaultSpecialiteShouldBeFound("frais.greaterThan=" + SMALLER_FRAIS);
    }

    @Test
    @Transactional
    void getAllSpecialitesByMedecinIsEqualToSomething() throws Exception {
        Medecin medecin;
        if (TestUtil.findAll(em, Medecin.class).isEmpty()) {
            specialiteRepository.saveAndFlush(specialite);
            medecin = MedecinResourceIT.createEntity(em);
        } else {
            medecin = TestUtil.findAll(em, Medecin.class).get(0);
        }
        em.persist(medecin);
        em.flush();
        specialite.addMedecin(medecin);
        specialiteRepository.saveAndFlush(specialite);
        Long medecinId = medecin.getId();

        // Get all the specialiteList where medecin equals to medecinId
        defaultSpecialiteShouldBeFound("medecinId.equals=" + medecinId);

        // Get all the specialiteList where medecin equals to (medecinId + 1)
        defaultSpecialiteShouldNotBeFound("medecinId.equals=" + (medecinId + 1));
    }

    @Test
    @Transactional
    void getAllSpecialitesByDemandeConsultationIsEqualToSomething() throws Exception {
        DemandeConsultation demandeConsultation;
        if (TestUtil.findAll(em, DemandeConsultation.class).isEmpty()) {
            specialiteRepository.saveAndFlush(specialite);
            demandeConsultation = DemandeConsultationResourceIT.createEntity(em);
        } else {
            demandeConsultation = TestUtil.findAll(em, DemandeConsultation.class).get(0);
        }
        em.persist(demandeConsultation);
        em.flush();
        specialite.addDemandeConsultation(demandeConsultation);
        specialiteRepository.saveAndFlush(specialite);
        Long demandeConsultationId = demandeConsultation.getId();

        // Get all the specialiteList where demandeConsultation equals to demandeConsultationId
        defaultSpecialiteShouldBeFound("demandeConsultationId.equals=" + demandeConsultationId);

        // Get all the specialiteList where demandeConsultation equals to (demandeConsultationId + 1)
        defaultSpecialiteShouldNotBeFound("demandeConsultationId.equals=" + (demandeConsultationId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpecialiteShouldBeFound(String filter) throws Exception {
        restSpecialiteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(specialite.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].frais").value(hasItem(DEFAULT_FRAIS.doubleValue())));

        // Check, that the count call also returns 1
        restSpecialiteMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpecialiteShouldNotBeFound(String filter) throws Exception {
        restSpecialiteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpecialiteMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingSpecialite() throws Exception {
        // Get the specialite
        restSpecialiteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSpecialite() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();

        // Update the specialite
        Specialite updatedSpecialite = specialiteRepository.findById(specialite.getId()).get();
        // Disconnect from session so that the updates on updatedSpecialite are not directly saved in db
        em.detach(updatedSpecialite);
        updatedSpecialite.designation(UPDATED_DESIGNATION).frais(UPDATED_FRAIS);
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(updatedSpecialite);

        restSpecialiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, specialiteDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(specialiteDTO))
            )
            .andExpect(status().isOk());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
        Specialite testSpecialite = specialiteList.get(specialiteList.size() - 1);
        assertThat(testSpecialite.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testSpecialite.getFrais()).isEqualTo(UPDATED_FRAIS);
    }

    @Test
    @Transactional
    void putNonExistingSpecialite() throws Exception {
        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();
        specialite.setId(count.incrementAndGet());

        // Create the Specialite
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpecialiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, specialiteDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(specialiteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSpecialite() throws Exception {
        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();
        specialite.setId(count.incrementAndGet());

        // Create the Specialite
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecialiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(specialiteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSpecialite() throws Exception {
        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();
        specialite.setId(count.incrementAndGet());

        // Create the Specialite
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecialiteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(specialiteDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSpecialiteWithPatch() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();

        // Update the specialite using partial update
        Specialite partialUpdatedSpecialite = new Specialite();
        partialUpdatedSpecialite.setId(specialite.getId());

        partialUpdatedSpecialite.designation(UPDATED_DESIGNATION).frais(UPDATED_FRAIS);

        restSpecialiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSpecialite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSpecialite))
            )
            .andExpect(status().isOk());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
        Specialite testSpecialite = specialiteList.get(specialiteList.size() - 1);
        assertThat(testSpecialite.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testSpecialite.getFrais()).isEqualTo(UPDATED_FRAIS);
    }

    @Test
    @Transactional
    void fullUpdateSpecialiteWithPatch() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();

        // Update the specialite using partial update
        Specialite partialUpdatedSpecialite = new Specialite();
        partialUpdatedSpecialite.setId(specialite.getId());

        partialUpdatedSpecialite.designation(UPDATED_DESIGNATION).frais(UPDATED_FRAIS);

        restSpecialiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSpecialite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSpecialite))
            )
            .andExpect(status().isOk());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
        Specialite testSpecialite = specialiteList.get(specialiteList.size() - 1);
        assertThat(testSpecialite.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testSpecialite.getFrais()).isEqualTo(UPDATED_FRAIS);
    }

    @Test
    @Transactional
    void patchNonExistingSpecialite() throws Exception {
        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();
        specialite.setId(count.incrementAndGet());

        // Create the Specialite
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpecialiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, specialiteDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(specialiteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSpecialite() throws Exception {
        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();
        specialite.setId(count.incrementAndGet());

        // Create the Specialite
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecialiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(specialiteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSpecialite() throws Exception {
        int databaseSizeBeforeUpdate = specialiteRepository.findAll().size();
        specialite.setId(count.incrementAndGet());

        // Create the Specialite
        SpecialiteDTO specialiteDTO = specialiteMapper.toDto(specialite);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpecialiteMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(specialiteDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Specialite in the database
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSpecialite() throws Exception {
        // Initialize the database
        specialiteRepository.saveAndFlush(specialite);

        int databaseSizeBeforeDelete = specialiteRepository.findAll().size();

        // Delete the specialite
        restSpecialiteMockMvc
            .perform(delete(ENTITY_API_URL_ID, specialite.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Specialite> specialiteList = specialiteRepository.findAll();
        assertThat(specialiteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
