package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Consultation;
import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.FormationSanitaire;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.domain.Specialite;
import com.pascob.teleconsultation.repository.MedecinRepository;
import com.pascob.teleconsultation.service.criteria.MedecinCriteria;
import com.pascob.teleconsultation.service.dto.MedecinDTO;
import com.pascob.teleconsultation.service.mapper.MedecinMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link MedecinResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MedecinResourceIT {

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;
    private static final Long SMALLER_USER_ID = 1L - 1L;

    private static final String DEFAULT_MATRICULE = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_SIGNATURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_SIGNATURE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_SIGNATURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_SIGNATURE_CONTENT_TYPE = "image/png";

    private static final String ENTITY_API_URL = "/api/medecins";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MedecinRepository medecinRepository;

    @Autowired
    private MedecinMapper medecinMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMedecinMockMvc;

    private Medecin medecin;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medecin createEntity(EntityManager em) {
        Medecin medecin = new Medecin()
            .matricule(DEFAULT_MATRICULE)
            .signature(DEFAULT_SIGNATURE)
            .signatureContentType(DEFAULT_SIGNATURE_CONTENT_TYPE);
        return medecin;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medecin createUpdatedEntity(EntityManager em) {
        Medecin medecin = new Medecin()
            .matricule(UPDATED_MATRICULE)
            .signature(UPDATED_SIGNATURE)
            .signatureContentType(UPDATED_SIGNATURE_CONTENT_TYPE);
        return medecin;
    }

    @BeforeEach
    public void initTest() {
        medecin = createEntity(em);
    }

    @Test
    @Transactional
    void createMedecin() throws Exception {
        int databaseSizeBeforeCreate = medecinRepository.findAll().size();
        // Create the Medecin
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);
        restMedecinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medecinDTO)))
            .andExpect(status().isCreated());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeCreate + 1);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getMatricule()).isEqualTo(DEFAULT_MATRICULE);
        assertThat(testMedecin.getSignature()).isEqualTo(DEFAULT_SIGNATURE);
        assertThat(testMedecin.getSignatureContentType()).isEqualTo(DEFAULT_SIGNATURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void createMedecinWithExistingId() throws Exception {
        // Create the Medecin with an existing ID
        medecin.setId(1L);
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        int databaseSizeBeforeCreate = medecinRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMedecinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medecinDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = medecinRepository.findAll().size();
        // set the field null

        // Create the Medecin, which fails.
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        restMedecinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medecinDTO)))
            .andExpect(status().isBadRequest());

        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMedecins() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList
        restMedecinMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(medecin.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)))
            .andExpect(jsonPath("$.[*].signatureContentType").value(hasItem(DEFAULT_SIGNATURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].signature").value(hasItem(Base64Utils.encodeToString(DEFAULT_SIGNATURE))));
    }

    @Test
    @Transactional
    void getMedecin() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get the medecin
        restMedecinMockMvc
            .perform(get(ENTITY_API_URL_ID, medecin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(medecin.getId().intValue()))
            .andExpect(jsonPath("$.matricule").value(DEFAULT_MATRICULE))
            .andExpect(jsonPath("$.signatureContentType").value(DEFAULT_SIGNATURE_CONTENT_TYPE))
            .andExpect(jsonPath("$.signature").value(Base64Utils.encodeToString(DEFAULT_SIGNATURE)));
    }

    @Test
    @Transactional
    void getMedecinsByIdFiltering() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        Long id = medecin.getId();

        defaultMedecinShouldBeFound("id.equals=" + id);
        defaultMedecinShouldNotBeFound("id.notEquals=" + id);

        defaultMedecinShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMedecinShouldNotBeFound("id.greaterThan=" + id);

        defaultMedecinShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMedecinShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMedecinsByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where userId equals to DEFAULT_USER_ID
        defaultMedecinShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the medecinList where userId equals to UPDATED_USER_ID
        defaultMedecinShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMedecinsByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultMedecinShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the medecinList where userId equals to UPDATED_USER_ID
        defaultMedecinShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMedecinsByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where userId is not null
        defaultMedecinShouldBeFound("userId.specified=true");

        // Get all the medecinList where userId is null
        defaultMedecinShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    void getAllMedecinsByUserIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where userId is greater than or equal to DEFAULT_USER_ID
        defaultMedecinShouldBeFound("userId.greaterThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the medecinList where userId is greater than or equal to UPDATED_USER_ID
        defaultMedecinShouldNotBeFound("userId.greaterThanOrEqual=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMedecinsByUserIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where userId is less than or equal to DEFAULT_USER_ID
        defaultMedecinShouldBeFound("userId.lessThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the medecinList where userId is less than or equal to SMALLER_USER_ID
        defaultMedecinShouldNotBeFound("userId.lessThanOrEqual=" + SMALLER_USER_ID);
    }

    @Test
    @Transactional
    void getAllMedecinsByUserIdIsLessThanSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where userId is less than DEFAULT_USER_ID
        defaultMedecinShouldNotBeFound("userId.lessThan=" + DEFAULT_USER_ID);

        // Get all the medecinList where userId is less than UPDATED_USER_ID
        defaultMedecinShouldBeFound("userId.lessThan=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMedecinsByUserIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where userId is greater than DEFAULT_USER_ID
        defaultMedecinShouldNotBeFound("userId.greaterThan=" + DEFAULT_USER_ID);

        // Get all the medecinList where userId is greater than SMALLER_USER_ID
        defaultMedecinShouldBeFound("userId.greaterThan=" + SMALLER_USER_ID);
    }

    @Test
    @Transactional
    void getAllMedecinsByMatriculeIsEqualToSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where matricule equals to DEFAULT_MATRICULE
        defaultMedecinShouldBeFound("matricule.equals=" + DEFAULT_MATRICULE);

        // Get all the medecinList where matricule equals to UPDATED_MATRICULE
        defaultMedecinShouldNotBeFound("matricule.equals=" + UPDATED_MATRICULE);
    }

    @Test
    @Transactional
    void getAllMedecinsByMatriculeIsInShouldWork() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where matricule in DEFAULT_MATRICULE or UPDATED_MATRICULE
        defaultMedecinShouldBeFound("matricule.in=" + DEFAULT_MATRICULE + "," + UPDATED_MATRICULE);

        // Get all the medecinList where matricule equals to UPDATED_MATRICULE
        defaultMedecinShouldNotBeFound("matricule.in=" + UPDATED_MATRICULE);
    }

    @Test
    @Transactional
    void getAllMedecinsByMatriculeIsNullOrNotNull() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where matricule is not null
        defaultMedecinShouldBeFound("matricule.specified=true");

        // Get all the medecinList where matricule is null
        defaultMedecinShouldNotBeFound("matricule.specified=false");
    }

    @Test
    @Transactional
    void getAllMedecinsByMatriculeContainsSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where matricule contains DEFAULT_MATRICULE
        defaultMedecinShouldBeFound("matricule.contains=" + DEFAULT_MATRICULE);

        // Get all the medecinList where matricule contains UPDATED_MATRICULE
        defaultMedecinShouldNotBeFound("matricule.contains=" + UPDATED_MATRICULE);
    }

    @Test
    @Transactional
    void getAllMedecinsByMatriculeNotContainsSomething() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        // Get all the medecinList where matricule does not contain DEFAULT_MATRICULE
        defaultMedecinShouldNotBeFound("matricule.doesNotContain=" + DEFAULT_MATRICULE);

        // Get all the medecinList where matricule does not contain UPDATED_MATRICULE
        defaultMedecinShouldBeFound("matricule.doesNotContain=" + UPDATED_MATRICULE);
    }

    @Test
    @Transactional
    void getAllMedecinsByDossierMedicalIsEqualToSomething() throws Exception {
        DossierMedical dossierMedical;
        if (TestUtil.findAll(em, DossierMedical.class).isEmpty()) {
            medecinRepository.saveAndFlush(medecin);
            dossierMedical = DossierMedicalResourceIT.createEntity(em);
        } else {
            dossierMedical = TestUtil.findAll(em, DossierMedical.class).get(0);
        }
        em.persist(dossierMedical);
        em.flush();
        medecin.addDossierMedical(dossierMedical);
        medecinRepository.saveAndFlush(medecin);
        Long dossierMedicalId = dossierMedical.getId();

        // Get all the medecinList where dossierMedical equals to dossierMedicalId
        defaultMedecinShouldBeFound("dossierMedicalId.equals=" + dossierMedicalId);

        // Get all the medecinList where dossierMedical equals to (dossierMedicalId + 1)
        defaultMedecinShouldNotBeFound("dossierMedicalId.equals=" + (dossierMedicalId + 1));
    }

    @Test
    @Transactional
    void getAllMedecinsByConsultationIsEqualToSomething() throws Exception {
        Consultation consultation;
        if (TestUtil.findAll(em, Consultation.class).isEmpty()) {
            medecinRepository.saveAndFlush(medecin);
            consultation = ConsultationResourceIT.createEntity(em);
        } else {
            consultation = TestUtil.findAll(em, Consultation.class).get(0);
        }
        em.persist(consultation);
        em.flush();
        medecin.addConsultation(consultation);
        medecinRepository.saveAndFlush(medecin);
        Long consultationId = consultation.getId();

        // Get all the medecinList where consultation equals to consultationId
        defaultMedecinShouldBeFound("consultationId.equals=" + consultationId);

        // Get all the medecinList where consultation equals to (consultationId + 1)
        defaultMedecinShouldNotBeFound("consultationId.equals=" + (consultationId + 1));
    }

    @Test
    @Transactional
    void getAllMedecinsByFicheConsultationIsEqualToSomething() throws Exception {
        FicheConsultation ficheConsultation;
        if (TestUtil.findAll(em, FicheConsultation.class).isEmpty()) {
            medecinRepository.saveAndFlush(medecin);
            ficheConsultation = FicheConsultationResourceIT.createEntity(em);
        } else {
            ficheConsultation = TestUtil.findAll(em, FicheConsultation.class).get(0);
        }
        em.persist(ficheConsultation);
        em.flush();
        medecin.addFicheConsultation(ficheConsultation);
        medecinRepository.saveAndFlush(medecin);
        Long ficheConsultationId = ficheConsultation.getId();

        // Get all the medecinList where ficheConsultation equals to ficheConsultationId
        defaultMedecinShouldBeFound("ficheConsultationId.equals=" + ficheConsultationId);

        // Get all the medecinList where ficheConsultation equals to (ficheConsultationId + 1)
        defaultMedecinShouldNotBeFound("ficheConsultationId.equals=" + (ficheConsultationId + 1));
    }

    @Test
    @Transactional
    void getAllMedecinsByFormationSanitaireIsEqualToSomething() throws Exception {
        FormationSanitaire formationSanitaire;
        if (TestUtil.findAll(em, FormationSanitaire.class).isEmpty()) {
            medecinRepository.saveAndFlush(medecin);
            formationSanitaire = FormationSanitaireResourceIT.createEntity(em);
        } else {
            formationSanitaire = TestUtil.findAll(em, FormationSanitaire.class).get(0);
        }
        em.persist(formationSanitaire);
        em.flush();
        medecin.setFormationSanitaire(formationSanitaire);
        medecinRepository.saveAndFlush(medecin);
        Long formationSanitaireId = formationSanitaire.getId();

        // Get all the medecinList where formationSanitaire equals to formationSanitaireId
        defaultMedecinShouldBeFound("formationSanitaireId.equals=" + formationSanitaireId);

        // Get all the medecinList where formationSanitaire equals to (formationSanitaireId + 1)
        defaultMedecinShouldNotBeFound("formationSanitaireId.equals=" + (formationSanitaireId + 1));
    }

    @Test
    @Transactional
    void getAllMedecinsBySpecialiteIsEqualToSomething() throws Exception {
        Specialite specialite;
        if (TestUtil.findAll(em, Specialite.class).isEmpty()) {
            medecinRepository.saveAndFlush(medecin);
            specialite = SpecialiteResourceIT.createEntity(em);
        } else {
            specialite = TestUtil.findAll(em, Specialite.class).get(0);
        }
        em.persist(specialite);
        em.flush();
        medecin.setSpecialite(specialite);
        medecinRepository.saveAndFlush(medecin);
        Long specialiteId = specialite.getId();

        // Get all the medecinList where specialite equals to specialiteId
        defaultMedecinShouldBeFound("specialiteId.equals=" + specialiteId);

        // Get all the medecinList where specialite equals to (specialiteId + 1)
        defaultMedecinShouldNotBeFound("specialiteId.equals=" + (specialiteId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMedecinShouldBeFound(String filter) throws Exception {
        restMedecinMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(medecin.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)))
            .andExpect(jsonPath("$.[*].signatureContentType").value(hasItem(DEFAULT_SIGNATURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].signature").value(hasItem(Base64Utils.encodeToString(DEFAULT_SIGNATURE))));

        // Check, that the count call also returns 1
        restMedecinMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMedecinShouldNotBeFound(String filter) throws Exception {
        restMedecinMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMedecinMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMedecin() throws Exception {
        // Get the medecin
        restMedecinMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingMedecin() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();

        // Update the medecin
        Medecin updatedMedecin = medecinRepository.findById(medecin.getId()).get();
        // Disconnect from session so that the updates on updatedMedecin are not directly saved in db
        em.detach(updatedMedecin);
        updatedMedecin.matricule(UPDATED_MATRICULE).signature(UPDATED_SIGNATURE).signatureContentType(UPDATED_SIGNATURE_CONTENT_TYPE);
        MedecinDTO medecinDTO = medecinMapper.toDto(updatedMedecin);

        restMedecinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, medecinDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(medecinDTO))
            )
            .andExpect(status().isOk());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testMedecin.getSignature()).isEqualTo(UPDATED_SIGNATURE);
        assertThat(testMedecin.getSignatureContentType()).isEqualTo(UPDATED_SIGNATURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();
        medecin.setId(count.incrementAndGet());

        // Create the Medecin
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMedecinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, medecinDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(medecinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();
        medecin.setId(count.incrementAndGet());

        // Create the Medecin
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMedecinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(medecinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();
        medecin.setId(count.incrementAndGet());

        // Create the Medecin
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMedecinMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(medecinDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMedecinWithPatch() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();

        // Update the medecin using partial update
        Medecin partialUpdatedMedecin = new Medecin();
        partialUpdatedMedecin.setId(medecin.getId());

        partialUpdatedMedecin
            .matricule(UPDATED_MATRICULE)
            .signature(UPDATED_SIGNATURE)
            .signatureContentType(UPDATED_SIGNATURE_CONTENT_TYPE);

        restMedecinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMedecin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMedecin))
            )
            .andExpect(status().isOk());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testMedecin.getSignature()).isEqualTo(UPDATED_SIGNATURE);
        assertThat(testMedecin.getSignatureContentType()).isEqualTo(UPDATED_SIGNATURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateMedecinWithPatch() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();

        // Update the medecin using partial update
        Medecin partialUpdatedMedecin = new Medecin();
        partialUpdatedMedecin.setId(medecin.getId());

        partialUpdatedMedecin
            .matricule(UPDATED_MATRICULE)
            .signature(UPDATED_SIGNATURE)
            .signatureContentType(UPDATED_SIGNATURE_CONTENT_TYPE);

        restMedecinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMedecin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMedecin))
            )
            .andExpect(status().isOk());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testMedecin.getSignature()).isEqualTo(UPDATED_SIGNATURE);
        assertThat(testMedecin.getSignatureContentType()).isEqualTo(UPDATED_SIGNATURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();
        medecin.setId(count.incrementAndGet());

        // Create the Medecin
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMedecinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, medecinDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(medecinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();
        medecin.setId(count.incrementAndGet());

        // Create the Medecin
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMedecinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(medecinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().size();
        medecin.setId(count.incrementAndGet());

        // Create the Medecin
        MedecinDTO medecinDTO = medecinMapper.toDto(medecin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMedecinMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(medecinDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMedecin() throws Exception {
        // Initialize the database
        medecinRepository.saveAndFlush(medecin);

        int databaseSizeBeforeDelete = medecinRepository.findAll().size();

        // Delete the medecin
        restMedecinMockMvc
            .perform(delete(ENTITY_API_URL_ID, medecin.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Medecin> medecinList = medecinRepository.findAll();
        assertThat(medecinList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
