package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Bulletin;
import com.pascob.teleconsultation.domain.ExamenMedical;
import com.pascob.teleconsultation.domain.PrescriptionExamen;
import com.pascob.teleconsultation.repository.PrescriptionExamenRepository;
import com.pascob.teleconsultation.service.criteria.PrescriptionExamenCriteria;
import com.pascob.teleconsultation.service.dto.PrescriptionExamenDTO;
import com.pascob.teleconsultation.service.mapper.PrescriptionExamenMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PrescriptionExamenResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PrescriptionExamenResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_RESULTAT = "AAAAAAAAAA";
    private static final String UPDATED_RESULTAT = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/prescription-examen";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PrescriptionExamenRepository prescriptionExamenRepository;

    @Autowired
    private PrescriptionExamenMapper prescriptionExamenMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPrescriptionExamenMockMvc;

    private PrescriptionExamen prescriptionExamen;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrescriptionExamen createEntity(EntityManager em) {
        PrescriptionExamen prescriptionExamen = new PrescriptionExamen().description(DEFAULT_DESCRIPTION).resultat(DEFAULT_RESULTAT);
        return prescriptionExamen;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrescriptionExamen createUpdatedEntity(EntityManager em) {
        PrescriptionExamen prescriptionExamen = new PrescriptionExamen().description(UPDATED_DESCRIPTION).resultat(UPDATED_RESULTAT);
        return prescriptionExamen;
    }

    @BeforeEach
    public void initTest() {
        prescriptionExamen = createEntity(em);
    }

    @Test
    @Transactional
    void createPrescriptionExamen() throws Exception {
        int databaseSizeBeforeCreate = prescriptionExamenRepository.findAll().size();
        // Create the PrescriptionExamen
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);
        restPrescriptionExamenMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeCreate + 1);
        PrescriptionExamen testPrescriptionExamen = prescriptionExamenList.get(prescriptionExamenList.size() - 1);
        assertThat(testPrescriptionExamen.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPrescriptionExamen.getResultat()).isEqualTo(DEFAULT_RESULTAT);
    }

    @Test
    @Transactional
    void createPrescriptionExamenWithExistingId() throws Exception {
        // Create the PrescriptionExamen with an existing ID
        prescriptionExamen.setId(1L);
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);

        int databaseSizeBeforeCreate = prescriptionExamenRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrescriptionExamenMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamen() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList
        restPrescriptionExamenMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prescriptionExamen.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].resultat").value(hasItem(DEFAULT_RESULTAT)));
    }

    @Test
    @Transactional
    void getPrescriptionExamen() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get the prescriptionExamen
        restPrescriptionExamenMockMvc
            .perform(get(ENTITY_API_URL_ID, prescriptionExamen.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(prescriptionExamen.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.resultat").value(DEFAULT_RESULTAT));
    }

    @Test
    @Transactional
    void getPrescriptionExamenByIdFiltering() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        Long id = prescriptionExamen.getId();

        defaultPrescriptionExamenShouldBeFound("id.equals=" + id);
        defaultPrescriptionExamenShouldNotBeFound("id.notEquals=" + id);

        defaultPrescriptionExamenShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPrescriptionExamenShouldNotBeFound("id.greaterThan=" + id);

        defaultPrescriptionExamenShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPrescriptionExamenShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where description equals to DEFAULT_DESCRIPTION
        defaultPrescriptionExamenShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the prescriptionExamenList where description equals to UPDATED_DESCRIPTION
        defaultPrescriptionExamenShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultPrescriptionExamenShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the prescriptionExamenList where description equals to UPDATED_DESCRIPTION
        defaultPrescriptionExamenShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where description is not null
        defaultPrescriptionExamenShouldBeFound("description.specified=true");

        // Get all the prescriptionExamenList where description is null
        defaultPrescriptionExamenShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where description contains DEFAULT_DESCRIPTION
        defaultPrescriptionExamenShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the prescriptionExamenList where description contains UPDATED_DESCRIPTION
        defaultPrescriptionExamenShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where description does not contain DEFAULT_DESCRIPTION
        defaultPrescriptionExamenShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the prescriptionExamenList where description does not contain UPDATED_DESCRIPTION
        defaultPrescriptionExamenShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByResultatIsEqualToSomething() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where resultat equals to DEFAULT_RESULTAT
        defaultPrescriptionExamenShouldBeFound("resultat.equals=" + DEFAULT_RESULTAT);

        // Get all the prescriptionExamenList where resultat equals to UPDATED_RESULTAT
        defaultPrescriptionExamenShouldNotBeFound("resultat.equals=" + UPDATED_RESULTAT);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByResultatIsInShouldWork() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where resultat in DEFAULT_RESULTAT or UPDATED_RESULTAT
        defaultPrescriptionExamenShouldBeFound("resultat.in=" + DEFAULT_RESULTAT + "," + UPDATED_RESULTAT);

        // Get all the prescriptionExamenList where resultat equals to UPDATED_RESULTAT
        defaultPrescriptionExamenShouldNotBeFound("resultat.in=" + UPDATED_RESULTAT);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByResultatIsNullOrNotNull() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where resultat is not null
        defaultPrescriptionExamenShouldBeFound("resultat.specified=true");

        // Get all the prescriptionExamenList where resultat is null
        defaultPrescriptionExamenShouldNotBeFound("resultat.specified=false");
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByResultatContainsSomething() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where resultat contains DEFAULT_RESULTAT
        defaultPrescriptionExamenShouldBeFound("resultat.contains=" + DEFAULT_RESULTAT);

        // Get all the prescriptionExamenList where resultat contains UPDATED_RESULTAT
        defaultPrescriptionExamenShouldNotBeFound("resultat.contains=" + UPDATED_RESULTAT);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByResultatNotContainsSomething() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        // Get all the prescriptionExamenList where resultat does not contain DEFAULT_RESULTAT
        defaultPrescriptionExamenShouldNotBeFound("resultat.doesNotContain=" + DEFAULT_RESULTAT);

        // Get all the prescriptionExamenList where resultat does not contain UPDATED_RESULTAT
        defaultPrescriptionExamenShouldBeFound("resultat.doesNotContain=" + UPDATED_RESULTAT);
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByBulletinIsEqualToSomething() throws Exception {
        Bulletin bulletin;
        if (TestUtil.findAll(em, Bulletin.class).isEmpty()) {
            prescriptionExamenRepository.saveAndFlush(prescriptionExamen);
            bulletin = BulletinResourceIT.createEntity(em);
        } else {
            bulletin = TestUtil.findAll(em, Bulletin.class).get(0);
        }
        em.persist(bulletin);
        em.flush();
        prescriptionExamen.setBulletin(bulletin);
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);
        Long bulletinId = bulletin.getId();

        // Get all the prescriptionExamenList where bulletin equals to bulletinId
        defaultPrescriptionExamenShouldBeFound("bulletinId.equals=" + bulletinId);

        // Get all the prescriptionExamenList where bulletin equals to (bulletinId + 1)
        defaultPrescriptionExamenShouldNotBeFound("bulletinId.equals=" + (bulletinId + 1));
    }

    @Test
    @Transactional
    void getAllPrescriptionExamenByExamenMedicalIsEqualToSomething() throws Exception {
        ExamenMedical examenMedical;
        if (TestUtil.findAll(em, ExamenMedical.class).isEmpty()) {
            prescriptionExamenRepository.saveAndFlush(prescriptionExamen);
            examenMedical = ExamenMedicalResourceIT.createEntity(em);
        } else {
            examenMedical = TestUtil.findAll(em, ExamenMedical.class).get(0);
        }
        em.persist(examenMedical);
        em.flush();
        prescriptionExamen.setExamenMedical(examenMedical);
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);
        Long examenMedicalId = examenMedical.getId();

        // Get all the prescriptionExamenList where examenMedical equals to examenMedicalId
        defaultPrescriptionExamenShouldBeFound("examenMedicalId.equals=" + examenMedicalId);

        // Get all the prescriptionExamenList where examenMedical equals to (examenMedicalId + 1)
        defaultPrescriptionExamenShouldNotBeFound("examenMedicalId.equals=" + (examenMedicalId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPrescriptionExamenShouldBeFound(String filter) throws Exception {
        restPrescriptionExamenMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prescriptionExamen.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].resultat").value(hasItem(DEFAULT_RESULTAT)));

        // Check, that the count call also returns 1
        restPrescriptionExamenMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPrescriptionExamenShouldNotBeFound(String filter) throws Exception {
        restPrescriptionExamenMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPrescriptionExamenMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPrescriptionExamen() throws Exception {
        // Get the prescriptionExamen
        restPrescriptionExamenMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPrescriptionExamen() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();

        // Update the prescriptionExamen
        PrescriptionExamen updatedPrescriptionExamen = prescriptionExamenRepository.findById(prescriptionExamen.getId()).get();
        // Disconnect from session so that the updates on updatedPrescriptionExamen are not directly saved in db
        em.detach(updatedPrescriptionExamen);
        updatedPrescriptionExamen.description(UPDATED_DESCRIPTION).resultat(UPDATED_RESULTAT);
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(updatedPrescriptionExamen);

        restPrescriptionExamenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, prescriptionExamenDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isOk());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
        PrescriptionExamen testPrescriptionExamen = prescriptionExamenList.get(prescriptionExamenList.size() - 1);
        assertThat(testPrescriptionExamen.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPrescriptionExamen.getResultat()).isEqualTo(UPDATED_RESULTAT);
    }

    @Test
    @Transactional
    void putNonExistingPrescriptionExamen() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();
        prescriptionExamen.setId(count.incrementAndGet());

        // Create the PrescriptionExamen
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrescriptionExamenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, prescriptionExamenDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPrescriptionExamen() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();
        prescriptionExamen.setId(count.incrementAndGet());

        // Create the PrescriptionExamen
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionExamenMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPrescriptionExamen() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();
        prescriptionExamen.setId(count.incrementAndGet());

        // Create the PrescriptionExamen
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionExamenMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePrescriptionExamenWithPatch() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();

        // Update the prescriptionExamen using partial update
        PrescriptionExamen partialUpdatedPrescriptionExamen = new PrescriptionExamen();
        partialUpdatedPrescriptionExamen.setId(prescriptionExamen.getId());

        partialUpdatedPrescriptionExamen.resultat(UPDATED_RESULTAT);

        restPrescriptionExamenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrescriptionExamen.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrescriptionExamen))
            )
            .andExpect(status().isOk());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
        PrescriptionExamen testPrescriptionExamen = prescriptionExamenList.get(prescriptionExamenList.size() - 1);
        assertThat(testPrescriptionExamen.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPrescriptionExamen.getResultat()).isEqualTo(UPDATED_RESULTAT);
    }

    @Test
    @Transactional
    void fullUpdatePrescriptionExamenWithPatch() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();

        // Update the prescriptionExamen using partial update
        PrescriptionExamen partialUpdatedPrescriptionExamen = new PrescriptionExamen();
        partialUpdatedPrescriptionExamen.setId(prescriptionExamen.getId());

        partialUpdatedPrescriptionExamen.description(UPDATED_DESCRIPTION).resultat(UPDATED_RESULTAT);

        restPrescriptionExamenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrescriptionExamen.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrescriptionExamen))
            )
            .andExpect(status().isOk());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
        PrescriptionExamen testPrescriptionExamen = prescriptionExamenList.get(prescriptionExamenList.size() - 1);
        assertThat(testPrescriptionExamen.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPrescriptionExamen.getResultat()).isEqualTo(UPDATED_RESULTAT);
    }

    @Test
    @Transactional
    void patchNonExistingPrescriptionExamen() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();
        prescriptionExamen.setId(count.incrementAndGet());

        // Create the PrescriptionExamen
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrescriptionExamenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, prescriptionExamenDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPrescriptionExamen() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();
        prescriptionExamen.setId(count.incrementAndGet());

        // Create the PrescriptionExamen
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionExamenMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPrescriptionExamen() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionExamenRepository.findAll().size();
        prescriptionExamen.setId(count.incrementAndGet());

        // Create the PrescriptionExamen
        PrescriptionExamenDTO prescriptionExamenDTO = prescriptionExamenMapper.toDto(prescriptionExamen);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionExamenMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionExamenDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PrescriptionExamen in the database
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePrescriptionExamen() throws Exception {
        // Initialize the database
        prescriptionExamenRepository.saveAndFlush(prescriptionExamen);

        int databaseSizeBeforeDelete = prescriptionExamenRepository.findAll().size();

        // Delete the prescriptionExamen
        restPrescriptionExamenMockMvc
            .perform(delete(ENTITY_API_URL_ID, prescriptionExamen.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PrescriptionExamen> prescriptionExamenList = prescriptionExamenRepository.findAll();
        assertThat(prescriptionExamenList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
