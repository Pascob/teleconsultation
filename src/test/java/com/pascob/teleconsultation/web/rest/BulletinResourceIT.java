package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Bulletin;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.PrescriptionExamen;
import com.pascob.teleconsultation.repository.BulletinRepository;
import com.pascob.teleconsultation.service.criteria.BulletinCriteria;
import com.pascob.teleconsultation.service.dto.BulletinDTO;
import com.pascob.teleconsultation.service.mapper.BulletinMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BulletinResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BulletinResourceIT {

    private static final LocalDate DEFAULT_DATE_BULLETIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_BULLETIN = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE_BULLETIN = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/bulletins";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BulletinRepository bulletinRepository;

    @Autowired
    private BulletinMapper bulletinMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBulletinMockMvc;

    private Bulletin bulletin;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bulletin createEntity(EntityManager em) {
        Bulletin bulletin = new Bulletin().dateBulletin(DEFAULT_DATE_BULLETIN).numero(DEFAULT_NUMERO);
        return bulletin;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bulletin createUpdatedEntity(EntityManager em) {
        Bulletin bulletin = new Bulletin().dateBulletin(UPDATED_DATE_BULLETIN).numero(UPDATED_NUMERO);
        return bulletin;
    }

    @BeforeEach
    public void initTest() {
        bulletin = createEntity(em);
    }

    @Test
    @Transactional
    void createBulletin() throws Exception {
        int databaseSizeBeforeCreate = bulletinRepository.findAll().size();
        // Create the Bulletin
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);
        restBulletinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bulletinDTO)))
            .andExpect(status().isCreated());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeCreate + 1);
        Bulletin testBulletin = bulletinList.get(bulletinList.size() - 1);
        assertThat(testBulletin.getDateBulletin()).isEqualTo(DEFAULT_DATE_BULLETIN);
        assertThat(testBulletin.getNumero()).isEqualTo(DEFAULT_NUMERO);
    }

    @Test
    @Transactional
    void createBulletinWithExistingId() throws Exception {
        // Create the Bulletin with an existing ID
        bulletin.setId(1L);
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        int databaseSizeBeforeCreate = bulletinRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBulletinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bulletinDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDateBulletinIsRequired() throws Exception {
        int databaseSizeBeforeTest = bulletinRepository.findAll().size();
        // set the field null
        bulletin.setDateBulletin(null);

        // Create the Bulletin, which fails.
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        restBulletinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bulletinDTO)))
            .andExpect(status().isBadRequest());

        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllBulletins() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList
        restBulletinMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bulletin.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateBulletin").value(hasItem(DEFAULT_DATE_BULLETIN.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)));
    }

    @Test
    @Transactional
    void getBulletin() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get the bulletin
        restBulletinMockMvc
            .perform(get(ENTITY_API_URL_ID, bulletin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bulletin.getId().intValue()))
            .andExpect(jsonPath("$.dateBulletin").value(DEFAULT_DATE_BULLETIN.toString()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO));
    }

    @Test
    @Transactional
    void getBulletinsByIdFiltering() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        Long id = bulletin.getId();

        defaultBulletinShouldBeFound("id.equals=" + id);
        defaultBulletinShouldNotBeFound("id.notEquals=" + id);

        defaultBulletinShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBulletinShouldNotBeFound("id.greaterThan=" + id);

        defaultBulletinShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBulletinShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllBulletinsByDateBulletinIsEqualToSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where dateBulletin equals to DEFAULT_DATE_BULLETIN
        defaultBulletinShouldBeFound("dateBulletin.equals=" + DEFAULT_DATE_BULLETIN);

        // Get all the bulletinList where dateBulletin equals to UPDATED_DATE_BULLETIN
        defaultBulletinShouldNotBeFound("dateBulletin.equals=" + UPDATED_DATE_BULLETIN);
    }

    @Test
    @Transactional
    void getAllBulletinsByDateBulletinIsInShouldWork() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where dateBulletin in DEFAULT_DATE_BULLETIN or UPDATED_DATE_BULLETIN
        defaultBulletinShouldBeFound("dateBulletin.in=" + DEFAULT_DATE_BULLETIN + "," + UPDATED_DATE_BULLETIN);

        // Get all the bulletinList where dateBulletin equals to UPDATED_DATE_BULLETIN
        defaultBulletinShouldNotBeFound("dateBulletin.in=" + UPDATED_DATE_BULLETIN);
    }

    @Test
    @Transactional
    void getAllBulletinsByDateBulletinIsNullOrNotNull() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where dateBulletin is not null
        defaultBulletinShouldBeFound("dateBulletin.specified=true");

        // Get all the bulletinList where dateBulletin is null
        defaultBulletinShouldNotBeFound("dateBulletin.specified=false");
    }

    @Test
    @Transactional
    void getAllBulletinsByDateBulletinIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where dateBulletin is greater than or equal to DEFAULT_DATE_BULLETIN
        defaultBulletinShouldBeFound("dateBulletin.greaterThanOrEqual=" + DEFAULT_DATE_BULLETIN);

        // Get all the bulletinList where dateBulletin is greater than or equal to UPDATED_DATE_BULLETIN
        defaultBulletinShouldNotBeFound("dateBulletin.greaterThanOrEqual=" + UPDATED_DATE_BULLETIN);
    }

    @Test
    @Transactional
    void getAllBulletinsByDateBulletinIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where dateBulletin is less than or equal to DEFAULT_DATE_BULLETIN
        defaultBulletinShouldBeFound("dateBulletin.lessThanOrEqual=" + DEFAULT_DATE_BULLETIN);

        // Get all the bulletinList where dateBulletin is less than or equal to SMALLER_DATE_BULLETIN
        defaultBulletinShouldNotBeFound("dateBulletin.lessThanOrEqual=" + SMALLER_DATE_BULLETIN);
    }

    @Test
    @Transactional
    void getAllBulletinsByDateBulletinIsLessThanSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where dateBulletin is less than DEFAULT_DATE_BULLETIN
        defaultBulletinShouldNotBeFound("dateBulletin.lessThan=" + DEFAULT_DATE_BULLETIN);

        // Get all the bulletinList where dateBulletin is less than UPDATED_DATE_BULLETIN
        defaultBulletinShouldBeFound("dateBulletin.lessThan=" + UPDATED_DATE_BULLETIN);
    }

    @Test
    @Transactional
    void getAllBulletinsByDateBulletinIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where dateBulletin is greater than DEFAULT_DATE_BULLETIN
        defaultBulletinShouldNotBeFound("dateBulletin.greaterThan=" + DEFAULT_DATE_BULLETIN);

        // Get all the bulletinList where dateBulletin is greater than SMALLER_DATE_BULLETIN
        defaultBulletinShouldBeFound("dateBulletin.greaterThan=" + SMALLER_DATE_BULLETIN);
    }

    @Test
    @Transactional
    void getAllBulletinsByNumeroIsEqualToSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where numero equals to DEFAULT_NUMERO
        defaultBulletinShouldBeFound("numero.equals=" + DEFAULT_NUMERO);

        // Get all the bulletinList where numero equals to UPDATED_NUMERO
        defaultBulletinShouldNotBeFound("numero.equals=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllBulletinsByNumeroIsInShouldWork() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where numero in DEFAULT_NUMERO or UPDATED_NUMERO
        defaultBulletinShouldBeFound("numero.in=" + DEFAULT_NUMERO + "," + UPDATED_NUMERO);

        // Get all the bulletinList where numero equals to UPDATED_NUMERO
        defaultBulletinShouldNotBeFound("numero.in=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllBulletinsByNumeroIsNullOrNotNull() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where numero is not null
        defaultBulletinShouldBeFound("numero.specified=true");

        // Get all the bulletinList where numero is null
        defaultBulletinShouldNotBeFound("numero.specified=false");
    }

    @Test
    @Transactional
    void getAllBulletinsByNumeroContainsSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where numero contains DEFAULT_NUMERO
        defaultBulletinShouldBeFound("numero.contains=" + DEFAULT_NUMERO);

        // Get all the bulletinList where numero contains UPDATED_NUMERO
        defaultBulletinShouldNotBeFound("numero.contains=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllBulletinsByNumeroNotContainsSomething() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        // Get all the bulletinList where numero does not contain DEFAULT_NUMERO
        defaultBulletinShouldNotBeFound("numero.doesNotContain=" + DEFAULT_NUMERO);

        // Get all the bulletinList where numero does not contain UPDATED_NUMERO
        defaultBulletinShouldBeFound("numero.doesNotContain=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllBulletinsByPrescriptionExamenIsEqualToSomething() throws Exception {
        PrescriptionExamen prescriptionExamen;
        if (TestUtil.findAll(em, PrescriptionExamen.class).isEmpty()) {
            bulletinRepository.saveAndFlush(bulletin);
            prescriptionExamen = PrescriptionExamenResourceIT.createEntity(em);
        } else {
            prescriptionExamen = TestUtil.findAll(em, PrescriptionExamen.class).get(0);
        }
        em.persist(prescriptionExamen);
        em.flush();
        bulletin.addPrescriptionExamen(prescriptionExamen);
        bulletinRepository.saveAndFlush(bulletin);
        Long prescriptionExamenId = prescriptionExamen.getId();

        // Get all the bulletinList where prescriptionExamen equals to prescriptionExamenId
        defaultBulletinShouldBeFound("prescriptionExamenId.equals=" + prescriptionExamenId);

        // Get all the bulletinList where prescriptionExamen equals to (prescriptionExamenId + 1)
        defaultBulletinShouldNotBeFound("prescriptionExamenId.equals=" + (prescriptionExamenId + 1));
    }

    @Test
    @Transactional
    void getAllBulletinsByFicheConsultationIsEqualToSomething() throws Exception {
        FicheConsultation ficheConsultation;
        if (TestUtil.findAll(em, FicheConsultation.class).isEmpty()) {
            bulletinRepository.saveAndFlush(bulletin);
            ficheConsultation = FicheConsultationResourceIT.createEntity(em);
        } else {
            ficheConsultation = TestUtil.findAll(em, FicheConsultation.class).get(0);
        }
        em.persist(ficheConsultation);
        em.flush();
        bulletin.setFicheConsultation(ficheConsultation);
        bulletinRepository.saveAndFlush(bulletin);
        Long ficheConsultationId = ficheConsultation.getId();

        // Get all the bulletinList where ficheConsultation equals to ficheConsultationId
        defaultBulletinShouldBeFound("ficheConsultationId.equals=" + ficheConsultationId);

        // Get all the bulletinList where ficheConsultation equals to (ficheConsultationId + 1)
        defaultBulletinShouldNotBeFound("ficheConsultationId.equals=" + (ficheConsultationId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBulletinShouldBeFound(String filter) throws Exception {
        restBulletinMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bulletin.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateBulletin").value(hasItem(DEFAULT_DATE_BULLETIN.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)));

        // Check, that the count call also returns 1
        restBulletinMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBulletinShouldNotBeFound(String filter) throws Exception {
        restBulletinMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBulletinMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingBulletin() throws Exception {
        // Get the bulletin
        restBulletinMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingBulletin() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();

        // Update the bulletin
        Bulletin updatedBulletin = bulletinRepository.findById(bulletin.getId()).get();
        // Disconnect from session so that the updates on updatedBulletin are not directly saved in db
        em.detach(updatedBulletin);
        updatedBulletin.dateBulletin(UPDATED_DATE_BULLETIN).numero(UPDATED_NUMERO);
        BulletinDTO bulletinDTO = bulletinMapper.toDto(updatedBulletin);

        restBulletinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bulletinDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bulletinDTO))
            )
            .andExpect(status().isOk());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
        Bulletin testBulletin = bulletinList.get(bulletinList.size() - 1);
        assertThat(testBulletin.getDateBulletin()).isEqualTo(UPDATED_DATE_BULLETIN);
        assertThat(testBulletin.getNumero()).isEqualTo(UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void putNonExistingBulletin() throws Exception {
        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();
        bulletin.setId(count.incrementAndGet());

        // Create the Bulletin
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBulletinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bulletinDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bulletinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBulletin() throws Exception {
        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();
        bulletin.setId(count.incrementAndGet());

        // Create the Bulletin
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBulletinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bulletinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBulletin() throws Exception {
        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();
        bulletin.setId(count.incrementAndGet());

        // Create the Bulletin
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBulletinMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bulletinDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBulletinWithPatch() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();

        // Update the bulletin using partial update
        Bulletin partialUpdatedBulletin = new Bulletin();
        partialUpdatedBulletin.setId(bulletin.getId());

        restBulletinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBulletin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBulletin))
            )
            .andExpect(status().isOk());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
        Bulletin testBulletin = bulletinList.get(bulletinList.size() - 1);
        assertThat(testBulletin.getDateBulletin()).isEqualTo(DEFAULT_DATE_BULLETIN);
        assertThat(testBulletin.getNumero()).isEqualTo(DEFAULT_NUMERO);
    }

    @Test
    @Transactional
    void fullUpdateBulletinWithPatch() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();

        // Update the bulletin using partial update
        Bulletin partialUpdatedBulletin = new Bulletin();
        partialUpdatedBulletin.setId(bulletin.getId());

        partialUpdatedBulletin.dateBulletin(UPDATED_DATE_BULLETIN).numero(UPDATED_NUMERO);

        restBulletinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBulletin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBulletin))
            )
            .andExpect(status().isOk());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
        Bulletin testBulletin = bulletinList.get(bulletinList.size() - 1);
        assertThat(testBulletin.getDateBulletin()).isEqualTo(UPDATED_DATE_BULLETIN);
        assertThat(testBulletin.getNumero()).isEqualTo(UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void patchNonExistingBulletin() throws Exception {
        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();
        bulletin.setId(count.incrementAndGet());

        // Create the Bulletin
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBulletinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, bulletinDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bulletinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBulletin() throws Exception {
        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();
        bulletin.setId(count.incrementAndGet());

        // Create the Bulletin
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBulletinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bulletinDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBulletin() throws Exception {
        int databaseSizeBeforeUpdate = bulletinRepository.findAll().size();
        bulletin.setId(count.incrementAndGet());

        // Create the Bulletin
        BulletinDTO bulletinDTO = bulletinMapper.toDto(bulletin);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBulletinMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(bulletinDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Bulletin in the database
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBulletin() throws Exception {
        // Initialize the database
        bulletinRepository.saveAndFlush(bulletin);

        int databaseSizeBeforeDelete = bulletinRepository.findAll().size();

        // Delete the bulletin
        restBulletinMockMvc
            .perform(delete(ENTITY_API_URL_ID, bulletin.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Bulletin> bulletinList = bulletinRepository.findAll();
        assertThat(bulletinList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
