package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Ordonnance;
import com.pascob.teleconsultation.domain.PrescriptionProduit;
import com.pascob.teleconsultation.domain.Produit;
import com.pascob.teleconsultation.repository.PrescriptionProduitRepository;
import com.pascob.teleconsultation.service.criteria.PrescriptionProduitCriteria;
import com.pascob.teleconsultation.service.dto.PrescriptionProduitDTO;
import com.pascob.teleconsultation.service.mapper.PrescriptionProduitMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PrescriptionProduitResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PrescriptionProduitResourceIT {

    private static final String DEFAULT_POSOLOGIE = "AAAAAAAAAA";
    private static final String UPDATED_POSOLOGIE = "BBBBBBBBBB";

    private static final String DEFAULT_OBSERVATION = "AAAAAAAAAA";
    private static final String UPDATED_OBSERVATION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/prescription-produits";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PrescriptionProduitRepository prescriptionProduitRepository;

    @Autowired
    private PrescriptionProduitMapper prescriptionProduitMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPrescriptionProduitMockMvc;

    private PrescriptionProduit prescriptionProduit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrescriptionProduit createEntity(EntityManager em) {
        PrescriptionProduit prescriptionProduit = new PrescriptionProduit().posologie(DEFAULT_POSOLOGIE).observation(DEFAULT_OBSERVATION);
        return prescriptionProduit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrescriptionProduit createUpdatedEntity(EntityManager em) {
        PrescriptionProduit prescriptionProduit = new PrescriptionProduit().posologie(UPDATED_POSOLOGIE).observation(UPDATED_OBSERVATION);
        return prescriptionProduit;
    }

    @BeforeEach
    public void initTest() {
        prescriptionProduit = createEntity(em);
    }

    @Test
    @Transactional
    void createPrescriptionProduit() throws Exception {
        int databaseSizeBeforeCreate = prescriptionProduitRepository.findAll().size();
        // Create the PrescriptionProduit
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);
        restPrescriptionProduitMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeCreate + 1);
        PrescriptionProduit testPrescriptionProduit = prescriptionProduitList.get(prescriptionProduitList.size() - 1);
        assertThat(testPrescriptionProduit.getPosologie()).isEqualTo(DEFAULT_POSOLOGIE);
        assertThat(testPrescriptionProduit.getObservation()).isEqualTo(DEFAULT_OBSERVATION);
    }

    @Test
    @Transactional
    void createPrescriptionProduitWithExistingId() throws Exception {
        // Create the PrescriptionProduit with an existing ID
        prescriptionProduit.setId(1L);
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);

        int databaseSizeBeforeCreate = prescriptionProduitRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrescriptionProduitMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduits() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList
        restPrescriptionProduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prescriptionProduit.getId().intValue())))
            .andExpect(jsonPath("$.[*].posologie").value(hasItem(DEFAULT_POSOLOGIE)))
            .andExpect(jsonPath("$.[*].observation").value(hasItem(DEFAULT_OBSERVATION)));
    }

    @Test
    @Transactional
    void getPrescriptionProduit() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get the prescriptionProduit
        restPrescriptionProduitMockMvc
            .perform(get(ENTITY_API_URL_ID, prescriptionProduit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(prescriptionProduit.getId().intValue()))
            .andExpect(jsonPath("$.posologie").value(DEFAULT_POSOLOGIE))
            .andExpect(jsonPath("$.observation").value(DEFAULT_OBSERVATION));
    }

    @Test
    @Transactional
    void getPrescriptionProduitsByIdFiltering() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        Long id = prescriptionProduit.getId();

        defaultPrescriptionProduitShouldBeFound("id.equals=" + id);
        defaultPrescriptionProduitShouldNotBeFound("id.notEquals=" + id);

        defaultPrescriptionProduitShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPrescriptionProduitShouldNotBeFound("id.greaterThan=" + id);

        defaultPrescriptionProduitShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPrescriptionProduitShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByPosologieIsEqualToSomething() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where posologie equals to DEFAULT_POSOLOGIE
        defaultPrescriptionProduitShouldBeFound("posologie.equals=" + DEFAULT_POSOLOGIE);

        // Get all the prescriptionProduitList where posologie equals to UPDATED_POSOLOGIE
        defaultPrescriptionProduitShouldNotBeFound("posologie.equals=" + UPDATED_POSOLOGIE);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByPosologieIsInShouldWork() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where posologie in DEFAULT_POSOLOGIE or UPDATED_POSOLOGIE
        defaultPrescriptionProduitShouldBeFound("posologie.in=" + DEFAULT_POSOLOGIE + "," + UPDATED_POSOLOGIE);

        // Get all the prescriptionProduitList where posologie equals to UPDATED_POSOLOGIE
        defaultPrescriptionProduitShouldNotBeFound("posologie.in=" + UPDATED_POSOLOGIE);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByPosologieIsNullOrNotNull() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where posologie is not null
        defaultPrescriptionProduitShouldBeFound("posologie.specified=true");

        // Get all the prescriptionProduitList where posologie is null
        defaultPrescriptionProduitShouldNotBeFound("posologie.specified=false");
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByPosologieContainsSomething() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where posologie contains DEFAULT_POSOLOGIE
        defaultPrescriptionProduitShouldBeFound("posologie.contains=" + DEFAULT_POSOLOGIE);

        // Get all the prescriptionProduitList where posologie contains UPDATED_POSOLOGIE
        defaultPrescriptionProduitShouldNotBeFound("posologie.contains=" + UPDATED_POSOLOGIE);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByPosologieNotContainsSomething() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where posologie does not contain DEFAULT_POSOLOGIE
        defaultPrescriptionProduitShouldNotBeFound("posologie.doesNotContain=" + DEFAULT_POSOLOGIE);

        // Get all the prescriptionProduitList where posologie does not contain UPDATED_POSOLOGIE
        defaultPrescriptionProduitShouldBeFound("posologie.doesNotContain=" + UPDATED_POSOLOGIE);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByObservationIsEqualToSomething() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where observation equals to DEFAULT_OBSERVATION
        defaultPrescriptionProduitShouldBeFound("observation.equals=" + DEFAULT_OBSERVATION);

        // Get all the prescriptionProduitList where observation equals to UPDATED_OBSERVATION
        defaultPrescriptionProduitShouldNotBeFound("observation.equals=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByObservationIsInShouldWork() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where observation in DEFAULT_OBSERVATION or UPDATED_OBSERVATION
        defaultPrescriptionProduitShouldBeFound("observation.in=" + DEFAULT_OBSERVATION + "," + UPDATED_OBSERVATION);

        // Get all the prescriptionProduitList where observation equals to UPDATED_OBSERVATION
        defaultPrescriptionProduitShouldNotBeFound("observation.in=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByObservationIsNullOrNotNull() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where observation is not null
        defaultPrescriptionProduitShouldBeFound("observation.specified=true");

        // Get all the prescriptionProduitList where observation is null
        defaultPrescriptionProduitShouldNotBeFound("observation.specified=false");
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByObservationContainsSomething() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where observation contains DEFAULT_OBSERVATION
        defaultPrescriptionProduitShouldBeFound("observation.contains=" + DEFAULT_OBSERVATION);

        // Get all the prescriptionProduitList where observation contains UPDATED_OBSERVATION
        defaultPrescriptionProduitShouldNotBeFound("observation.contains=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByObservationNotContainsSomething() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        // Get all the prescriptionProduitList where observation does not contain DEFAULT_OBSERVATION
        defaultPrescriptionProduitShouldNotBeFound("observation.doesNotContain=" + DEFAULT_OBSERVATION);

        // Get all the prescriptionProduitList where observation does not contain UPDATED_OBSERVATION
        defaultPrescriptionProduitShouldBeFound("observation.doesNotContain=" + UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByProduitIsEqualToSomething() throws Exception {
        Produit produit;
        if (TestUtil.findAll(em, Produit.class).isEmpty()) {
            prescriptionProduitRepository.saveAndFlush(prescriptionProduit);
            produit = ProduitResourceIT.createEntity(em);
        } else {
            produit = TestUtil.findAll(em, Produit.class).get(0);
        }
        em.persist(produit);
        em.flush();
        prescriptionProduit.setProduit(produit);
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);
        Long produitId = produit.getId();

        // Get all the prescriptionProduitList where produit equals to produitId
        defaultPrescriptionProduitShouldBeFound("produitId.equals=" + produitId);

        // Get all the prescriptionProduitList where produit equals to (produitId + 1)
        defaultPrescriptionProduitShouldNotBeFound("produitId.equals=" + (produitId + 1));
    }

    @Test
    @Transactional
    void getAllPrescriptionProduitsByOrdonnanceIsEqualToSomething() throws Exception {
        Ordonnance ordonnance;
        if (TestUtil.findAll(em, Ordonnance.class).isEmpty()) {
            prescriptionProduitRepository.saveAndFlush(prescriptionProduit);
            ordonnance = OrdonnanceResourceIT.createEntity(em);
        } else {
            ordonnance = TestUtil.findAll(em, Ordonnance.class).get(0);
        }
        em.persist(ordonnance);
        em.flush();
        prescriptionProduit.setOrdonnance(ordonnance);
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);
        Long ordonnanceId = ordonnance.getId();

        // Get all the prescriptionProduitList where ordonnance equals to ordonnanceId
        defaultPrescriptionProduitShouldBeFound("ordonnanceId.equals=" + ordonnanceId);

        // Get all the prescriptionProduitList where ordonnance equals to (ordonnanceId + 1)
        defaultPrescriptionProduitShouldNotBeFound("ordonnanceId.equals=" + (ordonnanceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPrescriptionProduitShouldBeFound(String filter) throws Exception {
        restPrescriptionProduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(prescriptionProduit.getId().intValue())))
            .andExpect(jsonPath("$.[*].posologie").value(hasItem(DEFAULT_POSOLOGIE)))
            .andExpect(jsonPath("$.[*].observation").value(hasItem(DEFAULT_OBSERVATION)));

        // Check, that the count call also returns 1
        restPrescriptionProduitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPrescriptionProduitShouldNotBeFound(String filter) throws Exception {
        restPrescriptionProduitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPrescriptionProduitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPrescriptionProduit() throws Exception {
        // Get the prescriptionProduit
        restPrescriptionProduitMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPrescriptionProduit() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();

        // Update the prescriptionProduit
        PrescriptionProduit updatedPrescriptionProduit = prescriptionProduitRepository.findById(prescriptionProduit.getId()).get();
        // Disconnect from session so that the updates on updatedPrescriptionProduit are not directly saved in db
        em.detach(updatedPrescriptionProduit);
        updatedPrescriptionProduit.posologie(UPDATED_POSOLOGIE).observation(UPDATED_OBSERVATION);
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(updatedPrescriptionProduit);

        restPrescriptionProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, prescriptionProduitDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isOk());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
        PrescriptionProduit testPrescriptionProduit = prescriptionProduitList.get(prescriptionProduitList.size() - 1);
        assertThat(testPrescriptionProduit.getPosologie()).isEqualTo(UPDATED_POSOLOGIE);
        assertThat(testPrescriptionProduit.getObservation()).isEqualTo(UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void putNonExistingPrescriptionProduit() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();
        prescriptionProduit.setId(count.incrementAndGet());

        // Create the PrescriptionProduit
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrescriptionProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, prescriptionProduitDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPrescriptionProduit() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();
        prescriptionProduit.setId(count.incrementAndGet());

        // Create the PrescriptionProduit
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionProduitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPrescriptionProduit() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();
        prescriptionProduit.setId(count.incrementAndGet());

        // Create the PrescriptionProduit
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionProduitMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePrescriptionProduitWithPatch() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();

        // Update the prescriptionProduit using partial update
        PrescriptionProduit partialUpdatedPrescriptionProduit = new PrescriptionProduit();
        partialUpdatedPrescriptionProduit.setId(prescriptionProduit.getId());

        partialUpdatedPrescriptionProduit.posologie(UPDATED_POSOLOGIE).observation(UPDATED_OBSERVATION);

        restPrescriptionProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrescriptionProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrescriptionProduit))
            )
            .andExpect(status().isOk());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
        PrescriptionProduit testPrescriptionProduit = prescriptionProduitList.get(prescriptionProduitList.size() - 1);
        assertThat(testPrescriptionProduit.getPosologie()).isEqualTo(UPDATED_POSOLOGIE);
        assertThat(testPrescriptionProduit.getObservation()).isEqualTo(UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void fullUpdatePrescriptionProduitWithPatch() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();

        // Update the prescriptionProduit using partial update
        PrescriptionProduit partialUpdatedPrescriptionProduit = new PrescriptionProduit();
        partialUpdatedPrescriptionProduit.setId(prescriptionProduit.getId());

        partialUpdatedPrescriptionProduit.posologie(UPDATED_POSOLOGIE).observation(UPDATED_OBSERVATION);

        restPrescriptionProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPrescriptionProduit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPrescriptionProduit))
            )
            .andExpect(status().isOk());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
        PrescriptionProduit testPrescriptionProduit = prescriptionProduitList.get(prescriptionProduitList.size() - 1);
        assertThat(testPrescriptionProduit.getPosologie()).isEqualTo(UPDATED_POSOLOGIE);
        assertThat(testPrescriptionProduit.getObservation()).isEqualTo(UPDATED_OBSERVATION);
    }

    @Test
    @Transactional
    void patchNonExistingPrescriptionProduit() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();
        prescriptionProduit.setId(count.incrementAndGet());

        // Create the PrescriptionProduit
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPrescriptionProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, prescriptionProduitDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPrescriptionProduit() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();
        prescriptionProduit.setId(count.incrementAndGet());

        // Create the PrescriptionProduit
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionProduitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPrescriptionProduit() throws Exception {
        int databaseSizeBeforeUpdate = prescriptionProduitRepository.findAll().size();
        prescriptionProduit.setId(count.incrementAndGet());

        // Create the PrescriptionProduit
        PrescriptionProduitDTO prescriptionProduitDTO = prescriptionProduitMapper.toDto(prescriptionProduit);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPrescriptionProduitMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(prescriptionProduitDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PrescriptionProduit in the database
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePrescriptionProduit() throws Exception {
        // Initialize the database
        prescriptionProduitRepository.saveAndFlush(prescriptionProduit);

        int databaseSizeBeforeDelete = prescriptionProduitRepository.findAll().size();

        // Delete the prescriptionProduit
        restPrescriptionProduitMockMvc
            .perform(delete(ENTITY_API_URL_ID, prescriptionProduit.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PrescriptionProduit> prescriptionProduitList = prescriptionProduitRepository.findAll();
        assertThat(prescriptionProduitList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
