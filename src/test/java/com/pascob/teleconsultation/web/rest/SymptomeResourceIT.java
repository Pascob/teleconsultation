package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.FicheSymptome;
import com.pascob.teleconsultation.domain.Symptome;
import com.pascob.teleconsultation.repository.SymptomeRepository;
import com.pascob.teleconsultation.service.criteria.SymptomeCriteria;
import com.pascob.teleconsultation.service.dto.SymptomeDTO;
import com.pascob.teleconsultation.service.mapper.SymptomeMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SymptomeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SymptomeResourceIT {

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/symptomes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SymptomeRepository symptomeRepository;

    @Autowired
    private SymptomeMapper symptomeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSymptomeMockMvc;

    private Symptome symptome;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Symptome createEntity(EntityManager em) {
        Symptome symptome = new Symptome().designation(DEFAULT_DESIGNATION).code(DEFAULT_CODE);
        return symptome;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Symptome createUpdatedEntity(EntityManager em) {
        Symptome symptome = new Symptome().designation(UPDATED_DESIGNATION).code(UPDATED_CODE);
        return symptome;
    }

    @BeforeEach
    public void initTest() {
        symptome = createEntity(em);
    }

    @Test
    @Transactional
    void createSymptome() throws Exception {
        int databaseSizeBeforeCreate = symptomeRepository.findAll().size();
        // Create the Symptome
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);
        restSymptomeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(symptomeDTO)))
            .andExpect(status().isCreated());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeCreate + 1);
        Symptome testSymptome = symptomeList.get(symptomeList.size() - 1);
        assertThat(testSymptome.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testSymptome.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createSymptomeWithExistingId() throws Exception {
        // Create the Symptome with an existing ID
        symptome.setId(1L);
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        int databaseSizeBeforeCreate = symptomeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSymptomeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(symptomeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDesignationIsRequired() throws Exception {
        int databaseSizeBeforeTest = symptomeRepository.findAll().size();
        // set the field null
        symptome.setDesignation(null);

        // Create the Symptome, which fails.
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        restSymptomeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(symptomeDTO)))
            .andExpect(status().isBadRequest());

        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllSymptomes() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList
        restSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(symptome.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getSymptome() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get the symptome
        restSymptomeMockMvc
            .perform(get(ENTITY_API_URL_ID, symptome.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(symptome.getId().intValue()))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getSymptomesByIdFiltering() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        Long id = symptome.getId();

        defaultSymptomeShouldBeFound("id.equals=" + id);
        defaultSymptomeShouldNotBeFound("id.notEquals=" + id);

        defaultSymptomeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSymptomeShouldNotBeFound("id.greaterThan=" + id);

        defaultSymptomeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSymptomeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllSymptomesByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where designation equals to DEFAULT_DESIGNATION
        defaultSymptomeShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the symptomeList where designation equals to UPDATED_DESIGNATION
        defaultSymptomeShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSymptomesByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultSymptomeShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the symptomeList where designation equals to UPDATED_DESIGNATION
        defaultSymptomeShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSymptomesByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where designation is not null
        defaultSymptomeShouldBeFound("designation.specified=true");

        // Get all the symptomeList where designation is null
        defaultSymptomeShouldNotBeFound("designation.specified=false");
    }

    @Test
    @Transactional
    void getAllSymptomesByDesignationContainsSomething() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where designation contains DEFAULT_DESIGNATION
        defaultSymptomeShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the symptomeList where designation contains UPDATED_DESIGNATION
        defaultSymptomeShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSymptomesByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where designation does not contain DEFAULT_DESIGNATION
        defaultSymptomeShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the symptomeList where designation does not contain UPDATED_DESIGNATION
        defaultSymptomeShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllSymptomesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where code equals to DEFAULT_CODE
        defaultSymptomeShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the symptomeList where code equals to UPDATED_CODE
        defaultSymptomeShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllSymptomesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where code in DEFAULT_CODE or UPDATED_CODE
        defaultSymptomeShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the symptomeList where code equals to UPDATED_CODE
        defaultSymptomeShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllSymptomesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where code is not null
        defaultSymptomeShouldBeFound("code.specified=true");

        // Get all the symptomeList where code is null
        defaultSymptomeShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllSymptomesByCodeContainsSomething() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where code contains DEFAULT_CODE
        defaultSymptomeShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the symptomeList where code contains UPDATED_CODE
        defaultSymptomeShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllSymptomesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        // Get all the symptomeList where code does not contain DEFAULT_CODE
        defaultSymptomeShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the symptomeList where code does not contain UPDATED_CODE
        defaultSymptomeShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllSymptomesByFicheSymptomeIsEqualToSomething() throws Exception {
        FicheSymptome ficheSymptome;
        if (TestUtil.findAll(em, FicheSymptome.class).isEmpty()) {
            symptomeRepository.saveAndFlush(symptome);
            ficheSymptome = FicheSymptomeResourceIT.createEntity(em);
        } else {
            ficheSymptome = TestUtil.findAll(em, FicheSymptome.class).get(0);
        }
        em.persist(ficheSymptome);
        em.flush();
        symptome.addFicheSymptome(ficheSymptome);
        symptomeRepository.saveAndFlush(symptome);
        Long ficheSymptomeId = ficheSymptome.getId();

        // Get all the symptomeList where ficheSymptome equals to ficheSymptomeId
        defaultSymptomeShouldBeFound("ficheSymptomeId.equals=" + ficheSymptomeId);

        // Get all the symptomeList where ficheSymptome equals to (ficheSymptomeId + 1)
        defaultSymptomeShouldNotBeFound("ficheSymptomeId.equals=" + (ficheSymptomeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSymptomeShouldBeFound(String filter) throws Exception {
        restSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(symptome.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));

        // Check, that the count call also returns 1
        restSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSymptomeShouldNotBeFound(String filter) throws Exception {
        restSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSymptomeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingSymptome() throws Exception {
        // Get the symptome
        restSymptomeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSymptome() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();

        // Update the symptome
        Symptome updatedSymptome = symptomeRepository.findById(symptome.getId()).get();
        // Disconnect from session so that the updates on updatedSymptome are not directly saved in db
        em.detach(updatedSymptome);
        updatedSymptome.designation(UPDATED_DESIGNATION).code(UPDATED_CODE);
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(updatedSymptome);

        restSymptomeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, symptomeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(symptomeDTO))
            )
            .andExpect(status().isOk());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
        Symptome testSymptome = symptomeList.get(symptomeList.size() - 1);
        assertThat(testSymptome.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testSymptome.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingSymptome() throws Exception {
        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();
        symptome.setId(count.incrementAndGet());

        // Create the Symptome
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSymptomeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, symptomeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(symptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSymptome() throws Exception {
        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();
        symptome.setId(count.incrementAndGet());

        // Create the Symptome
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSymptomeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(symptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSymptome() throws Exception {
        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();
        symptome.setId(count.incrementAndGet());

        // Create the Symptome
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSymptomeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(symptomeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSymptomeWithPatch() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();

        // Update the symptome using partial update
        Symptome partialUpdatedSymptome = new Symptome();
        partialUpdatedSymptome.setId(symptome.getId());

        partialUpdatedSymptome.code(UPDATED_CODE);

        restSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSymptome.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSymptome))
            )
            .andExpect(status().isOk());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
        Symptome testSymptome = symptomeList.get(symptomeList.size() - 1);
        assertThat(testSymptome.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testSymptome.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void fullUpdateSymptomeWithPatch() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();

        // Update the symptome using partial update
        Symptome partialUpdatedSymptome = new Symptome();
        partialUpdatedSymptome.setId(symptome.getId());

        partialUpdatedSymptome.designation(UPDATED_DESIGNATION).code(UPDATED_CODE);

        restSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSymptome.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSymptome))
            )
            .andExpect(status().isOk());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
        Symptome testSymptome = symptomeList.get(symptomeList.size() - 1);
        assertThat(testSymptome.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testSymptome.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingSymptome() throws Exception {
        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();
        symptome.setId(count.incrementAndGet());

        // Create the Symptome
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, symptomeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(symptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSymptome() throws Exception {
        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();
        symptome.setId(count.incrementAndGet());

        // Create the Symptome
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(symptomeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSymptome() throws Exception {
        int databaseSizeBeforeUpdate = symptomeRepository.findAll().size();
        symptome.setId(count.incrementAndGet());

        // Create the Symptome
        SymptomeDTO symptomeDTO = symptomeMapper.toDto(symptome);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSymptomeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(symptomeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Symptome in the database
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSymptome() throws Exception {
        // Initialize the database
        symptomeRepository.saveAndFlush(symptome);

        int databaseSizeBeforeDelete = symptomeRepository.findAll().size();

        // Delete the symptome
        restSymptomeMockMvc
            .perform(delete(ENTITY_API_URL_ID, symptome.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Symptome> symptomeList = symptomeRepository.findAll();
        assertThat(symptomeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
