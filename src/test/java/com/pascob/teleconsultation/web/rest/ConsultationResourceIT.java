package com.pascob.teleconsultation.web.rest;

import static com.pascob.teleconsultation.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Consultation;
import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.repository.ConsultationRepository;
import com.pascob.teleconsultation.service.criteria.ConsultationCriteria;
import com.pascob.teleconsultation.service.dto.ConsultationDTO;
import com.pascob.teleconsultation.service.mapper.ConsultationMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ConsultationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ConsultationResourceIT {

    private static final Integer DEFAULT_POIDS = 0;
    private static final Integer UPDATED_POIDS = 1;
    private static final Integer SMALLER_POIDS = 0 - 1;

    private static final Float DEFAULT_TEMPERATURE = 0F;
    private static final Float UPDATED_TEMPERATURE = 1F;
    private static final Float SMALLER_TEMPERATURE = 0F - 1F;

    private static final Integer DEFAULT_POULS = 0;
    private static final Integer UPDATED_POULS = 1;
    private static final Integer SMALLER_POULS = 0 - 1;

    private static final Integer DEFAULT_BPM = 0;
    private static final Integer UPDATED_BPM = 1;
    private static final Integer SMALLER_BPM = 0 - 1;

    private static final Float DEFAULT_AGE = 0F;
    private static final Float UPDATED_AGE = 1F;
    private static final Float SMALLER_AGE = 0F - 1F;

    private static final Integer DEFAULT_TAILLE = 0;
    private static final Integer UPDATED_TAILLE = 1;
    private static final Integer SMALLER_TAILLE = 0 - 1;

    private static final ZonedDateTime DEFAULT_DATE_DEBUT_CONSULATION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_DEBUT_CONSULATION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_DEBUT_CONSULATION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Boolean DEFAULT_IS_SUIVI = false;
    private static final Boolean UPDATED_IS_SUIVI = true;

    private static final Boolean DEFAULT_IS_CLOSE = false;
    private static final Boolean UPDATED_IS_CLOSE = true;

    private static final String ENTITY_API_URL = "/api/consultations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ConsultationRepository consultationRepository;

    @Autowired
    private ConsultationMapper consultationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restConsultationMockMvc;

    private Consultation consultation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Consultation createEntity(EntityManager em) {
        Consultation consultation = new Consultation()
            .poids(DEFAULT_POIDS)
            .temperature(DEFAULT_TEMPERATURE)
            .pouls(DEFAULT_POULS)
            .bpm(DEFAULT_BPM)
            .age(DEFAULT_AGE)
            .taille(DEFAULT_TAILLE)
            .dateDebutConsulation(DEFAULT_DATE_DEBUT_CONSULATION)
            .isSuivi(DEFAULT_IS_SUIVI)
            .isClose(DEFAULT_IS_CLOSE);
        return consultation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Consultation createUpdatedEntity(EntityManager em) {
        Consultation consultation = new Consultation()
            .poids(UPDATED_POIDS)
            .temperature(UPDATED_TEMPERATURE)
            .pouls(UPDATED_POULS)
            .bpm(UPDATED_BPM)
            .age(UPDATED_AGE)
            .taille(UPDATED_TAILLE)
            .dateDebutConsulation(UPDATED_DATE_DEBUT_CONSULATION)
            .isSuivi(UPDATED_IS_SUIVI)
            .isClose(UPDATED_IS_CLOSE);
        return consultation;
    }

    @BeforeEach
    public void initTest() {
        consultation = createEntity(em);
    }

    @Test
    @Transactional
    void createConsultation() throws Exception {
        int databaseSizeBeforeCreate = consultationRepository.findAll().size();
        // Create the Consultation
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);
        restConsultationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeCreate + 1);
        Consultation testConsultation = consultationList.get(consultationList.size() - 1);
        assertThat(testConsultation.getPoids()).isEqualTo(DEFAULT_POIDS);
        assertThat(testConsultation.getTemperature()).isEqualTo(DEFAULT_TEMPERATURE);
        assertThat(testConsultation.getPouls()).isEqualTo(DEFAULT_POULS);
        assertThat(testConsultation.getBpm()).isEqualTo(DEFAULT_BPM);
        assertThat(testConsultation.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testConsultation.getTaille()).isEqualTo(DEFAULT_TAILLE);
        assertThat(testConsultation.getDateDebutConsulation()).isEqualTo(DEFAULT_DATE_DEBUT_CONSULATION);
        assertThat(testConsultation.getIsSuivi()).isEqualTo(DEFAULT_IS_SUIVI);
        assertThat(testConsultation.getIsClose()).isEqualTo(DEFAULT_IS_CLOSE);
    }

    @Test
    @Transactional
    void createConsultationWithExistingId() throws Exception {
        // Create the Consultation with an existing ID
        consultation.setId(1L);
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);

        int databaseSizeBeforeCreate = consultationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restConsultationMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllConsultations() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList
        restConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(consultation.getId().intValue())))
            .andExpect(jsonPath("$.[*].poids").value(hasItem(DEFAULT_POIDS)))
            .andExpect(jsonPath("$.[*].temperature").value(hasItem(DEFAULT_TEMPERATURE.doubleValue())))
            .andExpect(jsonPath("$.[*].pouls").value(hasItem(DEFAULT_POULS)))
            .andExpect(jsonPath("$.[*].bpm").value(hasItem(DEFAULT_BPM)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.doubleValue())))
            .andExpect(jsonPath("$.[*].taille").value(hasItem(DEFAULT_TAILLE)))
            .andExpect(jsonPath("$.[*].dateDebutConsulation").value(hasItem(sameInstant(DEFAULT_DATE_DEBUT_CONSULATION))))
            .andExpect(jsonPath("$.[*].isSuivi").value(hasItem(DEFAULT_IS_SUIVI.booleanValue())))
            .andExpect(jsonPath("$.[*].isClose").value(hasItem(DEFAULT_IS_CLOSE.booleanValue())));
    }

    @Test
    @Transactional
    void getConsultation() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get the consultation
        restConsultationMockMvc
            .perform(get(ENTITY_API_URL_ID, consultation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(consultation.getId().intValue()))
            .andExpect(jsonPath("$.poids").value(DEFAULT_POIDS))
            .andExpect(jsonPath("$.temperature").value(DEFAULT_TEMPERATURE.doubleValue()))
            .andExpect(jsonPath("$.pouls").value(DEFAULT_POULS))
            .andExpect(jsonPath("$.bpm").value(DEFAULT_BPM))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE.doubleValue()))
            .andExpect(jsonPath("$.taille").value(DEFAULT_TAILLE))
            .andExpect(jsonPath("$.dateDebutConsulation").value(sameInstant(DEFAULT_DATE_DEBUT_CONSULATION)))
            .andExpect(jsonPath("$.isSuivi").value(DEFAULT_IS_SUIVI.booleanValue()))
            .andExpect(jsonPath("$.isClose").value(DEFAULT_IS_CLOSE.booleanValue()));
    }

    @Test
    @Transactional
    void getConsultationsByIdFiltering() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        Long id = consultation.getId();

        defaultConsultationShouldBeFound("id.equals=" + id);
        defaultConsultationShouldNotBeFound("id.notEquals=" + id);

        defaultConsultationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultConsultationShouldNotBeFound("id.greaterThan=" + id);

        defaultConsultationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultConsultationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoidsIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where poids equals to DEFAULT_POIDS
        defaultConsultationShouldBeFound("poids.equals=" + DEFAULT_POIDS);

        // Get all the consultationList where poids equals to UPDATED_POIDS
        defaultConsultationShouldNotBeFound("poids.equals=" + UPDATED_POIDS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoidsIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where poids in DEFAULT_POIDS or UPDATED_POIDS
        defaultConsultationShouldBeFound("poids.in=" + DEFAULT_POIDS + "," + UPDATED_POIDS);

        // Get all the consultationList where poids equals to UPDATED_POIDS
        defaultConsultationShouldNotBeFound("poids.in=" + UPDATED_POIDS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoidsIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where poids is not null
        defaultConsultationShouldBeFound("poids.specified=true");

        // Get all the consultationList where poids is null
        defaultConsultationShouldNotBeFound("poids.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByPoidsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where poids is greater than or equal to DEFAULT_POIDS
        defaultConsultationShouldBeFound("poids.greaterThanOrEqual=" + DEFAULT_POIDS);

        // Get all the consultationList where poids is greater than or equal to UPDATED_POIDS
        defaultConsultationShouldNotBeFound("poids.greaterThanOrEqual=" + UPDATED_POIDS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoidsIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where poids is less than or equal to DEFAULT_POIDS
        defaultConsultationShouldBeFound("poids.lessThanOrEqual=" + DEFAULT_POIDS);

        // Get all the consultationList where poids is less than or equal to SMALLER_POIDS
        defaultConsultationShouldNotBeFound("poids.lessThanOrEqual=" + SMALLER_POIDS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoidsIsLessThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where poids is less than DEFAULT_POIDS
        defaultConsultationShouldNotBeFound("poids.lessThan=" + DEFAULT_POIDS);

        // Get all the consultationList where poids is less than UPDATED_POIDS
        defaultConsultationShouldBeFound("poids.lessThan=" + UPDATED_POIDS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoidsIsGreaterThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where poids is greater than DEFAULT_POIDS
        defaultConsultationShouldNotBeFound("poids.greaterThan=" + DEFAULT_POIDS);

        // Get all the consultationList where poids is greater than SMALLER_POIDS
        defaultConsultationShouldBeFound("poids.greaterThan=" + SMALLER_POIDS);
    }

    @Test
    @Transactional
    void getAllConsultationsByTemperatureIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where temperature equals to DEFAULT_TEMPERATURE
        defaultConsultationShouldBeFound("temperature.equals=" + DEFAULT_TEMPERATURE);

        // Get all the consultationList where temperature equals to UPDATED_TEMPERATURE
        defaultConsultationShouldNotBeFound("temperature.equals=" + UPDATED_TEMPERATURE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTemperatureIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where temperature in DEFAULT_TEMPERATURE or UPDATED_TEMPERATURE
        defaultConsultationShouldBeFound("temperature.in=" + DEFAULT_TEMPERATURE + "," + UPDATED_TEMPERATURE);

        // Get all the consultationList where temperature equals to UPDATED_TEMPERATURE
        defaultConsultationShouldNotBeFound("temperature.in=" + UPDATED_TEMPERATURE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTemperatureIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where temperature is not null
        defaultConsultationShouldBeFound("temperature.specified=true");

        // Get all the consultationList where temperature is null
        defaultConsultationShouldNotBeFound("temperature.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByTemperatureIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where temperature is greater than or equal to DEFAULT_TEMPERATURE
        defaultConsultationShouldBeFound("temperature.greaterThanOrEqual=" + DEFAULT_TEMPERATURE);

        // Get all the consultationList where temperature is greater than or equal to UPDATED_TEMPERATURE
        defaultConsultationShouldNotBeFound("temperature.greaterThanOrEqual=" + UPDATED_TEMPERATURE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTemperatureIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where temperature is less than or equal to DEFAULT_TEMPERATURE
        defaultConsultationShouldBeFound("temperature.lessThanOrEqual=" + DEFAULT_TEMPERATURE);

        // Get all the consultationList where temperature is less than or equal to SMALLER_TEMPERATURE
        defaultConsultationShouldNotBeFound("temperature.lessThanOrEqual=" + SMALLER_TEMPERATURE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTemperatureIsLessThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where temperature is less than DEFAULT_TEMPERATURE
        defaultConsultationShouldNotBeFound("temperature.lessThan=" + DEFAULT_TEMPERATURE);

        // Get all the consultationList where temperature is less than UPDATED_TEMPERATURE
        defaultConsultationShouldBeFound("temperature.lessThan=" + UPDATED_TEMPERATURE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTemperatureIsGreaterThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where temperature is greater than DEFAULT_TEMPERATURE
        defaultConsultationShouldNotBeFound("temperature.greaterThan=" + DEFAULT_TEMPERATURE);

        // Get all the consultationList where temperature is greater than SMALLER_TEMPERATURE
        defaultConsultationShouldBeFound("temperature.greaterThan=" + SMALLER_TEMPERATURE);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoulsIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where pouls equals to DEFAULT_POULS
        defaultConsultationShouldBeFound("pouls.equals=" + DEFAULT_POULS);

        // Get all the consultationList where pouls equals to UPDATED_POULS
        defaultConsultationShouldNotBeFound("pouls.equals=" + UPDATED_POULS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoulsIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where pouls in DEFAULT_POULS or UPDATED_POULS
        defaultConsultationShouldBeFound("pouls.in=" + DEFAULT_POULS + "," + UPDATED_POULS);

        // Get all the consultationList where pouls equals to UPDATED_POULS
        defaultConsultationShouldNotBeFound("pouls.in=" + UPDATED_POULS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoulsIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where pouls is not null
        defaultConsultationShouldBeFound("pouls.specified=true");

        // Get all the consultationList where pouls is null
        defaultConsultationShouldNotBeFound("pouls.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByPoulsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where pouls is greater than or equal to DEFAULT_POULS
        defaultConsultationShouldBeFound("pouls.greaterThanOrEqual=" + DEFAULT_POULS);

        // Get all the consultationList where pouls is greater than or equal to UPDATED_POULS
        defaultConsultationShouldNotBeFound("pouls.greaterThanOrEqual=" + UPDATED_POULS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoulsIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where pouls is less than or equal to DEFAULT_POULS
        defaultConsultationShouldBeFound("pouls.lessThanOrEqual=" + DEFAULT_POULS);

        // Get all the consultationList where pouls is less than or equal to SMALLER_POULS
        defaultConsultationShouldNotBeFound("pouls.lessThanOrEqual=" + SMALLER_POULS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoulsIsLessThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where pouls is less than DEFAULT_POULS
        defaultConsultationShouldNotBeFound("pouls.lessThan=" + DEFAULT_POULS);

        // Get all the consultationList where pouls is less than UPDATED_POULS
        defaultConsultationShouldBeFound("pouls.lessThan=" + UPDATED_POULS);
    }

    @Test
    @Transactional
    void getAllConsultationsByPoulsIsGreaterThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where pouls is greater than DEFAULT_POULS
        defaultConsultationShouldNotBeFound("pouls.greaterThan=" + DEFAULT_POULS);

        // Get all the consultationList where pouls is greater than SMALLER_POULS
        defaultConsultationShouldBeFound("pouls.greaterThan=" + SMALLER_POULS);
    }

    @Test
    @Transactional
    void getAllConsultationsByBpmIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where bpm equals to DEFAULT_BPM
        defaultConsultationShouldBeFound("bpm.equals=" + DEFAULT_BPM);

        // Get all the consultationList where bpm equals to UPDATED_BPM
        defaultConsultationShouldNotBeFound("bpm.equals=" + UPDATED_BPM);
    }

    @Test
    @Transactional
    void getAllConsultationsByBpmIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where bpm in DEFAULT_BPM or UPDATED_BPM
        defaultConsultationShouldBeFound("bpm.in=" + DEFAULT_BPM + "," + UPDATED_BPM);

        // Get all the consultationList where bpm equals to UPDATED_BPM
        defaultConsultationShouldNotBeFound("bpm.in=" + UPDATED_BPM);
    }

    @Test
    @Transactional
    void getAllConsultationsByBpmIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where bpm is not null
        defaultConsultationShouldBeFound("bpm.specified=true");

        // Get all the consultationList where bpm is null
        defaultConsultationShouldNotBeFound("bpm.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByBpmIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where bpm is greater than or equal to DEFAULT_BPM
        defaultConsultationShouldBeFound("bpm.greaterThanOrEqual=" + DEFAULT_BPM);

        // Get all the consultationList where bpm is greater than or equal to UPDATED_BPM
        defaultConsultationShouldNotBeFound("bpm.greaterThanOrEqual=" + UPDATED_BPM);
    }

    @Test
    @Transactional
    void getAllConsultationsByBpmIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where bpm is less than or equal to DEFAULT_BPM
        defaultConsultationShouldBeFound("bpm.lessThanOrEqual=" + DEFAULT_BPM);

        // Get all the consultationList where bpm is less than or equal to SMALLER_BPM
        defaultConsultationShouldNotBeFound("bpm.lessThanOrEqual=" + SMALLER_BPM);
    }

    @Test
    @Transactional
    void getAllConsultationsByBpmIsLessThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where bpm is less than DEFAULT_BPM
        defaultConsultationShouldNotBeFound("bpm.lessThan=" + DEFAULT_BPM);

        // Get all the consultationList where bpm is less than UPDATED_BPM
        defaultConsultationShouldBeFound("bpm.lessThan=" + UPDATED_BPM);
    }

    @Test
    @Transactional
    void getAllConsultationsByBpmIsGreaterThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where bpm is greater than DEFAULT_BPM
        defaultConsultationShouldNotBeFound("bpm.greaterThan=" + DEFAULT_BPM);

        // Get all the consultationList where bpm is greater than SMALLER_BPM
        defaultConsultationShouldBeFound("bpm.greaterThan=" + SMALLER_BPM);
    }

    @Test
    @Transactional
    void getAllConsultationsByAgeIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where age equals to DEFAULT_AGE
        defaultConsultationShouldBeFound("age.equals=" + DEFAULT_AGE);

        // Get all the consultationList where age equals to UPDATED_AGE
        defaultConsultationShouldNotBeFound("age.equals=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    void getAllConsultationsByAgeIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where age in DEFAULT_AGE or UPDATED_AGE
        defaultConsultationShouldBeFound("age.in=" + DEFAULT_AGE + "," + UPDATED_AGE);

        // Get all the consultationList where age equals to UPDATED_AGE
        defaultConsultationShouldNotBeFound("age.in=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    void getAllConsultationsByAgeIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where age is not null
        defaultConsultationShouldBeFound("age.specified=true");

        // Get all the consultationList where age is null
        defaultConsultationShouldNotBeFound("age.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByAgeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where age is greater than or equal to DEFAULT_AGE
        defaultConsultationShouldBeFound("age.greaterThanOrEqual=" + DEFAULT_AGE);

        // Get all the consultationList where age is greater than or equal to UPDATED_AGE
        defaultConsultationShouldNotBeFound("age.greaterThanOrEqual=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    void getAllConsultationsByAgeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where age is less than or equal to DEFAULT_AGE
        defaultConsultationShouldBeFound("age.lessThanOrEqual=" + DEFAULT_AGE);

        // Get all the consultationList where age is less than or equal to SMALLER_AGE
        defaultConsultationShouldNotBeFound("age.lessThanOrEqual=" + SMALLER_AGE);
    }

    @Test
    @Transactional
    void getAllConsultationsByAgeIsLessThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where age is less than DEFAULT_AGE
        defaultConsultationShouldNotBeFound("age.lessThan=" + DEFAULT_AGE);

        // Get all the consultationList where age is less than UPDATED_AGE
        defaultConsultationShouldBeFound("age.lessThan=" + UPDATED_AGE);
    }

    @Test
    @Transactional
    void getAllConsultationsByAgeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where age is greater than DEFAULT_AGE
        defaultConsultationShouldNotBeFound("age.greaterThan=" + DEFAULT_AGE);

        // Get all the consultationList where age is greater than SMALLER_AGE
        defaultConsultationShouldBeFound("age.greaterThan=" + SMALLER_AGE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTailleIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where taille equals to DEFAULT_TAILLE
        defaultConsultationShouldBeFound("taille.equals=" + DEFAULT_TAILLE);

        // Get all the consultationList where taille equals to UPDATED_TAILLE
        defaultConsultationShouldNotBeFound("taille.equals=" + UPDATED_TAILLE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTailleIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where taille in DEFAULT_TAILLE or UPDATED_TAILLE
        defaultConsultationShouldBeFound("taille.in=" + DEFAULT_TAILLE + "," + UPDATED_TAILLE);

        // Get all the consultationList where taille equals to UPDATED_TAILLE
        defaultConsultationShouldNotBeFound("taille.in=" + UPDATED_TAILLE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTailleIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where taille is not null
        defaultConsultationShouldBeFound("taille.specified=true");

        // Get all the consultationList where taille is null
        defaultConsultationShouldNotBeFound("taille.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByTailleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where taille is greater than or equal to DEFAULT_TAILLE
        defaultConsultationShouldBeFound("taille.greaterThanOrEqual=" + DEFAULT_TAILLE);

        // Get all the consultationList where taille is greater than or equal to UPDATED_TAILLE
        defaultConsultationShouldNotBeFound("taille.greaterThanOrEqual=" + UPDATED_TAILLE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTailleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where taille is less than or equal to DEFAULT_TAILLE
        defaultConsultationShouldBeFound("taille.lessThanOrEqual=" + DEFAULT_TAILLE);

        // Get all the consultationList where taille is less than or equal to SMALLER_TAILLE
        defaultConsultationShouldNotBeFound("taille.lessThanOrEqual=" + SMALLER_TAILLE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTailleIsLessThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where taille is less than DEFAULT_TAILLE
        defaultConsultationShouldNotBeFound("taille.lessThan=" + DEFAULT_TAILLE);

        // Get all the consultationList where taille is less than UPDATED_TAILLE
        defaultConsultationShouldBeFound("taille.lessThan=" + UPDATED_TAILLE);
    }

    @Test
    @Transactional
    void getAllConsultationsByTailleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where taille is greater than DEFAULT_TAILLE
        defaultConsultationShouldNotBeFound("taille.greaterThan=" + DEFAULT_TAILLE);

        // Get all the consultationList where taille is greater than SMALLER_TAILLE
        defaultConsultationShouldBeFound("taille.greaterThan=" + SMALLER_TAILLE);
    }

    @Test
    @Transactional
    void getAllConsultationsByDateDebutConsulationIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where dateDebutConsulation equals to DEFAULT_DATE_DEBUT_CONSULATION
        defaultConsultationShouldBeFound("dateDebutConsulation.equals=" + DEFAULT_DATE_DEBUT_CONSULATION);

        // Get all the consultationList where dateDebutConsulation equals to UPDATED_DATE_DEBUT_CONSULATION
        defaultConsultationShouldNotBeFound("dateDebutConsulation.equals=" + UPDATED_DATE_DEBUT_CONSULATION);
    }

    @Test
    @Transactional
    void getAllConsultationsByDateDebutConsulationIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where dateDebutConsulation in DEFAULT_DATE_DEBUT_CONSULATION or UPDATED_DATE_DEBUT_CONSULATION
        defaultConsultationShouldBeFound(
            "dateDebutConsulation.in=" + DEFAULT_DATE_DEBUT_CONSULATION + "," + UPDATED_DATE_DEBUT_CONSULATION
        );

        // Get all the consultationList where dateDebutConsulation equals to UPDATED_DATE_DEBUT_CONSULATION
        defaultConsultationShouldNotBeFound("dateDebutConsulation.in=" + UPDATED_DATE_DEBUT_CONSULATION);
    }

    @Test
    @Transactional
    void getAllConsultationsByDateDebutConsulationIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where dateDebutConsulation is not null
        defaultConsultationShouldBeFound("dateDebutConsulation.specified=true");

        // Get all the consultationList where dateDebutConsulation is null
        defaultConsultationShouldNotBeFound("dateDebutConsulation.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByDateDebutConsulationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where dateDebutConsulation is greater than or equal to DEFAULT_DATE_DEBUT_CONSULATION
        defaultConsultationShouldBeFound("dateDebutConsulation.greaterThanOrEqual=" + DEFAULT_DATE_DEBUT_CONSULATION);

        // Get all the consultationList where dateDebutConsulation is greater than or equal to UPDATED_DATE_DEBUT_CONSULATION
        defaultConsultationShouldNotBeFound("dateDebutConsulation.greaterThanOrEqual=" + UPDATED_DATE_DEBUT_CONSULATION);
    }

    @Test
    @Transactional
    void getAllConsultationsByDateDebutConsulationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where dateDebutConsulation is less than or equal to DEFAULT_DATE_DEBUT_CONSULATION
        defaultConsultationShouldBeFound("dateDebutConsulation.lessThanOrEqual=" + DEFAULT_DATE_DEBUT_CONSULATION);

        // Get all the consultationList where dateDebutConsulation is less than or equal to SMALLER_DATE_DEBUT_CONSULATION
        defaultConsultationShouldNotBeFound("dateDebutConsulation.lessThanOrEqual=" + SMALLER_DATE_DEBUT_CONSULATION);
    }

    @Test
    @Transactional
    void getAllConsultationsByDateDebutConsulationIsLessThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where dateDebutConsulation is less than DEFAULT_DATE_DEBUT_CONSULATION
        defaultConsultationShouldNotBeFound("dateDebutConsulation.lessThan=" + DEFAULT_DATE_DEBUT_CONSULATION);

        // Get all the consultationList where dateDebutConsulation is less than UPDATED_DATE_DEBUT_CONSULATION
        defaultConsultationShouldBeFound("dateDebutConsulation.lessThan=" + UPDATED_DATE_DEBUT_CONSULATION);
    }

    @Test
    @Transactional
    void getAllConsultationsByDateDebutConsulationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where dateDebutConsulation is greater than DEFAULT_DATE_DEBUT_CONSULATION
        defaultConsultationShouldNotBeFound("dateDebutConsulation.greaterThan=" + DEFAULT_DATE_DEBUT_CONSULATION);

        // Get all the consultationList where dateDebutConsulation is greater than SMALLER_DATE_DEBUT_CONSULATION
        defaultConsultationShouldBeFound("dateDebutConsulation.greaterThan=" + SMALLER_DATE_DEBUT_CONSULATION);
    }

    @Test
    @Transactional
    void getAllConsultationsByIsSuiviIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where isSuivi equals to DEFAULT_IS_SUIVI
        defaultConsultationShouldBeFound("isSuivi.equals=" + DEFAULT_IS_SUIVI);

        // Get all the consultationList where isSuivi equals to UPDATED_IS_SUIVI
        defaultConsultationShouldNotBeFound("isSuivi.equals=" + UPDATED_IS_SUIVI);
    }

    @Test
    @Transactional
    void getAllConsultationsByIsSuiviIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where isSuivi in DEFAULT_IS_SUIVI or UPDATED_IS_SUIVI
        defaultConsultationShouldBeFound("isSuivi.in=" + DEFAULT_IS_SUIVI + "," + UPDATED_IS_SUIVI);

        // Get all the consultationList where isSuivi equals to UPDATED_IS_SUIVI
        defaultConsultationShouldNotBeFound("isSuivi.in=" + UPDATED_IS_SUIVI);
    }

    @Test
    @Transactional
    void getAllConsultationsByIsSuiviIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where isSuivi is not null
        defaultConsultationShouldBeFound("isSuivi.specified=true");

        // Get all the consultationList where isSuivi is null
        defaultConsultationShouldNotBeFound("isSuivi.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByIsCloseIsEqualToSomething() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where isClose equals to DEFAULT_IS_CLOSE
        defaultConsultationShouldBeFound("isClose.equals=" + DEFAULT_IS_CLOSE);

        // Get all the consultationList where isClose equals to UPDATED_IS_CLOSE
        defaultConsultationShouldNotBeFound("isClose.equals=" + UPDATED_IS_CLOSE);
    }

    @Test
    @Transactional
    void getAllConsultationsByIsCloseIsInShouldWork() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where isClose in DEFAULT_IS_CLOSE or UPDATED_IS_CLOSE
        defaultConsultationShouldBeFound("isClose.in=" + DEFAULT_IS_CLOSE + "," + UPDATED_IS_CLOSE);

        // Get all the consultationList where isClose equals to UPDATED_IS_CLOSE
        defaultConsultationShouldNotBeFound("isClose.in=" + UPDATED_IS_CLOSE);
    }

    @Test
    @Transactional
    void getAllConsultationsByIsCloseIsNullOrNotNull() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        // Get all the consultationList where isClose is not null
        defaultConsultationShouldBeFound("isClose.specified=true");

        // Get all the consultationList where isClose is null
        defaultConsultationShouldNotBeFound("isClose.specified=false");
    }

    @Test
    @Transactional
    void getAllConsultationsByFicheConsultationIsEqualToSomething() throws Exception {
        FicheConsultation ficheConsultation;
        if (TestUtil.findAll(em, FicheConsultation.class).isEmpty()) {
            consultationRepository.saveAndFlush(consultation);
            ficheConsultation = FicheConsultationResourceIT.createEntity(em);
        } else {
            ficheConsultation = TestUtil.findAll(em, FicheConsultation.class).get(0);
        }
        em.persist(ficheConsultation);
        em.flush();
        consultation.addFicheConsultation(ficheConsultation);
        consultationRepository.saveAndFlush(consultation);
        Long ficheConsultationId = ficheConsultation.getId();

        // Get all the consultationList where ficheConsultation equals to ficheConsultationId
        defaultConsultationShouldBeFound("ficheConsultationId.equals=" + ficheConsultationId);

        // Get all the consultationList where ficheConsultation equals to (ficheConsultationId + 1)
        defaultConsultationShouldNotBeFound("ficheConsultationId.equals=" + (ficheConsultationId + 1));
    }

    @Test
    @Transactional
    void getAllConsultationsByMedecinIsEqualToSomething() throws Exception {
        Medecin medecin;
        if (TestUtil.findAll(em, Medecin.class).isEmpty()) {
            consultationRepository.saveAndFlush(consultation);
            medecin = MedecinResourceIT.createEntity(em);
        } else {
            medecin = TestUtil.findAll(em, Medecin.class).get(0);
        }
        em.persist(medecin);
        em.flush();
        consultation.setMedecin(medecin);
        consultationRepository.saveAndFlush(consultation);
        Long medecinId = medecin.getId();

        // Get all the consultationList where medecin equals to medecinId
        defaultConsultationShouldBeFound("medecinId.equals=" + medecinId);

        // Get all the consultationList where medecin equals to (medecinId + 1)
        defaultConsultationShouldNotBeFound("medecinId.equals=" + (medecinId + 1));
    }

    @Test
    @Transactional
    void getAllConsultationsByDemandeConsultationIsEqualToSomething() throws Exception {
        DemandeConsultation demandeConsultation;
        if (TestUtil.findAll(em, DemandeConsultation.class).isEmpty()) {
            consultationRepository.saveAndFlush(consultation);
            demandeConsultation = DemandeConsultationResourceIT.createEntity(em);
        } else {
            demandeConsultation = TestUtil.findAll(em, DemandeConsultation.class).get(0);
        }
        em.persist(demandeConsultation);
        em.flush();
        consultation.setDemandeConsultation(demandeConsultation);
        consultationRepository.saveAndFlush(consultation);
        Long demandeConsultationId = demandeConsultation.getId();

        // Get all the consultationList where demandeConsultation equals to demandeConsultationId
        defaultConsultationShouldBeFound("demandeConsultationId.equals=" + demandeConsultationId);

        // Get all the consultationList where demandeConsultation equals to (demandeConsultationId + 1)
        defaultConsultationShouldNotBeFound("demandeConsultationId.equals=" + (demandeConsultationId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultConsultationShouldBeFound(String filter) throws Exception {
        restConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(consultation.getId().intValue())))
            .andExpect(jsonPath("$.[*].poids").value(hasItem(DEFAULT_POIDS)))
            .andExpect(jsonPath("$.[*].temperature").value(hasItem(DEFAULT_TEMPERATURE.doubleValue())))
            .andExpect(jsonPath("$.[*].pouls").value(hasItem(DEFAULT_POULS)))
            .andExpect(jsonPath("$.[*].bpm").value(hasItem(DEFAULT_BPM)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.doubleValue())))
            .andExpect(jsonPath("$.[*].taille").value(hasItem(DEFAULT_TAILLE)))
            .andExpect(jsonPath("$.[*].dateDebutConsulation").value(hasItem(sameInstant(DEFAULT_DATE_DEBUT_CONSULATION))))
            .andExpect(jsonPath("$.[*].isSuivi").value(hasItem(DEFAULT_IS_SUIVI.booleanValue())))
            .andExpect(jsonPath("$.[*].isClose").value(hasItem(DEFAULT_IS_CLOSE.booleanValue())));

        // Check, that the count call also returns 1
        restConsultationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultConsultationShouldNotBeFound(String filter) throws Exception {
        restConsultationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restConsultationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingConsultation() throws Exception {
        // Get the consultation
        restConsultationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingConsultation() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();

        // Update the consultation
        Consultation updatedConsultation = consultationRepository.findById(consultation.getId()).get();
        // Disconnect from session so that the updates on updatedConsultation are not directly saved in db
        em.detach(updatedConsultation);
        updatedConsultation
            .poids(UPDATED_POIDS)
            .temperature(UPDATED_TEMPERATURE)
            .pouls(UPDATED_POULS)
            .bpm(UPDATED_BPM)
            .age(UPDATED_AGE)
            .taille(UPDATED_TAILLE)
            .dateDebutConsulation(UPDATED_DATE_DEBUT_CONSULATION)
            .isSuivi(UPDATED_IS_SUIVI)
            .isClose(UPDATED_IS_CLOSE);
        ConsultationDTO consultationDTO = consultationMapper.toDto(updatedConsultation);

        restConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, consultationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isOk());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
        Consultation testConsultation = consultationList.get(consultationList.size() - 1);
        assertThat(testConsultation.getPoids()).isEqualTo(UPDATED_POIDS);
        assertThat(testConsultation.getTemperature()).isEqualTo(UPDATED_TEMPERATURE);
        assertThat(testConsultation.getPouls()).isEqualTo(UPDATED_POULS);
        assertThat(testConsultation.getBpm()).isEqualTo(UPDATED_BPM);
        assertThat(testConsultation.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testConsultation.getTaille()).isEqualTo(UPDATED_TAILLE);
        assertThat(testConsultation.getDateDebutConsulation()).isEqualTo(UPDATED_DATE_DEBUT_CONSULATION);
        assertThat(testConsultation.getIsSuivi()).isEqualTo(UPDATED_IS_SUIVI);
        assertThat(testConsultation.getIsClose()).isEqualTo(UPDATED_IS_CLOSE);
    }

    @Test
    @Transactional
    void putNonExistingConsultation() throws Exception {
        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();
        consultation.setId(count.incrementAndGet());

        // Create the Consultation
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, consultationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchConsultation() throws Exception {
        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();
        consultation.setId(count.incrementAndGet());

        // Create the Consultation
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConsultationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamConsultation() throws Exception {
        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();
        consultation.setId(count.incrementAndGet());

        // Create the Consultation
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConsultationMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateConsultationWithPatch() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();

        // Update the consultation using partial update
        Consultation partialUpdatedConsultation = new Consultation();
        partialUpdatedConsultation.setId(consultation.getId());

        partialUpdatedConsultation
            .temperature(UPDATED_TEMPERATURE)
            .bpm(UPDATED_BPM)
            .age(UPDATED_AGE)
            .isSuivi(UPDATED_IS_SUIVI)
            .isClose(UPDATED_IS_CLOSE);

        restConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConsultation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConsultation))
            )
            .andExpect(status().isOk());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
        Consultation testConsultation = consultationList.get(consultationList.size() - 1);
        assertThat(testConsultation.getPoids()).isEqualTo(DEFAULT_POIDS);
        assertThat(testConsultation.getTemperature()).isEqualTo(UPDATED_TEMPERATURE);
        assertThat(testConsultation.getPouls()).isEqualTo(DEFAULT_POULS);
        assertThat(testConsultation.getBpm()).isEqualTo(UPDATED_BPM);
        assertThat(testConsultation.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testConsultation.getTaille()).isEqualTo(DEFAULT_TAILLE);
        assertThat(testConsultation.getDateDebutConsulation()).isEqualTo(DEFAULT_DATE_DEBUT_CONSULATION);
        assertThat(testConsultation.getIsSuivi()).isEqualTo(UPDATED_IS_SUIVI);
        assertThat(testConsultation.getIsClose()).isEqualTo(UPDATED_IS_CLOSE);
    }

    @Test
    @Transactional
    void fullUpdateConsultationWithPatch() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();

        // Update the consultation using partial update
        Consultation partialUpdatedConsultation = new Consultation();
        partialUpdatedConsultation.setId(consultation.getId());

        partialUpdatedConsultation
            .poids(UPDATED_POIDS)
            .temperature(UPDATED_TEMPERATURE)
            .pouls(UPDATED_POULS)
            .bpm(UPDATED_BPM)
            .age(UPDATED_AGE)
            .taille(UPDATED_TAILLE)
            .dateDebutConsulation(UPDATED_DATE_DEBUT_CONSULATION)
            .isSuivi(UPDATED_IS_SUIVI)
            .isClose(UPDATED_IS_CLOSE);

        restConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConsultation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConsultation))
            )
            .andExpect(status().isOk());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
        Consultation testConsultation = consultationList.get(consultationList.size() - 1);
        assertThat(testConsultation.getPoids()).isEqualTo(UPDATED_POIDS);
        assertThat(testConsultation.getTemperature()).isEqualTo(UPDATED_TEMPERATURE);
        assertThat(testConsultation.getPouls()).isEqualTo(UPDATED_POULS);
        assertThat(testConsultation.getBpm()).isEqualTo(UPDATED_BPM);
        assertThat(testConsultation.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testConsultation.getTaille()).isEqualTo(UPDATED_TAILLE);
        assertThat(testConsultation.getDateDebutConsulation()).isEqualTo(UPDATED_DATE_DEBUT_CONSULATION);
        assertThat(testConsultation.getIsSuivi()).isEqualTo(UPDATED_IS_SUIVI);
        assertThat(testConsultation.getIsClose()).isEqualTo(UPDATED_IS_CLOSE);
    }

    @Test
    @Transactional
    void patchNonExistingConsultation() throws Exception {
        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();
        consultation.setId(count.incrementAndGet());

        // Create the Consultation
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, consultationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchConsultation() throws Exception {
        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();
        consultation.setId(count.incrementAndGet());

        // Create the Consultation
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamConsultation() throws Exception {
        int databaseSizeBeforeUpdate = consultationRepository.findAll().size();
        consultation.setId(count.incrementAndGet());

        // Create the Consultation
        ConsultationDTO consultationDTO = consultationMapper.toDto(consultation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConsultationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(consultationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Consultation in the database
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteConsultation() throws Exception {
        // Initialize the database
        consultationRepository.saveAndFlush(consultation);

        int databaseSizeBeforeDelete = consultationRepository.findAll().size();

        // Delete the consultation
        restConsultationMockMvc
            .perform(delete(ENTITY_API_URL_ID, consultation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Consultation> consultationList = consultationRepository.findAll();
        assertThat(consultationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
