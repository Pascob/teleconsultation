package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.domain.Patient;
import com.pascob.teleconsultation.domain.enumeration.GroupeSanguin;
import com.pascob.teleconsultation.repository.DossierMedicalRepository;
import com.pascob.teleconsultation.service.criteria.DossierMedicalCriteria;
import com.pascob.teleconsultation.service.dto.DossierMedicalDTO;
import com.pascob.teleconsultation.service.mapper.DossierMedicalMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DossierMedicalResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DossierMedicalResourceIT {

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final GroupeSanguin DEFAULT_GROUPE_SANGUIN = GroupeSanguin.O_PLUS;
    private static final GroupeSanguin UPDATED_GROUPE_SANGUIN = GroupeSanguin.AB_PLUS;

    private static final String ENTITY_API_URL = "/api/dossier-medicals";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DossierMedicalRepository dossierMedicalRepository;

    @Autowired
    private DossierMedicalMapper dossierMedicalMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDossierMedicalMockMvc;

    private DossierMedical dossierMedical;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DossierMedical createEntity(EntityManager em) {
        DossierMedical dossierMedical = new DossierMedical().numero(DEFAULT_NUMERO).groupeSanguin(DEFAULT_GROUPE_SANGUIN);
        return dossierMedical;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DossierMedical createUpdatedEntity(EntityManager em) {
        DossierMedical dossierMedical = new DossierMedical().numero(UPDATED_NUMERO).groupeSanguin(UPDATED_GROUPE_SANGUIN);
        return dossierMedical;
    }

    @BeforeEach
    public void initTest() {
        dossierMedical = createEntity(em);
    }

    @Test
    @Transactional
    void createDossierMedical() throws Exception {
        int databaseSizeBeforeCreate = dossierMedicalRepository.findAll().size();
        // Create the DossierMedical
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);
        restDossierMedicalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isCreated());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeCreate + 1);
        DossierMedical testDossierMedical = dossierMedicalList.get(dossierMedicalList.size() - 1);
        assertThat(testDossierMedical.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testDossierMedical.getGroupeSanguin()).isEqualTo(DEFAULT_GROUPE_SANGUIN);
    }

    @Test
    @Transactional
    void createDossierMedicalWithExistingId() throws Exception {
        // Create the DossierMedical with an existing ID
        dossierMedical.setId(1L);
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        int databaseSizeBeforeCreate = dossierMedicalRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDossierMedicalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNumeroIsRequired() throws Exception {
        int databaseSizeBeforeTest = dossierMedicalRepository.findAll().size();
        // set the field null
        dossierMedical.setNumero(null);

        // Create the DossierMedical, which fails.
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        restDossierMedicalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDossierMedicals() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList
        restDossierMedicalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dossierMedical.getId().intValue())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].groupeSanguin").value(hasItem(DEFAULT_GROUPE_SANGUIN.toString())));
    }

    @Test
    @Transactional
    void getDossierMedical() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get the dossierMedical
        restDossierMedicalMockMvc
            .perform(get(ENTITY_API_URL_ID, dossierMedical.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dossierMedical.getId().intValue()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO))
            .andExpect(jsonPath("$.groupeSanguin").value(DEFAULT_GROUPE_SANGUIN.toString()));
    }

    @Test
    @Transactional
    void getDossierMedicalsByIdFiltering() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        Long id = dossierMedical.getId();

        defaultDossierMedicalShouldBeFound("id.equals=" + id);
        defaultDossierMedicalShouldNotBeFound("id.notEquals=" + id);

        defaultDossierMedicalShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDossierMedicalShouldNotBeFound("id.greaterThan=" + id);

        defaultDossierMedicalShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDossierMedicalShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByNumeroIsEqualToSomething() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where numero equals to DEFAULT_NUMERO
        defaultDossierMedicalShouldBeFound("numero.equals=" + DEFAULT_NUMERO);

        // Get all the dossierMedicalList where numero equals to UPDATED_NUMERO
        defaultDossierMedicalShouldNotBeFound("numero.equals=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByNumeroIsInShouldWork() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where numero in DEFAULT_NUMERO or UPDATED_NUMERO
        defaultDossierMedicalShouldBeFound("numero.in=" + DEFAULT_NUMERO + "," + UPDATED_NUMERO);

        // Get all the dossierMedicalList where numero equals to UPDATED_NUMERO
        defaultDossierMedicalShouldNotBeFound("numero.in=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByNumeroIsNullOrNotNull() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where numero is not null
        defaultDossierMedicalShouldBeFound("numero.specified=true");

        // Get all the dossierMedicalList where numero is null
        defaultDossierMedicalShouldNotBeFound("numero.specified=false");
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByNumeroContainsSomething() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where numero contains DEFAULT_NUMERO
        defaultDossierMedicalShouldBeFound("numero.contains=" + DEFAULT_NUMERO);

        // Get all the dossierMedicalList where numero contains UPDATED_NUMERO
        defaultDossierMedicalShouldNotBeFound("numero.contains=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByNumeroNotContainsSomething() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where numero does not contain DEFAULT_NUMERO
        defaultDossierMedicalShouldNotBeFound("numero.doesNotContain=" + DEFAULT_NUMERO);

        // Get all the dossierMedicalList where numero does not contain UPDATED_NUMERO
        defaultDossierMedicalShouldBeFound("numero.doesNotContain=" + UPDATED_NUMERO);
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByGroupeSanguinIsEqualToSomething() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where groupeSanguin equals to DEFAULT_GROUPE_SANGUIN
        defaultDossierMedicalShouldBeFound("groupeSanguin.equals=" + DEFAULT_GROUPE_SANGUIN);

        // Get all the dossierMedicalList where groupeSanguin equals to UPDATED_GROUPE_SANGUIN
        defaultDossierMedicalShouldNotBeFound("groupeSanguin.equals=" + UPDATED_GROUPE_SANGUIN);
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByGroupeSanguinIsInShouldWork() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where groupeSanguin in DEFAULT_GROUPE_SANGUIN or UPDATED_GROUPE_SANGUIN
        defaultDossierMedicalShouldBeFound("groupeSanguin.in=" + DEFAULT_GROUPE_SANGUIN + "," + UPDATED_GROUPE_SANGUIN);

        // Get all the dossierMedicalList where groupeSanguin equals to UPDATED_GROUPE_SANGUIN
        defaultDossierMedicalShouldNotBeFound("groupeSanguin.in=" + UPDATED_GROUPE_SANGUIN);
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByGroupeSanguinIsNullOrNotNull() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        // Get all the dossierMedicalList where groupeSanguin is not null
        defaultDossierMedicalShouldBeFound("groupeSanguin.specified=true");

        // Get all the dossierMedicalList where groupeSanguin is null
        defaultDossierMedicalShouldNotBeFound("groupeSanguin.specified=false");
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByDemandeConsultationIsEqualToSomething() throws Exception {
        DemandeConsultation demandeConsultation;
        if (TestUtil.findAll(em, DemandeConsultation.class).isEmpty()) {
            dossierMedicalRepository.saveAndFlush(dossierMedical);
            demandeConsultation = DemandeConsultationResourceIT.createEntity(em);
        } else {
            demandeConsultation = TestUtil.findAll(em, DemandeConsultation.class).get(0);
        }
        em.persist(demandeConsultation);
        em.flush();
        dossierMedical.addDemandeConsultation(demandeConsultation);
        dossierMedicalRepository.saveAndFlush(dossierMedical);
        Long demandeConsultationId = demandeConsultation.getId();

        // Get all the dossierMedicalList where demandeConsultation equals to demandeConsultationId
        defaultDossierMedicalShouldBeFound("demandeConsultationId.equals=" + demandeConsultationId);

        // Get all the dossierMedicalList where demandeConsultation equals to (demandeConsultationId + 1)
        defaultDossierMedicalShouldNotBeFound("demandeConsultationId.equals=" + (demandeConsultationId + 1));
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByPatientIsEqualToSomething() throws Exception {
        Patient patient;
        if (TestUtil.findAll(em, Patient.class).isEmpty()) {
            dossierMedicalRepository.saveAndFlush(dossierMedical);
            patient = PatientResourceIT.createEntity(em);
        } else {
            patient = TestUtil.findAll(em, Patient.class).get(0);
        }
        em.persist(patient);
        em.flush();
        dossierMedical.setPatient(patient);
        patient.setDossierMedical(dossierMedical);
        dossierMedicalRepository.saveAndFlush(dossierMedical);
        Long patientId = patient.getId();

        // Get all the dossierMedicalList where patient equals to patientId
        defaultDossierMedicalShouldBeFound("patientId.equals=" + patientId);

        // Get all the dossierMedicalList where patient equals to (patientId + 1)
        defaultDossierMedicalShouldNotBeFound("patientId.equals=" + (patientId + 1));
    }

    @Test
    @Transactional
    void getAllDossierMedicalsByMedecinIsEqualToSomething() throws Exception {
        Medecin medecin;
        if (TestUtil.findAll(em, Medecin.class).isEmpty()) {
            dossierMedicalRepository.saveAndFlush(dossierMedical);
            medecin = MedecinResourceIT.createEntity(em);
        } else {
            medecin = TestUtil.findAll(em, Medecin.class).get(0);
        }
        em.persist(medecin);
        em.flush();
        dossierMedical.setMedecin(medecin);
        dossierMedicalRepository.saveAndFlush(dossierMedical);
        Long medecinId = medecin.getId();

        // Get all the dossierMedicalList where medecin equals to medecinId
        defaultDossierMedicalShouldBeFound("medecinId.equals=" + medecinId);

        // Get all the dossierMedicalList where medecin equals to (medecinId + 1)
        defaultDossierMedicalShouldNotBeFound("medecinId.equals=" + (medecinId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDossierMedicalShouldBeFound(String filter) throws Exception {
        restDossierMedicalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dossierMedical.getId().intValue())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].groupeSanguin").value(hasItem(DEFAULT_GROUPE_SANGUIN.toString())));

        // Check, that the count call also returns 1
        restDossierMedicalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDossierMedicalShouldNotBeFound(String filter) throws Exception {
        restDossierMedicalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDossierMedicalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingDossierMedical() throws Exception {
        // Get the dossierMedical
        restDossierMedicalMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDossierMedical() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();

        // Update the dossierMedical
        DossierMedical updatedDossierMedical = dossierMedicalRepository.findById(dossierMedical.getId()).get();
        // Disconnect from session so that the updates on updatedDossierMedical are not directly saved in db
        em.detach(updatedDossierMedical);
        updatedDossierMedical.numero(UPDATED_NUMERO).groupeSanguin(UPDATED_GROUPE_SANGUIN);
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(updatedDossierMedical);

        restDossierMedicalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dossierMedicalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isOk());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
        DossierMedical testDossierMedical = dossierMedicalList.get(dossierMedicalList.size() - 1);
        assertThat(testDossierMedical.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testDossierMedical.getGroupeSanguin()).isEqualTo(UPDATED_GROUPE_SANGUIN);
    }

    @Test
    @Transactional
    void putNonExistingDossierMedical() throws Exception {
        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();
        dossierMedical.setId(count.incrementAndGet());

        // Create the DossierMedical
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDossierMedicalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dossierMedicalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDossierMedical() throws Exception {
        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();
        dossierMedical.setId(count.incrementAndGet());

        // Create the DossierMedical
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDossierMedicalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDossierMedical() throws Exception {
        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();
        dossierMedical.setId(count.incrementAndGet());

        // Create the DossierMedical
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDossierMedicalMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDossierMedicalWithPatch() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();

        // Update the dossierMedical using partial update
        DossierMedical partialUpdatedDossierMedical = new DossierMedical();
        partialUpdatedDossierMedical.setId(dossierMedical.getId());

        restDossierMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDossierMedical.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDossierMedical))
            )
            .andExpect(status().isOk());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
        DossierMedical testDossierMedical = dossierMedicalList.get(dossierMedicalList.size() - 1);
        assertThat(testDossierMedical.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testDossierMedical.getGroupeSanguin()).isEqualTo(DEFAULT_GROUPE_SANGUIN);
    }

    @Test
    @Transactional
    void fullUpdateDossierMedicalWithPatch() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();

        // Update the dossierMedical using partial update
        DossierMedical partialUpdatedDossierMedical = new DossierMedical();
        partialUpdatedDossierMedical.setId(dossierMedical.getId());

        partialUpdatedDossierMedical.numero(UPDATED_NUMERO).groupeSanguin(UPDATED_GROUPE_SANGUIN);

        restDossierMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDossierMedical.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDossierMedical))
            )
            .andExpect(status().isOk());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
        DossierMedical testDossierMedical = dossierMedicalList.get(dossierMedicalList.size() - 1);
        assertThat(testDossierMedical.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testDossierMedical.getGroupeSanguin()).isEqualTo(UPDATED_GROUPE_SANGUIN);
    }

    @Test
    @Transactional
    void patchNonExistingDossierMedical() throws Exception {
        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();
        dossierMedical.setId(count.incrementAndGet());

        // Create the DossierMedical
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDossierMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dossierMedicalDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDossierMedical() throws Exception {
        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();
        dossierMedical.setId(count.incrementAndGet());

        // Create the DossierMedical
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDossierMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDossierMedical() throws Exception {
        int databaseSizeBeforeUpdate = dossierMedicalRepository.findAll().size();
        dossierMedical.setId(count.incrementAndGet());

        // Create the DossierMedical
        DossierMedicalDTO dossierMedicalDTO = dossierMedicalMapper.toDto(dossierMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDossierMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dossierMedicalDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DossierMedical in the database
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDossierMedical() throws Exception {
        // Initialize the database
        dossierMedicalRepository.saveAndFlush(dossierMedical);

        int databaseSizeBeforeDelete = dossierMedicalRepository.findAll().size();

        // Delete the dossierMedical
        restDossierMedicalMockMvc
            .perform(delete(ENTITY_API_URL_ID, dossierMedical.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DossierMedical> dossierMedicalList = dossierMedicalRepository.findAll();
        assertThat(dossierMedicalList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
