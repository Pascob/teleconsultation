package com.pascob.teleconsultation.web.rest;

import static com.pascob.teleconsultation.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.Message;
import com.pascob.teleconsultation.repository.MessageRepository;
import com.pascob.teleconsultation.service.criteria.MessageCriteria;
import com.pascob.teleconsultation.service.dto.MessageDTO;
import com.pascob.teleconsultation.service.mapper.MessageMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MessageResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MessageResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_MESSAGE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_MESSAGE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_MESSAGE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final Long DEFAULT_USER_ID_FROM = 1L;
    private static final Long UPDATED_USER_ID_FROM = 2L;
    private static final Long SMALLER_USER_ID_FROM = 1L - 1L;

    private static final Long DEFAULT_USER_ID_TO = 1L;
    private static final Long UPDATED_USER_ID_TO = 2L;
    private static final Long SMALLER_USER_ID_TO = 1L - 1L;

    private static final Boolean DEFAULT_READED = false;
    private static final Boolean UPDATED_READED = true;

    private static final String ENTITY_API_URL = "/api/messages";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMessageMockMvc;

    private Message message;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Message createEntity(EntityManager em) {
        Message message = new Message()
            .dateMessage(DEFAULT_DATE_MESSAGE)
            .message(DEFAULT_MESSAGE)
            .userIdFrom(DEFAULT_USER_ID_FROM)
            .userIdTo(DEFAULT_USER_ID_TO)
            .readed(DEFAULT_READED);
        return message;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Message createUpdatedEntity(EntityManager em) {
        Message message = new Message()
            .dateMessage(UPDATED_DATE_MESSAGE)
            .message(UPDATED_MESSAGE)
            .userIdFrom(UPDATED_USER_ID_FROM)
            .userIdTo(UPDATED_USER_ID_TO)
            .readed(UPDATED_READED);
        return message;
    }

    @BeforeEach
    public void initTest() {
        message = createEntity(em);
    }

    @Test
    @Transactional
    void createMessage() throws Exception {
        int databaseSizeBeforeCreate = messageRepository.findAll().size();
        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);
        restMessageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isCreated());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeCreate + 1);
        Message testMessage = messageList.get(messageList.size() - 1);
        assertThat(testMessage.getDateMessage()).isEqualTo(DEFAULT_DATE_MESSAGE);
        assertThat(testMessage.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testMessage.getUserIdFrom()).isEqualTo(DEFAULT_USER_ID_FROM);
        assertThat(testMessage.getUserIdTo()).isEqualTo(DEFAULT_USER_ID_TO);
        assertThat(testMessage.getReaded()).isEqualTo(DEFAULT_READED);
    }

    @Test
    @Transactional
    void createMessageWithExistingId() throws Exception {
        // Create the Message with an existing ID
        message.setId(1L);
        MessageDTO messageDTO = messageMapper.toDto(message);

        int databaseSizeBeforeCreate = messageRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMessageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMessages() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList
        restMessageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(message.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateMessage").value(hasItem(sameInstant(DEFAULT_DATE_MESSAGE))))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].userIdFrom").value(hasItem(DEFAULT_USER_ID_FROM.intValue())))
            .andExpect(jsonPath("$.[*].userIdTo").value(hasItem(DEFAULT_USER_ID_TO.intValue())))
            .andExpect(jsonPath("$.[*].readed").value(hasItem(DEFAULT_READED.booleanValue())));
    }

    @Test
    @Transactional
    void getMessage() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get the message
        restMessageMockMvc
            .perform(get(ENTITY_API_URL_ID, message.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(message.getId().intValue()))
            .andExpect(jsonPath("$.dateMessage").value(sameInstant(DEFAULT_DATE_MESSAGE)))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE))
            .andExpect(jsonPath("$.userIdFrom").value(DEFAULT_USER_ID_FROM.intValue()))
            .andExpect(jsonPath("$.userIdTo").value(DEFAULT_USER_ID_TO.intValue()))
            .andExpect(jsonPath("$.readed").value(DEFAULT_READED.booleanValue()));
    }

    @Test
    @Transactional
    void getMessagesByIdFiltering() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        Long id = message.getId();

        defaultMessageShouldBeFound("id.equals=" + id);
        defaultMessageShouldNotBeFound("id.notEquals=" + id);

        defaultMessageShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMessageShouldNotBeFound("id.greaterThan=" + id);

        defaultMessageShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMessageShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMessagesByDateMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage equals to DEFAULT_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.equals=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage equals to UPDATED_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.equals=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByDateMessageIsInShouldWork() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage in DEFAULT_DATE_MESSAGE or UPDATED_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.in=" + DEFAULT_DATE_MESSAGE + "," + UPDATED_DATE_MESSAGE);

        // Get all the messageList where dateMessage equals to UPDATED_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.in=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByDateMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is not null
        defaultMessageShouldBeFound("dateMessage.specified=true");

        // Get all the messageList where dateMessage is null
        defaultMessageShouldNotBeFound("dateMessage.specified=false");
    }

    @Test
    @Transactional
    void getAllMessagesByDateMessageIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is greater than or equal to DEFAULT_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.greaterThanOrEqual=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is greater than or equal to UPDATED_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.greaterThanOrEqual=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByDateMessageIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is less than or equal to DEFAULT_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.lessThanOrEqual=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is less than or equal to SMALLER_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.lessThanOrEqual=" + SMALLER_DATE_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByDateMessageIsLessThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is less than DEFAULT_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.lessThan=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is less than UPDATED_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.lessThan=" + UPDATED_DATE_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByDateMessageIsGreaterThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where dateMessage is greater than DEFAULT_DATE_MESSAGE
        defaultMessageShouldNotBeFound("dateMessage.greaterThan=" + DEFAULT_DATE_MESSAGE);

        // Get all the messageList where dateMessage is greater than SMALLER_DATE_MESSAGE
        defaultMessageShouldBeFound("dateMessage.greaterThan=" + SMALLER_DATE_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message equals to DEFAULT_MESSAGE
        defaultMessageShouldBeFound("message.equals=" + DEFAULT_MESSAGE);

        // Get all the messageList where message equals to UPDATED_MESSAGE
        defaultMessageShouldNotBeFound("message.equals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByMessageIsInShouldWork() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message in DEFAULT_MESSAGE or UPDATED_MESSAGE
        defaultMessageShouldBeFound("message.in=" + DEFAULT_MESSAGE + "," + UPDATED_MESSAGE);

        // Get all the messageList where message equals to UPDATED_MESSAGE
        defaultMessageShouldNotBeFound("message.in=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message is not null
        defaultMessageShouldBeFound("message.specified=true");

        // Get all the messageList where message is null
        defaultMessageShouldNotBeFound("message.specified=false");
    }

    @Test
    @Transactional
    void getAllMessagesByMessageContainsSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message contains DEFAULT_MESSAGE
        defaultMessageShouldBeFound("message.contains=" + DEFAULT_MESSAGE);

        // Get all the messageList where message contains UPDATED_MESSAGE
        defaultMessageShouldNotBeFound("message.contains=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByMessageNotContainsSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where message does not contain DEFAULT_MESSAGE
        defaultMessageShouldNotBeFound("message.doesNotContain=" + DEFAULT_MESSAGE);

        // Get all the messageList where message does not contain UPDATED_MESSAGE
        defaultMessageShouldBeFound("message.doesNotContain=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdFromIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdFrom equals to DEFAULT_USER_ID_FROM
        defaultMessageShouldBeFound("userIdFrom.equals=" + DEFAULT_USER_ID_FROM);

        // Get all the messageList where userIdFrom equals to UPDATED_USER_ID_FROM
        defaultMessageShouldNotBeFound("userIdFrom.equals=" + UPDATED_USER_ID_FROM);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdFromIsInShouldWork() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdFrom in DEFAULT_USER_ID_FROM or UPDATED_USER_ID_FROM
        defaultMessageShouldBeFound("userIdFrom.in=" + DEFAULT_USER_ID_FROM + "," + UPDATED_USER_ID_FROM);

        // Get all the messageList where userIdFrom equals to UPDATED_USER_ID_FROM
        defaultMessageShouldNotBeFound("userIdFrom.in=" + UPDATED_USER_ID_FROM);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdFrom is not null
        defaultMessageShouldBeFound("userIdFrom.specified=true");

        // Get all the messageList where userIdFrom is null
        defaultMessageShouldNotBeFound("userIdFrom.specified=false");
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdFrom is greater than or equal to DEFAULT_USER_ID_FROM
        defaultMessageShouldBeFound("userIdFrom.greaterThanOrEqual=" + DEFAULT_USER_ID_FROM);

        // Get all the messageList where userIdFrom is greater than or equal to UPDATED_USER_ID_FROM
        defaultMessageShouldNotBeFound("userIdFrom.greaterThanOrEqual=" + UPDATED_USER_ID_FROM);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdFromIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdFrom is less than or equal to DEFAULT_USER_ID_FROM
        defaultMessageShouldBeFound("userIdFrom.lessThanOrEqual=" + DEFAULT_USER_ID_FROM);

        // Get all the messageList where userIdFrom is less than or equal to SMALLER_USER_ID_FROM
        defaultMessageShouldNotBeFound("userIdFrom.lessThanOrEqual=" + SMALLER_USER_ID_FROM);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdFromIsLessThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdFrom is less than DEFAULT_USER_ID_FROM
        defaultMessageShouldNotBeFound("userIdFrom.lessThan=" + DEFAULT_USER_ID_FROM);

        // Get all the messageList where userIdFrom is less than UPDATED_USER_ID_FROM
        defaultMessageShouldBeFound("userIdFrom.lessThan=" + UPDATED_USER_ID_FROM);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdFromIsGreaterThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdFrom is greater than DEFAULT_USER_ID_FROM
        defaultMessageShouldNotBeFound("userIdFrom.greaterThan=" + DEFAULT_USER_ID_FROM);

        // Get all the messageList where userIdFrom is greater than SMALLER_USER_ID_FROM
        defaultMessageShouldBeFound("userIdFrom.greaterThan=" + SMALLER_USER_ID_FROM);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdToIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdTo equals to DEFAULT_USER_ID_TO
        defaultMessageShouldBeFound("userIdTo.equals=" + DEFAULT_USER_ID_TO);

        // Get all the messageList where userIdTo equals to UPDATED_USER_ID_TO
        defaultMessageShouldNotBeFound("userIdTo.equals=" + UPDATED_USER_ID_TO);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdToIsInShouldWork() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdTo in DEFAULT_USER_ID_TO or UPDATED_USER_ID_TO
        defaultMessageShouldBeFound("userIdTo.in=" + DEFAULT_USER_ID_TO + "," + UPDATED_USER_ID_TO);

        // Get all the messageList where userIdTo equals to UPDATED_USER_ID_TO
        defaultMessageShouldNotBeFound("userIdTo.in=" + UPDATED_USER_ID_TO);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdToIsNullOrNotNull() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdTo is not null
        defaultMessageShouldBeFound("userIdTo.specified=true");

        // Get all the messageList where userIdTo is null
        defaultMessageShouldNotBeFound("userIdTo.specified=false");
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdTo is greater than or equal to DEFAULT_USER_ID_TO
        defaultMessageShouldBeFound("userIdTo.greaterThanOrEqual=" + DEFAULT_USER_ID_TO);

        // Get all the messageList where userIdTo is greater than or equal to UPDATED_USER_ID_TO
        defaultMessageShouldNotBeFound("userIdTo.greaterThanOrEqual=" + UPDATED_USER_ID_TO);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdToIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdTo is less than or equal to DEFAULT_USER_ID_TO
        defaultMessageShouldBeFound("userIdTo.lessThanOrEqual=" + DEFAULT_USER_ID_TO);

        // Get all the messageList where userIdTo is less than or equal to SMALLER_USER_ID_TO
        defaultMessageShouldNotBeFound("userIdTo.lessThanOrEqual=" + SMALLER_USER_ID_TO);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdToIsLessThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdTo is less than DEFAULT_USER_ID_TO
        defaultMessageShouldNotBeFound("userIdTo.lessThan=" + DEFAULT_USER_ID_TO);

        // Get all the messageList where userIdTo is less than UPDATED_USER_ID_TO
        defaultMessageShouldBeFound("userIdTo.lessThan=" + UPDATED_USER_ID_TO);
    }

    @Test
    @Transactional
    void getAllMessagesByUserIdToIsGreaterThanSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where userIdTo is greater than DEFAULT_USER_ID_TO
        defaultMessageShouldNotBeFound("userIdTo.greaterThan=" + DEFAULT_USER_ID_TO);

        // Get all the messageList where userIdTo is greater than SMALLER_USER_ID_TO
        defaultMessageShouldBeFound("userIdTo.greaterThan=" + SMALLER_USER_ID_TO);
    }

    @Test
    @Transactional
    void getAllMessagesByReadedIsEqualToSomething() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where readed equals to DEFAULT_READED
        defaultMessageShouldBeFound("readed.equals=" + DEFAULT_READED);

        // Get all the messageList where readed equals to UPDATED_READED
        defaultMessageShouldNotBeFound("readed.equals=" + UPDATED_READED);
    }

    @Test
    @Transactional
    void getAllMessagesByReadedIsInShouldWork() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where readed in DEFAULT_READED or UPDATED_READED
        defaultMessageShouldBeFound("readed.in=" + DEFAULT_READED + "," + UPDATED_READED);

        // Get all the messageList where readed equals to UPDATED_READED
        defaultMessageShouldNotBeFound("readed.in=" + UPDATED_READED);
    }

    @Test
    @Transactional
    void getAllMessagesByReadedIsNullOrNotNull() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        // Get all the messageList where readed is not null
        defaultMessageShouldBeFound("readed.specified=true");

        // Get all the messageList where readed is null
        defaultMessageShouldNotBeFound("readed.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMessageShouldBeFound(String filter) throws Exception {
        restMessageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(message.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateMessage").value(hasItem(sameInstant(DEFAULT_DATE_MESSAGE))))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].userIdFrom").value(hasItem(DEFAULT_USER_ID_FROM.intValue())))
            .andExpect(jsonPath("$.[*].userIdTo").value(hasItem(DEFAULT_USER_ID_TO.intValue())))
            .andExpect(jsonPath("$.[*].readed").value(hasItem(DEFAULT_READED.booleanValue())));

        // Check, that the count call also returns 1
        restMessageMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMessageShouldNotBeFound(String filter) throws Exception {
        restMessageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMessageMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMessage() throws Exception {
        // Get the message
        restMessageMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingMessage() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        int databaseSizeBeforeUpdate = messageRepository.findAll().size();

        // Update the message
        Message updatedMessage = messageRepository.findById(message.getId()).get();
        // Disconnect from session so that the updates on updatedMessage are not directly saved in db
        em.detach(updatedMessage);
        updatedMessage
            .dateMessage(UPDATED_DATE_MESSAGE)
            .message(UPDATED_MESSAGE)
            .userIdFrom(UPDATED_USER_ID_FROM)
            .userIdTo(UPDATED_USER_ID_TO)
            .readed(UPDATED_READED);
        MessageDTO messageDTO = messageMapper.toDto(updatedMessage);

        restMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, messageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(messageDTO))
            )
            .andExpect(status().isOk());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
        Message testMessage = messageList.get(messageList.size() - 1);
        assertThat(testMessage.getDateMessage()).isEqualTo(UPDATED_DATE_MESSAGE);
        assertThat(testMessage.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testMessage.getUserIdFrom()).isEqualTo(UPDATED_USER_ID_FROM);
        assertThat(testMessage.getUserIdTo()).isEqualTo(UPDATED_USER_ID_TO);
        assertThat(testMessage.getReaded()).isEqualTo(UPDATED_READED);
    }

    @Test
    @Transactional
    void putNonExistingMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        message.setId(count.incrementAndGet());

        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, messageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(messageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        message.setId(count.incrementAndGet());

        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(messageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        message.setId(count.incrementAndGet());

        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMessageMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(messageDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMessageWithPatch() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        int databaseSizeBeforeUpdate = messageRepository.findAll().size();

        // Update the message using partial update
        Message partialUpdatedMessage = new Message();
        partialUpdatedMessage.setId(message.getId());

        partialUpdatedMessage.readed(UPDATED_READED);

        restMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMessage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMessage))
            )
            .andExpect(status().isOk());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
        Message testMessage = messageList.get(messageList.size() - 1);
        assertThat(testMessage.getDateMessage()).isEqualTo(DEFAULT_DATE_MESSAGE);
        assertThat(testMessage.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testMessage.getUserIdFrom()).isEqualTo(DEFAULT_USER_ID_FROM);
        assertThat(testMessage.getUserIdTo()).isEqualTo(DEFAULT_USER_ID_TO);
        assertThat(testMessage.getReaded()).isEqualTo(UPDATED_READED);
    }

    @Test
    @Transactional
    void fullUpdateMessageWithPatch() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        int databaseSizeBeforeUpdate = messageRepository.findAll().size();

        // Update the message using partial update
        Message partialUpdatedMessage = new Message();
        partialUpdatedMessage.setId(message.getId());

        partialUpdatedMessage
            .dateMessage(UPDATED_DATE_MESSAGE)
            .message(UPDATED_MESSAGE)
            .userIdFrom(UPDATED_USER_ID_FROM)
            .userIdTo(UPDATED_USER_ID_TO)
            .readed(UPDATED_READED);

        restMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMessage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMessage))
            )
            .andExpect(status().isOk());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
        Message testMessage = messageList.get(messageList.size() - 1);
        assertThat(testMessage.getDateMessage()).isEqualTo(UPDATED_DATE_MESSAGE);
        assertThat(testMessage.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testMessage.getUserIdFrom()).isEqualTo(UPDATED_USER_ID_FROM);
        assertThat(testMessage.getUserIdTo()).isEqualTo(UPDATED_USER_ID_TO);
        assertThat(testMessage.getReaded()).isEqualTo(UPDATED_READED);
    }

    @Test
    @Transactional
    void patchNonExistingMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        message.setId(count.incrementAndGet());

        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, messageDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(messageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        message.setId(count.incrementAndGet());

        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(messageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMessage() throws Exception {
        int databaseSizeBeforeUpdate = messageRepository.findAll().size();
        message.setId(count.incrementAndGet());

        // Create the Message
        MessageDTO messageDTO = messageMapper.toDto(message);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMessageMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(messageDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Message in the database
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMessage() throws Exception {
        // Initialize the database
        messageRepository.saveAndFlush(message);

        int databaseSizeBeforeDelete = messageRepository.findAll().size();

        // Delete the message
        restMessageMockMvc
            .perform(delete(ENTITY_API_URL_ID, message.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
