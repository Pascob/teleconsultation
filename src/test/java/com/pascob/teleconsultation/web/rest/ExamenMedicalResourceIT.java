package com.pascob.teleconsultation.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pascob.teleconsultation.IntegrationTest;
import com.pascob.teleconsultation.domain.ExamenMedical;
import com.pascob.teleconsultation.domain.PrescriptionExamen;
import com.pascob.teleconsultation.repository.ExamenMedicalRepository;
import com.pascob.teleconsultation.service.criteria.ExamenMedicalCriteria;
import com.pascob.teleconsultation.service.dto.ExamenMedicalDTO;
import com.pascob.teleconsultation.service.mapper.ExamenMedicalMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExamenMedicalResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExamenMedicalResourceIT {

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/examen-medicals";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ExamenMedicalRepository examenMedicalRepository;

    @Autowired
    private ExamenMedicalMapper examenMedicalMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExamenMedicalMockMvc;

    private ExamenMedical examenMedical;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExamenMedical createEntity(EntityManager em) {
        ExamenMedical examenMedical = new ExamenMedical().designation(DEFAULT_DESIGNATION).code(DEFAULT_CODE);
        return examenMedical;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExamenMedical createUpdatedEntity(EntityManager em) {
        ExamenMedical examenMedical = new ExamenMedical().designation(UPDATED_DESIGNATION).code(UPDATED_CODE);
        return examenMedical;
    }

    @BeforeEach
    public void initTest() {
        examenMedical = createEntity(em);
    }

    @Test
    @Transactional
    void createExamenMedical() throws Exception {
        int databaseSizeBeforeCreate = examenMedicalRepository.findAll().size();
        // Create the ExamenMedical
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);
        restExamenMedicalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeCreate + 1);
        ExamenMedical testExamenMedical = examenMedicalList.get(examenMedicalList.size() - 1);
        assertThat(testExamenMedical.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testExamenMedical.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    void createExamenMedicalWithExistingId() throws Exception {
        // Create the ExamenMedical with an existing ID
        examenMedical.setId(1L);
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        int databaseSizeBeforeCreate = examenMedicalRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExamenMedicalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDesignationIsRequired() throws Exception {
        int databaseSizeBeforeTest = examenMedicalRepository.findAll().size();
        // set the field null
        examenMedical.setDesignation(null);

        // Create the ExamenMedical, which fails.
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        restExamenMedicalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllExamenMedicals() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList
        restExamenMedicalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(examenMedical.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getExamenMedical() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get the examenMedical
        restExamenMedicalMockMvc
            .perform(get(ENTITY_API_URL_ID, examenMedical.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(examenMedical.getId().intValue()))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getExamenMedicalsByIdFiltering() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        Long id = examenMedical.getId();

        defaultExamenMedicalShouldBeFound("id.equals=" + id);
        defaultExamenMedicalShouldNotBeFound("id.notEquals=" + id);

        defaultExamenMedicalShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultExamenMedicalShouldNotBeFound("id.greaterThan=" + id);

        defaultExamenMedicalShouldBeFound("id.lessThanOrEqual=" + id);
        defaultExamenMedicalShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where designation equals to DEFAULT_DESIGNATION
        defaultExamenMedicalShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the examenMedicalList where designation equals to UPDATED_DESIGNATION
        defaultExamenMedicalShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultExamenMedicalShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the examenMedicalList where designation equals to UPDATED_DESIGNATION
        defaultExamenMedicalShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where designation is not null
        defaultExamenMedicalShouldBeFound("designation.specified=true");

        // Get all the examenMedicalList where designation is null
        defaultExamenMedicalShouldNotBeFound("designation.specified=false");
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByDesignationContainsSomething() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where designation contains DEFAULT_DESIGNATION
        defaultExamenMedicalShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the examenMedicalList where designation contains UPDATED_DESIGNATION
        defaultExamenMedicalShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where designation does not contain DEFAULT_DESIGNATION
        defaultExamenMedicalShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the examenMedicalList where designation does not contain UPDATED_DESIGNATION
        defaultExamenMedicalShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where code equals to DEFAULT_CODE
        defaultExamenMedicalShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the examenMedicalList where code equals to UPDATED_CODE
        defaultExamenMedicalShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where code in DEFAULT_CODE or UPDATED_CODE
        defaultExamenMedicalShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the examenMedicalList where code equals to UPDATED_CODE
        defaultExamenMedicalShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where code is not null
        defaultExamenMedicalShouldBeFound("code.specified=true");

        // Get all the examenMedicalList where code is null
        defaultExamenMedicalShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByCodeContainsSomething() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where code contains DEFAULT_CODE
        defaultExamenMedicalShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the examenMedicalList where code contains UPDATED_CODE
        defaultExamenMedicalShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        // Get all the examenMedicalList where code does not contain DEFAULT_CODE
        defaultExamenMedicalShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the examenMedicalList where code does not contain UPDATED_CODE
        defaultExamenMedicalShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllExamenMedicalsByPrescriptionExamenIsEqualToSomething() throws Exception {
        PrescriptionExamen prescriptionExamen;
        if (TestUtil.findAll(em, PrescriptionExamen.class).isEmpty()) {
            examenMedicalRepository.saveAndFlush(examenMedical);
            prescriptionExamen = PrescriptionExamenResourceIT.createEntity(em);
        } else {
            prescriptionExamen = TestUtil.findAll(em, PrescriptionExamen.class).get(0);
        }
        em.persist(prescriptionExamen);
        em.flush();
        examenMedical.addPrescriptionExamen(prescriptionExamen);
        examenMedicalRepository.saveAndFlush(examenMedical);
        Long prescriptionExamenId = prescriptionExamen.getId();

        // Get all the examenMedicalList where prescriptionExamen equals to prescriptionExamenId
        defaultExamenMedicalShouldBeFound("prescriptionExamenId.equals=" + prescriptionExamenId);

        // Get all the examenMedicalList where prescriptionExamen equals to (prescriptionExamenId + 1)
        defaultExamenMedicalShouldNotBeFound("prescriptionExamenId.equals=" + (prescriptionExamenId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultExamenMedicalShouldBeFound(String filter) throws Exception {
        restExamenMedicalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(examenMedical.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));

        // Check, that the count call also returns 1
        restExamenMedicalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultExamenMedicalShouldNotBeFound(String filter) throws Exception {
        restExamenMedicalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restExamenMedicalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingExamenMedical() throws Exception {
        // Get the examenMedical
        restExamenMedicalMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExamenMedical() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();

        // Update the examenMedical
        ExamenMedical updatedExamenMedical = examenMedicalRepository.findById(examenMedical.getId()).get();
        // Disconnect from session so that the updates on updatedExamenMedical are not directly saved in db
        em.detach(updatedExamenMedical);
        updatedExamenMedical.designation(UPDATED_DESIGNATION).code(UPDATED_CODE);
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(updatedExamenMedical);

        restExamenMedicalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, examenMedicalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isOk());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
        ExamenMedical testExamenMedical = examenMedicalList.get(examenMedicalList.size() - 1);
        assertThat(testExamenMedical.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testExamenMedical.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void putNonExistingExamenMedical() throws Exception {
        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();
        examenMedical.setId(count.incrementAndGet());

        // Create the ExamenMedical
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExamenMedicalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, examenMedicalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExamenMedical() throws Exception {
        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();
        examenMedical.setId(count.incrementAndGet());

        // Create the ExamenMedical
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExamenMedicalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExamenMedical() throws Exception {
        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();
        examenMedical.setId(count.incrementAndGet());

        // Create the ExamenMedical
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExamenMedicalMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExamenMedicalWithPatch() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();

        // Update the examenMedical using partial update
        ExamenMedical partialUpdatedExamenMedical = new ExamenMedical();
        partialUpdatedExamenMedical.setId(examenMedical.getId());

        partialUpdatedExamenMedical.code(UPDATED_CODE);

        restExamenMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExamenMedical.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExamenMedical))
            )
            .andExpect(status().isOk());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
        ExamenMedical testExamenMedical = examenMedicalList.get(examenMedicalList.size() - 1);
        assertThat(testExamenMedical.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testExamenMedical.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void fullUpdateExamenMedicalWithPatch() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();

        // Update the examenMedical using partial update
        ExamenMedical partialUpdatedExamenMedical = new ExamenMedical();
        partialUpdatedExamenMedical.setId(examenMedical.getId());

        partialUpdatedExamenMedical.designation(UPDATED_DESIGNATION).code(UPDATED_CODE);

        restExamenMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExamenMedical.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedExamenMedical))
            )
            .andExpect(status().isOk());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
        ExamenMedical testExamenMedical = examenMedicalList.get(examenMedicalList.size() - 1);
        assertThat(testExamenMedical.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testExamenMedical.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingExamenMedical() throws Exception {
        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();
        examenMedical.setId(count.incrementAndGet());

        // Create the ExamenMedical
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExamenMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, examenMedicalDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExamenMedical() throws Exception {
        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();
        examenMedical.setId(count.incrementAndGet());

        // Create the ExamenMedical
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExamenMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExamenMedical() throws Exception {
        int databaseSizeBeforeUpdate = examenMedicalRepository.findAll().size();
        examenMedical.setId(count.incrementAndGet());

        // Create the ExamenMedical
        ExamenMedicalDTO examenMedicalDTO = examenMedicalMapper.toDto(examenMedical);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExamenMedicalMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(examenMedicalDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ExamenMedical in the database
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExamenMedical() throws Exception {
        // Initialize the database
        examenMedicalRepository.saveAndFlush(examenMedical);

        int databaseSizeBeforeDelete = examenMedicalRepository.findAll().size();

        // Delete the examenMedical
        restExamenMedicalMockMvc
            .perform(delete(ENTITY_API_URL_ID, examenMedical.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExamenMedical> examenMedicalList = examenMedicalRepository.findAll();
        assertThat(examenMedicalList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
