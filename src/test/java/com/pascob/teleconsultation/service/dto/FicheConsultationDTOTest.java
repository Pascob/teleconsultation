package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FicheConsultationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FicheConsultationDTO.class);
        FicheConsultationDTO ficheConsultationDTO1 = new FicheConsultationDTO();
        ficheConsultationDTO1.setId(1L);
        FicheConsultationDTO ficheConsultationDTO2 = new FicheConsultationDTO();
        assertThat(ficheConsultationDTO1).isNotEqualTo(ficheConsultationDTO2);
        ficheConsultationDTO2.setId(ficheConsultationDTO1.getId());
        assertThat(ficheConsultationDTO1).isEqualTo(ficheConsultationDTO2);
        ficheConsultationDTO2.setId(2L);
        assertThat(ficheConsultationDTO1).isNotEqualTo(ficheConsultationDTO2);
        ficheConsultationDTO1.setId(null);
        assertThat(ficheConsultationDTO1).isNotEqualTo(ficheConsultationDTO2);
    }
}
