package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrdonnanceDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrdonnanceDTO.class);
        OrdonnanceDTO ordonnanceDTO1 = new OrdonnanceDTO();
        ordonnanceDTO1.setId(1L);
        OrdonnanceDTO ordonnanceDTO2 = new OrdonnanceDTO();
        assertThat(ordonnanceDTO1).isNotEqualTo(ordonnanceDTO2);
        ordonnanceDTO2.setId(ordonnanceDTO1.getId());
        assertThat(ordonnanceDTO1).isEqualTo(ordonnanceDTO2);
        ordonnanceDTO2.setId(2L);
        assertThat(ordonnanceDTO1).isNotEqualTo(ordonnanceDTO2);
        ordonnanceDTO1.setId(null);
        assertThat(ordonnanceDTO1).isNotEqualTo(ordonnanceDTO2);
    }
}
