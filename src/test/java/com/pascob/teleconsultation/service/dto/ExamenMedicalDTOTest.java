package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExamenMedicalDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExamenMedicalDTO.class);
        ExamenMedicalDTO examenMedicalDTO1 = new ExamenMedicalDTO();
        examenMedicalDTO1.setId(1L);
        ExamenMedicalDTO examenMedicalDTO2 = new ExamenMedicalDTO();
        assertThat(examenMedicalDTO1).isNotEqualTo(examenMedicalDTO2);
        examenMedicalDTO2.setId(examenMedicalDTO1.getId());
        assertThat(examenMedicalDTO1).isEqualTo(examenMedicalDTO2);
        examenMedicalDTO2.setId(2L);
        assertThat(examenMedicalDTO1).isNotEqualTo(examenMedicalDTO2);
        examenMedicalDTO1.setId(null);
        assertThat(examenMedicalDTO1).isNotEqualTo(examenMedicalDTO2);
    }
}
