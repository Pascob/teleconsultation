package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SymptomeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SymptomeDTO.class);
        SymptomeDTO symptomeDTO1 = new SymptomeDTO();
        symptomeDTO1.setId(1L);
        SymptomeDTO symptomeDTO2 = new SymptomeDTO();
        assertThat(symptomeDTO1).isNotEqualTo(symptomeDTO2);
        symptomeDTO2.setId(symptomeDTO1.getId());
        assertThat(symptomeDTO1).isEqualTo(symptomeDTO2);
        symptomeDTO2.setId(2L);
        assertThat(symptomeDTO1).isNotEqualTo(symptomeDTO2);
        symptomeDTO1.setId(null);
        assertThat(symptomeDTO1).isNotEqualTo(symptomeDTO2);
    }
}
