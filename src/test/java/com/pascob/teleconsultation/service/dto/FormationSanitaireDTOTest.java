package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FormationSanitaireDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FormationSanitaireDTO.class);
        FormationSanitaireDTO formationSanitaireDTO1 = new FormationSanitaireDTO();
        formationSanitaireDTO1.setId(1L);
        FormationSanitaireDTO formationSanitaireDTO2 = new FormationSanitaireDTO();
        assertThat(formationSanitaireDTO1).isNotEqualTo(formationSanitaireDTO2);
        formationSanitaireDTO2.setId(formationSanitaireDTO1.getId());
        assertThat(formationSanitaireDTO1).isEqualTo(formationSanitaireDTO2);
        formationSanitaireDTO2.setId(2L);
        assertThat(formationSanitaireDTO1).isNotEqualTo(formationSanitaireDTO2);
        formationSanitaireDTO1.setId(null);
        assertThat(formationSanitaireDTO1).isNotEqualTo(formationSanitaireDTO2);
    }
}
