package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FicheSymptomeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FicheSymptomeDTO.class);
        FicheSymptomeDTO ficheSymptomeDTO1 = new FicheSymptomeDTO();
        ficheSymptomeDTO1.setId(1L);
        FicheSymptomeDTO ficheSymptomeDTO2 = new FicheSymptomeDTO();
        assertThat(ficheSymptomeDTO1).isNotEqualTo(ficheSymptomeDTO2);
        ficheSymptomeDTO2.setId(ficheSymptomeDTO1.getId());
        assertThat(ficheSymptomeDTO1).isEqualTo(ficheSymptomeDTO2);
        ficheSymptomeDTO2.setId(2L);
        assertThat(ficheSymptomeDTO1).isNotEqualTo(ficheSymptomeDTO2);
        ficheSymptomeDTO1.setId(null);
        assertThat(ficheSymptomeDTO1).isNotEqualTo(ficheSymptomeDTO2);
    }
}
