package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DemandeConsultationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DemandeConsultationDTO.class);
        DemandeConsultationDTO demandeConsultationDTO1 = new DemandeConsultationDTO();
        demandeConsultationDTO1.setId(1L);
        DemandeConsultationDTO demandeConsultationDTO2 = new DemandeConsultationDTO();
        assertThat(demandeConsultationDTO1).isNotEqualTo(demandeConsultationDTO2);
        demandeConsultationDTO2.setId(demandeConsultationDTO1.getId());
        assertThat(demandeConsultationDTO1).isEqualTo(demandeConsultationDTO2);
        demandeConsultationDTO2.setId(2L);
        assertThat(demandeConsultationDTO1).isNotEqualTo(demandeConsultationDTO2);
        demandeConsultationDTO1.setId(null);
        assertThat(demandeConsultationDTO1).isNotEqualTo(demandeConsultationDTO2);
    }
}
