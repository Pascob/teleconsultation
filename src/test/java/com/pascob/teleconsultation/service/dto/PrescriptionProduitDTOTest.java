package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PrescriptionProduitDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrescriptionProduitDTO.class);
        PrescriptionProduitDTO prescriptionProduitDTO1 = new PrescriptionProduitDTO();
        prescriptionProduitDTO1.setId(1L);
        PrescriptionProduitDTO prescriptionProduitDTO2 = new PrescriptionProduitDTO();
        assertThat(prescriptionProduitDTO1).isNotEqualTo(prescriptionProduitDTO2);
        prescriptionProduitDTO2.setId(prescriptionProduitDTO1.getId());
        assertThat(prescriptionProduitDTO1).isEqualTo(prescriptionProduitDTO2);
        prescriptionProduitDTO2.setId(2L);
        assertThat(prescriptionProduitDTO1).isNotEqualTo(prescriptionProduitDTO2);
        prescriptionProduitDTO1.setId(null);
        assertThat(prescriptionProduitDTO1).isNotEqualTo(prescriptionProduitDTO2);
    }
}
