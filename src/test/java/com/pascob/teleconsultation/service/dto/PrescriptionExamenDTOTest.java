package com.pascob.teleconsultation.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.pascob.teleconsultation.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PrescriptionExamenDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrescriptionExamenDTO.class);
        PrescriptionExamenDTO prescriptionExamenDTO1 = new PrescriptionExamenDTO();
        prescriptionExamenDTO1.setId(1L);
        PrescriptionExamenDTO prescriptionExamenDTO2 = new PrescriptionExamenDTO();
        assertThat(prescriptionExamenDTO1).isNotEqualTo(prescriptionExamenDTO2);
        prescriptionExamenDTO2.setId(prescriptionExamenDTO1.getId());
        assertThat(prescriptionExamenDTO1).isEqualTo(prescriptionExamenDTO2);
        prescriptionExamenDTO2.setId(2L);
        assertThat(prescriptionExamenDTO1).isNotEqualTo(prescriptionExamenDTO2);
        prescriptionExamenDTO1.setId(null);
        assertThat(prescriptionExamenDTO1).isNotEqualTo(prescriptionExamenDTO2);
    }
}
