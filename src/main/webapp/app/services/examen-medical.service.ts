import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IExamenMedical, NewExamenMedical } from '../models/examen-medical.model';

export type PartialUpdateExamenMedical = Partial<IExamenMedical> & Pick<IExamenMedical, 'id'>;

export type EntityResponseType = HttpResponse<IExamenMedical>;
export type EntityArrayResponseType = HttpResponse<IExamenMedical[]>;

@Injectable({ providedIn: 'root' })
export class ExamenMedicalService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/examen-medicals');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(examenMedical: NewExamenMedical): Observable<EntityResponseType> {
    return this.http.post<IExamenMedical>(this.resourceUrl, examenMedical, { observe: 'response' });
  }

  update(examenMedical: IExamenMedical): Observable<EntityResponseType> {
    return this.http.put<IExamenMedical>(`${this.resourceUrl}/${this.getExamenMedicalIdentifier(examenMedical)}`, examenMedical, {
      observe: 'response',
    });
  }

  partialUpdate(examenMedical: PartialUpdateExamenMedical): Observable<EntityResponseType> {
    return this.http.patch<IExamenMedical>(`${this.resourceUrl}/${this.getExamenMedicalIdentifier(examenMedical)}`, examenMedical, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IExamenMedical>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExamenMedical[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getExamenMedicalIdentifier(examenMedical: Pick<IExamenMedical, 'id'>): number {
    return examenMedical.id;
  }

  compareExamenMedical(o1: Pick<IExamenMedical, 'id'> | null, o2: Pick<IExamenMedical, 'id'> | null): boolean {
    return o1 && o2 ? this.getExamenMedicalIdentifier(o1) === this.getExamenMedicalIdentifier(o2) : o1 === o2;
  }

  addExamenMedicalToCollectionIfMissing<Type extends Pick<IExamenMedical, 'id'>>(
    examenMedicalCollection: Type[],
    ...examenMedicalsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const examenMedicals: Type[] = examenMedicalsToCheck.filter(isPresent);
    if (examenMedicals.length > 0) {
      const examenMedicalCollectionIdentifiers = examenMedicalCollection.map(
        examenMedicalItem => this.getExamenMedicalIdentifier(examenMedicalItem)!
      );
      const examenMedicalsToAdd = examenMedicals.filter(examenMedicalItem => {
        const examenMedicalIdentifier = this.getExamenMedicalIdentifier(examenMedicalItem);
        if (examenMedicalCollectionIdentifiers.includes(examenMedicalIdentifier)) {
          return false;
        }
        examenMedicalCollectionIdentifiers.push(examenMedicalIdentifier);
        return true;
      });
      return [...examenMedicalsToAdd, ...examenMedicalCollection];
    }
    return examenMedicalCollection;
  }
}
