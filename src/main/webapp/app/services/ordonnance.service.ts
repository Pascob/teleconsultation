import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOrdonnance, NewOrdonnance } from '../models/ordonnance.model';

export type PartialUpdateOrdonnance = Partial<IOrdonnance> & Pick<IOrdonnance, 'id'>;

type RestOf<T extends IOrdonnance | NewOrdonnance> = Omit<T, 'dateOrdonnance'> & {
  dateOrdonnance?: string | null;
};

export type RestOrdonnance = RestOf<IOrdonnance>;

export type NewRestOrdonnance = RestOf<NewOrdonnance>;

export type PartialUpdateRestOrdonnance = RestOf<PartialUpdateOrdonnance>;

export type EntityResponseType = HttpResponse<IOrdonnance>;
export type EntityArrayResponseType = HttpResponse<IOrdonnance[]>;

@Injectable({ providedIn: 'root' })
export class OrdonnanceService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/ordonnances');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(ordonnance: NewOrdonnance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ordonnance);
    return this.http
      .post<RestOrdonnance>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(ordonnance: IOrdonnance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ordonnance);
    return this.http
      .put<RestOrdonnance>(`${this.resourceUrl}/${this.getOrdonnanceIdentifier(ordonnance)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(ordonnance: PartialUpdateOrdonnance): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ordonnance);
    return this.http
      .patch<RestOrdonnance>(`${this.resourceUrl}/${this.getOrdonnanceIdentifier(ordonnance)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestOrdonnance>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestOrdonnance[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getOrdonnanceIdentifier(ordonnance: Pick<IOrdonnance, 'id'>): number {
    return ordonnance.id;
  }

  compareOrdonnance(o1: Pick<IOrdonnance, 'id'> | null, o2: Pick<IOrdonnance, 'id'> | null): boolean {
    return o1 && o2 ? this.getOrdonnanceIdentifier(o1) === this.getOrdonnanceIdentifier(o2) : o1 === o2;
  }

  addOrdonnanceToCollectionIfMissing<Type extends Pick<IOrdonnance, 'id'>>(
    ordonnanceCollection: Type[],
    ...ordonnancesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const ordonnances: Type[] = ordonnancesToCheck.filter(isPresent);
    if (ordonnances.length > 0) {
      const ordonnanceCollectionIdentifiers = ordonnanceCollection.map(ordonnanceItem => this.getOrdonnanceIdentifier(ordonnanceItem)!);
      const ordonnancesToAdd = ordonnances.filter(ordonnanceItem => {
        const ordonnanceIdentifier = this.getOrdonnanceIdentifier(ordonnanceItem);
        if (ordonnanceCollectionIdentifiers.includes(ordonnanceIdentifier)) {
          return false;
        }
        ordonnanceCollectionIdentifiers.push(ordonnanceIdentifier);
        return true;
      });
      return [...ordonnancesToAdd, ...ordonnanceCollection];
    }
    return ordonnanceCollection;
  }

  protected convertDateFromClient<T extends IOrdonnance | NewOrdonnance | PartialUpdateOrdonnance>(ordonnance: T): RestOf<T> {
    return {
      ...ordonnance,
      dateOrdonnance: ordonnance.dateOrdonnance?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restOrdonnance: RestOrdonnance): IOrdonnance {
    return {
      ...restOrdonnance,
      dateOrdonnance: restOrdonnance.dateOrdonnance ? dayjs(restOrdonnance.dateOrdonnance) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestOrdonnance>): HttpResponse<IOrdonnance> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestOrdonnance[]>): HttpResponse<IOrdonnance[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
