import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDemandeConsultation } from '../models/demande-consultation.model';
import { DemandeConsultationService } from './demande-consultation.service';

@Injectable({ providedIn: 'root' })
export class DemandeConsultationRoutingResolveService implements Resolve<IDemandeConsultation | null> {
  constructor(protected service: DemandeConsultationService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDemandeConsultation | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((demandeConsultation: HttpResponse<IDemandeConsultation>) => {
          if (demandeConsultation.body) {
            return of(demandeConsultation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
