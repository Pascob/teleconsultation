import { TestBed } from '@angular/core/testing';

import { RtcStreamService } from './rtc-stream.service';

describe('RtcStreamService', () => {
  let service: RtcStreamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RtcStreamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
