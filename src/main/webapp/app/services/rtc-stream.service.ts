import { Injectable } from '@angular/core';
import AgoraRTC, { IAgoraRTCRemoteUser } from 'agora-rtc-sdk-ng';
import { BehaviorSubject } from 'rxjs';
import { IAgoraChannelParameter } from '../models/agora-channel-parameter';
import { IAgoraOptions } from '../models/agora-options';

export const APP_ID = '3042ef055cb44e7192709d29abec67ad';
export const CHANNEL = 'consultation';

@Injectable({
  providedIn: 'root',
})
export class RtcStreamService {
  rtc: IAgoraChannelParameter = {
    localAudioTrack: null,
    localVideoTrack: null,
    remoteAudioTrack: null,
    remoteUid: null,
    remoteVideoTrack: null,
    client: null,
  };

  options: IAgoraOptions = {
    appId: APP_ID,
    channel: 'consultation',
    token:
      '007eJxTYMg2exhwXOCh76Oip+LZxxeXLAiZf/Zhz96lFy3O5JcyhOsqMBgbmBilphmYmiYnmZikmhtaGpkbWKYYWSYmpSabmSembJ+xMrkhkJEhuLSUkZEBAkF8Hobk/Lzi0pySxJLM/DwGBgDXEyRM',
    uid: 0,
  };

  remoteUsers: IAgoraRTCRemoteUser[] = [];
  updateUserInfo = new BehaviorSubject<any>(null); // to update remote users name

  constructor() {}

  createRTCClient(): void {
    this.rtc.client = AgoraRTC.createClient({ mode: 'rtc', codec: 'h264' });
  }

  // To join a call with tracks (video or audio)
  async localUser(token: string | null, uuid: number | string | null): Promise<void> {
    if (this.rtc.client) {
      const uid = await this.rtc.client.join(this.options.appId, this.options.channel, token, uuid);

      // Create an audio track from the audio sampled by a microphone.
      this.rtc.localAudioTrack = await AgoraRTC.createMicrophoneAudioTrack();
      // Create a video track from the video captured by a camera.
      this.rtc.localVideoTrack = await AgoraRTC.createCameraVideoTrack({
        encoderConfig: '360p',
      });

      // Publish the local audio and video tracks to the channel.
      this.rtc.localVideoTrack.play('local-player');
      // channel for other users to subscribe to it.
      await this.rtc.client.publish([this.rtc.localAudioTrack, this.rtc.localVideoTrack]);
    }
  }

  agoraServerEvents(rtc: IAgoraChannelParameter): void {
    if (rtc.client) {
      rtc.client.on('user-published', async (user, mediaType) => {
        console.log(user, mediaType, 'user-published');

        await rtc.client!.subscribe(user, mediaType);
        if (mediaType === 'video') {
          const remoteVideoTrack = user.videoTrack;
          remoteVideoTrack?.play('remote-playerlist' + user.uid);
        }
        if (mediaType === 'audio') {
          const remoteAudioTrack = user.audioTrack;
          remoteAudioTrack?.play();
        }
      });

      rtc.client.on('user-unpublished', user => {
        console.log(user, 'user-unpublished');
      });

      rtc.client.on('user-joined', user => {
        let id = user.uid;
        this.remoteUsers.push({ hasAudio: false, hasVideo: false, uid: +id });
        this.updateUserInfo.next(id);
        console.log('user-joined', user, this.remoteUsers, 'event1');
      });
    }
  }

  // To leave channel-
  async leaveCall(): Promise<void> {
    if (this.rtc) {
      // Destroy the local audio and video tracks.
      this.rtc.localAudioTrack?.close();
      this.rtc.localVideoTrack?.close();
      // Traverse all remote users.
      this.rtc.client?.remoteUsers.forEach(user => {
        // Destroy the dynamically created DIV container.
        const playerContainer = document.getElementById('remote-playerlist' + user.uid.toString());
        playerContainer && playerContainer.remove();
      });
      // Leave the channel.
      await this.rtc.client?.leave();
    }
  }

  // rtc token
  async generateTokenAndUid(uid: number): Promise<ITokenUID> {
    // let url = 'https://test-agora.herokuapp.com/access_token?&#39;';
    // const opts = { params: new HttpParams({ fromString: "channel=test&uid=" + uid }) };
    // const data = await this.api.getRequest(url, opts.params).toPromise();
    // return { 'uid': uid, token: data['token'] };
    return { uid: uid, token: this.options.token };
  }

  generateUid(): number {
    const length = 5;
    return Math.floor(Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1));
  }
}

export interface ITokenUID {
  uid: number;
  token: string | null;
}
