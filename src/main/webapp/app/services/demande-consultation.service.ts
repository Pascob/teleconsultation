import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDemandeConsultation, NewDemandeConsultation } from '../models/demande-consultation.model';

export type PartialUpdateDemandeConsultation = Partial<IDemandeConsultation> & Pick<IDemandeConsultation, 'id'>;

type RestOf<T extends IDemandeConsultation | NewDemandeConsultation> = Omit<T, 'dateHeureDemande'> & {
  dateHeureDemande?: string | null;
};

export type RestDemandeConsultation = RestOf<IDemandeConsultation>;

export type NewRestDemandeConsultation = RestOf<NewDemandeConsultation>;

export type PartialUpdateRestDemandeConsultation = RestOf<PartialUpdateDemandeConsultation>;

export type EntityResponseType = HttpResponse<IDemandeConsultation>;
export type EntityArrayResponseType = HttpResponse<IDemandeConsultation[]>;

@Injectable({ providedIn: 'root' })
export class DemandeConsultationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/demande-consultations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(demandeConsultation: NewDemandeConsultation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(demandeConsultation);
    return this.http
      .post<RestDemandeConsultation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(demandeConsultation: IDemandeConsultation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(demandeConsultation);
    return this.http
      .put<RestDemandeConsultation>(`${this.resourceUrl}/${this.getDemandeConsultationIdentifier(demandeConsultation)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(demandeConsultation: PartialUpdateDemandeConsultation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(demandeConsultation);
    return this.http
      .patch<RestDemandeConsultation>(`${this.resourceUrl}/${this.getDemandeConsultationIdentifier(demandeConsultation)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestDemandeConsultation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestDemandeConsultation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getDemandeConsultationIdentifier(demandeConsultation: Pick<IDemandeConsultation, 'id'>): number {
    return demandeConsultation.id;
  }

  compareDemandeConsultation(o1: Pick<IDemandeConsultation, 'id'> | null, o2: Pick<IDemandeConsultation, 'id'> | null): boolean {
    return o1 && o2 ? this.getDemandeConsultationIdentifier(o1) === this.getDemandeConsultationIdentifier(o2) : o1 === o2;
  }

  addDemandeConsultationToCollectionIfMissing<Type extends Pick<IDemandeConsultation, 'id'>>(
    demandeConsultationCollection: Type[],
    ...demandeConsultationsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const demandeConsultations: Type[] = demandeConsultationsToCheck.filter(isPresent);
    if (demandeConsultations.length > 0) {
      const demandeConsultationCollectionIdentifiers = demandeConsultationCollection.map(
        demandeConsultationItem => this.getDemandeConsultationIdentifier(demandeConsultationItem)!
      );
      const demandeConsultationsToAdd = demandeConsultations.filter(demandeConsultationItem => {
        const demandeConsultationIdentifier = this.getDemandeConsultationIdentifier(demandeConsultationItem);
        if (demandeConsultationCollectionIdentifiers.includes(demandeConsultationIdentifier)) {
          return false;
        }
        demandeConsultationCollectionIdentifiers.push(demandeConsultationIdentifier);
        return true;
      });
      return [...demandeConsultationsToAdd, ...demandeConsultationCollection];
    }
    return demandeConsultationCollection;
  }

  protected convertDateFromClient<T extends IDemandeConsultation | NewDemandeConsultation | PartialUpdateDemandeConsultation>(
    demandeConsultation: T
  ): RestOf<T> {
    return {
      ...demandeConsultation,
      dateHeureDemande: demandeConsultation.dateHeureDemande?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restDemandeConsultation: RestDemandeConsultation): IDemandeConsultation {
    return {
      ...restDemandeConsultation,
      dateHeureDemande: restDemandeConsultation.dateHeureDemande ? dayjs(restDemandeConsultation.dateHeureDemande) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestDemandeConsultation>): HttpResponse<IDemandeConsultation> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestDemandeConsultation[]>): HttpResponse<IDemandeConsultation[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
