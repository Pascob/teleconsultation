import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFormationSanitaire, NewFormationSanitaire } from '../models/formation-sanitaire.model';

export type PartialUpdateFormationSanitaire = Partial<IFormationSanitaire> & Pick<IFormationSanitaire, 'id'>;

export type EntityResponseType = HttpResponse<IFormationSanitaire>;
export type EntityArrayResponseType = HttpResponse<IFormationSanitaire[]>;

@Injectable({ providedIn: 'root' })
export class FormationSanitaireService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/formation-sanitaires');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(formationSanitaire: NewFormationSanitaire): Observable<EntityResponseType> {
    return this.http.post<IFormationSanitaire>(this.resourceUrl, formationSanitaire, { observe: 'response' });
  }

  update(formationSanitaire: IFormationSanitaire): Observable<EntityResponseType> {
    return this.http.put<IFormationSanitaire>(
      `${this.resourceUrl}/${this.getFormationSanitaireIdentifier(formationSanitaire)}`,
      formationSanitaire,
      { observe: 'response' }
    );
  }

  partialUpdate(formationSanitaire: PartialUpdateFormationSanitaire): Observable<EntityResponseType> {
    return this.http.patch<IFormationSanitaire>(
      `${this.resourceUrl}/${this.getFormationSanitaireIdentifier(formationSanitaire)}`,
      formationSanitaire,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFormationSanitaire>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFormationSanitaire[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getFormationSanitaireIdentifier(formationSanitaire: Pick<IFormationSanitaire, 'id'>): number {
    return formationSanitaire.id;
  }

  compareFormationSanitaire(o1: Pick<IFormationSanitaire, 'id'> | null, o2: Pick<IFormationSanitaire, 'id'> | null): boolean {
    return o1 && o2 ? this.getFormationSanitaireIdentifier(o1) === this.getFormationSanitaireIdentifier(o2) : o1 === o2;
  }

  addFormationSanitaireToCollectionIfMissing<Type extends Pick<IFormationSanitaire, 'id'>>(
    formationSanitaireCollection: Type[],
    ...formationSanitairesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const formationSanitaires: Type[] = formationSanitairesToCheck.filter(isPresent);
    if (formationSanitaires.length > 0) {
      const formationSanitaireCollectionIdentifiers = formationSanitaireCollection.map(
        formationSanitaireItem => this.getFormationSanitaireIdentifier(formationSanitaireItem)!
      );
      const formationSanitairesToAdd = formationSanitaires.filter(formationSanitaireItem => {
        const formationSanitaireIdentifier = this.getFormationSanitaireIdentifier(formationSanitaireItem);
        if (formationSanitaireCollectionIdentifiers.includes(formationSanitaireIdentifier)) {
          return false;
        }
        formationSanitaireCollectionIdentifiers.push(formationSanitaireIdentifier);
        return true;
      });
      return [...formationSanitairesToAdd, ...formationSanitaireCollection];
    }
    return formationSanitaireCollection;
  }
}
