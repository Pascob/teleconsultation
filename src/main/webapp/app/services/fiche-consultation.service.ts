import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFicheConsultation, NewFicheConsultation } from '../models/fiche-consultation.model';

export type PartialUpdateFicheConsultation = Partial<IFicheConsultation> & Pick<IFicheConsultation, 'id'>;

type RestOf<T extends IFicheConsultation | NewFicheConsultation> = Omit<T, 'dateFiche'> & {
  dateFiche?: string | null;
};

export type RestFicheConsultation = RestOf<IFicheConsultation>;

export type NewRestFicheConsultation = RestOf<NewFicheConsultation>;

export type PartialUpdateRestFicheConsultation = RestOf<PartialUpdateFicheConsultation>;

export type EntityResponseType = HttpResponse<IFicheConsultation>;
export type EntityArrayResponseType = HttpResponse<IFicheConsultation[]>;

@Injectable({ providedIn: 'root' })
export class FicheConsultationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/fiche-consultations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(ficheConsultation: NewFicheConsultation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ficheConsultation);
    return this.http
      .post<RestFicheConsultation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(ficheConsultation: IFicheConsultation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ficheConsultation);
    return this.http
      .put<RestFicheConsultation>(`${this.resourceUrl}/${this.getFicheConsultationIdentifier(ficheConsultation)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(ficheConsultation: PartialUpdateFicheConsultation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ficheConsultation);
    return this.http
      .patch<RestFicheConsultation>(`${this.resourceUrl}/${this.getFicheConsultationIdentifier(ficheConsultation)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestFicheConsultation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestFicheConsultation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getFicheConsultationIdentifier(ficheConsultation: Pick<IFicheConsultation, 'id'>): number {
    return ficheConsultation.id;
  }

  compareFicheConsultation(o1: Pick<IFicheConsultation, 'id'> | null, o2: Pick<IFicheConsultation, 'id'> | null): boolean {
    return o1 && o2 ? this.getFicheConsultationIdentifier(o1) === this.getFicheConsultationIdentifier(o2) : o1 === o2;
  }

  addFicheConsultationToCollectionIfMissing<Type extends Pick<IFicheConsultation, 'id'>>(
    ficheConsultationCollection: Type[],
    ...ficheConsultationsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const ficheConsultations: Type[] = ficheConsultationsToCheck.filter(isPresent);
    if (ficheConsultations.length > 0) {
      const ficheConsultationCollectionIdentifiers = ficheConsultationCollection.map(
        ficheConsultationItem => this.getFicheConsultationIdentifier(ficheConsultationItem)!
      );
      const ficheConsultationsToAdd = ficheConsultations.filter(ficheConsultationItem => {
        const ficheConsultationIdentifier = this.getFicheConsultationIdentifier(ficheConsultationItem);
        if (ficheConsultationCollectionIdentifiers.includes(ficheConsultationIdentifier)) {
          return false;
        }
        ficheConsultationCollectionIdentifiers.push(ficheConsultationIdentifier);
        return true;
      });
      return [...ficheConsultationsToAdd, ...ficheConsultationCollection];
    }
    return ficheConsultationCollection;
  }

  protected convertDateFromClient<T extends IFicheConsultation | NewFicheConsultation | PartialUpdateFicheConsultation>(
    ficheConsultation: T
  ): RestOf<T> {
    return {
      ...ficheConsultation,
      dateFiche: ficheConsultation.dateFiche?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restFicheConsultation: RestFicheConsultation): IFicheConsultation {
    return {
      ...restFicheConsultation,
      dateFiche: restFicheConsultation.dateFiche ? dayjs(restFicheConsultation.dateFiche) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestFicheConsultation>): HttpResponse<IFicheConsultation> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestFicheConsultation[]>): HttpResponse<IFicheConsultation[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
