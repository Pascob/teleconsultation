import { Injectable } from '@angular/core';
import { ITEMS_PER_PAGE } from '../config/pagination.constants';
import { from, Observable } from 'rxjs';
import Swal from 'sweetalert2';

const USER_SESSION_KEY = 'sss_id_!_used';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  constructor() {}

  setUserIdInSession(userId?: number): void {
    window.sessionStorage.setItem(USER_SESSION_KEY, JSON.stringify(userId));
  }

  getUserIdInSession(): number {
    let id = 0;
    const idSession = window.sessionStorage.getItem(USER_SESSION_KEY);
    if (idSession) {
      id = JSON.parse(idSession);
    }
    return id;
  }

  sortMethod(predicate: string, reverse: boolean): string[] {
    let rev = true;
    if (reverse != null) {
      rev = reverse;
    }
    return [predicate + ',' + (rev ? 'asc' : 'desc')];
  }

  buildReq(requestHeaders: IRequestHeader[], page?: number, size?: number, defaulSort?: string, sortReverse?: boolean): any {
    const req = {
      page: page || 0,
      size: size || ITEMS_PER_PAGE,
      sort: this.sortMethod(defaulSort || 'id', sortReverse || false),
    };

    return this.requestHeadersInObjet(requestHeaders, req);
  }

  private requestHeadersInObjet(requestHeaders: IRequestHeader[], req: any): any {
    requestHeaders.forEach(request => {
      const obj = {};
      const id = request.propriete!.concat('.').concat(request.expression!);
      if ((request.value != undefined && request.value != '') || (typeof request.value == 'boolean' && !Boolean(request.value))) {
        // @ts-ignore
        obj[id] = request.value;
        req = Object.assign({}, req, obj);
      }
    });

    return req;
  }

  afficherConfirmation(
    titre: string,
    message: string,
    icon: 'success' | 'error' | 'warning' | 'info' | 'question' | undefined
  ): Observable<any> {
    return from(
      Swal.fire({
        title: titre,
        html: '<span class="text-bold">' + message + '</span>',
        icon: icon,
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: 'Oui, je confirme',
        confirmButtonColor: 'green',
        cancelButtonText: "Non, j'annule",
        cancelButtonColor: 'red',
        // showLoaderOnConfirm: true,
        // allowOutsideClick: () => !Swal.isLoading(),
      }).then()
    );
  }
}

export interface IRequestHeader {
  propriete?: string;
  expression?:
    | 'equals'
    | 'contains'
    | 'specified'
    | 'in'
    | 'notIn'
    | 'greaterThan'
    | 'lessThan'
    | 'greaterThanOrEqual'
    | 'lessThanOrEqual'
    | 'notEquals';
  value?: any;
}

export class RequestHeader implements IRequestHeader {
  constructor(
    public propriete: string,
    public expression:
      | 'equals'
      | 'contains'
      | 'specified'
      | 'in'
      | 'notIn'
      | 'greaterThan'
      | 'lessThan'
      | 'greaterThanOrEqual'
      | 'lessThanOrEqual'
      | 'notEquals',
    public value: any
  ) {}
}
