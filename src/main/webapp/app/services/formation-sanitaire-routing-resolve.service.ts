import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IFormationSanitaire } from '../models/formation-sanitaire.model';
import { FormationSanitaireService } from './formation-sanitaire.service';

@Injectable({ providedIn: 'root' })
export class FormationSanitaireRoutingResolveService implements Resolve<IFormationSanitaire | null> {
  constructor(protected service: FormationSanitaireService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFormationSanitaire | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((formationSanitaire: HttpResponse<IFormationSanitaire>) => {
          if (formationSanitaire.body) {
            return of(formationSanitaire.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
