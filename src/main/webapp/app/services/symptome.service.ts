import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISymptome, NewSymptome } from '../models/symptome.model';

export type PartialUpdateSymptome = Partial<ISymptome> & Pick<ISymptome, 'id'>;

export type EntityResponseType = HttpResponse<ISymptome>;
export type EntityArrayResponseType = HttpResponse<ISymptome[]>;

@Injectable({ providedIn: 'root' })
export class SymptomeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/symptomes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(symptome: NewSymptome): Observable<EntityResponseType> {
    return this.http.post<ISymptome>(this.resourceUrl, symptome, { observe: 'response' });
  }

  update(symptome: ISymptome): Observable<EntityResponseType> {
    return this.http.put<ISymptome>(`${this.resourceUrl}/${this.getSymptomeIdentifier(symptome)}`, symptome, { observe: 'response' });
  }

  partialUpdate(symptome: PartialUpdateSymptome): Observable<EntityResponseType> {
    return this.http.patch<ISymptome>(`${this.resourceUrl}/${this.getSymptomeIdentifier(symptome)}`, symptome, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISymptome>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISymptome[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSymptomeIdentifier(symptome: Pick<ISymptome, 'id'>): number {
    return symptome.id;
  }

  compareSymptome(o1: Pick<ISymptome, 'id'> | null, o2: Pick<ISymptome, 'id'> | null): boolean {
    return o1 && o2 ? this.getSymptomeIdentifier(o1) === this.getSymptomeIdentifier(o2) : o1 === o2;
  }

  addSymptomeToCollectionIfMissing<Type extends Pick<ISymptome, 'id'>>(
    symptomeCollection: Type[],
    ...symptomesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const symptomes: Type[] = symptomesToCheck.filter(isPresent);
    if (symptomes.length > 0) {
      const symptomeCollectionIdentifiers = symptomeCollection.map(symptomeItem => this.getSymptomeIdentifier(symptomeItem)!);
      const symptomesToAdd = symptomes.filter(symptomeItem => {
        const symptomeIdentifier = this.getSymptomeIdentifier(symptomeItem);
        if (symptomeCollectionIdentifiers.includes(symptomeIdentifier)) {
          return false;
        }
        symptomeCollectionIdentifiers.push(symptomeIdentifier);
        return true;
      });
      return [...symptomesToAdd, ...symptomeCollection];
    }
    return symptomeCollection;
  }
}
