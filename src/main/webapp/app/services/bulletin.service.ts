import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBulletin, NewBulletin } from '../models/bulletin.model';

export type PartialUpdateBulletin = Partial<IBulletin> & Pick<IBulletin, 'id'>;

type RestOf<T extends IBulletin | NewBulletin> = Omit<T, 'dateBulletin'> & {
  dateBulletin?: string | null;
};

export type RestBulletin = RestOf<IBulletin>;

export type NewRestBulletin = RestOf<NewBulletin>;

export type PartialUpdateRestBulletin = RestOf<PartialUpdateBulletin>;

export type EntityResponseType = HttpResponse<IBulletin>;
export type EntityArrayResponseType = HttpResponse<IBulletin[]>;

@Injectable({ providedIn: 'root' })
export class BulletinService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/bulletins');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(bulletin: NewBulletin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bulletin);
    return this.http
      .post<RestBulletin>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(bulletin: IBulletin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bulletin);
    return this.http
      .put<RestBulletin>(`${this.resourceUrl}/${this.getBulletinIdentifier(bulletin)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(bulletin: PartialUpdateBulletin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bulletin);
    return this.http
      .patch<RestBulletin>(`${this.resourceUrl}/${this.getBulletinIdentifier(bulletin)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestBulletin>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestBulletin[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getBulletinIdentifier(bulletin: Pick<IBulletin, 'id'>): number {
    return bulletin.id;
  }

  compareBulletin(o1: Pick<IBulletin, 'id'> | null, o2: Pick<IBulletin, 'id'> | null): boolean {
    return o1 && o2 ? this.getBulletinIdentifier(o1) === this.getBulletinIdentifier(o2) : o1 === o2;
  }

  addBulletinToCollectionIfMissing<Type extends Pick<IBulletin, 'id'>>(
    bulletinCollection: Type[],
    ...bulletinsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const bulletins: Type[] = bulletinsToCheck.filter(isPresent);
    if (bulletins.length > 0) {
      const bulletinCollectionIdentifiers = bulletinCollection.map(bulletinItem => this.getBulletinIdentifier(bulletinItem)!);
      const bulletinsToAdd = bulletins.filter(bulletinItem => {
        const bulletinIdentifier = this.getBulletinIdentifier(bulletinItem);
        if (bulletinCollectionIdentifiers.includes(bulletinIdentifier)) {
          return false;
        }
        bulletinCollectionIdentifiers.push(bulletinIdentifier);
        return true;
      });
      return [...bulletinsToAdd, ...bulletinCollection];
    }
    return bulletinCollection;
  }

  protected convertDateFromClient<T extends IBulletin | NewBulletin | PartialUpdateBulletin>(bulletin: T): RestOf<T> {
    return {
      ...bulletin,
      dateBulletin: bulletin.dateBulletin?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restBulletin: RestBulletin): IBulletin {
    return {
      ...restBulletin,
      dateBulletin: restBulletin.dateBulletin ? dayjs(restBulletin.dateBulletin) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestBulletin>): HttpResponse<IBulletin> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestBulletin[]>): HttpResponse<IBulletin[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
