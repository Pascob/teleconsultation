import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IDemandeConsultation, NewDemandeConsultation } from '../../models/demande-consultation.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IDemandeConsultation for edit and NewDemandeConsultationFormGroupInput for create.
 */
type DemandeConsultationFormGroupInput = IDemandeConsultation | PartialWithRequiredKeyOf<NewDemandeConsultation>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IDemandeConsultation | NewDemandeConsultation> = Omit<T, 'dateHeureDemande'> & {
  dateHeureDemande?: string | null;
};

type DemandeConsultationFormRawValue = FormValueOf<IDemandeConsultation>;

type NewDemandeConsultationFormRawValue = FormValueOf<NewDemandeConsultation>;

type DemandeConsultationFormDefaults = Pick<NewDemandeConsultation, 'id' | 'dateHeureDemande'>;

type DemandeConsultationFormGroupContent = {
  id: FormControl<DemandeConsultationFormRawValue['id'] | NewDemandeConsultation['id']>;
  dateHeureDemande: FormControl<DemandeConsultationFormRawValue['dateHeureDemande']>;
  ordre: FormControl<DemandeConsultationFormRawValue['ordre']>;
  statut: FormControl<DemandeConsultationFormRawValue['statut']>;
  codeOtp: FormControl<DemandeConsultationFormRawValue['codeOtp']>;
  telephonePaiement: FormControl<DemandeConsultationFormRawValue['telephonePaiement']>;
  montant: FormControl<DemandeConsultationFormRawValue['montant']>;
  specialite: FormControl<DemandeConsultationFormRawValue['specialite']>;
  dossierMedical: FormControl<DemandeConsultationFormRawValue['dossierMedical']>;
  moyenPaiment: FormControl<DemandeConsultationFormRawValue['moyenPaiment']>;
};

export type DemandeConsultationFormGroup = FormGroup<DemandeConsultationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class DemandeConsultationFormService {
  createDemandeConsultationFormGroup(demandeConsultation: DemandeConsultationFormGroupInput = { id: null }): DemandeConsultationFormGroup {
    const demandeConsultationRawValue = this.convertDemandeConsultationToDemandeConsultationRawValue({
      ...this.getFormDefaults(),
      ...demandeConsultation,
    });
    return new FormGroup<DemandeConsultationFormGroupContent>({
      id: new FormControl(
        { value: demandeConsultationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateHeureDemande: new FormControl(demandeConsultationRawValue.dateHeureDemande),
      ordre: new FormControl(demandeConsultationRawValue.ordre),
      statut: new FormControl(demandeConsultationRawValue.statut),
      codeOtp: new FormControl(demandeConsultationRawValue.codeOtp),
      telephonePaiement: new FormControl(demandeConsultationRawValue.telephonePaiement),
      montant: new FormControl(demandeConsultationRawValue.montant, {
        validators: [Validators.min(0)],
      }),
      specialite: new FormControl(demandeConsultationRawValue.specialite),
      dossierMedical: new FormControl(demandeConsultationRawValue.dossierMedical),
      moyenPaiment: new FormControl(demandeConsultationRawValue.moyenPaiment),
    });
  }

  getDemandeConsultation(form: DemandeConsultationFormGroup): IDemandeConsultation | NewDemandeConsultation {
    return this.convertDemandeConsultationRawValueToDemandeConsultation(
      form.getRawValue() as DemandeConsultationFormRawValue | NewDemandeConsultationFormRawValue
    );
  }

  resetForm(form: DemandeConsultationFormGroup, demandeConsultation: DemandeConsultationFormGroupInput): void {
    const demandeConsultationRawValue = this.convertDemandeConsultationToDemandeConsultationRawValue({
      ...this.getFormDefaults(),
      ...demandeConsultation,
    });
    form.reset(
      {
        ...demandeConsultationRawValue,
        id: { value: demandeConsultationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): DemandeConsultationFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      dateHeureDemande: currentTime,
    };
  }

  private convertDemandeConsultationRawValueToDemandeConsultation(
    rawDemandeConsultation: DemandeConsultationFormRawValue | NewDemandeConsultationFormRawValue
  ): IDemandeConsultation | NewDemandeConsultation {
    return {
      ...rawDemandeConsultation,
      dateHeureDemande: dayjs(rawDemandeConsultation.dateHeureDemande, DATE_TIME_FORMAT),
    };
  }

  private convertDemandeConsultationToDemandeConsultationRawValue(
    demandeConsultation: IDemandeConsultation | (Partial<NewDemandeConsultation> & DemandeConsultationFormDefaults)
  ): DemandeConsultationFormRawValue | PartialWithRequiredKeyOf<NewDemandeConsultationFormRawValue> {
    return {
      ...demandeConsultation,
      dateHeureDemande: demandeConsultation.dateHeureDemande ? demandeConsultation.dateHeureDemande.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
