import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { DemandeConsultationFormService, DemandeConsultationFormGroup } from './demande-consultation-form.service';
import { IDemandeConsultation } from '../../models/demande-consultation.model';
import { DemandeConsultationService } from '../demande-consultation.service';
import { IDossierMedical } from 'app/models/dossier-medical.model';
import { DossierMedicalService } from 'app/services/dossier-medical.service';
import { StatutDemandeConsultation } from 'app/enumerations/statut-demande-consultation.model';
import { ISpecialite } from '../../models/specialite.model';
import { SpecialiteService } from '../specialite.service';

@Component({
  selector: 'jhi-demande-consultation-update',
  templateUrl: '../../entities/demande-consultation/update/demande-consultation-update.component.html',
})
export class DemandeConsultationUpdateComponent implements OnInit {
  isSaving = false;
  demandeConsultation: IDemandeConsultation | null = null;
  statutDemandeConsultationValues = Object.keys(StatutDemandeConsultation);

  specialitesSharedCollection: ISpecialite[] = [];
  dossierMedicalsSharedCollection: IDossierMedical[] = [];

  editForm: DemandeConsultationFormGroup = this.demandeConsultationFormService.createDemandeConsultationFormGroup();

  constructor(
    protected demandeConsultationService: DemandeConsultationService,
    protected demandeConsultationFormService: DemandeConsultationFormService,
    protected specialiteService: SpecialiteService,
    protected dossierMedicalService: DossierMedicalService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareSpecialite = (o1: ISpecialite | null, o2: ISpecialite | null): boolean => this.specialiteService.compareSpecialite(o1, o2);

  compareDossierMedical = (o1: IDossierMedical | null, o2: IDossierMedical | null): boolean =>
    this.dossierMedicalService.compareDossierMedical(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ demandeConsultation }) => {
      this.demandeConsultation = demandeConsultation;
      if (demandeConsultation) {
        this.updateForm(demandeConsultation);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const demandeConsultation = this.demandeConsultationFormService.getDemandeConsultation(this.editForm);
    if (demandeConsultation.id !== null) {
      this.subscribeToSaveResponse(this.demandeConsultationService.update(demandeConsultation));
    } else {
      this.subscribeToSaveResponse(this.demandeConsultationService.create(demandeConsultation));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDemandeConsultation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(demandeConsultation: IDemandeConsultation): void {
    this.demandeConsultation = demandeConsultation;
    this.demandeConsultationFormService.resetForm(this.editForm, demandeConsultation);

    this.specialitesSharedCollection = this.specialiteService.addSpecialiteToCollectionIfMissing<ISpecialite>(
      this.specialitesSharedCollection,
      demandeConsultation.specialite
    );
    this.dossierMedicalsSharedCollection = this.dossierMedicalService.addDossierMedicalToCollectionIfMissing<IDossierMedical>(
      this.dossierMedicalsSharedCollection,
      demandeConsultation.dossierMedical
    );
  }

  protected loadRelationshipsOptions(): void {
    this.specialiteService
      .query()
      .pipe(map((res: HttpResponse<ISpecialite[]>) => res.body ?? []))
      .pipe(
        map((specialites: ISpecialite[]) =>
          this.specialiteService.addSpecialiteToCollectionIfMissing<ISpecialite>(specialites, this.demandeConsultation?.specialite)
        )
      )
      .subscribe((specialites: ISpecialite[]) => (this.specialitesSharedCollection = specialites));

    this.dossierMedicalService
      .query()
      .pipe(map((res: HttpResponse<IDossierMedical[]>) => res.body ?? []))
      .pipe(
        map((dossierMedicals: IDossierMedical[]) =>
          this.dossierMedicalService.addDossierMedicalToCollectionIfMissing<IDossierMedical>(
            dossierMedicals,
            this.demandeConsultation?.dossierMedical
          )
        )
      )
      .subscribe((dossierMedicals: IDossierMedical[]) => (this.dossierMedicalsSharedCollection = dossierMedicals));
  }
}
