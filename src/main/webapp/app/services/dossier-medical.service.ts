import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDossierMedical, NewDossierMedical } from '../models/dossier-medical.model';

export type PartialUpdateDossierMedical = Partial<IDossierMedical> & Pick<IDossierMedical, 'id'>;

export type EntityResponseType = HttpResponse<IDossierMedical>;
export type EntityArrayResponseType = HttpResponse<IDossierMedical[]>;

@Injectable({ providedIn: 'root' })
export class DossierMedicalService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/dossier-medicals');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(dossierMedical: NewDossierMedical): Observable<EntityResponseType> {
    return this.http.post<IDossierMedical>(this.resourceUrl, dossierMedical, { observe: 'response' });
  }

  update(dossierMedical: IDossierMedical): Observable<EntityResponseType> {
    return this.http.put<IDossierMedical>(`${this.resourceUrl}/${this.getDossierMedicalIdentifier(dossierMedical)}`, dossierMedical, {
      observe: 'response',
    });
  }

  partialUpdate(dossierMedical: PartialUpdateDossierMedical): Observable<EntityResponseType> {
    return this.http.patch<IDossierMedical>(`${this.resourceUrl}/${this.getDossierMedicalIdentifier(dossierMedical)}`, dossierMedical, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDossierMedical>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDossierMedical[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getDossierMedicalIdentifier(dossierMedical: Pick<IDossierMedical, 'id'>): number {
    return dossierMedical.id;
  }

  compareDossierMedical(o1: Pick<IDossierMedical, 'id'> | null, o2: Pick<IDossierMedical, 'id'> | null): boolean {
    return o1 && o2 ? this.getDossierMedicalIdentifier(o1) === this.getDossierMedicalIdentifier(o2) : o1 === o2;
  }

  addDossierMedicalToCollectionIfMissing<Type extends Pick<IDossierMedical, 'id'>>(
    dossierMedicalCollection: Type[],
    ...dossierMedicalsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const dossierMedicals: Type[] = dossierMedicalsToCheck.filter(isPresent);
    if (dossierMedicals.length > 0) {
      const dossierMedicalCollectionIdentifiers = dossierMedicalCollection.map(
        dossierMedicalItem => this.getDossierMedicalIdentifier(dossierMedicalItem)!
      );
      const dossierMedicalsToAdd = dossierMedicals.filter(dossierMedicalItem => {
        const dossierMedicalIdentifier = this.getDossierMedicalIdentifier(dossierMedicalItem);
        if (dossierMedicalCollectionIdentifiers.includes(dossierMedicalIdentifier)) {
          return false;
        }
        dossierMedicalCollectionIdentifiers.push(dossierMedicalIdentifier);
        return true;
      });
      return [...dossierMedicalsToAdd, ...dossierMedicalCollection];
    }
    return dossierMedicalCollection;
  }
}
