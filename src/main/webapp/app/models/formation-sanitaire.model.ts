export interface IFormationSanitaire {
  id: number;
  designation?: string | null;
  sigle?: string | null;
  telephone?: string | null;
  fixe?: string | null;
  bp?: string | null;
  email?: string | null;
  logo?: string | null;
  logoContentType?: string | null;
}

export type NewFormationSanitaire = Omit<IFormationSanitaire, 'id'> & { id: null };
