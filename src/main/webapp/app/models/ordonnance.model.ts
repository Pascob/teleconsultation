import dayjs from 'dayjs/esm';
import { IFicheConsultation } from 'app/models/fiche-consultation.model';

export interface IOrdonnance {
  id: number;
  dateOrdonnance?: dayjs.Dayjs | null;
  numero?: string | null;
  ficheConsultation?: Pick<IFicheConsultation, 'id'> | null;
}

export type NewOrdonnance = Omit<IOrdonnance, 'id'> & { id: null };
