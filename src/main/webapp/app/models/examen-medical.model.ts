export interface IExamenMedical {
  id: number;
  designation?: string | null;
  code?: string | null;
}

export type NewExamenMedical = Omit<IExamenMedical, 'id'> & { id: null };
