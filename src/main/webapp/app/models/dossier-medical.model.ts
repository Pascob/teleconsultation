import { IMedecin } from 'app/models/medecin.model';
import { GroupeSanguin } from 'app/enumerations/groupe-sanguin.model';

export interface IDossierMedical {
  id: number;
  numero?: string | null;
  groupeSanguin?: GroupeSanguin | null;
  medecin?: Pick<IMedecin, 'id'> | null;
}

export type NewDossierMedical = Omit<IDossierMedical, 'id'> & { id: null };
