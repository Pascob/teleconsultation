import dayjs from 'dayjs/esm';
import { IMedecin } from 'app/models/medecin.model';
import { IDemandeConsultation } from 'app/models/demande-consultation.model';

export interface IConsultation {
  id: number;
  poids?: number | null;
  temperature?: number | null;
  pouls?: number | null;
  bpm?: number | null;
  age?: number | null;
  taille?: number | null;
  dateDebutConsulation?: dayjs.Dayjs | null;
  isSuivi?: boolean | null;
  isClose?: boolean | null;
  medecin?: Pick<IMedecin, 'id'> | null;
  demandeConsultation?: Pick<IDemandeConsultation, 'id'> | null;
}

export type NewConsultation = Omit<IConsultation, 'id'> & { id: null };
