export interface ISpecialite {
  id: number;
  designation?: string | null;
  frais?: number | null;
}

export type NewSpecialite = Omit<ISpecialite, 'id'> & { id: null };
