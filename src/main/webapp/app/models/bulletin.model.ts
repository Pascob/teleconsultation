import dayjs from 'dayjs/esm';
import { IFicheConsultation } from 'app/models/fiche-consultation.model';

export interface IBulletin {
  id: number;
  dateBulletin?: dayjs.Dayjs | null;
  numero?: string | null;
  ficheConsultation?: Pick<IFicheConsultation, 'id'> | null;
}

export type NewBulletin = Omit<IBulletin, 'id'> & { id: null };
