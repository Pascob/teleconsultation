export interface IAgoraOptions {
  appId: string;
  // Set the channel name.
  channel: string | '';
  // Pass your temp token here.
  token: string | null;
  // Set the user ID.
  uid: number | string | null;
}
