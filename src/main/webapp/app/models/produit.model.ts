export interface IProduit {
  id: number;
  designation?: string | null;
}

export type NewProduit = Omit<IProduit, 'id'> & { id: null };
