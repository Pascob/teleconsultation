import { IFormationSanitaire } from './formation-sanitaire.model';
import { ISpecialite } from './specialite.model';

export interface IMedecin {
  id: number;
  userId?: number | null;
  matricule?: string | null;
  signature?: string | null;
  signatureContentType?: string | null;
  formationSanitaire?: Pick<IFormationSanitaire, 'id'> | null;
  specialite?: Pick<ISpecialite, 'id'> | null;
}

export type NewMedecin = Omit<IMedecin, 'id'> & { id: null };
