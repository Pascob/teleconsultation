import { IAgoraRTCClient, ICameraVideoTrack, IMicrophoneAudioTrack, IRemoteAudioTrack, IRemoteVideoTrack } from 'agora-rtc-sdk-ng';

export interface IAgoraChannelParameter {
  localAudioTrack: IMicrophoneAudioTrack | null;
  // A variable to hold a local video track.
  localVideoTrack: ICameraVideoTrack | null;
  // A variable to hold a remote audio track.
  remoteAudioTrack: IRemoteAudioTrack | null;
  // A variable to hold a remote video track.
  remoteVideoTrack: IRemoteVideoTrack | null;
  // A variable to hold the remote user id.s
  remoteUid: string | null;
  // client
  client: IAgoraRTCClient | null;
}
