export interface ISymptome {
  id: number;
  designation?: string | null;
  code?: string | null;
}

export type NewSymptome = Omit<ISymptome, 'id'> & { id: null };
