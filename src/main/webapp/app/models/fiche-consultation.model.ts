import dayjs from 'dayjs/esm';
import { IMedecin } from 'app/models/medecin.model';
import { IConsultation } from 'app/models/consultation.model';

export interface IFicheConsultation {
  id: number;
  dateFiche?: dayjs.Dayjs | null;
  numero?: string | null;
  observation?: string | null;
  diagnostic?: string | null;
  medecin?: Pick<IMedecin, 'id'> | null;
  consultation?: Pick<IConsultation, 'id'> | null;
}

export type NewFicheConsultation = Omit<IFicheConsultation, 'id'> & { id: null };
