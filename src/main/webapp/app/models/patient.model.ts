import dayjs from 'dayjs/esm';
import { IDossierMedical } from 'app/models/dossier-medical.model';
import { Genre } from 'app/enumerations/genre.model';
import { IUser } from '../admin/user-management/user-management.model';

export interface IPatient {
  id: number;
  userId?: number | null;
  dateNaissance?: dayjs.Dayjs | null;
  genre?: Genre | null;
  dossierMedical?: Pick<IDossierMedical, 'id'> | null;
  dossier?: IDossierMedical;
  user?: IUser;
}

export type NewPatient = Omit<IPatient, 'id'> & { id: null };
