import dayjs from 'dayjs/esm';
import { IDossierMedical } from 'app/models/dossier-medical.model';
import { StatutDemandeConsultation } from 'app/enumerations/statut-demande-consultation.model';
import { ISpecialite } from './specialite.model';

export interface IDemandeConsultation {
  id: number;
  dateHeureDemande?: dayjs.Dayjs | null;
  ordre?: number | null;
  statut?: StatutDemandeConsultation | null;
  codeOtp?: string | null;
  telephonePaiement?: string | null;
  montant?: number | null;
  specialite?: Pick<ISpecialite, 'id'> | null;
  dossierMedical?: Pick<IDossierMedical, 'id'> | null;
  specialiteDesignation?: string;
  moyenPaiment?: string | null;
}

export type NewDemandeConsultation = Omit<IDemandeConsultation, 'id'> & { id: null };
