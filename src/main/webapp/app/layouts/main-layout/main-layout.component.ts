import { Component, OnInit } from '@angular/core';
import { Account } from '../../core/auth/account.model';
import { AccountService } from '../../core/auth/account.service';
import { SessionStorageService } from 'ngx-webstorage';
import { LoginService } from '../../login/login.service';
import { Router } from '@angular/router';
import { Authority } from '../../config/authority.constants';

@Component({
  selector: 'jhi-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  account: Account | null = null;
  enumAutority = Authority;

  constructor(
    private loginService: LoginService,
    private sessionStorageService: SessionStorageService,
    private accountService: AccountService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.accountService.getAuthenticationState().subscribe(account => {
      this.account = account;
    });

    console.warn('URL', location.href);

    this.manageMenu();
  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  private manageMenu(): void {
    const allSideMenu = document.querySelectorAll('#sidebar .side-menu.top li a');

    allSideMenu.forEach(item => {
      const li = item.parentElement;

      item.addEventListener('click', function () {
        allSideMenu.forEach(i => {
          i.parentElement?.classList.remove('active');
        });
        li?.classList.add('active');
      });
    });

    // TOGGLE SIDEBAR
    const menuBar = document.querySelector('#content nav .bx.bx-menu');
    const sidebar = document.getElementById('sidebar');

    if (menuBar) {
      menuBar.addEventListener('click', function () {
        sidebar?.classList.toggle('hide');
      });
    }

    if (window.innerWidth < 768) {
      sidebar?.classList.add('hide');
    }

    const switchMode = document.getElementById('switch-mode');
    if (switchMode) {
      switchMode.addEventListener('change', function () {
        if (!document.body.classList.contains('dark')) {
          document.body.classList.add('dark');
        } else {
          document.body.classList.remove('dark');
        }
      });
    }
  }
}
