export enum TypeNotification {
  EMAIL = 'EMAIL',

  ALERT = 'ALERT',

  SMS = 'SMS',
}
