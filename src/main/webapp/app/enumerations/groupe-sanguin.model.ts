export enum GroupeSanguin {
  O_PLUS = 'O_PLUS',

  AB_PLUS = 'AB_PLUS',

  A_PLUS = 'A_PLUS',

  B_PLUS = 'B_PLUS',

  O_MOINS = 'O_MOINS',

  AB_MOINS = 'AB_MOINS',

  A_MOINS = 'A_MOINS',

  B_MOINS = 'B_MOINS',
}
