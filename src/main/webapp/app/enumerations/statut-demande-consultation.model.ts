export enum StatutDemandeConsultation {
  EN_COURS = 'EN_COURS',

  PAYE = 'PAYE',

  EN_CONSULTATION = 'EN_CONSULTATION',

  DELAIS_EXPIRE = 'DELAIS_EXPIRE',

  CONSULTE = 'CONSULTE',
}

export const ListeStatutDemandeConsultation = [
  { code: 'EN_COURS', libelle: 'EN COURS' },
  { code: 'PAYE', libelle: 'PAYÉ' },
  { code: 'EN_CONSULTATION', libelle: 'EN CONSULTATION' },
  { code: 'DELAIS_EXPIRE', libelle: 'DÉLAI EXPIRÉ' },
  { code: 'CONSULTE', libelle: 'CONSULTÉ' },
];
