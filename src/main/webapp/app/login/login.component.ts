import { Component, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from 'app/login/login.service';
import { AccountService } from 'app/core/auth/account.service';
import { Authority } from '../config/authority.constants';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'jhi-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild('username', { static: false })
  username!: ElementRef;

  authenticationError = false;

  loginForm = new FormGroup({
    username: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    password: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    rememberMe: new FormControl(false, { nonNullable: true, validators: [Validators.required] }),
  });

  constructor(
    private accountService: AccountService,
    private globalService: GlobalService,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // if already authenticated then navigate to home page
    this.accountService.identity().subscribe(() => {
      if (this.accountService.isAuthenticated()) {
        if (this.accountService.hasAnyAuthority(Authority.PATIENT)) {
          this.router.navigate(['services/acceuil/patient']);
        } else if (this.accountService.hasAnyAuthority(Authority.MEDECIN)) {
          this.router.navigate(['services/acceuil/medecin']);
        } else {
          this.router.navigate(['services/tdb']);
        }
      }
    });
  }

  ngAfterViewInit(): void {
    this.username.nativeElement.focus();
  }

  login(): void {
    this.loginService.login(this.loginForm.getRawValue()).subscribe({
      next: res => {
        this.authenticationError = false;
        this.accountService.identity().subscribe(res => {
          if (res && res.id) {
            this.globalService.setUserIdInSession(res.id);
          }
        });
        if (!this.router.getCurrentNavigation()) {
          // There were no routing during login (eg from navigationToStoredUrl)
          if (this.accountService.hasAnyAuthority(Authority.PATIENT)) {
            this.router.navigate(['services/acceuil/patient']);
          } else if (this.accountService.hasAnyAuthority(Authority.MEDECIN)) {
            this.router.navigate(['services/acceuil/medecin']);
          } else {
            this.router.navigate(['services/tdb']);
          }
        }
      },
      error: () => (this.authenticationError = true),
    });
  }

  goBack(): void {
    history.back();
  }

  createAccount(): void {
    this.router.navigate(['account/register']);
  }

  goToResetPwd(): void {
    this.router.navigate(['account/reset/request']);
  }
}
