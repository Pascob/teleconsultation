export enum Authority {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER',
  PATIENT = 'ROLE_PATIENT',
  MEDECIN = 'ROLE_MEDECIN',
}
