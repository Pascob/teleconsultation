import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { Authority } from '../config/authority.constants';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;

  private readonly destroy$ = new Subject<void>();

  constructor(private accountService: AccountService, private router: Router) {}

  ngOnInit(): void {
    // this.accountService
    //   .getAuthenticationState()
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe(account => (this.account = account));
    this.accountService.identity().subscribe(() => {
      if (this.accountService.isAuthenticated()) {
        if (this.accountService.hasAnyAuthority([Authority.PATIENT, Authority.MEDECIN])) {
          this.router.navigate(['services/acceuil']);
        } else {
          this.router.navigate(['services/tdb']);
        }
      }
    });
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  createAccount(): void {
    this.router.navigate(['account', 'register']);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
