import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoPatientComponent } from './info-patient/info-patient.component';
import { InfoDemandeComponent } from './info-demande/info-demande.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [InfoPatientComponent, InfoDemandeComponent],
  exports: [InfoDemandeComponent, InfoPatientComponent],
  imports: [CommonModule, SharedModule],
})
export class SharedViewModule {}
