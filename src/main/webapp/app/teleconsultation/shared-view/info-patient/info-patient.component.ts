import { Component, Input, OnInit } from '@angular/core';
import { IPatient } from '../../../models/patient.model';

@Component({
  selector: 'jhi-info-patient',
  templateUrl: './info-patient.component.html',
  styleUrls: ['./info-patient.component.scss'],
})
export class InfoPatientComponent implements OnInit {
  @Input()
  patient?: IPatient;

  @Input()
  titre = 'Patient';

  @Input()
  enConsultation = false;

  @Input()
  color: 'info' | 'primary' = 'info';

  constructor() {}

  ngOnInit(): void {}
}
