import { Component, Input, OnInit } from '@angular/core';
import { IDemandeConsultation } from '../../../models/demande-consultation.model';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-info-demande',
  templateUrl: './info-demande.component.html',
  styleUrls: ['./info-demande.component.scss'],
})
export class InfoDemandeComponent implements OnInit {
  @Input()
  demande?: IDemandeConsultation;

  @Input()
  titre = 'Demande de consultation';

  constructor(private router: Router) {}

  ngOnInit(): void {}

  joinCall(): void {
    this.router.navigate(['services', 'consultations', 'en-consultation', this.demande?.id]);
  }
}
