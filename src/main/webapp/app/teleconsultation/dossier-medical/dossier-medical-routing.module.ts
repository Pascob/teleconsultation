import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailDossierComponent } from './detail-dossier/detail-dossier.component';

const routes: Routes = [
  {
    path: 'mon-dossier',
    component: DetailDossierComponent,
  },
  {
    path: 'dossier/:id',
    component: DetailDossierComponent,
  },
  {
    path: '**',
    redirectTo: '/404',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DossierMedicalRoutingModule {}
