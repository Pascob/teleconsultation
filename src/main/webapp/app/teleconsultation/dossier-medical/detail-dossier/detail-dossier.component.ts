import { Component, OnInit } from '@angular/core';
import { IUser } from '../../../admin/user-management/user-management.model';
import { Genre } from '../../../enumerations/genre.model';
import dayjs from 'dayjs/esm';
import { GroupeSanguin } from '../../../enumerations/groupe-sanguin.model';
import { StatutDemandeConsultation } from '../../../enumerations/statut-demande-consultation.model';
import { IPatient } from '../../../models/patient.model';
import { IDemandeConsultation } from '../../../models/demande-consultation.model';

@Component({
  selector: 'jhi-detail-dossier',
  templateUrl: './detail-dossier.component.html',
  styleUrls: ['./detail-dossier.component.scss'],
})
export class DetailDossierComponent implements OnInit {
  user?: IUser = {
    id: 1,
    firstName: 'KAFANDO',
    lastName: 'Rodolphe',
    email: 'krodolphe@gmail.com',
    telephone: '22600000000',
  };

  patient?: IPatient = {
    id: 1,
    dateNaissance: dayjs('1997-05-18'),
    genre: Genre.M,
    userId: 1,
  };

  demande?: IDemandeConsultation = {
    id: 1,
    ordre: 1,
    statut: StatutDemandeConsultation.EN_COURS,
    specialiteDesignation: 'Médécine générale',
    moyenPaiment: 'Orange',
    codeOtp: '85321',
    telephonePaiement: '55252525',
    dateHeureDemande: dayjs('2022-12-07 20:10'),
    montant: 5000,
  };

  constructor() {
    this.patient = {
      id: this.patient!.id,
      ...this.patient,
      user: this.user,
      dossier: {
        id: 1,
        groupeSanguin: GroupeSanguin.AB_MOINS,
        numero: '0102030',
      },
    };
  }

  ngOnInit(): void {}

  navigateToPage($event: any): void {}
}
