import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DossierMedicalRoutingModule } from './dossier-medical-routing.module';
import { DetailDossierComponent } from './detail-dossier/detail-dossier.component';
import { GestionDossierComponent } from './gestion-dossier/gestion-dossier.component';
import { SharedModule } from '../../shared/shared.module';
import { SharedViewModule } from '../shared-view/shared-view.module';

@NgModule({
  declarations: [DetailDossierComponent, GestionDossierComponent],
  imports: [CommonModule, DossierMedicalRoutingModule, SharedModule, SharedViewModule],
})
export class DossierMedicalModule {}
