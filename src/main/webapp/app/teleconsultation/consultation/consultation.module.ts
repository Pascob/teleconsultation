import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultationRoutingModule } from './consultation-routing.module';
import { DemandesComponent } from './demandes/demandes.component';
import { ConsultationsComponent } from './consultations.component';
import { SharedModule } from '../../shared/shared.module';
import { NouvelleDemandeComponent } from './demandes/nouvelle-demande/nouvelle-demande.component';
import { FormsModule } from '@angular/forms';
import { ConsulterComponent } from './consulter/consulter.component';
import { SharedViewModule } from '../shared-view/shared-view.module';
import { ConsulterPatientComponent } from './consulter-patient/consulter-patient.component';

@NgModule({
  declarations: [DemandesComponent, ConsultationsComponent, NouvelleDemandeComponent, ConsulterComponent, ConsulterPatientComponent],
  imports: [CommonModule, FormsModule, SharedModule, ConsultationRoutingModule, SharedViewModule],
})
export class ConsultationModule {}
