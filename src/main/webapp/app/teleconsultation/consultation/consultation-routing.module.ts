import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import {Authority} from "../../config/authority.constants";
// import {UserRouteAccessService} from "../../core/auth/user-route-access.service";
import { DemandesComponent } from './demandes/demandes.component';
import { ConsultationsComponent } from './consultations.component';
import { NouvelleDemandeComponent } from './demandes/nouvelle-demande/nouvelle-demande.component';
import { ConsulterComponent } from './consulter/consulter.component';
import { AcceuilMedecinComponent } from '../acceuil/acceuil-medecin/acceuil-medecin.component';
import { MedecinSessionResolveService } from '../../resolvers/medecin-routing-resolve.service';
import { DemandeConsultationRoutingResolveService } from '../../services/demande-consultation-routing-resolve.service';
import { Authority } from '../../config/authority.constants';
import { UserRouteAccessService } from '../../core/auth/user-route-access.service';
import { ConsulterPatientComponent } from './consulter-patient/consulter-patient.component';
import { PatientSessionResolveService } from '../../shared/patient-routing-resolve.service';

const routes: Routes = [
  {
    path: '',
    // data: {
    //   authorities: [Authority.ADMIN],
    // },
    // canActivate: [UserRouteAccessService],
    component: ConsultationsComponent,
  },
  {
    path: 'consulter/:id',
    component: ConsulterComponent,
    resolve: {
      medecin: MedecinSessionResolveService,
      demande: DemandeConsultationRoutingResolveService,
    },
    data: {
      authorities: [Authority.MEDECIN],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'en-consultation/:id',
    component: ConsulterPatientComponent,
    resolve: {
      patient: PatientSessionResolveService,
      demande: DemandeConsultationRoutingResolveService,
    },
    data: {
      authorities: [Authority.PATIENT],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'demandes',
    // data: {
    //   authorities: [Authority.ADMIN],
    // },
    // canActivate: [UserRouteAccessService],
    component: DemandesComponent,
  },
  {
    path: 'nouvelle-demande',
    // data: {
    //   authorities: [Authority.ADMIN],
    // },
    // canActivate: [UserRouteAccessService],
    component: NouvelleDemandeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultationRoutingModule {}
