import { Component, OnInit } from '@angular/core';
import dayjs from 'dayjs/esm';
import { IPatient } from '../../../models/patient.model';
import { IAgoraOptions } from '../../../models/agora-options';
import { APP_ID, CHANNEL, RtcStreamService } from '../../../services/rtc-stream.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IDemandeConsultation } from '../../../models/demande-consultation.model';

@Component({
  selector: 'jhi-consulter',
  templateUrl: './consulter.component.html',
  styleUrls: ['./consulter.component.scss'],
})
export class ConsulterComponent implements OnInit {
  age = 0;
  patient?: IPatient;
  demande?: IDemandeConsultation;

  options: IAgoraOptions = {
    appId: APP_ID,
    channel: CHANNEL,
    uid: 0,
    token: null,
  };

  hideBtns = true;

  constructor(protected rtcStreamService: RtcStreamService, private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.patient = data.patient;
      this.demande = data.demande;
      if (this.patient && this.patient.dateNaissance) {
        this.age = dayjs().diff(this.patient.dateNaissance, 'years');
      }
      // this.loadSpecialite();

      this.call();
    });
  }

  // Listen to the Join button click event.
  async call(): Promise<void> {
    const uid = this.rtcStreamService.generateUid();
    const rtcDetails = await this.rtcStreamService.generateTokenAndUid(uid);
    this.rtcStreamService.createRTCClient();
    this.rtcStreamService.agoraServerEvents(this.rtcStreamService.rtc);
    console.warn('CALL', rtcDetails);
    await this.rtcStreamService.localUser(rtcDetails.token, uid);

    this.hideBtns = false;
  }

  async endCall(): Promise<void> {
    await this.rtcStreamService.leaveCall();
  }

  finishConsultation(): void {
    this.endCall().then(() => {});
    this.router.navigate(['services/acceuil/medecin']);
  }
}
