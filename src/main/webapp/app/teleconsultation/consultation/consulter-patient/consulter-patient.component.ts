import { Component, OnInit } from '@angular/core';
import { IAgoraOptions } from '../../../models/agora-options';
import { APP_ID, CHANNEL, RtcStreamService } from '../../../services/rtc-stream.service';

@Component({
  selector: 'jhi-consulter-patient',
  templateUrl: './consulter-patient.component.html',
  styleUrls: ['./consulter-patient.component.scss'],
})
export class ConsulterPatientComponent implements OnInit {
  options: IAgoraOptions = {
    appId: APP_ID,
    channel: CHANNEL,
    uid: 0,
    token: null,
  };

  hideBtns = true;

  constructor(protected rtcStreamService: RtcStreamService) {}

  ngOnInit(): void {}

  // Listen to the Join button click event.
  async call(): Promise<void> {
    const uid = this.rtcStreamService.generateUid();
    const rtcDetails = await this.rtcStreamService.generateTokenAndUid(uid);
    this.rtcStreamService.createRTCClient();
    this.rtcStreamService.agoraServerEvents(this.rtcStreamService.rtc);
    await this.rtcStreamService.localUser(rtcDetails.token, uid);

    this.hideBtns = false;
  }

  async endCall(): Promise<void> {
    await this.rtcStreamService.leaveCall();
  }
}
