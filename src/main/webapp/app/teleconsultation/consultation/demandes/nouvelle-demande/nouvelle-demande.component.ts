import { Component, OnInit } from '@angular/core';
import { ISpecialite } from '../../../../models/specialite.model';
import { SpecialiteService } from '../../../../services/specialite.service';
import { DemandeConsultationFormGroup, DemandeConsultationFormService } from '../../../../services/forms/demande-consultation-form.service';
import { HttpResponse } from '@angular/common/http';
import { IDemandeConsultation, NewDemandeConsultation } from '../../../../models/demande-consultation.model';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DemandeConsultationService } from '../../../../services/demande-consultation.service';

@Component({
  selector: 'jhi-nouvelle-demande',
  templateUrl: './nouvelle-demande.component.html',
  styleUrls: ['./nouvelle-demande.component.scss'],
})
export class NouvelleDemandeComponent implements OnInit {
  listeSpecialite: ISpecialite[] = [];

  moyenPaiements: any[] = [
    {
      code: 'OM',
      libelle: 'ORANGE MONEY',
    },
    // {
    //   code: 'MM',
    //   libelle: 'MOOV MONEY'
    // }
  ];

  // @ts-ignore
  specialite?: ISpecialite = {};

  demande: DemandeConsultationFormGroup = this.demandeConsultationFormService.createDemandeConsultationFormGroup();
  isSaving = false;

  constructor(
    private specialiteService: SpecialiteService,
    private demandeConsultationFormService: DemandeConsultationFormService,
    private demandeConsultationService: DemandeConsultationService
  ) {}

  ngOnInit(): void {
    this.loadSpecialite();
    // this.demande.controls['montant'].disable();
  }

  private loadSpecialite(): void {
    this.specialiteService.query().subscribe(
      res => (this.listeSpecialite = res.body || []),
      () => {}
    );
  }

  setDemandeMontant(): void {
    this.specialite = this.listeSpecialite.find(sp => sp.id === this.demande.value.specialite?.id);
    if (this.specialite && this.specialite.frais) {
      this.demande.patchValue({
        ...this.demande.value,
        montant: this.specialite.frais,
      });
    }
    console.warn('specialite', this.demande, this.specialite);
  }

  save(): void {
    this.isSaving = true;
    // @ts-ignore
    const demandeConsultation: NewDemandeConsultation = this.demandeConsultationFormService.getDemandeConsultation(this.demande);
    this.subscribeToSaveResponse(this.demandeConsultationService.create(demandeConsultation));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDemandeConsultation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  previousState(): void {
    window.history.back();
  }
}
