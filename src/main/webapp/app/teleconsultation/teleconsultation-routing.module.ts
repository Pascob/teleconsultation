import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from '../layouts/main-layout/main-layout.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { TdbComponent } from './tdb/tdb.component';
import { MedecinSessionResolveService } from '../resolvers/medecin-routing-resolve.service';
import { PatientSessionResolveService } from '../shared/patient-routing-resolve.service';
import { AcceuilPatientComponent } from './acceuil/acceuil-patient/acceuil-patient.component';
import { AcceuilMedecinComponent } from './acceuil/acceuil-medecin/acceuil-medecin.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'tdb',
        component: TdbComponent,
      },
      {
        path: 'acceuil',
        component: AcceuilComponent,
      },
      {
        path: 'acceuil/patient',
        component: AcceuilPatientComponent,
        resolve: {
          patient: PatientSessionResolveService,
        },
      },
      {
        path: 'acceuil/medecin',
        component: AcceuilMedecinComponent,
        resolve: {
          medecin: MedecinSessionResolveService,
        },
      },
      {
        path: 'consultations',
        loadChildren: () => import(`./consultation/consultation.module`).then(m => m.ConsultationModule),
      },
      {
        path: 'dossiers-medicaux',
        loadChildren: () => import(`./dossier-medical/dossier-medical.module`).then(m => m.DossierMedicalModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeleconsultationRoutingModule {}
