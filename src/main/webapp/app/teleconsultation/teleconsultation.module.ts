import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeleconsultationRoutingModule } from './teleconsultation-routing.module';
import { TeleconsultationComponent } from './teleconsultation.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { AcceuilPatientComponent } from './acceuil/acceuil-patient/acceuil-patient.component';
import { AcceuilMedecinComponent } from './acceuil/acceuil-medecin/acceuil-medecin.component';
import { SharedViewModule } from './shared-view/shared-view.module';
import { SharedModule } from '../shared/shared.module';
import { TdbComponent } from './tdb/tdb.component';

@NgModule({
  declarations: [TeleconsultationComponent, AcceuilComponent, AcceuilPatientComponent, AcceuilMedecinComponent, TdbComponent],
  imports: [CommonModule, TeleconsultationRoutingModule, SharedViewModule, SharedModule],
})
export class TeleconsultationModule {}
