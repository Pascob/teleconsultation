import { Component, OnInit } from '@angular/core';
import { ListeStatutDemandeConsultation, StatutDemandeConsultation } from '../../../enumerations/statut-demande-consultation.model';
import { ITEMS_PER_PAGE, PAGE_HEADER, TOTAL_COUNT_RESPONSE_HEADER } from '../../../config/pagination.constants';
import { IDemandeConsultation } from '../../../models/demande-consultation.model';
import { ISpecialite } from '../../../models/specialite.model';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';
import { IMedecin } from '../../../models/medecin.model';
import { SpecialiteService } from '../../../services/specialite.service';
import { DemandeConsultationService, EntityArrayResponseType } from '../../../services/demande-consultation.service';
import { ASC, DEFAULT_SORT_DATA, DESC, SORT } from '../../../config/navigation.constants';
import { HttpHeaders } from '@angular/common/http';
import { FilterOptions, IFilterOption, IFilterOptions } from '../../../shared/filter/filter.model';
import { combineLatest, Observable, switchMap, tap } from 'rxjs';
import { GlobalService } from '../../../services/global.service';
import { Authority } from '../../../config/authority.constants';

@Component({
  selector: 'jhi-acceuil-medecin',
  templateUrl: './acceuil-medecin.component.html',
  styleUrls: ['./acceuil-medecin.component.scss'],
})
export class AcceuilMedecinComponent implements OnInit {
  itemsPerPage = ITEMS_PER_PAGE;
  totalItems = 0;
  page = 1;
  predicate = 'id';
  ascending = true;
  isLoading = false;

  demandes: IDemandeConsultation[] = [];

  listeSpecialite: ISpecialite[] = [];

  medecin?: IMedecin;
  filters: IFilterOptions = new FilterOptions();

  listeStatutDemandeConsultation = ListeStatutDemandeConsultation;

  constructor(
    private activatedRoute: ActivatedRoute,
    private specialiteService: SpecialiteService,
    private globalService: GlobalService,
    private router: Router,
    private demandeConsultationService: DemandeConsultationService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(medecin => {
      // @ts-ignore
      this.medecin = medecin;
      this.totalItems = this.demandes.length;
      this.loadSpecialite();
      this.load();
    });
  }

  navigateToPage($event: any): void {}

  private loadSpecialite(): void {
    this.specialiteService.query().subscribe(
      res => (this.listeSpecialite = res.body || []),
      () => {}
    );
  }

  private load(): void {
    this.loadFromBackendWithRouteInformations().subscribe({
      next: (res: EntityArrayResponseType) => {
        this.onResponseSuccess(res);
      },
    });
  }

  protected loadFromBackendWithRouteInformations(): Observable<EntityArrayResponseType> {
    return combineLatest([this.activatedRoute.queryParamMap, this.activatedRoute.data]).pipe(
      tap(([params, data]) => this.fillComponentAttributeFromRoute(params, data)),
      switchMap(() => this.queryBackend(this.page, this.predicate, this.ascending, this.filters.filterOptions))
    );
  }

  protected fillComponentAttributeFromRoute(params: ParamMap, data: Data): void {
    const page = params.get(PAGE_HEADER);
    this.page = +(page ?? 1);
    const sort = (params.get(SORT) ?? data[DEFAULT_SORT_DATA])?.split(',');
    if (sort) {
      this.predicate = sort[0];
      this.ascending = sort[1] === ASC;
    }
    this.filters.initializeFromParams(params);
  }

  protected onResponseSuccess(response: EntityArrayResponseType): void {
    this.fillComponentAttributesFromResponseHeader(response.headers);
    const dataFromBody = this.fillComponentAttributesFromResponseBody(response.body);
    this.demandes = dataFromBody;
  }

  protected fillComponentAttributesFromResponseBody(data: IDemandeConsultation[] | null): IDemandeConsultation[] {
    return data ?? [];
  }

  protected fillComponentAttributesFromResponseHeader(headers: HttpHeaders): void {
    this.totalItems = Number(headers.get(TOTAL_COUNT_RESPONSE_HEADER));
  }

  protected queryBackend(
    page?: number,
    predicate?: string,
    ascending?: boolean,
    filterOptions?: IFilterOption[]
  ): Observable<EntityArrayResponseType> {
    this.isLoading = true;
    const pageToLoad: number = page ?? 1;
    const queryObject: any = {
      page: pageToLoad - 1,
      size: this.itemsPerPage,
      sort: this.getSortQueryParam(predicate, ascending),
    };
    filterOptions?.forEach(filterOption => {
      queryObject[filterOption.name] = filterOption.values;
    });
    return this.demandeConsultationService.query(queryObject).pipe(tap(() => (this.isLoading = false)));
  }

  protected getSortQueryParam(predicate = this.predicate, ascending = this.ascending): string[] {
    const ascendingQueryParam = ascending ? ASC : DESC;
    if (predicate === '') {
      return [];
    } else {
      return [predicate + ',' + ascendingQueryParam];
    }
  }

  getSpecialiteName(id: number): string {
    const specialite = this.listeSpecialite.find(sp => sp.id === id);
    return specialite && specialite.designation ? specialite.designation : '';
  }

  call(demande: IDemandeConsultation): void {
    this.globalService.afficherConfirmation('Consultation', "Souhaitez-vous démarrer l'appel vidéo?", 'question').subscribe(result => {
      if (result.isConfirmed) {
        demande.statut = StatutDemandeConsultation.EN_CONSULTATION;
        this.demandeConsultationService.update(demande).subscribe(
          () => {
            this.router.navigate(['services', 'consultations', 'consulter', demande.id]);
          },
          () => {
            demande.statut = StatutDemandeConsultation.EN_COURS;
          }
        );
      }
    });
  }

  dossier(demande: IDemandeConsultation): void {
    this.router.navigate(['services', 'dossiers-medicaux', 'dossier', demande.dossierMedical?.id]);
  }
}
