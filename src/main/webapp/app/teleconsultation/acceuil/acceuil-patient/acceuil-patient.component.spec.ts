import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceuilPatientComponent } from './acceuil-patient.component';

describe('AcceuilPatientComponent', () => {
  let component: AcceuilPatientComponent;
  let fixture: ComponentFixture<AcceuilPatientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AcceuilPatientComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AcceuilPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
