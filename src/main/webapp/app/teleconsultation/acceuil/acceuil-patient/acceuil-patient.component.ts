import { Component, OnInit } from '@angular/core';
import { StatutDemandeConsultation } from '../../../enumerations/statut-demande-consultation.model';
import dayjs from 'dayjs/esm';
import { IDemandeConsultation } from '../../../models/demande-consultation.model';
import { ActivatedRoute, Router } from '@angular/router';
import { IPatient } from '../../../models/patient.model';
import { DemandeConsultationService } from '../../../services/demande-consultation.service';
import { GlobalService } from '../../../services/global.service';
import { SpecialiteService } from '../../../services/specialite.service';

@Component({
  selector: 'jhi-acceuil-patient',
  templateUrl: './acceuil-patient.component.html',
  styleUrls: ['./acceuil-patient.component.scss'],
})
export class AcceuilPatientComponent implements OnInit {
  demande?: IDemandeConsultation;

  patient?: IPatient;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private demandeConsulationService: DemandeConsultationService,
    private globalService: GlobalService,
    private specialiteService: SpecialiteService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(patient => {
      this.patient = patient.patient;
      this.loadDemande();
    });
  }

  demanderConsultation(): void {
    this.router.navigate(['services', 'consultations', 'nouvelle-demande']);
  }

  private loadDemande(): void {
    if (this.patient && this.patient.id) {
      const req = this.globalService.buildReq(
        [
          {
            propriete: 'dossierMedicalId',
            expression: 'equals',
            value: this.patient.id,
          },
          {
            propriete: 'statut',
            expression: 'in',
            value: [StatutDemandeConsultation.EN_COURS, StatutDemandeConsultation.EN_CONSULTATION],
          },
        ],
        0,
        1,
        'ordre',
        true
      );
      this.demandeConsulationService.query(req).subscribe(
        res => {
          if (res.body && res.body.length > 0) {
            this.demande = res.body[0];
            this.loadSpecialite();
          }
        },
        () => {}
      );
    }
  }

  private loadSpecialite(): void {
    if (this.demande && this.demande.specialite && this.demande.specialite.id) {
      this.specialiteService.find(this.demande.specialite.id).subscribe(
        res => {
          if (res.body) {
            // @ts-ignore
            this.demande.specialiteDesignation = res.body.designation;
          }
        },
        () => {
          console.warn('SPECIALITE INTROUVABLE');
        }
      );
    }
  }
}
