import { Component, OnInit } from '@angular/core';
import { Authority } from '../../config/authority.constants';
import { AccountService } from '../../core/auth/account.service';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss'],
})
export class AcceuilComponent implements OnInit {
  constructor(private accountService: AccountService, private globalService: GlobalService, private router: Router) {}

  ngOnInit(): void {
    if (this.accountService.hasAnyAuthority(Authority.PATIENT)) {
      this.router.navigate(['services/acceuil/patient']);
    } else if (this.accountService.hasAnyAuthority(Authority.MEDECIN)) {
      this.router.navigate(['services/acceuil/medecin']);
    } else if (this.accountService.hasAnyAuthority(Authority.ADMIN)) {
      this.router.navigate(['services/tdb']);
    }
  }
}
