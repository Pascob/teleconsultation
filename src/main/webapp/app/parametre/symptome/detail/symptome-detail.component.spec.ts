import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SymptomeDetailComponent } from './symptome-detail.component';

describe('Symptome Management Detail Component', () => {
  let comp: SymptomeDetailComponent;
  let fixture: ComponentFixture<SymptomeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SymptomeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ symptome: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(SymptomeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(SymptomeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load symptome on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.symptome).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
