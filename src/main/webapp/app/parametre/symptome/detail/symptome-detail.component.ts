import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISymptome } from '../../../models/symptome.model';

@Component({
  selector: 'jhi-symptome-detail',
  templateUrl: './symptome-detail.component.html',
})
export class SymptomeDetailComponent implements OnInit {
  symptome: ISymptome | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ symptome }) => {
      this.symptome = symptome;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
