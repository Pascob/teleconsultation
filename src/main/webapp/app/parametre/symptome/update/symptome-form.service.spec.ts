import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../../../entities/symptome/symptome.test-samples';

import { SymptomeFormService } from './symptome-form.service';

describe('Symptome Form Service', () => {
  let service: SymptomeFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SymptomeFormService);
  });

  describe('Service methods', () => {
    describe('createSymptomeFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createSymptomeFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            designation: expect.any(Object),
            code: expect.any(Object),
          })
        );
      });

      it('passing ISymptome should create a new form with FormGroup', () => {
        const formGroup = service.createSymptomeFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            designation: expect.any(Object),
            code: expect.any(Object),
          })
        );
      });
    });

    describe('getSymptome', () => {
      it('should return NewSymptome for default Symptome initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createSymptomeFormGroup(sampleWithNewData);

        const symptome = service.getSymptome(formGroup) as any;

        expect(symptome).toMatchObject(sampleWithNewData);
      });

      it('should return NewSymptome for empty Symptome initial value', () => {
        const formGroup = service.createSymptomeFormGroup();

        const symptome = service.getSymptome(formGroup) as any;

        expect(symptome).toMatchObject({});
      });

      it('should return ISymptome', () => {
        const formGroup = service.createSymptomeFormGroup(sampleWithRequiredData);

        const symptome = service.getSymptome(formGroup) as any;

        expect(symptome).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ISymptome should not enable id FormControl', () => {
        const formGroup = service.createSymptomeFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewSymptome should disable id FormControl', () => {
        const formGroup = service.createSymptomeFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
