import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ISymptome, NewSymptome } from '../../../models/symptome.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISymptome for edit and NewSymptomeFormGroupInput for create.
 */
type SymptomeFormGroupInput = ISymptome | PartialWithRequiredKeyOf<NewSymptome>;

type SymptomeFormDefaults = Pick<NewSymptome, 'id'>;

type SymptomeFormGroupContent = {
  id: FormControl<ISymptome['id'] | NewSymptome['id']>;
  designation: FormControl<ISymptome['designation']>;
  code: FormControl<ISymptome['code']>;
};

export type SymptomeFormGroup = FormGroup<SymptomeFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SymptomeFormService {
  createSymptomeFormGroup(symptome: SymptomeFormGroupInput = { id: null }): SymptomeFormGroup {
    const symptomeRawValue = {
      ...this.getFormDefaults(),
      ...symptome,
    };
    return new FormGroup<SymptomeFormGroupContent>({
      id: new FormControl(
        { value: symptomeRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      designation: new FormControl(symptomeRawValue.designation, {
        validators: [Validators.required],
      }),
      code: new FormControl(symptomeRawValue.code),
    });
  }

  getSymptome(form: SymptomeFormGroup): ISymptome | NewSymptome {
    return form.getRawValue() as ISymptome | NewSymptome;
  }

  resetForm(form: SymptomeFormGroup, symptome: SymptomeFormGroupInput): void {
    const symptomeRawValue = { ...this.getFormDefaults(), ...symptome };
    form.reset(
      {
        ...symptomeRawValue,
        id: { value: symptomeRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): SymptomeFormDefaults {
    return {
      id: null,
    };
  }
}
