import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { SymptomeFormService, SymptomeFormGroup } from './symptome-form.service';
import { ISymptome } from '../../../models/symptome.model';
import { SymptomeService } from '../../../services/symptome.service';

@Component({
  selector: 'jhi-symptome-update',
  templateUrl: './symptome-update.component.html',
})
export class SymptomeUpdateComponent implements OnInit {
  isSaving = false;
  symptome: ISymptome | null = null;

  editForm: SymptomeFormGroup = this.symptomeFormService.createSymptomeFormGroup();

  constructor(
    protected symptomeService: SymptomeService,
    protected symptomeFormService: SymptomeFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ symptome }) => {
      this.symptome = symptome;
      if (symptome) {
        this.updateForm(symptome);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const symptome = this.symptomeFormService.getSymptome(this.editForm);
    if (symptome.id !== null) {
      this.subscribeToSaveResponse(this.symptomeService.update(symptome));
    } else {
      this.subscribeToSaveResponse(this.symptomeService.create(symptome));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISymptome>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(symptome: ISymptome): void {
    this.symptome = symptome;
    this.symptomeFormService.resetForm(this.editForm, symptome);
  }
}
