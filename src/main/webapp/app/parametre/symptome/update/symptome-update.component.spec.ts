import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SymptomeFormService } from './symptome-form.service';
import { SymptomeService } from '../../../services/symptome.service';
import { ISymptome } from '../../../models/symptome.model';

import { SymptomeUpdateComponent } from './symptome-update.component';

describe('Symptome Management Update Component', () => {
  let comp: SymptomeUpdateComponent;
  let fixture: ComponentFixture<SymptomeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let symptomeFormService: SymptomeFormService;
  let symptomeService: SymptomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [SymptomeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SymptomeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SymptomeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    symptomeFormService = TestBed.inject(SymptomeFormService);
    symptomeService = TestBed.inject(SymptomeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const symptome: ISymptome = { id: 456 };

      activatedRoute.data = of({ symptome });
      comp.ngOnInit();

      expect(comp.symptome).toEqual(symptome);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISymptome>>();
      const symptome = { id: 123 };
      jest.spyOn(symptomeFormService, 'getSymptome').mockReturnValue(symptome);
      jest.spyOn(symptomeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ symptome });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: symptome }));
      saveSubject.complete();

      // THEN
      expect(symptomeFormService.getSymptome).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(symptomeService.update).toHaveBeenCalledWith(expect.objectContaining(symptome));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISymptome>>();
      const symptome = { id: 123 };
      jest.spyOn(symptomeFormService, 'getSymptome').mockReturnValue({ id: null });
      jest.spyOn(symptomeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ symptome: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: symptome }));
      saveSubject.complete();

      // THEN
      expect(symptomeFormService.getSymptome).toHaveBeenCalled();
      expect(symptomeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISymptome>>();
      const symptome = { id: 123 };
      jest.spyOn(symptomeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ symptome });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(symptomeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
