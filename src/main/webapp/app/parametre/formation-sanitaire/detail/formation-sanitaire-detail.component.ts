import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFormationSanitaire } from '../../../models/formation-sanitaire.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-formation-sanitaire-detail',
  templateUrl: './formation-sanitaire-detail.component.html',
})
export class FormationSanitaireDetailComponent implements OnInit {
  formationSanitaire: IFormationSanitaire | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ formationSanitaire }) => {
      this.formationSanitaire = formationSanitaire;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
