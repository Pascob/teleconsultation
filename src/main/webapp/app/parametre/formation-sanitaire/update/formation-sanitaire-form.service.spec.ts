import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../formation-sanitaire.test-samples';

import { FormationSanitaireFormService } from './formation-sanitaire-form.service';

describe('FormationSanitaire Form Service', () => {
  let service: FormationSanitaireFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormationSanitaireFormService);
  });

  describe('Service methods', () => {
    describe('createFormationSanitaireFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createFormationSanitaireFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            designation: expect.any(Object),
            sigle: expect.any(Object),
            telephone: expect.any(Object),
            fixe: expect.any(Object),
            bp: expect.any(Object),
            email: expect.any(Object),
            logo: expect.any(Object),
          })
        );
      });

      it('passing IFormationSanitaire should create a new form with FormGroup', () => {
        const formGroup = service.createFormationSanitaireFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            designation: expect.any(Object),
            sigle: expect.any(Object),
            telephone: expect.any(Object),
            fixe: expect.any(Object),
            bp: expect.any(Object),
            email: expect.any(Object),
            logo: expect.any(Object),
          })
        );
      });
    });

    describe('getFormationSanitaire', () => {
      it('should return NewFormationSanitaire for default FormationSanitaire initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createFormationSanitaireFormGroup(sampleWithNewData);

        const formationSanitaire = service.getFormationSanitaire(formGroup) as any;

        expect(formationSanitaire).toMatchObject(sampleWithNewData);
      });

      it('should return NewFormationSanitaire for empty FormationSanitaire initial value', () => {
        const formGroup = service.createFormationSanitaireFormGroup();

        const formationSanitaire = service.getFormationSanitaire(formGroup) as any;

        expect(formationSanitaire).toMatchObject({});
      });

      it('should return IFormationSanitaire', () => {
        const formGroup = service.createFormationSanitaireFormGroup(sampleWithRequiredData);

        const formationSanitaire = service.getFormationSanitaire(formGroup) as any;

        expect(formationSanitaire).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IFormationSanitaire should not enable id FormControl', () => {
        const formGroup = service.createFormationSanitaireFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewFormationSanitaire should disable id FormControl', () => {
        const formGroup = service.createFormationSanitaireFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
