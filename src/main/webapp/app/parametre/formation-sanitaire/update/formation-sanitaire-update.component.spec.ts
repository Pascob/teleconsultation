import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { FormationSanitaireFormService } from './formation-sanitaire-form.service';
import { FormationSanitaireService } from '../../../services/formation-sanitaire.service';
import { IFormationSanitaire } from '../../../models/formation-sanitaire.model';

import { FormationSanitaireUpdateComponent } from './formation-sanitaire-update.component';

describe('FormationSanitaire Management Update Component', () => {
  let comp: FormationSanitaireUpdateComponent;
  let fixture: ComponentFixture<FormationSanitaireUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let formationSanitaireFormService: FormationSanitaireFormService;
  let formationSanitaireService: FormationSanitaireService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [FormationSanitaireUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(FormationSanitaireUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FormationSanitaireUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    formationSanitaireFormService = TestBed.inject(FormationSanitaireFormService);
    formationSanitaireService = TestBed.inject(FormationSanitaireService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const formationSanitaire: IFormationSanitaire = { id: 456 };

      activatedRoute.data = of({ formationSanitaire });
      comp.ngOnInit();

      expect(comp.formationSanitaire).toEqual(formationSanitaire);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IFormationSanitaire>>();
      const formationSanitaire = { id: 123 };
      jest.spyOn(formationSanitaireFormService, 'getFormationSanitaire').mockReturnValue(formationSanitaire);
      jest.spyOn(formationSanitaireService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ formationSanitaire });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: formationSanitaire }));
      saveSubject.complete();

      // THEN
      expect(formationSanitaireFormService.getFormationSanitaire).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(formationSanitaireService.update).toHaveBeenCalledWith(expect.objectContaining(formationSanitaire));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IFormationSanitaire>>();
      const formationSanitaire = { id: 123 };
      jest.spyOn(formationSanitaireFormService, 'getFormationSanitaire').mockReturnValue({ id: null });
      jest.spyOn(formationSanitaireService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ formationSanitaire: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: formationSanitaire }));
      saveSubject.complete();

      // THEN
      expect(formationSanitaireFormService.getFormationSanitaire).toHaveBeenCalled();
      expect(formationSanitaireService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IFormationSanitaire>>();
      const formationSanitaire = { id: 123 };
      jest.spyOn(formationSanitaireService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ formationSanitaire });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(formationSanitaireService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
