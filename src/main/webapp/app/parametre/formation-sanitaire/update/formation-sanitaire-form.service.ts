import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IFormationSanitaire, NewFormationSanitaire } from '../../../models/formation-sanitaire.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IFormationSanitaire for edit and NewFormationSanitaireFormGroupInput for create.
 */
type FormationSanitaireFormGroupInput = IFormationSanitaire | PartialWithRequiredKeyOf<NewFormationSanitaire>;

type FormationSanitaireFormDefaults = Pick<NewFormationSanitaire, 'id'>;

type FormationSanitaireFormGroupContent = {
  id: FormControl<IFormationSanitaire['id'] | NewFormationSanitaire['id']>;
  designation: FormControl<IFormationSanitaire['designation']>;
  sigle: FormControl<IFormationSanitaire['sigle']>;
  telephone: FormControl<IFormationSanitaire['telephone']>;
  fixe: FormControl<IFormationSanitaire['fixe']>;
  bp: FormControl<IFormationSanitaire['bp']>;
  email: FormControl<IFormationSanitaire['email']>;
  logo: FormControl<IFormationSanitaire['logo']>;
  logoContentType: FormControl<IFormationSanitaire['logoContentType']>;
};

export type FormationSanitaireFormGroup = FormGroup<FormationSanitaireFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class FormationSanitaireFormService {
  createFormationSanitaireFormGroup(formationSanitaire: FormationSanitaireFormGroupInput = { id: null }): FormationSanitaireFormGroup {
    const formationSanitaireRawValue = {
      ...this.getFormDefaults(),
      ...formationSanitaire,
    };
    return new FormGroup<FormationSanitaireFormGroupContent>({
      id: new FormControl(
        { value: formationSanitaireRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      designation: new FormControl(formationSanitaireRawValue.designation, {
        validators: [Validators.required],
      }),
      sigle: new FormControl(formationSanitaireRawValue.sigle),
      telephone: new FormControl(formationSanitaireRawValue.telephone),
      fixe: new FormControl(formationSanitaireRawValue.fixe),
      bp: new FormControl(formationSanitaireRawValue.bp),
      email: new FormControl(formationSanitaireRawValue.email, {
        validators: [Validators.required, Validators.pattern('^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$')],
      }),
      logo: new FormControl(formationSanitaireRawValue.logo),
      logoContentType: new FormControl(formationSanitaireRawValue.logoContentType),
    });
  }

  getFormationSanitaire(form: FormationSanitaireFormGroup): IFormationSanitaire | NewFormationSanitaire {
    return form.getRawValue() as IFormationSanitaire | NewFormationSanitaire;
  }

  resetForm(form: FormationSanitaireFormGroup, formationSanitaire: FormationSanitaireFormGroupInput): void {
    const formationSanitaireRawValue = { ...this.getFormDefaults(), ...formationSanitaire };
    form.reset(
      {
        ...formationSanitaireRawValue,
        id: { value: formationSanitaireRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): FormationSanitaireFormDefaults {
    return {
      id: null,
    };
  }
}
