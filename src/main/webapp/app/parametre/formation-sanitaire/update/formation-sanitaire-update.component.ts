import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { FormationSanitaireFormService, FormationSanitaireFormGroup } from './formation-sanitaire-form.service';
import { IFormationSanitaire } from '../../../models/formation-sanitaire.model';
import { FormationSanitaireService } from '../../../services/formation-sanitaire.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-formation-sanitaire-update',
  templateUrl: './formation-sanitaire-update.component.html',
})
export class FormationSanitaireUpdateComponent implements OnInit {
  isSaving = false;
  formationSanitaire: IFormationSanitaire | null = null;

  editForm: FormationSanitaireFormGroup = this.formationSanitaireFormService.createFormationSanitaireFormGroup();

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected formationSanitaireService: FormationSanitaireService,
    protected formationSanitaireFormService: FormationSanitaireFormService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ formationSanitaire }) => {
      this.formationSanitaire = formationSanitaire;
      if (formationSanitaire) {
        this.updateForm(formationSanitaire);
      }
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('teleconsultationApp.error', { ...err, key: 'error.file.' + err.key })
        ),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const formationSanitaire = this.formationSanitaireFormService.getFormationSanitaire(this.editForm);
    if (formationSanitaire.id !== null) {
      this.subscribeToSaveResponse(this.formationSanitaireService.update(formationSanitaire));
    } else {
      this.subscribeToSaveResponse(this.formationSanitaireService.create(formationSanitaire));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFormationSanitaire>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(formationSanitaire: IFormationSanitaire): void {
    this.formationSanitaire = formationSanitaire;
    this.formationSanitaireFormService.resetForm(this.editForm, formationSanitaire);
  }
}
