import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormationSanitaireComponent } from './formation-sanitaire/list/formation-sanitaire.component';
import { MainLayoutComponent } from '../layouts/main-layout/main-layout.component';
import { SpecialiteComponent } from './specialite/list/specialite.component';
import { ProduitComponent } from './produit/list/produit.component';
import { ExamenMedicalComponent } from './examen-medical/list/examen-medical.component';
import { SymptomeComponent } from './symptome/list/symptome.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'formations-sanitaires',
        component: FormationSanitaireComponent,
      },
      {
        path: 'specialites',
        component: SpecialiteComponent,
      },
      {
        path: 'produits',
        component: ProduitComponent,
      },
      {
        path: 'examens',
        component: ExamenMedicalComponent,
      },
      {
        path: 'symptomes',
        component: SymptomeComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParametreRoutingModule {}
