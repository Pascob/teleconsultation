import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../../../entities/examen-medical/examen-medical.test-samples';

import { ExamenMedicalFormService } from './examen-medical-form.service';

describe('ExamenMedical Form Service', () => {
  let service: ExamenMedicalFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExamenMedicalFormService);
  });

  describe('Service methods', () => {
    describe('createExamenMedicalFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createExamenMedicalFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            designation: expect.any(Object),
            code: expect.any(Object),
          })
        );
      });

      it('passing IExamenMedical should create a new form with FormGroup', () => {
        const formGroup = service.createExamenMedicalFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            designation: expect.any(Object),
            code: expect.any(Object),
          })
        );
      });
    });

    describe('getExamenMedical', () => {
      it('should return NewExamenMedical for default ExamenMedical initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createExamenMedicalFormGroup(sampleWithNewData);

        const examenMedical = service.getExamenMedical(formGroup) as any;

        expect(examenMedical).toMatchObject(sampleWithNewData);
      });

      it('should return NewExamenMedical for empty ExamenMedical initial value', () => {
        const formGroup = service.createExamenMedicalFormGroup();

        const examenMedical = service.getExamenMedical(formGroup) as any;

        expect(examenMedical).toMatchObject({});
      });

      it('should return IExamenMedical', () => {
        const formGroup = service.createExamenMedicalFormGroup(sampleWithRequiredData);

        const examenMedical = service.getExamenMedical(formGroup) as any;

        expect(examenMedical).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IExamenMedical should not enable id FormControl', () => {
        const formGroup = service.createExamenMedicalFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewExamenMedical should disable id FormControl', () => {
        const formGroup = service.createExamenMedicalFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
