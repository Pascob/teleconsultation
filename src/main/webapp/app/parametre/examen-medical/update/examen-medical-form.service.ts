import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IExamenMedical, NewExamenMedical } from '../../../models/examen-medical.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IExamenMedical for edit and NewExamenMedicalFormGroupInput for create.
 */
type ExamenMedicalFormGroupInput = IExamenMedical | PartialWithRequiredKeyOf<NewExamenMedical>;

type ExamenMedicalFormDefaults = Pick<NewExamenMedical, 'id'>;

type ExamenMedicalFormGroupContent = {
  id: FormControl<IExamenMedical['id'] | NewExamenMedical['id']>;
  designation: FormControl<IExamenMedical['designation']>;
  code: FormControl<IExamenMedical['code']>;
};

export type ExamenMedicalFormGroup = FormGroup<ExamenMedicalFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ExamenMedicalFormService {
  createExamenMedicalFormGroup(examenMedical: ExamenMedicalFormGroupInput = { id: null }): ExamenMedicalFormGroup {
    const examenMedicalRawValue = {
      ...this.getFormDefaults(),
      ...examenMedical,
    };
    return new FormGroup<ExamenMedicalFormGroupContent>({
      id: new FormControl(
        { value: examenMedicalRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      designation: new FormControl(examenMedicalRawValue.designation, {
        validators: [Validators.required],
      }),
      code: new FormControl(examenMedicalRawValue.code),
    });
  }

  getExamenMedical(form: ExamenMedicalFormGroup): IExamenMedical | NewExamenMedical {
    return form.getRawValue() as IExamenMedical | NewExamenMedical;
  }

  resetForm(form: ExamenMedicalFormGroup, examenMedical: ExamenMedicalFormGroupInput): void {
    const examenMedicalRawValue = { ...this.getFormDefaults(), ...examenMedical };
    form.reset(
      {
        ...examenMedicalRawValue,
        id: { value: examenMedicalRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ExamenMedicalFormDefaults {
    return {
      id: null,
    };
  }
}
