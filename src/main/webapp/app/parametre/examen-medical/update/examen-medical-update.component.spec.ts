import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ExamenMedicalFormService } from './examen-medical-form.service';
import { ExamenMedicalService } from '../../../services/examen-medical.service';
import { IExamenMedical } from '../../../models/examen-medical.model';

import { ExamenMedicalUpdateComponent } from './examen-medical-update.component';

describe('ExamenMedical Management Update Component', () => {
  let comp: ExamenMedicalUpdateComponent;
  let fixture: ComponentFixture<ExamenMedicalUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let examenMedicalFormService: ExamenMedicalFormService;
  let examenMedicalService: ExamenMedicalService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ExamenMedicalUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ExamenMedicalUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ExamenMedicalUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    examenMedicalFormService = TestBed.inject(ExamenMedicalFormService);
    examenMedicalService = TestBed.inject(ExamenMedicalService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const examenMedical: IExamenMedical = { id: 456 };

      activatedRoute.data = of({ examenMedical });
      comp.ngOnInit();

      expect(comp.examenMedical).toEqual(examenMedical);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IExamenMedical>>();
      const examenMedical = { id: 123 };
      jest.spyOn(examenMedicalFormService, 'getExamenMedical').mockReturnValue(examenMedical);
      jest.spyOn(examenMedicalService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ examenMedical });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: examenMedical }));
      saveSubject.complete();

      // THEN
      expect(examenMedicalFormService.getExamenMedical).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(examenMedicalService.update).toHaveBeenCalledWith(expect.objectContaining(examenMedical));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IExamenMedical>>();
      const examenMedical = { id: 123 };
      jest.spyOn(examenMedicalFormService, 'getExamenMedical').mockReturnValue({ id: null });
      jest.spyOn(examenMedicalService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ examenMedical: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: examenMedical }));
      saveSubject.complete();

      // THEN
      expect(examenMedicalFormService.getExamenMedical).toHaveBeenCalled();
      expect(examenMedicalService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IExamenMedical>>();
      const examenMedical = { id: 123 };
      jest.spyOn(examenMedicalService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ examenMedical });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(examenMedicalService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
