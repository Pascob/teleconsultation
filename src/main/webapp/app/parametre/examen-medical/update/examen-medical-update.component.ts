import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ExamenMedicalFormService, ExamenMedicalFormGroup } from './examen-medical-form.service';
import { IExamenMedical } from '../../../models/examen-medical.model';
import { ExamenMedicalService } from '../../../services/examen-medical.service';

@Component({
  selector: 'jhi-examen-medical-update',
  templateUrl: './examen-medical-update.component.html',
})
export class ExamenMedicalUpdateComponent implements OnInit {
  isSaving = false;
  examenMedical: IExamenMedical | null = null;

  editForm: ExamenMedicalFormGroup = this.examenMedicalFormService.createExamenMedicalFormGroup();

  constructor(
    protected examenMedicalService: ExamenMedicalService,
    protected examenMedicalFormService: ExamenMedicalFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ examenMedical }) => {
      this.examenMedical = examenMedical;
      if (examenMedical) {
        this.updateForm(examenMedical);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const examenMedical = this.examenMedicalFormService.getExamenMedical(this.editForm);
    if (examenMedical.id !== null) {
      this.subscribeToSaveResponse(this.examenMedicalService.update(examenMedical));
    } else {
      this.subscribeToSaveResponse(this.examenMedicalService.create(examenMedical));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExamenMedical>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(examenMedical: IExamenMedical): void {
    this.examenMedical = examenMedical;
    this.examenMedicalFormService.resetForm(this.editForm, examenMedical);
  }
}
