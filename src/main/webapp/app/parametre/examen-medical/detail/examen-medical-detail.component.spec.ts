import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExamenMedicalDetailComponent } from './examen-medical-detail.component';

describe('ExamenMedical Management Detail Component', () => {
  let comp: ExamenMedicalDetailComponent;
  let fixture: ComponentFixture<ExamenMedicalDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ExamenMedicalDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ examenMedical: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ExamenMedicalDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ExamenMedicalDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load examenMedical on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.examenMedical).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
