import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExamenMedical } from '../../../models/examen-medical.model';

@Component({
  selector: 'jhi-examen-medical-detail',
  templateUrl: './examen-medical-detail.component.html',
})
export class ExamenMedicalDetailComponent implements OnInit {
  examenMedical: IExamenMedical | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ examenMedical }) => {
      this.examenMedical = examenMedical;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
