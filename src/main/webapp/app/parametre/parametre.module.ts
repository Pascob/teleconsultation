import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParametreRoutingModule } from './parametre-routing.module';
import { FormationSanitaireComponent } from './formation-sanitaire/list/formation-sanitaire.component';
import { FormationSanitaireDetailComponent } from './formation-sanitaire/detail/formation-sanitaire-detail.component';
import { FormationSanitaireUpdateComponent } from './formation-sanitaire/update/formation-sanitaire-update.component';
import { SharedModule } from '../shared/shared.module';
import { ExamenMedicalComponent } from './examen-medical/list/examen-medical.component';
import { ExamenMedicalDetailComponent } from './examen-medical/detail/examen-medical-detail.component';
import { ExamenMedicalUpdateComponent } from './examen-medical/update/examen-medical-update.component';
import { ProduitComponent } from './produit/list/produit.component';
import { ProduitDetailComponent } from './produit/detail/produit-detail.component';
import { ProduitUpdateComponent } from './produit/update/produit-update.component';
import { SymptomeComponent } from './symptome/list/symptome.component';
import { SymptomeDetailComponent } from './symptome/detail/symptome-detail.component';
import { SymptomeUpdateComponent } from './symptome/update/symptome-update.component';
import { SpecialiteComponent } from './specialite/list/specialite.component';
import { SpecialiteDetailComponent } from './specialite/detail/specialite-detail.component';
import { SpecialiteUpdateComponent } from './specialite/update/specialite-update.component';

@NgModule({
  declarations: [
    FormationSanitaireComponent,
    FormationSanitaireDetailComponent,
    FormationSanitaireUpdateComponent,
    ExamenMedicalComponent,
    ExamenMedicalDetailComponent,
    ExamenMedicalUpdateComponent,
    ProduitComponent,
    ProduitDetailComponent,
    ProduitUpdateComponent,
    SymptomeComponent,
    SymptomeDetailComponent,
    SymptomeUpdateComponent,
    SpecialiteComponent,
    SpecialiteDetailComponent,
    SpecialiteUpdateComponent,
  ],
  imports: [CommonModule, ParametreRoutingModule, SharedModule],
})
export class ParametreModule {}
