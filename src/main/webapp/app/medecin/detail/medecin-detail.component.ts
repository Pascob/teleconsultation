import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMedecin } from '../../models/medecin.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-medecin-detail',
  templateUrl: './medecin-detail.component.html',
})
export class MedecinDetailComponent implements OnInit {
  medecin: IMedecin | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ medecin }) => {
      this.medecin = medecin;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
