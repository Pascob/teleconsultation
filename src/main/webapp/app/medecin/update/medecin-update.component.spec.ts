import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { MedecinFormService } from './medecin-form.service';
import { MedecinService } from '../../services/medecin.service';
import { IMedecin } from '../../models/medecin.model';
import { IFormationSanitaire } from 'app/entities/formation-sanitaire/formation-sanitaire.model';
import { FormationSanitaireService } from 'app/entities/formation-sanitaire/service/formation-sanitaire.service';
import { ISpecialite } from 'app/entities/specialite/specialite.model';
import { SpecialiteService } from 'app/entities/specialite/service/specialite.service';

import { MedecinUpdateComponent } from './medecin-update.component';

describe('Medecin Management Update Component', () => {
  let comp: MedecinUpdateComponent;
  let fixture: ComponentFixture<MedecinUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let medecinFormService: MedecinFormService;
  let medecinService: MedecinService;
  let formationSanitaireService: FormationSanitaireService;
  let specialiteService: SpecialiteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [MedecinUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(MedecinUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MedecinUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    medecinFormService = TestBed.inject(MedecinFormService);
    medecinService = TestBed.inject(MedecinService);
    formationSanitaireService = TestBed.inject(FormationSanitaireService);
    specialiteService = TestBed.inject(SpecialiteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call FormationSanitaire query and add missing value', () => {
      const medecin: IMedecin = { id: 456 };
      const formationSanitaire: IFormationSanitaire = { id: 72658 };
      medecin.formationSanitaire = formationSanitaire;

      const formationSanitaireCollection: IFormationSanitaire[] = [{ id: 68177 }];
      jest.spyOn(formationSanitaireService, 'query').mockReturnValue(of(new HttpResponse({ body: formationSanitaireCollection })));
      const additionalFormationSanitaires = [formationSanitaire];
      const expectedCollection: IFormationSanitaire[] = [...additionalFormationSanitaires, ...formationSanitaireCollection];
      jest.spyOn(formationSanitaireService, 'addFormationSanitaireToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ medecin });
      comp.ngOnInit();

      expect(formationSanitaireService.query).toHaveBeenCalled();
      expect(formationSanitaireService.addFormationSanitaireToCollectionIfMissing).toHaveBeenCalledWith(
        formationSanitaireCollection,
        ...additionalFormationSanitaires.map(expect.objectContaining)
      );
      expect(comp.formationSanitairesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Specialite query and add missing value', () => {
      const medecin: IMedecin = { id: 456 };
      const specialite: ISpecialite = { id: 79246 };
      medecin.specialite = specialite;

      const specialiteCollection: ISpecialite[] = [{ id: 99232 }];
      jest.spyOn(specialiteService, 'query').mockReturnValue(of(new HttpResponse({ body: specialiteCollection })));
      const additionalSpecialites = [specialite];
      const expectedCollection: ISpecialite[] = [...additionalSpecialites, ...specialiteCollection];
      jest.spyOn(specialiteService, 'addSpecialiteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ medecin });
      comp.ngOnInit();

      expect(specialiteService.query).toHaveBeenCalled();
      expect(specialiteService.addSpecialiteToCollectionIfMissing).toHaveBeenCalledWith(
        specialiteCollection,
        ...additionalSpecialites.map(expect.objectContaining)
      );
      expect(comp.specialitesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const medecin: IMedecin = { id: 456 };
      const formationSanitaire: IFormationSanitaire = { id: 13792 };
      medecin.formationSanitaire = formationSanitaire;
      const specialite: ISpecialite = { id: 2321 };
      medecin.specialite = specialite;

      activatedRoute.data = of({ medecin });
      comp.ngOnInit();

      expect(comp.formationSanitairesSharedCollection).toContain(formationSanitaire);
      expect(comp.specialitesSharedCollection).toContain(specialite);
      expect(comp.medecin).toEqual(medecin);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IMedecin>>();
      const medecin = { id: 123 };
      jest.spyOn(medecinFormService, 'getMedecin').mockReturnValue(medecin);
      jest.spyOn(medecinService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ medecin });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: medecin }));
      saveSubject.complete();

      // THEN
      expect(medecinFormService.getMedecin).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(medecinService.update).toHaveBeenCalledWith(expect.objectContaining(medecin));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IMedecin>>();
      const medecin = { id: 123 };
      jest.spyOn(medecinFormService, 'getMedecin').mockReturnValue({ id: null });
      jest.spyOn(medecinService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ medecin: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: medecin }));
      saveSubject.complete();

      // THEN
      expect(medecinFormService.getMedecin).toHaveBeenCalled();
      expect(medecinService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IMedecin>>();
      const medecin = { id: 123 };
      jest.spyOn(medecinService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ medecin });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(medecinService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareFormationSanitaire', () => {
      it('Should forward to formationSanitaireService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(formationSanitaireService, 'compareFormationSanitaire');
        comp.compareFormationSanitaire(entity, entity2);
        expect(formationSanitaireService.compareFormationSanitaire).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareSpecialite', () => {
      it('Should forward to specialiteService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(specialiteService, 'compareSpecialite');
        comp.compareSpecialite(entity, entity2);
        expect(specialiteService.compareSpecialite).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
