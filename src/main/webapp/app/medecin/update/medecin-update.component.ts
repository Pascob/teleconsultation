import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { MedecinFormService, MedecinFormGroup } from './medecin-form.service';
import { IMedecin } from '../../models/medecin.model';
import { MedecinService } from '../../services/medecin.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IFormationSanitaire } from '../../models/formation-sanitaire.model';
import { ISpecialite } from '../../models/specialite.model';
import { SpecialiteService } from '../../services/specialite.service';
import { FormationSanitaireService } from '../../services/formation-sanitaire.service';

@Component({
  selector: 'jhi-medecin-update',
  templateUrl: './medecin-update.component.html',
})
export class MedecinUpdateComponent implements OnInit {
  isSaving = false;
  medecin: IMedecin | null = null;

  formationSanitairesSharedCollection: IFormationSanitaire[] = [];
  specialitesSharedCollection: ISpecialite[] = [];

  editForm: MedecinFormGroup = this.medecinFormService.createMedecinFormGroup();

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected medecinService: MedecinService,
    protected medecinFormService: MedecinFormService,
    protected formationSanitaireService: FormationSanitaireService,
    protected specialiteService: SpecialiteService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareFormationSanitaire = (o1: IFormationSanitaire | null, o2: IFormationSanitaire | null): boolean =>
    this.formationSanitaireService.compareFormationSanitaire(o1, o2);

  compareSpecialite = (o1: ISpecialite | null, o2: ISpecialite | null): boolean => this.specialiteService.compareSpecialite(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ medecin }) => {
      this.medecin = medecin;
      if (medecin) {
        this.updateForm(medecin);
      }

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('teleconsultationApp.error', { ...err, key: 'error.file.' + err.key })
        ),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const medecin = this.medecinFormService.getMedecin(this.editForm);
    if (medecin.id !== null) {
      this.subscribeToSaveResponse(this.medecinService.update(medecin));
    } else {
      this.subscribeToSaveResponse(this.medecinService.create(medecin));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMedecin>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(medecin: IMedecin): void {
    this.medecin = medecin;
    this.medecinFormService.resetForm(this.editForm, medecin);

    this.formationSanitairesSharedCollection =
      this.formationSanitaireService.addFormationSanitaireToCollectionIfMissing<IFormationSanitaire>(
        this.formationSanitairesSharedCollection,
        medecin.formationSanitaire
      );
    this.specialitesSharedCollection = this.specialiteService.addSpecialiteToCollectionIfMissing<ISpecialite>(
      this.specialitesSharedCollection,
      medecin.specialite
    );
  }

  protected loadRelationshipsOptions(): void {
    this.formationSanitaireService
      .query()
      .pipe(map((res: HttpResponse<IFormationSanitaire[]>) => res.body ?? []))
      .pipe(
        map((formationSanitaires: IFormationSanitaire[]) =>
          this.formationSanitaireService.addFormationSanitaireToCollectionIfMissing<IFormationSanitaire>(
            formationSanitaires,
            this.medecin?.formationSanitaire
          )
        )
      )
      .subscribe((formationSanitaires: IFormationSanitaire[]) => (this.formationSanitairesSharedCollection = formationSanitaires));

    this.specialiteService
      .query()
      .pipe(map((res: HttpResponse<ISpecialite[]>) => res.body ?? []))
      .pipe(
        map((specialites: ISpecialite[]) =>
          this.specialiteService.addSpecialiteToCollectionIfMissing<ISpecialite>(specialites, this.medecin?.specialite)
        )
      )
      .subscribe((specialites: ISpecialite[]) => (this.specialitesSharedCollection = specialites));
  }
}
