import dayjs from 'dayjs/esm';
import { TypeNotification } from 'app/enumerations/type-notification.model';

export interface INotification {
  id: number;
  dateNotification?: dayjs.Dayjs | null;
  objet?: string | null;
  corpus?: string | null;
  sender?: string | null;
  receiver?: string | null;
  typeNotification?: TypeNotification | null;
}

export type NewNotification = Omit<INotification, 'id'> & { id: null };
