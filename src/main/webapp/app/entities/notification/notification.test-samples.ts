import dayjs from 'dayjs/esm';

import { TypeNotification } from 'app/enumerations/type-notification.model';

import { INotification, NewNotification } from './notification.model';

export const sampleWithRequiredData: INotification = {
  id: 30621,
};

export const sampleWithPartialData: INotification = {
  id: 25492,
  corpus: 'Brooks',
  sender: 'Djibouti',
  receiver: 'synergies enterprise',
  typeNotification: TypeNotification['EMAIL'],
};

export const sampleWithFullData: INotification = {
  id: 19731,
  dateNotification: dayjs('2022-12-13T02:13'),
  objet: 'artificial bandwidth Denmark',
  corpus: 'Salad Market hacking',
  sender: 'Technician tan',
  receiver: 'Gorgeous Savings',
  typeNotification: TypeNotification['ALERT'],
};

export const sampleWithNewData: NewNotification = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
