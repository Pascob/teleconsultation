import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDossierMedical } from '../../../models/dossier-medical.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../dossier-medical.test-samples';

import { DossierMedicalService } from '../../../services/dossier-medical.service';

const requireRestSample: IDossierMedical = {
  ...sampleWithRequiredData,
};

describe('DossierMedical Service', () => {
  let service: DossierMedicalService;
  let httpMock: HttpTestingController;
  let expectedResult: IDossierMedical | IDossierMedical[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DossierMedicalService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a DossierMedical', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const dossierMedical = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(dossierMedical).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a DossierMedical', () => {
      const dossierMedical = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(dossierMedical).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a DossierMedical', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of DossierMedical', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a DossierMedical', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addDossierMedicalToCollectionIfMissing', () => {
      it('should add a DossierMedical to an empty array', () => {
        const dossierMedical: IDossierMedical = sampleWithRequiredData;
        expectedResult = service.addDossierMedicalToCollectionIfMissing([], dossierMedical);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dossierMedical);
      });

      it('should not add a DossierMedical to an array that contains it', () => {
        const dossierMedical: IDossierMedical = sampleWithRequiredData;
        const dossierMedicalCollection: IDossierMedical[] = [
          {
            ...dossierMedical,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addDossierMedicalToCollectionIfMissing(dossierMedicalCollection, dossierMedical);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a DossierMedical to an array that doesn't contain it", () => {
        const dossierMedical: IDossierMedical = sampleWithRequiredData;
        const dossierMedicalCollection: IDossierMedical[] = [sampleWithPartialData];
        expectedResult = service.addDossierMedicalToCollectionIfMissing(dossierMedicalCollection, dossierMedical);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dossierMedical);
      });

      it('should add only unique DossierMedical to an array', () => {
        const dossierMedicalArray: IDossierMedical[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const dossierMedicalCollection: IDossierMedical[] = [sampleWithRequiredData];
        expectedResult = service.addDossierMedicalToCollectionIfMissing(dossierMedicalCollection, ...dossierMedicalArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const dossierMedical: IDossierMedical = sampleWithRequiredData;
        const dossierMedical2: IDossierMedical = sampleWithPartialData;
        expectedResult = service.addDossierMedicalToCollectionIfMissing([], dossierMedical, dossierMedical2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dossierMedical);
        expect(expectedResult).toContain(dossierMedical2);
      });

      it('should accept null and undefined values', () => {
        const dossierMedical: IDossierMedical = sampleWithRequiredData;
        expectedResult = service.addDossierMedicalToCollectionIfMissing([], null, dossierMedical, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dossierMedical);
      });

      it('should return initial array if no DossierMedical is added', () => {
        const dossierMedicalCollection: IDossierMedical[] = [sampleWithRequiredData];
        expectedResult = service.addDossierMedicalToCollectionIfMissing(dossierMedicalCollection, undefined, null);
        expect(expectedResult).toEqual(dossierMedicalCollection);
      });
    });

    describe('compareDossierMedical', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareDossierMedical(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareDossierMedical(entity1, entity2);
        const compareResult2 = service.compareDossierMedical(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareDossierMedical(entity1, entity2);
        const compareResult2 = service.compareDossierMedical(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareDossierMedical(entity1, entity2);
        const compareResult2 = service.compareDossierMedical(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
