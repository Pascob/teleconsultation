import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DossierMedicalComponent } from '../list/dossier-medical.component';
import { DossierMedicalDetailComponent } from '../detail/dossier-medical-detail.component';
import { DossierMedicalUpdateComponent } from '../update/dossier-medical-update.component';
import { DossierMedicalRoutingResolveService } from '../../../resolvers/dossier-medical-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const dossierMedicalRoute: Routes = [
  {
    path: '',
    component: DossierMedicalComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DossierMedicalDetailComponent,
    resolve: {
      dossierMedical: DossierMedicalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DossierMedicalUpdateComponent,
    resolve: {
      dossierMedical: DossierMedicalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DossierMedicalUpdateComponent,
    resolve: {
      dossierMedical: DossierMedicalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dossierMedicalRoute)],
  exports: [RouterModule],
})
export class DossierMedicalRoutingModule {}
