import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DossierMedicalComponent } from './list/dossier-medical.component';
import { DossierMedicalDetailComponent } from './detail/dossier-medical-detail.component';
import { DossierMedicalUpdateComponent } from './update/dossier-medical-update.component';
import { DossierMedicalDeleteDialogComponent } from './delete/dossier-medical-delete-dialog.component';
import { DossierMedicalRoutingModule } from './route/dossier-medical-routing.module';

@NgModule({
  imports: [SharedModule, DossierMedicalRoutingModule],
  declarations: [
    DossierMedicalComponent,
    DossierMedicalDetailComponent,
    DossierMedicalUpdateComponent,
    DossierMedicalDeleteDialogComponent,
  ],
})
export class DossierMedicalModule {}
