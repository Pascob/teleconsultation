import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { DossierMedicalFormService, DossierMedicalFormGroup } from './dossier-medical-form.service';
import { IDossierMedical } from '../../../models/dossier-medical.model';
import { DossierMedicalService } from '../../../services/dossier-medical.service';
import { IMedecin } from 'app/models/medecin.model';
import { MedecinService } from 'app/services/medecin.service';
import { GroupeSanguin } from 'app/enumerations/groupe-sanguin.model';

@Component({
  selector: 'jhi-dossier-medical-update',
  templateUrl: './dossier-medical-update.component.html',
})
export class DossierMedicalUpdateComponent implements OnInit {
  isSaving = false;
  dossierMedical: IDossierMedical | null = null;
  groupeSanguinValues = Object.keys(GroupeSanguin);

  medecinsSharedCollection: IMedecin[] = [];

  editForm: DossierMedicalFormGroup = this.dossierMedicalFormService.createDossierMedicalFormGroup();

  constructor(
    protected dossierMedicalService: DossierMedicalService,
    protected dossierMedicalFormService: DossierMedicalFormService,
    protected medecinService: MedecinService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareMedecin = (o1: IMedecin | null, o2: IMedecin | null): boolean => this.medecinService.compareMedecin(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dossierMedical }) => {
      this.dossierMedical = dossierMedical;
      if (dossierMedical) {
        this.updateForm(dossierMedical);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dossierMedical = this.dossierMedicalFormService.getDossierMedical(this.editForm);
    if (dossierMedical.id !== null) {
      this.subscribeToSaveResponse(this.dossierMedicalService.update(dossierMedical));
    } else {
      this.subscribeToSaveResponse(this.dossierMedicalService.create(dossierMedical));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDossierMedical>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dossierMedical: IDossierMedical): void {
    this.dossierMedical = dossierMedical;
    this.dossierMedicalFormService.resetForm(this.editForm, dossierMedical);

    this.medecinsSharedCollection = this.medecinService.addMedecinToCollectionIfMissing<IMedecin>(
      this.medecinsSharedCollection,
      dossierMedical.medecin
    );
  }

  protected loadRelationshipsOptions(): void {
    this.medecinService
      .query()
      .pipe(map((res: HttpResponse<IMedecin[]>) => res.body ?? []))
      .pipe(
        map((medecins: IMedecin[]) => this.medecinService.addMedecinToCollectionIfMissing<IMedecin>(medecins, this.dossierMedical?.medecin))
      )
      .subscribe((medecins: IMedecin[]) => (this.medecinsSharedCollection = medecins));
  }
}
