import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../dossier-medical.test-samples';

import { DossierMedicalFormService } from './dossier-medical-form.service';

describe('DossierMedical Form Service', () => {
  let service: DossierMedicalFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DossierMedicalFormService);
  });

  describe('Service methods', () => {
    describe('createDossierMedicalFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createDossierMedicalFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            numero: expect.any(Object),
            groupeSanguin: expect.any(Object),
            medecin: expect.any(Object),
          })
        );
      });

      it('passing IDossierMedical should create a new form with FormGroup', () => {
        const formGroup = service.createDossierMedicalFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            numero: expect.any(Object),
            groupeSanguin: expect.any(Object),
            medecin: expect.any(Object),
          })
        );
      });
    });

    describe('getDossierMedical', () => {
      it('should return NewDossierMedical for default DossierMedical initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createDossierMedicalFormGroup(sampleWithNewData);

        const dossierMedical = service.getDossierMedical(formGroup) as any;

        expect(dossierMedical).toMatchObject(sampleWithNewData);
      });

      it('should return NewDossierMedical for empty DossierMedical initial value', () => {
        const formGroup = service.createDossierMedicalFormGroup();

        const dossierMedical = service.getDossierMedical(formGroup) as any;

        expect(dossierMedical).toMatchObject({});
      });

      it('should return IDossierMedical', () => {
        const formGroup = service.createDossierMedicalFormGroup(sampleWithRequiredData);

        const dossierMedical = service.getDossierMedical(formGroup) as any;

        expect(dossierMedical).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IDossierMedical should not enable id FormControl', () => {
        const formGroup = service.createDossierMedicalFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewDossierMedical should disable id FormControl', () => {
        const formGroup = service.createDossierMedicalFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
