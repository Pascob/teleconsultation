import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IDossierMedical, NewDossierMedical } from '../../../models/dossier-medical.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IDossierMedical for edit and NewDossierMedicalFormGroupInput for create.
 */
type DossierMedicalFormGroupInput = IDossierMedical | PartialWithRequiredKeyOf<NewDossierMedical>;

type DossierMedicalFormDefaults = Pick<NewDossierMedical, 'id'>;

type DossierMedicalFormGroupContent = {
  id: FormControl<IDossierMedical['id'] | NewDossierMedical['id']>;
  numero: FormControl<IDossierMedical['numero']>;
  groupeSanguin: FormControl<IDossierMedical['groupeSanguin']>;
  medecin: FormControl<IDossierMedical['medecin']>;
};

export type DossierMedicalFormGroup = FormGroup<DossierMedicalFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class DossierMedicalFormService {
  createDossierMedicalFormGroup(dossierMedical: DossierMedicalFormGroupInput = { id: null }): DossierMedicalFormGroup {
    const dossierMedicalRawValue = {
      ...this.getFormDefaults(),
      ...dossierMedical,
    };
    return new FormGroup<DossierMedicalFormGroupContent>({
      id: new FormControl(
        { value: dossierMedicalRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      numero: new FormControl(dossierMedicalRawValue.numero, {
        validators: [Validators.required],
      }),
      groupeSanguin: new FormControl(dossierMedicalRawValue.groupeSanguin),
      medecin: new FormControl(dossierMedicalRawValue.medecin),
    });
  }

  getDossierMedical(form: DossierMedicalFormGroup): IDossierMedical | NewDossierMedical {
    return form.getRawValue() as IDossierMedical | NewDossierMedical;
  }

  resetForm(form: DossierMedicalFormGroup, dossierMedical: DossierMedicalFormGroupInput): void {
    const dossierMedicalRawValue = { ...this.getFormDefaults(), ...dossierMedical };
    form.reset(
      {
        ...dossierMedicalRawValue,
        id: { value: dossierMedicalRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): DossierMedicalFormDefaults {
    return {
      id: null,
    };
  }
}
