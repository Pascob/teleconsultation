import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { DossierMedicalFormService } from './dossier-medical-form.service';
import { DossierMedicalService } from '../../../services/dossier-medical.service';
import { IDossierMedical } from '../../../models/dossier-medical.model';
import { IMedecin } from 'app/models/medecin.model';
import { MedecinService } from 'app/services/medecin.service';

import { DossierMedicalUpdateComponent } from './dossier-medical-update.component';

describe('DossierMedical Management Update Component', () => {
  let comp: DossierMedicalUpdateComponent;
  let fixture: ComponentFixture<DossierMedicalUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let dossierMedicalFormService: DossierMedicalFormService;
  let dossierMedicalService: DossierMedicalService;
  let medecinService: MedecinService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [DossierMedicalUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DossierMedicalUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DossierMedicalUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    dossierMedicalFormService = TestBed.inject(DossierMedicalFormService);
    dossierMedicalService = TestBed.inject(DossierMedicalService);
    medecinService = TestBed.inject(MedecinService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Medecin query and add missing value', () => {
      const dossierMedical: IDossierMedical = { id: 456 };
      const medecin: IMedecin = { id: 21043 };
      dossierMedical.medecin = medecin;

      const medecinCollection: IMedecin[] = [{ id: 77507 }];
      jest.spyOn(medecinService, 'query').mockReturnValue(of(new HttpResponse({ body: medecinCollection })));
      const additionalMedecins = [medecin];
      const expectedCollection: IMedecin[] = [...additionalMedecins, ...medecinCollection];
      jest.spyOn(medecinService, 'addMedecinToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ dossierMedical });
      comp.ngOnInit();

      expect(medecinService.query).toHaveBeenCalled();
      expect(medecinService.addMedecinToCollectionIfMissing).toHaveBeenCalledWith(
        medecinCollection,
        ...additionalMedecins.map(expect.objectContaining)
      );
      expect(comp.medecinsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const dossierMedical: IDossierMedical = { id: 456 };
      const medecin: IMedecin = { id: 97853 };
      dossierMedical.medecin = medecin;

      activatedRoute.data = of({ dossierMedical });
      comp.ngOnInit();

      expect(comp.medecinsSharedCollection).toContain(medecin);
      expect(comp.dossierMedical).toEqual(dossierMedical);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDossierMedical>>();
      const dossierMedical = { id: 123 };
      jest.spyOn(dossierMedicalFormService, 'getDossierMedical').mockReturnValue(dossierMedical);
      jest.spyOn(dossierMedicalService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ dossierMedical });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: dossierMedical }));
      saveSubject.complete();

      // THEN
      expect(dossierMedicalFormService.getDossierMedical).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(dossierMedicalService.update).toHaveBeenCalledWith(expect.objectContaining(dossierMedical));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDossierMedical>>();
      const dossierMedical = { id: 123 };
      jest.spyOn(dossierMedicalFormService, 'getDossierMedical').mockReturnValue({ id: null });
      jest.spyOn(dossierMedicalService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ dossierMedical: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: dossierMedical }));
      saveSubject.complete();

      // THEN
      expect(dossierMedicalFormService.getDossierMedical).toHaveBeenCalled();
      expect(dossierMedicalService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDossierMedical>>();
      const dossierMedical = { id: 123 };
      jest.spyOn(dossierMedicalService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ dossierMedical });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(dossierMedicalService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareMedecin', () => {
      it('Should forward to medecinService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(medecinService, 'compareMedecin');
        comp.compareMedecin(entity, entity2);
        expect(medecinService.compareMedecin).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
