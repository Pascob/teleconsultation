import { GroupeSanguin } from 'app/enumerations/groupe-sanguin.model';

import { IDossierMedical, NewDossierMedical } from '../../models/dossier-medical.model';

export const sampleWithRequiredData: IDossierMedical = {
  id: 28762,
  numero: 'efficient withdrawal',
};

export const sampleWithPartialData: IDossierMedical = {
  id: 3950,
  numero: 'Florida compress',
  groupeSanguin: GroupeSanguin['O_PLUS'],
};

export const sampleWithFullData: IDossierMedical = {
  id: 37609,
  numero: 'withdrawal SMS',
  groupeSanguin: GroupeSanguin['AB_MOINS'],
};

export const sampleWithNewData: NewDossierMedical = {
  numero: 'Frozen synthesize group',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
