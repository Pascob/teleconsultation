import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDossierMedical } from '../../../models/dossier-medical.model';
import { DossierMedicalService } from '../../../services/dossier-medical.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './dossier-medical-delete-dialog.component.html',
})
export class DossierMedicalDeleteDialogComponent {
  dossierMedical?: IDossierMedical;

  constructor(protected dossierMedicalService: DossierMedicalService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dossierMedicalService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
