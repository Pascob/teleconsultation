import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DossierMedicalDetailComponent } from './dossier-medical-detail.component';

describe('DossierMedical Management Detail Component', () => {
  let comp: DossierMedicalDetailComponent;
  let fixture: ComponentFixture<DossierMedicalDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DossierMedicalDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ dossierMedical: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(DossierMedicalDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(DossierMedicalDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load dossierMedical on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.dossierMedical).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
