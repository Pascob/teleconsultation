import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDossierMedical } from '../../../models/dossier-medical.model';

@Component({
  selector: 'jhi-dossier-medical-detail',
  templateUrl: './dossier-medical-detail.component.html',
})
export class DossierMedicalDetailComponent implements OnInit {
  dossierMedical: IDossierMedical | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dossierMedical }) => {
      this.dossierMedical = dossierMedical;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
