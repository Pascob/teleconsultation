import dayjs from 'dayjs/esm';

import { IMessage, NewMessage } from './message.model';

export const sampleWithRequiredData: IMessage = {
  id: 29027,
};

export const sampleWithPartialData: IMessage = {
  id: 28491,
  dateMessage: dayjs('2022-12-13T17:16'),
  userIdFrom: 57303,
  readed: true,
};

export const sampleWithFullData: IMessage = {
  id: 94765,
  dateMessage: dayjs('2022-12-13T08:26'),
  message: 'hack Global Louisiana',
  userIdFrom: 57475,
  userIdTo: 16312,
  readed: false,
};

export const sampleWithNewData: NewMessage = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
