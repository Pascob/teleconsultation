import dayjs from 'dayjs/esm';

export interface IMessage {
  id: number;
  dateMessage?: dayjs.Dayjs | null;
  message?: string | null;
  userIdFrom?: number | null;
  userIdTo?: number | null;
  readed?: boolean | null;
}

export type NewMessage = Omit<IMessage, 'id'> & { id: null };
