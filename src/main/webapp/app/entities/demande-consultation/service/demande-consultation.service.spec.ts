import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDemandeConsultation } from '../../../models/demande-consultation.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../demande-consultation.test-samples';

import { DemandeConsultationService, RestDemandeConsultation } from '../../../services/demande-consultation.service';

const requireRestSample: RestDemandeConsultation = {
  ...sampleWithRequiredData,
  dateHeureDemande: sampleWithRequiredData.dateHeureDemande?.toJSON(),
};

describe('DemandeConsultation Service', () => {
  let service: DemandeConsultationService;
  let httpMock: HttpTestingController;
  let expectedResult: IDemandeConsultation | IDemandeConsultation[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DemandeConsultationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a DemandeConsultation', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const demandeConsultation = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(demandeConsultation).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a DemandeConsultation', () => {
      const demandeConsultation = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(demandeConsultation).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a DemandeConsultation', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of DemandeConsultation', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a DemandeConsultation', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addDemandeConsultationToCollectionIfMissing', () => {
      it('should add a DemandeConsultation to an empty array', () => {
        const demandeConsultation: IDemandeConsultation = sampleWithRequiredData;
        expectedResult = service.addDemandeConsultationToCollectionIfMissing([], demandeConsultation);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(demandeConsultation);
      });

      it('should not add a DemandeConsultation to an array that contains it', () => {
        const demandeConsultation: IDemandeConsultation = sampleWithRequiredData;
        const demandeConsultationCollection: IDemandeConsultation[] = [
          {
            ...demandeConsultation,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addDemandeConsultationToCollectionIfMissing(demandeConsultationCollection, demandeConsultation);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a DemandeConsultation to an array that doesn't contain it", () => {
        const demandeConsultation: IDemandeConsultation = sampleWithRequiredData;
        const demandeConsultationCollection: IDemandeConsultation[] = [sampleWithPartialData];
        expectedResult = service.addDemandeConsultationToCollectionIfMissing(demandeConsultationCollection, demandeConsultation);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(demandeConsultation);
      });

      it('should add only unique DemandeConsultation to an array', () => {
        const demandeConsultationArray: IDemandeConsultation[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const demandeConsultationCollection: IDemandeConsultation[] = [sampleWithRequiredData];
        expectedResult = service.addDemandeConsultationToCollectionIfMissing(demandeConsultationCollection, ...demandeConsultationArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const demandeConsultation: IDemandeConsultation = sampleWithRequiredData;
        const demandeConsultation2: IDemandeConsultation = sampleWithPartialData;
        expectedResult = service.addDemandeConsultationToCollectionIfMissing([], demandeConsultation, demandeConsultation2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(demandeConsultation);
        expect(expectedResult).toContain(demandeConsultation2);
      });

      it('should accept null and undefined values', () => {
        const demandeConsultation: IDemandeConsultation = sampleWithRequiredData;
        expectedResult = service.addDemandeConsultationToCollectionIfMissing([], null, demandeConsultation, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(demandeConsultation);
      });

      it('should return initial array if no DemandeConsultation is added', () => {
        const demandeConsultationCollection: IDemandeConsultation[] = [sampleWithRequiredData];
        expectedResult = service.addDemandeConsultationToCollectionIfMissing(demandeConsultationCollection, undefined, null);
        expect(expectedResult).toEqual(demandeConsultationCollection);
      });
    });

    describe('compareDemandeConsultation', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareDemandeConsultation(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareDemandeConsultation(entity1, entity2);
        const compareResult2 = service.compareDemandeConsultation(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareDemandeConsultation(entity1, entity2);
        const compareResult2 = service.compareDemandeConsultation(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareDemandeConsultation(entity1, entity2);
        const compareResult2 = service.compareDemandeConsultation(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
