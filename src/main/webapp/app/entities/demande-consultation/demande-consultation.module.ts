import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DemandeConsultationComponent } from './list/demande-consultation.component';
import { DemandeConsultationDetailComponent } from './detail/demande-consultation-detail.component';
import { DemandeConsultationUpdateComponent } from '../../services/forms/demande-consultation-update.component';
import { DemandeConsultationDeleteDialogComponent } from './delete/demande-consultation-delete-dialog.component';
import { DemandeConsultationRoutingModule } from './route/demande-consultation-routing.module';

@NgModule({
  imports: [SharedModule, DemandeConsultationRoutingModule],
  declarations: [
    DemandeConsultationComponent,
    DemandeConsultationDetailComponent,
    DemandeConsultationUpdateComponent,
    DemandeConsultationDeleteDialogComponent,
  ],
})
export class DemandeConsultationModule {}
