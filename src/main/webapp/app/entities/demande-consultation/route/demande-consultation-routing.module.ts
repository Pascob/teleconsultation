import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DemandeConsultationComponent } from '../list/demande-consultation.component';
import { DemandeConsultationDetailComponent } from '../detail/demande-consultation-detail.component';
import { DemandeConsultationUpdateComponent } from '../../../services/forms/demande-consultation-update.component';
import { DemandeConsultationRoutingResolveService } from '../../../services/demande-consultation-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const demandeConsultationRoute: Routes = [
  {
    path: '',
    component: DemandeConsultationComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DemandeConsultationDetailComponent,
    resolve: {
      demandeConsultation: DemandeConsultationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DemandeConsultationUpdateComponent,
    resolve: {
      demandeConsultation: DemandeConsultationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DemandeConsultationUpdateComponent,
    resolve: {
      demandeConsultation: DemandeConsultationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(demandeConsultationRoute)],
  exports: [RouterModule],
})
export class DemandeConsultationRoutingModule {}
