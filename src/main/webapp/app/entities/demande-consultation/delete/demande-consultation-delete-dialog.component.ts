import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDemandeConsultation } from '../../../models/demande-consultation.model';
import { DemandeConsultationService } from '../../../services/demande-consultation.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './demande-consultation-delete-dialog.component.html',
})
export class DemandeConsultationDeleteDialogComponent {
  demandeConsultation?: IDemandeConsultation;

  constructor(protected demandeConsultationService: DemandeConsultationService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.demandeConsultationService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
