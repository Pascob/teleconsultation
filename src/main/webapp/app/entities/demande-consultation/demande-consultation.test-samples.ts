import dayjs from 'dayjs/esm';

import { StatutDemandeConsultation } from 'app/enumerations/statut-demande-consultation.model';

import { IDemandeConsultation, NewDemandeConsultation } from '../../models/demande-consultation.model';

export const sampleWithRequiredData: IDemandeConsultation = {
  id: 25396,
};

export const sampleWithPartialData: IDemandeConsultation = {
  id: 86737,
  ordre: 96849,
  codeOtp: 'middleware',
  telephonePaiement: 'Chicken 1080p Loan',
  montant: 22388,
};

export const sampleWithFullData: IDemandeConsultation = {
  id: 11358,
  dateHeureDemande: dayjs('2022-12-12T22:17'),
  ordre: 72338,
  statut: StatutDemandeConsultation['EN_CONSULTATION'],
  codeOtp: 'Chief',
  telephonePaiement: 'SQL withdrawal platforms',
  montant: 1528,
};

export const sampleWithNewData: NewDemandeConsultation = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
