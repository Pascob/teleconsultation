import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DemandeConsultationDetailComponent } from './demande-consultation-detail.component';

describe('DemandeConsultation Management Detail Component', () => {
  let comp: DemandeConsultationDetailComponent;
  let fixture: ComponentFixture<DemandeConsultationDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DemandeConsultationDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ demandeConsultation: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(DemandeConsultationDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(DemandeConsultationDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load demandeConsultation on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.demandeConsultation).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
