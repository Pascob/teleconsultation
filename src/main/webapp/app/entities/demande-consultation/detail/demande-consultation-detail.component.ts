import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDemandeConsultation } from '../../../models/demande-consultation.model';

@Component({
  selector: 'jhi-demande-consultation-detail',
  templateUrl: './demande-consultation-detail.component.html',
})
export class DemandeConsultationDetailComponent implements OnInit {
  demandeConsultation: IDemandeConsultation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ demandeConsultation }) => {
      this.demandeConsultation = demandeConsultation;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
