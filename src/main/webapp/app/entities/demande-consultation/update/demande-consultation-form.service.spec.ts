import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../demande-consultation.test-samples';

import { DemandeConsultationFormService } from '../../../services/forms/demande-consultation-form.service';

describe('DemandeConsultation Form Service', () => {
  let service: DemandeConsultationFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DemandeConsultationFormService);
  });

  describe('Service methods', () => {
    describe('createDemandeConsultationFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createDemandeConsultationFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateHeureDemande: expect.any(Object),
            ordre: expect.any(Object),
            statut: expect.any(Object),
            codeOtp: expect.any(Object),
            telephonePaiement: expect.any(Object),
            montant: expect.any(Object),
            specialite: expect.any(Object),
            dossierMedical: expect.any(Object),
          })
        );
      });

      it('passing IDemandeConsultation should create a new form with FormGroup', () => {
        const formGroup = service.createDemandeConsultationFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateHeureDemande: expect.any(Object),
            ordre: expect.any(Object),
            statut: expect.any(Object),
            codeOtp: expect.any(Object),
            telephonePaiement: expect.any(Object),
            montant: expect.any(Object),
            specialite: expect.any(Object),
            dossierMedical: expect.any(Object),
          })
        );
      });
    });

    describe('getDemandeConsultation', () => {
      it('should return NewDemandeConsultation for default DemandeConsultation initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createDemandeConsultationFormGroup(sampleWithNewData);

        const demandeConsultation = service.getDemandeConsultation(formGroup) as any;

        expect(demandeConsultation).toMatchObject(sampleWithNewData);
      });

      it('should return NewDemandeConsultation for empty DemandeConsultation initial value', () => {
        const formGroup = service.createDemandeConsultationFormGroup();

        const demandeConsultation = service.getDemandeConsultation(formGroup) as any;

        expect(demandeConsultation).toMatchObject({});
      });

      it('should return IDemandeConsultation', () => {
        const formGroup = service.createDemandeConsultationFormGroup(sampleWithRequiredData);

        const demandeConsultation = service.getDemandeConsultation(formGroup) as any;

        expect(demandeConsultation).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IDemandeConsultation should not enable id FormControl', () => {
        const formGroup = service.createDemandeConsultationFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewDemandeConsultation should disable id FormControl', () => {
        const formGroup = service.createDemandeConsultationFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
