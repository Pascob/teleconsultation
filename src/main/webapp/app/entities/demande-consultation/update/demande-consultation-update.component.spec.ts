import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { DemandeConsultationFormService } from '../../../services/forms/demande-consultation-form.service';
import { DemandeConsultationService } from '../../../services/demande-consultation.service';
import { IDemandeConsultation } from '../../../models/demande-consultation.model';
import { IDossierMedical } from 'app/models/dossier-medical.model';
import { DossierMedicalService } from 'app/services/dossier-medical.service';

import { DemandeConsultationUpdateComponent } from '../../../services/forms/demande-consultation-update.component';
import { SpecialiteService } from '../../../services/specialite.service';
import { ISpecialite } from '../../../models/specialite.model';

describe('DemandeConsultation Management Update Component', () => {
  let comp: DemandeConsultationUpdateComponent;
  let fixture: ComponentFixture<DemandeConsultationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let demandeConsultationFormService: DemandeConsultationFormService;
  let demandeConsultationService: DemandeConsultationService;
  let specialiteService: SpecialiteService;
  let dossierMedicalService: DossierMedicalService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [DemandeConsultationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DemandeConsultationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DemandeConsultationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    demandeConsultationFormService = TestBed.inject(DemandeConsultationFormService);
    demandeConsultationService = TestBed.inject(DemandeConsultationService);
    specialiteService = TestBed.inject(SpecialiteService);
    dossierMedicalService = TestBed.inject(DossierMedicalService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Specialite query and add missing value', () => {
      const demandeConsultation: IDemandeConsultation = { id: 456 };
      const specialite: ISpecialite = { id: 45119 };
      demandeConsultation.specialite = specialite;

      const specialiteCollection: ISpecialite[] = [{ id: 56393 }];
      jest.spyOn(specialiteService, 'query').mockReturnValue(of(new HttpResponse({ body: specialiteCollection })));
      const additionalSpecialites = [specialite];
      const expectedCollection: ISpecialite[] = [...additionalSpecialites, ...specialiteCollection];
      jest.spyOn(specialiteService, 'addSpecialiteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ demandeConsultation });
      comp.ngOnInit();

      expect(specialiteService.query).toHaveBeenCalled();
      expect(specialiteService.addSpecialiteToCollectionIfMissing).toHaveBeenCalledWith(
        specialiteCollection,
        ...additionalSpecialites.map(expect.objectContaining)
      );
      expect(comp.specialitesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call DossierMedical query and add missing value', () => {
      const demandeConsultation: IDemandeConsultation = { id: 456 };
      const dossierMedical: IDossierMedical = { id: 69765 };
      demandeConsultation.dossierMedical = dossierMedical;

      const dossierMedicalCollection: IDossierMedical[] = [{ id: 7573 }];
      jest.spyOn(dossierMedicalService, 'query').mockReturnValue(of(new HttpResponse({ body: dossierMedicalCollection })));
      const additionalDossierMedicals = [dossierMedical];
      const expectedCollection: IDossierMedical[] = [...additionalDossierMedicals, ...dossierMedicalCollection];
      jest.spyOn(dossierMedicalService, 'addDossierMedicalToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ demandeConsultation });
      comp.ngOnInit();

      expect(dossierMedicalService.query).toHaveBeenCalled();
      expect(dossierMedicalService.addDossierMedicalToCollectionIfMissing).toHaveBeenCalledWith(
        dossierMedicalCollection,
        ...additionalDossierMedicals.map(expect.objectContaining)
      );
      expect(comp.dossierMedicalsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const demandeConsultation: IDemandeConsultation = { id: 456 };
      const specialite: ISpecialite = { id: 18897 };
      demandeConsultation.specialite = specialite;
      const dossierMedical: IDossierMedical = { id: 17340 };
      demandeConsultation.dossierMedical = dossierMedical;

      activatedRoute.data = of({ demandeConsultation });
      comp.ngOnInit();

      expect(comp.specialitesSharedCollection).toContain(specialite);
      expect(comp.dossierMedicalsSharedCollection).toContain(dossierMedical);
      expect(comp.demandeConsultation).toEqual(demandeConsultation);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDemandeConsultation>>();
      const demandeConsultation = { id: 123 };
      jest.spyOn(demandeConsultationFormService, 'getDemandeConsultation').mockReturnValue(demandeConsultation);
      jest.spyOn(demandeConsultationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ demandeConsultation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: demandeConsultation }));
      saveSubject.complete();

      // THEN
      expect(demandeConsultationFormService.getDemandeConsultation).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(demandeConsultationService.update).toHaveBeenCalledWith(expect.objectContaining(demandeConsultation));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDemandeConsultation>>();
      const demandeConsultation = { id: 123 };
      jest.spyOn(demandeConsultationFormService, 'getDemandeConsultation').mockReturnValue({ id: null });
      jest.spyOn(demandeConsultationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ demandeConsultation: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: demandeConsultation }));
      saveSubject.complete();

      // THEN
      expect(demandeConsultationFormService.getDemandeConsultation).toHaveBeenCalled();
      expect(demandeConsultationService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDemandeConsultation>>();
      const demandeConsultation = { id: 123 };
      jest.spyOn(demandeConsultationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ demandeConsultation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(demandeConsultationService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareSpecialite', () => {
      it('Should forward to specialiteService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(specialiteService, 'compareSpecialite');
        comp.compareSpecialite(entity, entity2);
        expect(specialiteService.compareSpecialite).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareDossierMedical', () => {
      it('Should forward to dossierMedicalService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(dossierMedicalService, 'compareDossierMedical');
        comp.compareDossierMedical(entity, entity2);
        expect(dossierMedicalService.compareDossierMedical).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
