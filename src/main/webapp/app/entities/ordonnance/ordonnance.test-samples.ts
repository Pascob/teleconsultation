import dayjs from 'dayjs/esm';

import { IOrdonnance, NewOrdonnance } from '../../models/ordonnance.model';

export const sampleWithRequiredData: IOrdonnance = {
  id: 30275,
  dateOrdonnance: dayjs('2022-12-13'),
};

export const sampleWithPartialData: IOrdonnance = {
  id: 81424,
  dateOrdonnance: dayjs('2022-12-13'),
  numero: 'Music',
};

export const sampleWithFullData: IOrdonnance = {
  id: 85106,
  dateOrdonnance: dayjs('2022-12-13'),
  numero: 'Cambridgeshire Bedfordshire Utah',
};

export const sampleWithNewData: NewOrdonnance = {
  dateOrdonnance: dayjs('2022-12-13'),
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
