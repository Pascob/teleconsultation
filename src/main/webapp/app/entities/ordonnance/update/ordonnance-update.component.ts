import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { OrdonnanceFormService, OrdonnanceFormGroup } from './ordonnance-form.service';
import { IOrdonnance } from '../../../models/ordonnance.model';
import { OrdonnanceService } from '../../../services/ordonnance.service';
import { IFicheConsultation } from 'app/models/fiche-consultation.model';
import { FicheConsultationService } from 'app/services/fiche-consultation.service';

@Component({
  selector: 'jhi-ordonnance-update',
  templateUrl: './ordonnance-update.component.html',
})
export class OrdonnanceUpdateComponent implements OnInit {
  isSaving = false;
  ordonnance: IOrdonnance | null = null;

  ficheConsultationsSharedCollection: IFicheConsultation[] = [];

  editForm: OrdonnanceFormGroup = this.ordonnanceFormService.createOrdonnanceFormGroup();

  constructor(
    protected ordonnanceService: OrdonnanceService,
    protected ordonnanceFormService: OrdonnanceFormService,
    protected ficheConsultationService: FicheConsultationService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareFicheConsultation = (o1: IFicheConsultation | null, o2: IFicheConsultation | null): boolean =>
    this.ficheConsultationService.compareFicheConsultation(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ordonnance }) => {
      this.ordonnance = ordonnance;
      if (ordonnance) {
        this.updateForm(ordonnance);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ordonnance = this.ordonnanceFormService.getOrdonnance(this.editForm);
    if (ordonnance.id !== null) {
      this.subscribeToSaveResponse(this.ordonnanceService.update(ordonnance));
    } else {
      this.subscribeToSaveResponse(this.ordonnanceService.create(ordonnance));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrdonnance>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(ordonnance: IOrdonnance): void {
    this.ordonnance = ordonnance;
    this.ordonnanceFormService.resetForm(this.editForm, ordonnance);

    this.ficheConsultationsSharedCollection = this.ficheConsultationService.addFicheConsultationToCollectionIfMissing<IFicheConsultation>(
      this.ficheConsultationsSharedCollection,
      ordonnance.ficheConsultation
    );
  }

  protected loadRelationshipsOptions(): void {
    this.ficheConsultationService
      .query()
      .pipe(map((res: HttpResponse<IFicheConsultation[]>) => res.body ?? []))
      .pipe(
        map((ficheConsultations: IFicheConsultation[]) =>
          this.ficheConsultationService.addFicheConsultationToCollectionIfMissing<IFicheConsultation>(
            ficheConsultations,
            this.ordonnance?.ficheConsultation
          )
        )
      )
      .subscribe((ficheConsultations: IFicheConsultation[]) => (this.ficheConsultationsSharedCollection = ficheConsultations));
  }
}
