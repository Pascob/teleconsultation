import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { OrdonnanceFormService } from './ordonnance-form.service';
import { OrdonnanceService } from '../../../services/ordonnance.service';
import { IOrdonnance } from '../../../models/ordonnance.model';
import { IFicheConsultation } from 'app/models/fiche-consultation.model';
import { FicheConsultationService } from 'app/services/fiche-consultation.service';

import { OrdonnanceUpdateComponent } from './ordonnance-update.component';

describe('Ordonnance Management Update Component', () => {
  let comp: OrdonnanceUpdateComponent;
  let fixture: ComponentFixture<OrdonnanceUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let ordonnanceFormService: OrdonnanceFormService;
  let ordonnanceService: OrdonnanceService;
  let ficheConsultationService: FicheConsultationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [OrdonnanceUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(OrdonnanceUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OrdonnanceUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    ordonnanceFormService = TestBed.inject(OrdonnanceFormService);
    ordonnanceService = TestBed.inject(OrdonnanceService);
    ficheConsultationService = TestBed.inject(FicheConsultationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call FicheConsultation query and add missing value', () => {
      const ordonnance: IOrdonnance = { id: 456 };
      const ficheConsultation: IFicheConsultation = { id: 43253 };
      ordonnance.ficheConsultation = ficheConsultation;

      const ficheConsultationCollection: IFicheConsultation[] = [{ id: 64886 }];
      jest.spyOn(ficheConsultationService, 'query').mockReturnValue(of(new HttpResponse({ body: ficheConsultationCollection })));
      const additionalFicheConsultations = [ficheConsultation];
      const expectedCollection: IFicheConsultation[] = [...additionalFicheConsultations, ...ficheConsultationCollection];
      jest.spyOn(ficheConsultationService, 'addFicheConsultationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ ordonnance });
      comp.ngOnInit();

      expect(ficheConsultationService.query).toHaveBeenCalled();
      expect(ficheConsultationService.addFicheConsultationToCollectionIfMissing).toHaveBeenCalledWith(
        ficheConsultationCollection,
        ...additionalFicheConsultations.map(expect.objectContaining)
      );
      expect(comp.ficheConsultationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const ordonnance: IOrdonnance = { id: 456 };
      const ficheConsultation: IFicheConsultation = { id: 983 };
      ordonnance.ficheConsultation = ficheConsultation;

      activatedRoute.data = of({ ordonnance });
      comp.ngOnInit();

      expect(comp.ficheConsultationsSharedCollection).toContain(ficheConsultation);
      expect(comp.ordonnance).toEqual(ordonnance);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrdonnance>>();
      const ordonnance = { id: 123 };
      jest.spyOn(ordonnanceFormService, 'getOrdonnance').mockReturnValue(ordonnance);
      jest.spyOn(ordonnanceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ordonnance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ordonnance }));
      saveSubject.complete();

      // THEN
      expect(ordonnanceFormService.getOrdonnance).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(ordonnanceService.update).toHaveBeenCalledWith(expect.objectContaining(ordonnance));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrdonnance>>();
      const ordonnance = { id: 123 };
      jest.spyOn(ordonnanceFormService, 'getOrdonnance').mockReturnValue({ id: null });
      jest.spyOn(ordonnanceService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ordonnance: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ordonnance }));
      saveSubject.complete();

      // THEN
      expect(ordonnanceFormService.getOrdonnance).toHaveBeenCalled();
      expect(ordonnanceService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOrdonnance>>();
      const ordonnance = { id: 123 };
      jest.spyOn(ordonnanceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ordonnance });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(ordonnanceService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareFicheConsultation', () => {
      it('Should forward to ficheConsultationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(ficheConsultationService, 'compareFicheConsultation');
        comp.compareFicheConsultation(entity, entity2);
        expect(ficheConsultationService.compareFicheConsultation).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
