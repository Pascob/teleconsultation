import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { BulletinFormService } from './bulletin-form.service';
import { BulletinService } from '../../../services/bulletin.service';
import { IBulletin } from '../../../models/bulletin.model';
import { IFicheConsultation } from 'app/models/fiche-consultation.model';
import { FicheConsultationService } from 'app/services/fiche-consultation.service';

import { BulletinUpdateComponent } from './bulletin-update.component';

describe('Bulletin Management Update Component', () => {
  let comp: BulletinUpdateComponent;
  let fixture: ComponentFixture<BulletinUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let bulletinFormService: BulletinFormService;
  let bulletinService: BulletinService;
  let ficheConsultationService: FicheConsultationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [BulletinUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BulletinUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BulletinUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    bulletinFormService = TestBed.inject(BulletinFormService);
    bulletinService = TestBed.inject(BulletinService);
    ficheConsultationService = TestBed.inject(FicheConsultationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call FicheConsultation query and add missing value', () => {
      const bulletin: IBulletin = { id: 456 };
      const ficheConsultation: IFicheConsultation = { id: 57588 };
      bulletin.ficheConsultation = ficheConsultation;

      const ficheConsultationCollection: IFicheConsultation[] = [{ id: 36485 }];
      jest.spyOn(ficheConsultationService, 'query').mockReturnValue(of(new HttpResponse({ body: ficheConsultationCollection })));
      const additionalFicheConsultations = [ficheConsultation];
      const expectedCollection: IFicheConsultation[] = [...additionalFicheConsultations, ...ficheConsultationCollection];
      jest.spyOn(ficheConsultationService, 'addFicheConsultationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ bulletin });
      comp.ngOnInit();

      expect(ficheConsultationService.query).toHaveBeenCalled();
      expect(ficheConsultationService.addFicheConsultationToCollectionIfMissing).toHaveBeenCalledWith(
        ficheConsultationCollection,
        ...additionalFicheConsultations.map(expect.objectContaining)
      );
      expect(comp.ficheConsultationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const bulletin: IBulletin = { id: 456 };
      const ficheConsultation: IFicheConsultation = { id: 29096 };
      bulletin.ficheConsultation = ficheConsultation;

      activatedRoute.data = of({ bulletin });
      comp.ngOnInit();

      expect(comp.ficheConsultationsSharedCollection).toContain(ficheConsultation);
      expect(comp.bulletin).toEqual(bulletin);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBulletin>>();
      const bulletin = { id: 123 };
      jest.spyOn(bulletinFormService, 'getBulletin').mockReturnValue(bulletin);
      jest.spyOn(bulletinService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ bulletin });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: bulletin }));
      saveSubject.complete();

      // THEN
      expect(bulletinFormService.getBulletin).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(bulletinService.update).toHaveBeenCalledWith(expect.objectContaining(bulletin));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBulletin>>();
      const bulletin = { id: 123 };
      jest.spyOn(bulletinFormService, 'getBulletin').mockReturnValue({ id: null });
      jest.spyOn(bulletinService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ bulletin: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: bulletin }));
      saveSubject.complete();

      // THEN
      expect(bulletinFormService.getBulletin).toHaveBeenCalled();
      expect(bulletinService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBulletin>>();
      const bulletin = { id: 123 };
      jest.spyOn(bulletinService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ bulletin });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(bulletinService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareFicheConsultation', () => {
      it('Should forward to ficheConsultationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(ficheConsultationService, 'compareFicheConsultation');
        comp.compareFicheConsultation(entity, entity2);
        expect(ficheConsultationService.compareFicheConsultation).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
