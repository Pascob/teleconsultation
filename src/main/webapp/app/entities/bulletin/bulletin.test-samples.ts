import dayjs from 'dayjs/esm';

import { IBulletin, NewBulletin } from '../../models/bulletin.model';

export const sampleWithRequiredData: IBulletin = {
  id: 65533,
  dateBulletin: dayjs('2022-12-13'),
};

export const sampleWithPartialData: IBulletin = {
  id: 89148,
  dateBulletin: dayjs('2022-12-13'),
  numero: 'withdrawal gold',
};

export const sampleWithFullData: IBulletin = {
  id: 21522,
  dateBulletin: dayjs('2022-12-13'),
  numero: 'optical Soft',
};

export const sampleWithNewData: NewBulletin = {
  dateBulletin: dayjs('2022-12-13'),
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
