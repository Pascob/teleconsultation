import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PatientFormService } from './patient-form.service';
import { PatientService } from '../../../services/patient.service';
import { IPatient } from '../../../models/patient.model';
import { IDossierMedical } from 'app/models/dossier-medical.model';
import { DossierMedicalService } from 'app/services/dossier-medical.service';

import { PatientUpdateComponent } from './patient-update.component';

describe('Patient Management Update Component', () => {
  let comp: PatientUpdateComponent;
  let fixture: ComponentFixture<PatientUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let patientFormService: PatientFormService;
  let patientService: PatientService;
  let dossierMedicalService: DossierMedicalService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PatientUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PatientUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PatientUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    patientFormService = TestBed.inject(PatientFormService);
    patientService = TestBed.inject(PatientService);
    dossierMedicalService = TestBed.inject(DossierMedicalService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call dossierMedical query and add missing value', () => {
      const patient: IPatient = { id: 456 };
      const dossierMedical: IDossierMedical = { id: 37363 };
      patient.dossierMedical = dossierMedical;

      const dossierMedicalCollection: IDossierMedical[] = [{ id: 79433 }];
      jest.spyOn(dossierMedicalService, 'query').mockReturnValue(of(new HttpResponse({ body: dossierMedicalCollection })));
      const expectedCollection: IDossierMedical[] = [dossierMedical, ...dossierMedicalCollection];
      jest.spyOn(dossierMedicalService, 'addDossierMedicalToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ patient });
      comp.ngOnInit();

      expect(dossierMedicalService.query).toHaveBeenCalled();
      expect(dossierMedicalService.addDossierMedicalToCollectionIfMissing).toHaveBeenCalledWith(dossierMedicalCollection, dossierMedical);
      expect(comp.dossierMedicalsCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const patient: IPatient = { id: 456 };
      const dossierMedical: IDossierMedical = { id: 66641 };
      patient.dossierMedical = dossierMedical;

      activatedRoute.data = of({ patient });
      comp.ngOnInit();

      expect(comp.dossierMedicalsCollection).toContain(dossierMedical);
      expect(comp.patient).toEqual(patient);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPatient>>();
      const patient = { id: 123 };
      jest.spyOn(patientFormService, 'getPatient').mockReturnValue(patient);
      jest.spyOn(patientService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ patient });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: patient }));
      saveSubject.complete();

      // THEN
      expect(patientFormService.getPatient).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(patientService.update).toHaveBeenCalledWith(expect.objectContaining(patient));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPatient>>();
      const patient = { id: 123 };
      jest.spyOn(patientFormService, 'getPatient').mockReturnValue({ id: null });
      jest.spyOn(patientService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ patient: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: patient }));
      saveSubject.complete();

      // THEN
      expect(patientFormService.getPatient).toHaveBeenCalled();
      expect(patientService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPatient>>();
      const patient = { id: 123 };
      jest.spyOn(patientService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ patient });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(patientService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareDossierMedical', () => {
      it('Should forward to dossierMedicalService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(dossierMedicalService, 'compareDossierMedical');
        comp.compareDossierMedical(entity, entity2);
        expect(dossierMedicalService.compareDossierMedical).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
