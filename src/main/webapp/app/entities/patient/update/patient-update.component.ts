import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { PatientFormService, PatientFormGroup } from './patient-form.service';
import { IPatient } from '../../../models/patient.model';
import { PatientService } from '../../../services/patient.service';
import { IDossierMedical } from 'app/models/dossier-medical.model';
import { DossierMedicalService } from 'app/services/dossier-medical.service';
import { Genre } from 'app/enumerations/genre.model';

@Component({
  selector: 'jhi-patient-update',
  templateUrl: './patient-update.component.html',
})
export class PatientUpdateComponent implements OnInit {
  isSaving = false;
  patient: IPatient | null = null;
  genreValues = Object.keys(Genre);

  dossierMedicalsCollection: IDossierMedical[] = [];

  editForm: PatientFormGroup = this.patientFormService.createPatientFormGroup();

  constructor(
    protected patientService: PatientService,
    protected patientFormService: PatientFormService,
    protected dossierMedicalService: DossierMedicalService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareDossierMedical = (o1: IDossierMedical | null, o2: IDossierMedical | null): boolean =>
    this.dossierMedicalService.compareDossierMedical(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ patient }) => {
      this.patient = patient;
      if (patient) {
        this.updateForm(patient);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const patient = this.patientFormService.getPatient(this.editForm);
    if (patient.id !== null) {
      this.subscribeToSaveResponse(this.patientService.update(patient));
    } else {
      this.subscribeToSaveResponse(this.patientService.create(patient));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPatient>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(patient: IPatient): void {
    this.patient = patient;
    this.patientFormService.resetForm(this.editForm, patient);

    this.dossierMedicalsCollection = this.dossierMedicalService.addDossierMedicalToCollectionIfMissing<IDossierMedical>(
      this.dossierMedicalsCollection,
      patient.dossierMedical
    );
  }

  protected loadRelationshipsOptions(): void {
    this.dossierMedicalService
      .query({ 'patientId.specified': 'false' })
      .pipe(map((res: HttpResponse<IDossierMedical[]>) => res.body ?? []))
      .pipe(
        map((dossierMedicals: IDossierMedical[]) =>
          this.dossierMedicalService.addDossierMedicalToCollectionIfMissing<IDossierMedical>(dossierMedicals, this.patient?.dossierMedical)
        )
      )
      .subscribe((dossierMedicals: IDossierMedical[]) => (this.dossierMedicalsCollection = dossierMedicals));
  }
}
