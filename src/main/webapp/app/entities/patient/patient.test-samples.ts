import dayjs from 'dayjs/esm';

import { Genre } from 'app/enumerations/genre.model';

import { IPatient, NewPatient } from '../../models/patient.model';

export const sampleWithRequiredData: IPatient = {
  id: 39843,
  userId: 13496,
};

export const sampleWithPartialData: IPatient = {
  id: 57289,
  userId: 16988,
  dateNaissance: dayjs('2022-12-13'),
};

export const sampleWithFullData: IPatient = {
  id: 61013,
  userId: 21672,
  dateNaissance: dayjs('2022-12-13'),
  genre: Genre['M'],
};

export const sampleWithNewData: NewPatient = {
  userId: 96891,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
