import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFicheConsultation } from '../../../models/fiche-consultation.model';

@Component({
  selector: 'jhi-fiche-consultation-detail',
  templateUrl: './fiche-consultation-detail.component.html',
})
export class FicheConsultationDetailComponent implements OnInit {
  ficheConsultation: IFicheConsultation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ficheConsultation }) => {
      this.ficheConsultation = ficheConsultation;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
