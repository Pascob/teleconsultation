import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FicheConsultationDetailComponent } from './fiche-consultation-detail.component';

describe('FicheConsultation Management Detail Component', () => {
  let comp: FicheConsultationDetailComponent;
  let fixture: ComponentFixture<FicheConsultationDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FicheConsultationDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ ficheConsultation: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(FicheConsultationDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(FicheConsultationDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load ficheConsultation on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.ficheConsultation).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
