import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { FicheConsultationComponent } from './list/fiche-consultation.component';
import { FicheConsultationDetailComponent } from './detail/fiche-consultation-detail.component';
import { FicheConsultationUpdateComponent } from './update/fiche-consultation-update.component';
import { FicheConsultationDeleteDialogComponent } from './delete/fiche-consultation-delete-dialog.component';
import { FicheConsultationRoutingModule } from './route/fiche-consultation-routing.module';

@NgModule({
  imports: [SharedModule, FicheConsultationRoutingModule],
  declarations: [
    FicheConsultationComponent,
    FicheConsultationDetailComponent,
    FicheConsultationUpdateComponent,
    FicheConsultationDeleteDialogComponent,
  ],
})
export class FicheConsultationModule {}
