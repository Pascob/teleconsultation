import dayjs from 'dayjs/esm';

import { IFicheConsultation, NewFicheConsultation } from '../../models/fiche-consultation.model';

export const sampleWithRequiredData: IFicheConsultation = {
  id: 79702,
  dateFiche: dayjs('2022-12-13'),
};

export const sampleWithPartialData: IFicheConsultation = {
  id: 40608,
  dateFiche: dayjs('2022-12-13'),
};

export const sampleWithFullData: IFicheConsultation = {
  id: 5574,
  dateFiche: dayjs('2022-12-13'),
  numero: 'programming',
  observation: 'Rustic',
  diagnostic: 'Strategist West',
};

export const sampleWithNewData: NewFicheConsultation = {
  dateFiche: dayjs('2022-12-13'),
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
