import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IFicheConsultation } from '../../../models/fiche-consultation.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../fiche-consultation.test-samples';

import { FicheConsultationService, RestFicheConsultation } from '../../../services/fiche-consultation.service';

const requireRestSample: RestFicheConsultation = {
  ...sampleWithRequiredData,
  dateFiche: sampleWithRequiredData.dateFiche?.format(DATE_FORMAT),
};

describe('FicheConsultation Service', () => {
  let service: FicheConsultationService;
  let httpMock: HttpTestingController;
  let expectedResult: IFicheConsultation | IFicheConsultation[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(FicheConsultationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a FicheConsultation', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const ficheConsultation = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(ficheConsultation).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a FicheConsultation', () => {
      const ficheConsultation = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(ficheConsultation).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a FicheConsultation', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of FicheConsultation', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a FicheConsultation', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addFicheConsultationToCollectionIfMissing', () => {
      it('should add a FicheConsultation to an empty array', () => {
        const ficheConsultation: IFicheConsultation = sampleWithRequiredData;
        expectedResult = service.addFicheConsultationToCollectionIfMissing([], ficheConsultation);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(ficheConsultation);
      });

      it('should not add a FicheConsultation to an array that contains it', () => {
        const ficheConsultation: IFicheConsultation = sampleWithRequiredData;
        const ficheConsultationCollection: IFicheConsultation[] = [
          {
            ...ficheConsultation,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addFicheConsultationToCollectionIfMissing(ficheConsultationCollection, ficheConsultation);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a FicheConsultation to an array that doesn't contain it", () => {
        const ficheConsultation: IFicheConsultation = sampleWithRequiredData;
        const ficheConsultationCollection: IFicheConsultation[] = [sampleWithPartialData];
        expectedResult = service.addFicheConsultationToCollectionIfMissing(ficheConsultationCollection, ficheConsultation);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(ficheConsultation);
      });

      it('should add only unique FicheConsultation to an array', () => {
        const ficheConsultationArray: IFicheConsultation[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const ficheConsultationCollection: IFicheConsultation[] = [sampleWithRequiredData];
        expectedResult = service.addFicheConsultationToCollectionIfMissing(ficheConsultationCollection, ...ficheConsultationArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const ficheConsultation: IFicheConsultation = sampleWithRequiredData;
        const ficheConsultation2: IFicheConsultation = sampleWithPartialData;
        expectedResult = service.addFicheConsultationToCollectionIfMissing([], ficheConsultation, ficheConsultation2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(ficheConsultation);
        expect(expectedResult).toContain(ficheConsultation2);
      });

      it('should accept null and undefined values', () => {
        const ficheConsultation: IFicheConsultation = sampleWithRequiredData;
        expectedResult = service.addFicheConsultationToCollectionIfMissing([], null, ficheConsultation, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(ficheConsultation);
      });

      it('should return initial array if no FicheConsultation is added', () => {
        const ficheConsultationCollection: IFicheConsultation[] = [sampleWithRequiredData];
        expectedResult = service.addFicheConsultationToCollectionIfMissing(ficheConsultationCollection, undefined, null);
        expect(expectedResult).toEqual(ficheConsultationCollection);
      });
    });

    describe('compareFicheConsultation', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareFicheConsultation(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareFicheConsultation(entity1, entity2);
        const compareResult2 = service.compareFicheConsultation(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareFicheConsultation(entity1, entity2);
        const compareResult2 = service.compareFicheConsultation(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareFicheConsultation(entity1, entity2);
        const compareResult2 = service.compareFicheConsultation(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
