import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IFicheConsultation } from '../../../models/fiche-consultation.model';
import { FicheConsultationService } from '../../../services/fiche-consultation.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './fiche-consultation-delete-dialog.component.html',
})
export class FicheConsultationDeleteDialogComponent {
  ficheConsultation?: IFicheConsultation;

  constructor(protected ficheConsultationService: FicheConsultationService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.ficheConsultationService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
