jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { FicheConsultationService } from '../../../services/fiche-consultation.service';

import { FicheConsultationDeleteDialogComponent } from './fiche-consultation-delete-dialog.component';

describe('FicheConsultation Management Delete Component', () => {
  let comp: FicheConsultationDeleteDialogComponent;
  let fixture: ComponentFixture<FicheConsultationDeleteDialogComponent>;
  let service: FicheConsultationService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [FicheConsultationDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(FicheConsultationDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(FicheConsultationDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(FicheConsultationService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});
