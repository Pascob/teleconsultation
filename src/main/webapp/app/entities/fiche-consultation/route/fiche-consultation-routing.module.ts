import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { FicheConsultationComponent } from '../list/fiche-consultation.component';
import { FicheConsultationDetailComponent } from '../detail/fiche-consultation-detail.component';
import { FicheConsultationUpdateComponent } from '../update/fiche-consultation-update.component';
import { FicheConsultationRoutingResolveService } from '../../../resolvers/fiche-consultation-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const ficheConsultationRoute: Routes = [
  {
    path: '',
    component: FicheConsultationComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FicheConsultationDetailComponent,
    resolve: {
      ficheConsultation: FicheConsultationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FicheConsultationUpdateComponent,
    resolve: {
      ficheConsultation: FicheConsultationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FicheConsultationUpdateComponent,
    resolve: {
      ficheConsultation: FicheConsultationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(ficheConsultationRoute)],
  exports: [RouterModule],
})
export class FicheConsultationRoutingModule {}
