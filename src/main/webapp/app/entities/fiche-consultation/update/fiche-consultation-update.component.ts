import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { FicheConsultationFormService, FicheConsultationFormGroup } from './fiche-consultation-form.service';
import { IFicheConsultation } from '../../../models/fiche-consultation.model';
import { FicheConsultationService } from '../../../services/fiche-consultation.service';
import { IMedecin } from 'app/models/medecin.model';
import { MedecinService } from 'app/services/medecin.service';
import { IConsultation } from 'app/models/consultation.model';
import { ConsultationService } from 'app/services/consultation.service';

@Component({
  selector: 'jhi-fiche-consultation-update',
  templateUrl: './fiche-consultation-update.component.html',
})
export class FicheConsultationUpdateComponent implements OnInit {
  isSaving = false;
  ficheConsultation: IFicheConsultation | null = null;

  medecinsSharedCollection: IMedecin[] = [];
  consultationsSharedCollection: IConsultation[] = [];

  editForm: FicheConsultationFormGroup = this.ficheConsultationFormService.createFicheConsultationFormGroup();

  constructor(
    protected ficheConsultationService: FicheConsultationService,
    protected ficheConsultationFormService: FicheConsultationFormService,
    protected medecinService: MedecinService,
    protected consultationService: ConsultationService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareMedecin = (o1: IMedecin | null, o2: IMedecin | null): boolean => this.medecinService.compareMedecin(o1, o2);

  compareConsultation = (o1: IConsultation | null, o2: IConsultation | null): boolean =>
    this.consultationService.compareConsultation(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ficheConsultation }) => {
      this.ficheConsultation = ficheConsultation;
      if (ficheConsultation) {
        this.updateForm(ficheConsultation);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ficheConsultation = this.ficheConsultationFormService.getFicheConsultation(this.editForm);
    if (ficheConsultation.id !== null) {
      this.subscribeToSaveResponse(this.ficheConsultationService.update(ficheConsultation));
    } else {
      this.subscribeToSaveResponse(this.ficheConsultationService.create(ficheConsultation));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFicheConsultation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(ficheConsultation: IFicheConsultation): void {
    this.ficheConsultation = ficheConsultation;
    this.ficheConsultationFormService.resetForm(this.editForm, ficheConsultation);

    this.medecinsSharedCollection = this.medecinService.addMedecinToCollectionIfMissing<IMedecin>(
      this.medecinsSharedCollection,
      ficheConsultation.medecin
    );
    this.consultationsSharedCollection = this.consultationService.addConsultationToCollectionIfMissing<IConsultation>(
      this.consultationsSharedCollection,
      ficheConsultation.consultation
    );
  }

  protected loadRelationshipsOptions(): void {
    this.medecinService
      .query()
      .pipe(map((res: HttpResponse<IMedecin[]>) => res.body ?? []))
      .pipe(
        map((medecins: IMedecin[]) =>
          this.medecinService.addMedecinToCollectionIfMissing<IMedecin>(medecins, this.ficheConsultation?.medecin)
        )
      )
      .subscribe((medecins: IMedecin[]) => (this.medecinsSharedCollection = medecins));

    this.consultationService
      .query()
      .pipe(map((res: HttpResponse<IConsultation[]>) => res.body ?? []))
      .pipe(
        map((consultations: IConsultation[]) =>
          this.consultationService.addConsultationToCollectionIfMissing<IConsultation>(consultations, this.ficheConsultation?.consultation)
        )
      )
      .subscribe((consultations: IConsultation[]) => (this.consultationsSharedCollection = consultations));
  }
}
