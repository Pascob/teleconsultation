import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IFicheConsultation, NewFicheConsultation } from '../../../models/fiche-consultation.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IFicheConsultation for edit and NewFicheConsultationFormGroupInput for create.
 */
type FicheConsultationFormGroupInput = IFicheConsultation | PartialWithRequiredKeyOf<NewFicheConsultation>;

type FicheConsultationFormDefaults = Pick<NewFicheConsultation, 'id'>;

type FicheConsultationFormGroupContent = {
  id: FormControl<IFicheConsultation['id'] | NewFicheConsultation['id']>;
  dateFiche: FormControl<IFicheConsultation['dateFiche']>;
  numero: FormControl<IFicheConsultation['numero']>;
  observation: FormControl<IFicheConsultation['observation']>;
  diagnostic: FormControl<IFicheConsultation['diagnostic']>;
  medecin: FormControl<IFicheConsultation['medecin']>;
  consultation: FormControl<IFicheConsultation['consultation']>;
};

export type FicheConsultationFormGroup = FormGroup<FicheConsultationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class FicheConsultationFormService {
  createFicheConsultationFormGroup(ficheConsultation: FicheConsultationFormGroupInput = { id: null }): FicheConsultationFormGroup {
    const ficheConsultationRawValue = {
      ...this.getFormDefaults(),
      ...ficheConsultation,
    };
    return new FormGroup<FicheConsultationFormGroupContent>({
      id: new FormControl(
        { value: ficheConsultationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateFiche: new FormControl(ficheConsultationRawValue.dateFiche, {
        validators: [Validators.required],
      }),
      numero: new FormControl(ficheConsultationRawValue.numero),
      observation: new FormControl(ficheConsultationRawValue.observation),
      diagnostic: new FormControl(ficheConsultationRawValue.diagnostic),
      medecin: new FormControl(ficheConsultationRawValue.medecin),
      consultation: new FormControl(ficheConsultationRawValue.consultation),
    });
  }

  getFicheConsultation(form: FicheConsultationFormGroup): IFicheConsultation | NewFicheConsultation {
    return form.getRawValue() as IFicheConsultation | NewFicheConsultation;
  }

  resetForm(form: FicheConsultationFormGroup, ficheConsultation: FicheConsultationFormGroupInput): void {
    const ficheConsultationRawValue = { ...this.getFormDefaults(), ...ficheConsultation };
    form.reset(
      {
        ...ficheConsultationRawValue,
        id: { value: ficheConsultationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): FicheConsultationFormDefaults {
    return {
      id: null,
    };
  }
}
