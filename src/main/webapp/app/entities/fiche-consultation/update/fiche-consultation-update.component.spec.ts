import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { FicheConsultationFormService } from './fiche-consultation-form.service';
import { FicheConsultationService } from '../../../services/fiche-consultation.service';
import { IFicheConsultation } from '../../../models/fiche-consultation.model';
import { IMedecin } from 'app/models/medecin.model';
import { MedecinService } from 'app/services/medecin.service';
import { IConsultation } from 'app/models/consultation.model';
import { ConsultationService } from 'app/services/consultation.service';

import { FicheConsultationUpdateComponent } from './fiche-consultation-update.component';

describe('FicheConsultation Management Update Component', () => {
  let comp: FicheConsultationUpdateComponent;
  let fixture: ComponentFixture<FicheConsultationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let ficheConsultationFormService: FicheConsultationFormService;
  let ficheConsultationService: FicheConsultationService;
  let medecinService: MedecinService;
  let consultationService: ConsultationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [FicheConsultationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(FicheConsultationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FicheConsultationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    ficheConsultationFormService = TestBed.inject(FicheConsultationFormService);
    ficheConsultationService = TestBed.inject(FicheConsultationService);
    medecinService = TestBed.inject(MedecinService);
    consultationService = TestBed.inject(ConsultationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Medecin query and add missing value', () => {
      const ficheConsultation: IFicheConsultation = { id: 456 };
      const medecin: IMedecin = { id: 25380 };
      ficheConsultation.medecin = medecin;

      const medecinCollection: IMedecin[] = [{ id: 79915 }];
      jest.spyOn(medecinService, 'query').mockReturnValue(of(new HttpResponse({ body: medecinCollection })));
      const additionalMedecins = [medecin];
      const expectedCollection: IMedecin[] = [...additionalMedecins, ...medecinCollection];
      jest.spyOn(medecinService, 'addMedecinToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ ficheConsultation });
      comp.ngOnInit();

      expect(medecinService.query).toHaveBeenCalled();
      expect(medecinService.addMedecinToCollectionIfMissing).toHaveBeenCalledWith(
        medecinCollection,
        ...additionalMedecins.map(expect.objectContaining)
      );
      expect(comp.medecinsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Consultation query and add missing value', () => {
      const ficheConsultation: IFicheConsultation = { id: 456 };
      const consultation: IConsultation = { id: 1415 };
      ficheConsultation.consultation = consultation;

      const consultationCollection: IConsultation[] = [{ id: 4033 }];
      jest.spyOn(consultationService, 'query').mockReturnValue(of(new HttpResponse({ body: consultationCollection })));
      const additionalConsultations = [consultation];
      const expectedCollection: IConsultation[] = [...additionalConsultations, ...consultationCollection];
      jest.spyOn(consultationService, 'addConsultationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ ficheConsultation });
      comp.ngOnInit();

      expect(consultationService.query).toHaveBeenCalled();
      expect(consultationService.addConsultationToCollectionIfMissing).toHaveBeenCalledWith(
        consultationCollection,
        ...additionalConsultations.map(expect.objectContaining)
      );
      expect(comp.consultationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const ficheConsultation: IFicheConsultation = { id: 456 };
      const medecin: IMedecin = { id: 44768 };
      ficheConsultation.medecin = medecin;
      const consultation: IConsultation = { id: 35035 };
      ficheConsultation.consultation = consultation;

      activatedRoute.data = of({ ficheConsultation });
      comp.ngOnInit();

      expect(comp.medecinsSharedCollection).toContain(medecin);
      expect(comp.consultationsSharedCollection).toContain(consultation);
      expect(comp.ficheConsultation).toEqual(ficheConsultation);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IFicheConsultation>>();
      const ficheConsultation = { id: 123 };
      jest.spyOn(ficheConsultationFormService, 'getFicheConsultation').mockReturnValue(ficheConsultation);
      jest.spyOn(ficheConsultationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ficheConsultation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ficheConsultation }));
      saveSubject.complete();

      // THEN
      expect(ficheConsultationFormService.getFicheConsultation).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(ficheConsultationService.update).toHaveBeenCalledWith(expect.objectContaining(ficheConsultation));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IFicheConsultation>>();
      const ficheConsultation = { id: 123 };
      jest.spyOn(ficheConsultationFormService, 'getFicheConsultation').mockReturnValue({ id: null });
      jest.spyOn(ficheConsultationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ficheConsultation: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ficheConsultation }));
      saveSubject.complete();

      // THEN
      expect(ficheConsultationFormService.getFicheConsultation).toHaveBeenCalled();
      expect(ficheConsultationService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IFicheConsultation>>();
      const ficheConsultation = { id: 123 };
      jest.spyOn(ficheConsultationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ficheConsultation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(ficheConsultationService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareMedecin', () => {
      it('Should forward to medecinService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(medecinService, 'compareMedecin');
        comp.compareMedecin(entity, entity2);
        expect(medecinService.compareMedecin).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareConsultation', () => {
      it('Should forward to consultationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(consultationService, 'compareConsultation');
        comp.compareConsultation(entity, entity2);
        expect(consultationService.compareConsultation).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
