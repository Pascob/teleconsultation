import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../fiche-consultation.test-samples';

import { FicheConsultationFormService } from './fiche-consultation-form.service';

describe('FicheConsultation Form Service', () => {
  let service: FicheConsultationFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FicheConsultationFormService);
  });

  describe('Service methods', () => {
    describe('createFicheConsultationFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createFicheConsultationFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateFiche: expect.any(Object),
            numero: expect.any(Object),
            observation: expect.any(Object),
            diagnostic: expect.any(Object),
            medecin: expect.any(Object),
            consultation: expect.any(Object),
          })
        );
      });

      it('passing IFicheConsultation should create a new form with FormGroup', () => {
        const formGroup = service.createFicheConsultationFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateFiche: expect.any(Object),
            numero: expect.any(Object),
            observation: expect.any(Object),
            diagnostic: expect.any(Object),
            medecin: expect.any(Object),
            consultation: expect.any(Object),
          })
        );
      });
    });

    describe('getFicheConsultation', () => {
      it('should return NewFicheConsultation for default FicheConsultation initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createFicheConsultationFormGroup(sampleWithNewData);

        const ficheConsultation = service.getFicheConsultation(formGroup) as any;

        expect(ficheConsultation).toMatchObject(sampleWithNewData);
      });

      it('should return NewFicheConsultation for empty FicheConsultation initial value', () => {
        const formGroup = service.createFicheConsultationFormGroup();

        const ficheConsultation = service.getFicheConsultation(formGroup) as any;

        expect(ficheConsultation).toMatchObject({});
      });

      it('should return IFicheConsultation', () => {
        const formGroup = service.createFicheConsultationFormGroup(sampleWithRequiredData);

        const ficheConsultation = service.getFicheConsultation(formGroup) as any;

        expect(ficheConsultation).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IFicheConsultation should not enable id FormControl', () => {
        const formGroup = service.createFicheConsultationFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewFicheConsultation should disable id FormControl', () => {
        const formGroup = service.createFicheConsultationFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
