import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IConsultation, NewConsultation } from '../../../models/consultation.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IConsultation for edit and NewConsultationFormGroupInput for create.
 */
type ConsultationFormGroupInput = IConsultation | PartialWithRequiredKeyOf<NewConsultation>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IConsultation | NewConsultation> = Omit<T, 'dateDebutConsulation'> & {
  dateDebutConsulation?: string | null;
};

type ConsultationFormRawValue = FormValueOf<IConsultation>;

type NewConsultationFormRawValue = FormValueOf<NewConsultation>;

type ConsultationFormDefaults = Pick<NewConsultation, 'id' | 'dateDebutConsulation' | 'isSuivi' | 'isClose'>;

type ConsultationFormGroupContent = {
  id: FormControl<ConsultationFormRawValue['id'] | NewConsultation['id']>;
  poids: FormControl<ConsultationFormRawValue['poids']>;
  temperature: FormControl<ConsultationFormRawValue['temperature']>;
  pouls: FormControl<ConsultationFormRawValue['pouls']>;
  bpm: FormControl<ConsultationFormRawValue['bpm']>;
  age: FormControl<ConsultationFormRawValue['age']>;
  taille: FormControl<ConsultationFormRawValue['taille']>;
  dateDebutConsulation: FormControl<ConsultationFormRawValue['dateDebutConsulation']>;
  isSuivi: FormControl<ConsultationFormRawValue['isSuivi']>;
  isClose: FormControl<ConsultationFormRawValue['isClose']>;
  medecin: FormControl<ConsultationFormRawValue['medecin']>;
  demandeConsultation: FormControl<ConsultationFormRawValue['demandeConsultation']>;
};

export type ConsultationFormGroup = FormGroup<ConsultationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ConsultationFormService {
  createConsultationFormGroup(consultation: ConsultationFormGroupInput = { id: null }): ConsultationFormGroup {
    const consultationRawValue = this.convertConsultationToConsultationRawValue({
      ...this.getFormDefaults(),
      ...consultation,
    });
    return new FormGroup<ConsultationFormGroupContent>({
      id: new FormControl(
        { value: consultationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      poids: new FormControl(consultationRawValue.poids, {
        validators: [Validators.min(0)],
      }),
      temperature: new FormControl(consultationRawValue.temperature, {
        validators: [Validators.min(0)],
      }),
      pouls: new FormControl(consultationRawValue.pouls, {
        validators: [Validators.min(0)],
      }),
      bpm: new FormControl(consultationRawValue.bpm, {
        validators: [Validators.min(0)],
      }),
      age: new FormControl(consultationRawValue.age, {
        validators: [Validators.min(0)],
      }),
      taille: new FormControl(consultationRawValue.taille, {
        validators: [Validators.min(0)],
      }),
      dateDebutConsulation: new FormControl(consultationRawValue.dateDebutConsulation),
      isSuivi: new FormControl(consultationRawValue.isSuivi),
      isClose: new FormControl(consultationRawValue.isClose),
      medecin: new FormControl(consultationRawValue.medecin),
      demandeConsultation: new FormControl(consultationRawValue.demandeConsultation),
    });
  }

  getConsultation(form: ConsultationFormGroup): IConsultation | NewConsultation {
    return this.convertConsultationRawValueToConsultation(form.getRawValue() as ConsultationFormRawValue | NewConsultationFormRawValue);
  }

  resetForm(form: ConsultationFormGroup, consultation: ConsultationFormGroupInput): void {
    const consultationRawValue = this.convertConsultationToConsultationRawValue({ ...this.getFormDefaults(), ...consultation });
    form.reset(
      {
        ...consultationRawValue,
        id: { value: consultationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ConsultationFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      dateDebutConsulation: currentTime,
      isSuivi: false,
      isClose: false,
    };
  }

  private convertConsultationRawValueToConsultation(
    rawConsultation: ConsultationFormRawValue | NewConsultationFormRawValue
  ): IConsultation | NewConsultation {
    return {
      ...rawConsultation,
      dateDebutConsulation: dayjs(rawConsultation.dateDebutConsulation, DATE_TIME_FORMAT),
    };
  }

  private convertConsultationToConsultationRawValue(
    consultation: IConsultation | (Partial<NewConsultation> & ConsultationFormDefaults)
  ): ConsultationFormRawValue | PartialWithRequiredKeyOf<NewConsultationFormRawValue> {
    return {
      ...consultation,
      dateDebutConsulation: consultation.dateDebutConsulation ? consultation.dateDebutConsulation.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
