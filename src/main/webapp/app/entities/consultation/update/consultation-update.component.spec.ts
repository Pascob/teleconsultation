import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ConsultationFormService } from './consultation-form.service';
import { ConsultationService } from '../../../services/consultation.service';
import { IConsultation } from '../../../models/consultation.model';
import { IMedecin } from 'app/models/medecin.model';
import { MedecinService } from 'app/services/medecin.service';
import { IDemandeConsultation } from 'app/models/demande-consultation.model';
import { DemandeConsultationService } from 'app/services/demande-consultation.service';

import { ConsultationUpdateComponent } from './consultation-update.component';

describe('Consultation Management Update Component', () => {
  let comp: ConsultationUpdateComponent;
  let fixture: ComponentFixture<ConsultationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let consultationFormService: ConsultationFormService;
  let consultationService: ConsultationService;
  let medecinService: MedecinService;
  let demandeConsultationService: DemandeConsultationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ConsultationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ConsultationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConsultationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    consultationFormService = TestBed.inject(ConsultationFormService);
    consultationService = TestBed.inject(ConsultationService);
    medecinService = TestBed.inject(MedecinService);
    demandeConsultationService = TestBed.inject(DemandeConsultationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Medecin query and add missing value', () => {
      const consultation: IConsultation = { id: 456 };
      const medecin: IMedecin = { id: 54047 };
      consultation.medecin = medecin;

      const medecinCollection: IMedecin[] = [{ id: 5578 }];
      jest.spyOn(medecinService, 'query').mockReturnValue(of(new HttpResponse({ body: medecinCollection })));
      const additionalMedecins = [medecin];
      const expectedCollection: IMedecin[] = [...additionalMedecins, ...medecinCollection];
      jest.spyOn(medecinService, 'addMedecinToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ consultation });
      comp.ngOnInit();

      expect(medecinService.query).toHaveBeenCalled();
      expect(medecinService.addMedecinToCollectionIfMissing).toHaveBeenCalledWith(
        medecinCollection,
        ...additionalMedecins.map(expect.objectContaining)
      );
      expect(comp.medecinsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call DemandeConsultation query and add missing value', () => {
      const consultation: IConsultation = { id: 456 };
      const demandeConsultation: IDemandeConsultation = { id: 36796 };
      consultation.demandeConsultation = demandeConsultation;

      const demandeConsultationCollection: IDemandeConsultation[] = [{ id: 59885 }];
      jest.spyOn(demandeConsultationService, 'query').mockReturnValue(of(new HttpResponse({ body: demandeConsultationCollection })));
      const additionalDemandeConsultations = [demandeConsultation];
      const expectedCollection: IDemandeConsultation[] = [...additionalDemandeConsultations, ...demandeConsultationCollection];
      jest.spyOn(demandeConsultationService, 'addDemandeConsultationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ consultation });
      comp.ngOnInit();

      expect(demandeConsultationService.query).toHaveBeenCalled();
      expect(demandeConsultationService.addDemandeConsultationToCollectionIfMissing).toHaveBeenCalledWith(
        demandeConsultationCollection,
        ...additionalDemandeConsultations.map(expect.objectContaining)
      );
      expect(comp.demandeConsultationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const consultation: IConsultation = { id: 456 };
      const medecin: IMedecin = { id: 4694 };
      consultation.medecin = medecin;
      const demandeConsultation: IDemandeConsultation = { id: 41252 };
      consultation.demandeConsultation = demandeConsultation;

      activatedRoute.data = of({ consultation });
      comp.ngOnInit();

      expect(comp.medecinsSharedCollection).toContain(medecin);
      expect(comp.demandeConsultationsSharedCollection).toContain(demandeConsultation);
      expect(comp.consultation).toEqual(consultation);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConsultation>>();
      const consultation = { id: 123 };
      jest.spyOn(consultationFormService, 'getConsultation').mockReturnValue(consultation);
      jest.spyOn(consultationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ consultation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: consultation }));
      saveSubject.complete();

      // THEN
      expect(consultationFormService.getConsultation).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(consultationService.update).toHaveBeenCalledWith(expect.objectContaining(consultation));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConsultation>>();
      const consultation = { id: 123 };
      jest.spyOn(consultationFormService, 'getConsultation').mockReturnValue({ id: null });
      jest.spyOn(consultationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ consultation: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: consultation }));
      saveSubject.complete();

      // THEN
      expect(consultationFormService.getConsultation).toHaveBeenCalled();
      expect(consultationService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IConsultation>>();
      const consultation = { id: 123 };
      jest.spyOn(consultationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ consultation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(consultationService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareMedecin', () => {
      it('Should forward to medecinService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(medecinService, 'compareMedecin');
        comp.compareMedecin(entity, entity2);
        expect(medecinService.compareMedecin).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareDemandeConsultation', () => {
      it('Should forward to demandeConsultationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(demandeConsultationService, 'compareDemandeConsultation');
        comp.compareDemandeConsultation(entity, entity2);
        expect(demandeConsultationService.compareDemandeConsultation).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
