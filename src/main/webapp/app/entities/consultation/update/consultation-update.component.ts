import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ConsultationFormService, ConsultationFormGroup } from './consultation-form.service';
import { IConsultation } from '../../../models/consultation.model';
import { ConsultationService } from '../../../services/consultation.service';
import { IMedecin } from 'app/models/medecin.model';
import { MedecinService } from 'app/services/medecin.service';
import { IDemandeConsultation } from 'app/models/demande-consultation.model';
import { DemandeConsultationService } from 'app/services/demande-consultation.service';

@Component({
  selector: 'jhi-consultation-update',
  templateUrl: './consultation-update.component.html',
})
export class ConsultationUpdateComponent implements OnInit {
  isSaving = false;
  consultation: IConsultation | null = null;

  medecinsSharedCollection: IMedecin[] = [];
  demandeConsultationsSharedCollection: IDemandeConsultation[] = [];

  editForm: ConsultationFormGroup = this.consultationFormService.createConsultationFormGroup();

  constructor(
    protected consultationService: ConsultationService,
    protected consultationFormService: ConsultationFormService,
    protected medecinService: MedecinService,
    protected demandeConsultationService: DemandeConsultationService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareMedecin = (o1: IMedecin | null, o2: IMedecin | null): boolean => this.medecinService.compareMedecin(o1, o2);

  compareDemandeConsultation = (o1: IDemandeConsultation | null, o2: IDemandeConsultation | null): boolean =>
    this.demandeConsultationService.compareDemandeConsultation(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ consultation }) => {
      this.consultation = consultation;
      if (consultation) {
        this.updateForm(consultation);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const consultation = this.consultationFormService.getConsultation(this.editForm);
    if (consultation.id !== null) {
      this.subscribeToSaveResponse(this.consultationService.update(consultation));
    } else {
      this.subscribeToSaveResponse(this.consultationService.create(consultation));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IConsultation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(consultation: IConsultation): void {
    this.consultation = consultation;
    this.consultationFormService.resetForm(this.editForm, consultation);

    this.medecinsSharedCollection = this.medecinService.addMedecinToCollectionIfMissing<IMedecin>(
      this.medecinsSharedCollection,
      consultation.medecin
    );
    this.demandeConsultationsSharedCollection =
      this.demandeConsultationService.addDemandeConsultationToCollectionIfMissing<IDemandeConsultation>(
        this.demandeConsultationsSharedCollection,
        consultation.demandeConsultation
      );
  }

  protected loadRelationshipsOptions(): void {
    this.medecinService
      .query()
      .pipe(map((res: HttpResponse<IMedecin[]>) => res.body ?? []))
      .pipe(
        map((medecins: IMedecin[]) => this.medecinService.addMedecinToCollectionIfMissing<IMedecin>(medecins, this.consultation?.medecin))
      )
      .subscribe((medecins: IMedecin[]) => (this.medecinsSharedCollection = medecins));

    this.demandeConsultationService
      .query()
      .pipe(map((res: HttpResponse<IDemandeConsultation[]>) => res.body ?? []))
      .pipe(
        map((demandeConsultations: IDemandeConsultation[]) =>
          this.demandeConsultationService.addDemandeConsultationToCollectionIfMissing<IDemandeConsultation>(
            demandeConsultations,
            this.consultation?.demandeConsultation
          )
        )
      )
      .subscribe((demandeConsultations: IDemandeConsultation[]) => (this.demandeConsultationsSharedCollection = demandeConsultations));
  }
}
