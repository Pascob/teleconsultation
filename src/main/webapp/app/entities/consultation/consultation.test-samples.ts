import dayjs from 'dayjs/esm';

import { IConsultation, NewConsultation } from '../../models/consultation.model';

export const sampleWithRequiredData: IConsultation = {
  id: 57781,
};

export const sampleWithPartialData: IConsultation = {
  id: 62469,
  temperature: 56076,
  bpm: 86435,
  age: 60885,
  isSuivi: false,
  isClose: true,
};

export const sampleWithFullData: IConsultation = {
  id: 17369,
  poids: 95549,
  temperature: 94064,
  pouls: 74391,
  bpm: 66407,
  age: 22433,
  taille: 24766,
  dateDebutConsulation: dayjs('2022-12-12T23:48'),
  isSuivi: false,
  isClose: false,
};

export const sampleWithNewData: NewConsultation = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
