import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDossierMedical } from '../models/dossier-medical.model';
import { DossierMedicalService } from '../services/dossier-medical.service';

@Injectable({ providedIn: 'root' })
export class DossierMedicalRoutingResolveService implements Resolve<IDossierMedical | null> {
  constructor(protected service: DossierMedicalService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDossierMedical | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dossierMedical: HttpResponse<IDossierMedical>) => {
          if (dossierMedical.body) {
            return of(dossierMedical.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
