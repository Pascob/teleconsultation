import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMedecin } from '../models/medecin.model';
import { MedecinService } from '../services/medecin.service';
import { PatientService } from '../services/patient.service';
import { GlobalService } from '../services/global.service';

@Injectable({ providedIn: 'root' })
export class MedecinRoutingResolveService implements Resolve<IMedecin | null> {
  constructor(protected service: MedecinService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMedecin | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((medecin: HttpResponse<IMedecin>) => {
          if (medecin.body) {
            return of(medecin.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

@Injectable({ providedIn: 'root' })
export class MedecinSessionResolveService implements Resolve<IMedecin | null> {
  constructor(protected service: MedecinService, protected globalService: GlobalService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMedecin | null | never> {
    const id = this.globalService.getUserIdInSession();
    if (id) {
      return this.service.findByUserId(id).pipe(
        mergeMap((medecin: HttpResponse<IMedecin>) => {
          if (medecin.body) {
            return of(medecin.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
