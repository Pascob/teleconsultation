import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IExamenMedical } from '../models/examen-medical.model';
import { ExamenMedicalService } from '../services/examen-medical.service';

@Injectable({ providedIn: 'root' })
export class ExamenMedicalRoutingResolveService implements Resolve<IExamenMedical | null> {
  constructor(protected service: ExamenMedicalService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExamenMedical | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((examenMedical: HttpResponse<IExamenMedical>) => {
          if (examenMedical.body) {
            return of(examenMedical.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
