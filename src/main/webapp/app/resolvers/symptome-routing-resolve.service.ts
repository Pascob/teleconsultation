import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ISymptome } from '../models/symptome.model';
import { SymptomeService } from '../services/symptome.service';

@Injectable({ providedIn: 'root' })
export class SymptomeRoutingResolveService implements Resolve<ISymptome | null> {
  constructor(protected service: SymptomeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISymptome | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((symptome: HttpResponse<ISymptome>) => {
          if (symptome.body) {
            return of(symptome.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
