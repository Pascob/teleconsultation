package com.pascob.teleconsultation.config;

import java.time.Duration;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.*;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.cache.PrefixedKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration =
            Eh107Configuration.fromEhcacheCacheConfiguration(
                CacheConfigurationBuilder
                    .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                    .build()
            );
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.pascob.teleconsultation.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.pascob.teleconsultation.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.pascob.teleconsultation.domain.User.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Authority.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.User.class.getName() + ".authorities");
            createCache(cm, com.pascob.teleconsultation.domain.FormationSanitaire.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.FormationSanitaire.class.getName() + ".medecins");
            createCache(cm, com.pascob.teleconsultation.domain.Specialite.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Specialite.class.getName() + ".medecins");
            createCache(cm, com.pascob.teleconsultation.domain.Specialite.class.getName() + ".demandeConsultations");
            createCache(cm, com.pascob.teleconsultation.domain.Medecin.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Medecin.class.getName() + ".dossierMedicals");
            createCache(cm, com.pascob.teleconsultation.domain.Medecin.class.getName() + ".consultations");
            createCache(cm, com.pascob.teleconsultation.domain.Medecin.class.getName() + ".ficheConsultations");
            createCache(cm, com.pascob.teleconsultation.domain.Patient.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.DossierMedical.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.DossierMedical.class.getName() + ".demandeConsultations");
            createCache(cm, com.pascob.teleconsultation.domain.DemandeConsultation.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.DemandeConsultation.class.getName() + ".consultations");
            createCache(cm, com.pascob.teleconsultation.domain.Consultation.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Consultation.class.getName() + ".ficheConsultations");
            createCache(cm, com.pascob.teleconsultation.domain.FicheConsultation.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.FicheConsultation.class.getName() + ".ordonnances");
            createCache(cm, com.pascob.teleconsultation.domain.FicheConsultation.class.getName() + ".bulletins");
            createCache(cm, com.pascob.teleconsultation.domain.FicheConsultation.class.getName() + ".ficheSymptomes");
            createCache(cm, com.pascob.teleconsultation.domain.Produit.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Produit.class.getName() + ".prescriptionProduits");
            createCache(cm, com.pascob.teleconsultation.domain.Ordonnance.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Ordonnance.class.getName() + ".prescriptionProduits");
            createCache(cm, com.pascob.teleconsultation.domain.PrescriptionProduit.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.ExamenMedical.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.ExamenMedical.class.getName() + ".prescriptionExamen");
            createCache(cm, com.pascob.teleconsultation.domain.Bulletin.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Bulletin.class.getName() + ".prescriptionExamen");
            createCache(cm, com.pascob.teleconsultation.domain.PrescriptionExamen.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Symptome.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Symptome.class.getName() + ".ficheSymptomes");
            createCache(cm, com.pascob.teleconsultation.domain.FicheSymptome.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Message.class.getName());
            createCache(cm, com.pascob.teleconsultation.domain.Notification.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cache.clear();
        } else {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
