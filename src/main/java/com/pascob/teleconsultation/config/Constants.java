package com.pascob.teleconsultation.config;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";

    public static final String SYSTEM = "system";
    public static final String DEFAULT_LANGUAGE = "en";

    public static final ZoneId GMT_ZONE = ZoneId.of("Etc/UTC");

    private Constants() {}

    public static ZonedDateTime getDebutJournee(Boolean isDebut) {
        if (isDebut) {
            return ZonedDateTime.of(
                LocalDate.now().getYear(),
                LocalDate.now().getMonthValue(),
                LocalDate.now().getDayOfMonth(),
                0,
                0,
                0,
                0,
                GMT_ZONE
            );
        }

        return ZonedDateTime.of(
            LocalDate.now().getYear(),
            LocalDate.now().getMonthValue(),
            LocalDate.now().getDayOfMonth(),
            23,
            59,
            59,
            0,
            GMT_ZONE
        );
    }
}
