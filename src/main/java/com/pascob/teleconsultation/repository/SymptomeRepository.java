package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.Symptome;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Symptome entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SymptomeRepository extends JpaRepository<Symptome, Long>, JpaSpecificationExecutor<Symptome> {}
