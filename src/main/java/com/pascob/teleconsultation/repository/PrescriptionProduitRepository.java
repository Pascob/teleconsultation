package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.PrescriptionProduit;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PrescriptionProduit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrescriptionProduitRepository
    extends JpaRepository<PrescriptionProduit, Long>, JpaSpecificationExecutor<PrescriptionProduit> {}
