package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.Ordonnance;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Ordonnance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrdonnanceRepository extends JpaRepository<Ordonnance, Long>, JpaSpecificationExecutor<Ordonnance> {}
