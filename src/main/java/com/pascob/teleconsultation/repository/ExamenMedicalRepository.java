package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.ExamenMedical;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ExamenMedical entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExamenMedicalRepository extends JpaRepository<ExamenMedical, Long>, JpaSpecificationExecutor<ExamenMedical> {}
