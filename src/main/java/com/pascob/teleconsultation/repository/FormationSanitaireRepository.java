package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.FormationSanitaire;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the FormationSanitaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FormationSanitaireRepository
    extends JpaRepository<FormationSanitaire, Long>, JpaSpecificationExecutor<FormationSanitaire> {}
