package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.Bulletin;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Bulletin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BulletinRepository extends JpaRepository<Bulletin, Long>, JpaSpecificationExecutor<Bulletin> {}
