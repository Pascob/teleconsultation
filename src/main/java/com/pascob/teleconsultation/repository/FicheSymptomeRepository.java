package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.FicheSymptome;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the FicheSymptome entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FicheSymptomeRepository extends JpaRepository<FicheSymptome, Long>, JpaSpecificationExecutor<FicheSymptome> {}
