package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.FicheConsultation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the FicheConsultation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FicheConsultationRepository extends JpaRepository<FicheConsultation, Long>, JpaSpecificationExecutor<FicheConsultation> {}
