package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.PrescriptionExamen;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PrescriptionExamen entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrescriptionExamenRepository
    extends JpaRepository<PrescriptionExamen, Long>, JpaSpecificationExecutor<PrescriptionExamen> {}
