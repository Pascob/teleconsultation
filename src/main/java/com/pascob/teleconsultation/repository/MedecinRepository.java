package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.Medecin;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Medecin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedecinRepository extends JpaRepository<Medecin, Long>, JpaSpecificationExecutor<Medecin> {
    Optional<Medecin> findByUserId(Long userId);
}
