package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.DossierMedical;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DossierMedical entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DossierMedicalRepository extends JpaRepository<DossierMedical, Long>, JpaSpecificationExecutor<DossierMedical> {
    Optional<DossierMedical> findByPatientUserId(Long userId);
}
