package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.Specialite;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Specialite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpecialiteRepository extends JpaRepository<Specialite, Long>, JpaSpecificationExecutor<Specialite> {}
