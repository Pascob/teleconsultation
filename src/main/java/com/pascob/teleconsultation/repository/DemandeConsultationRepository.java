package com.pascob.teleconsultation.repository;

import com.pascob.teleconsultation.domain.DemandeConsultation;
import java.time.ZonedDateTime;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DemandeConsultation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DemandeConsultationRepository
    extends JpaRepository<DemandeConsultation, Long>, JpaSpecificationExecutor<DemandeConsultation> {
    Integer countAllByDateHeureDemandeBetween(ZonedDateTime debut, ZonedDateTime fin);
}
