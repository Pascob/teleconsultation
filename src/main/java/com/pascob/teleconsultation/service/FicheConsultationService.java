package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.repository.FicheConsultationRepository;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import com.pascob.teleconsultation.service.mapper.FicheConsultationMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FicheConsultation}.
 */
@Service
@Transactional
public class FicheConsultationService {

    private final Logger log = LoggerFactory.getLogger(FicheConsultationService.class);

    private final FicheConsultationRepository ficheConsultationRepository;

    private final FicheConsultationMapper ficheConsultationMapper;

    public FicheConsultationService(
        FicheConsultationRepository ficheConsultationRepository,
        FicheConsultationMapper ficheConsultationMapper
    ) {
        this.ficheConsultationRepository = ficheConsultationRepository;
        this.ficheConsultationMapper = ficheConsultationMapper;
    }

    /**
     * Save a ficheConsultation.
     *
     * @param ficheConsultationDTO the entity to save.
     * @return the persisted entity.
     */
    public FicheConsultationDTO save(FicheConsultationDTO ficheConsultationDTO) {
        log.debug("Request to save FicheConsultation : {}", ficheConsultationDTO);
        FicheConsultation ficheConsultation = ficheConsultationMapper.toEntity(ficheConsultationDTO);
        ficheConsultation = ficheConsultationRepository.save(ficheConsultation);
        return ficheConsultationMapper.toDto(ficheConsultation);
    }

    /**
     * Update a ficheConsultation.
     *
     * @param ficheConsultationDTO the entity to save.
     * @return the persisted entity.
     */
    public FicheConsultationDTO update(FicheConsultationDTO ficheConsultationDTO) {
        log.debug("Request to update FicheConsultation : {}", ficheConsultationDTO);
        FicheConsultation ficheConsultation = ficheConsultationMapper.toEntity(ficheConsultationDTO);
        ficheConsultation = ficheConsultationRepository.save(ficheConsultation);
        return ficheConsultationMapper.toDto(ficheConsultation);
    }

    /**
     * Partially update a ficheConsultation.
     *
     * @param ficheConsultationDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<FicheConsultationDTO> partialUpdate(FicheConsultationDTO ficheConsultationDTO) {
        log.debug("Request to partially update FicheConsultation : {}", ficheConsultationDTO);

        return ficheConsultationRepository
            .findById(ficheConsultationDTO.getId())
            .map(existingFicheConsultation -> {
                ficheConsultationMapper.partialUpdate(existingFicheConsultation, ficheConsultationDTO);

                return existingFicheConsultation;
            })
            .map(ficheConsultationRepository::save)
            .map(ficheConsultationMapper::toDto);
    }

    /**
     * Get all the ficheConsultations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FicheConsultationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FicheConsultations");
        return ficheConsultationRepository.findAll(pageable).map(ficheConsultationMapper::toDto);
    }

    /**
     * Get one ficheConsultation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FicheConsultationDTO> findOne(Long id) {
        log.debug("Request to get FicheConsultation : {}", id);
        return ficheConsultationRepository.findById(id).map(ficheConsultationMapper::toDto);
    }

    /**
     * Delete the ficheConsultation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FicheConsultation : {}", id);
        ficheConsultationRepository.deleteById(id);
    }
}
