package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.PrescriptionProduit;
import com.pascob.teleconsultation.repository.PrescriptionProduitRepository;
import com.pascob.teleconsultation.service.dto.PrescriptionProduitDTO;
import com.pascob.teleconsultation.service.mapper.PrescriptionProduitMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PrescriptionProduit}.
 */
@Service
@Transactional
public class PrescriptionProduitService {

    private final Logger log = LoggerFactory.getLogger(PrescriptionProduitService.class);

    private final PrescriptionProduitRepository prescriptionProduitRepository;

    private final PrescriptionProduitMapper prescriptionProduitMapper;

    public PrescriptionProduitService(
        PrescriptionProduitRepository prescriptionProduitRepository,
        PrescriptionProduitMapper prescriptionProduitMapper
    ) {
        this.prescriptionProduitRepository = prescriptionProduitRepository;
        this.prescriptionProduitMapper = prescriptionProduitMapper;
    }

    /**
     * Save a prescriptionProduit.
     *
     * @param prescriptionProduitDTO the entity to save.
     * @return the persisted entity.
     */
    public PrescriptionProduitDTO save(PrescriptionProduitDTO prescriptionProduitDTO) {
        log.debug("Request to save PrescriptionProduit : {}", prescriptionProduitDTO);
        PrescriptionProduit prescriptionProduit = prescriptionProduitMapper.toEntity(prescriptionProduitDTO);
        prescriptionProduit = prescriptionProduitRepository.save(prescriptionProduit);
        return prescriptionProduitMapper.toDto(prescriptionProduit);
    }

    /**
     * Update a prescriptionProduit.
     *
     * @param prescriptionProduitDTO the entity to save.
     * @return the persisted entity.
     */
    public PrescriptionProduitDTO update(PrescriptionProduitDTO prescriptionProduitDTO) {
        log.debug("Request to update PrescriptionProduit : {}", prescriptionProduitDTO);
        PrescriptionProduit prescriptionProduit = prescriptionProduitMapper.toEntity(prescriptionProduitDTO);
        prescriptionProduit = prescriptionProduitRepository.save(prescriptionProduit);
        return prescriptionProduitMapper.toDto(prescriptionProduit);
    }

    /**
     * Partially update a prescriptionProduit.
     *
     * @param prescriptionProduitDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PrescriptionProduitDTO> partialUpdate(PrescriptionProduitDTO prescriptionProduitDTO) {
        log.debug("Request to partially update PrescriptionProduit : {}", prescriptionProduitDTO);

        return prescriptionProduitRepository
            .findById(prescriptionProduitDTO.getId())
            .map(existingPrescriptionProduit -> {
                prescriptionProduitMapper.partialUpdate(existingPrescriptionProduit, prescriptionProduitDTO);

                return existingPrescriptionProduit;
            })
            .map(prescriptionProduitRepository::save)
            .map(prescriptionProduitMapper::toDto);
    }

    /**
     * Get all the prescriptionProduits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PrescriptionProduitDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PrescriptionProduits");
        return prescriptionProduitRepository.findAll(pageable).map(prescriptionProduitMapper::toDto);
    }

    /**
     * Get one prescriptionProduit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PrescriptionProduitDTO> findOne(Long id) {
        log.debug("Request to get PrescriptionProduit : {}", id);
        return prescriptionProduitRepository.findById(id).map(prescriptionProduitMapper::toDto);
    }

    /**
     * Delete the prescriptionProduit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PrescriptionProduit : {}", id);
        prescriptionProduitRepository.deleteById(id);
    }
}
