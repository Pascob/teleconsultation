package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.Ordonnance;
import com.pascob.teleconsultation.repository.OrdonnanceRepository;
import com.pascob.teleconsultation.service.criteria.OrdonnanceCriteria;
import com.pascob.teleconsultation.service.dto.OrdonnanceDTO;
import com.pascob.teleconsultation.service.mapper.OrdonnanceMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Ordonnance} entities in the database.
 * The main input is a {@link OrdonnanceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrdonnanceDTO} or a {@link Page} of {@link OrdonnanceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrdonnanceQueryService extends QueryService<Ordonnance> {

    private final Logger log = LoggerFactory.getLogger(OrdonnanceQueryService.class);

    private final OrdonnanceRepository ordonnanceRepository;

    private final OrdonnanceMapper ordonnanceMapper;

    public OrdonnanceQueryService(OrdonnanceRepository ordonnanceRepository, OrdonnanceMapper ordonnanceMapper) {
        this.ordonnanceRepository = ordonnanceRepository;
        this.ordonnanceMapper = ordonnanceMapper;
    }

    /**
     * Return a {@link List} of {@link OrdonnanceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrdonnanceDTO> findByCriteria(OrdonnanceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Ordonnance> specification = createSpecification(criteria);
        return ordonnanceMapper.toDto(ordonnanceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OrdonnanceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrdonnanceDTO> findByCriteria(OrdonnanceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Ordonnance> specification = createSpecification(criteria);
        return ordonnanceRepository.findAll(specification, page).map(ordonnanceMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrdonnanceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Ordonnance> specification = createSpecification(criteria);
        return ordonnanceRepository.count(specification);
    }

    /**
     * Function to convert {@link OrdonnanceCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Ordonnance> createSpecification(OrdonnanceCriteria criteria) {
        Specification<Ordonnance> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Ordonnance_.id));
            }
            if (criteria.getDateOrdonnance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateOrdonnance(), Ordonnance_.dateOrdonnance));
            }
            if (criteria.getNumero() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumero(), Ordonnance_.numero));
            }
            if (criteria.getPrescriptionProduitId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPrescriptionProduitId(),
                            root -> root.join(Ordonnance_.prescriptionProduits, JoinType.LEFT).get(PrescriptionProduit_.id)
                        )
                    );
            }
            if (criteria.getFicheConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFicheConsultationId(),
                            root -> root.join(Ordonnance_.ficheConsultation, JoinType.LEFT).get(FicheConsultation_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
