package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Message} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.MessageResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /messages?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MessageCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter dateMessage;

    private StringFilter message;

    private LongFilter userIdFrom;

    private LongFilter userIdTo;

    private BooleanFilter readed;

    private Boolean distinct;

    public MessageCriteria() {}

    public MessageCriteria(MessageCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateMessage = other.dateMessage == null ? null : other.dateMessage.copy();
        this.message = other.message == null ? null : other.message.copy();
        this.userIdFrom = other.userIdFrom == null ? null : other.userIdFrom.copy();
        this.userIdTo = other.userIdTo == null ? null : other.userIdTo.copy();
        this.readed = other.readed == null ? null : other.readed.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MessageCriteria copy() {
        return new MessageCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getDateMessage() {
        return dateMessage;
    }

    public ZonedDateTimeFilter dateMessage() {
        if (dateMessage == null) {
            dateMessage = new ZonedDateTimeFilter();
        }
        return dateMessage;
    }

    public void setDateMessage(ZonedDateTimeFilter dateMessage) {
        this.dateMessage = dateMessage;
    }

    public StringFilter getMessage() {
        return message;
    }

    public StringFilter message() {
        if (message == null) {
            message = new StringFilter();
        }
        return message;
    }

    public void setMessage(StringFilter message) {
        this.message = message;
    }

    public LongFilter getUserIdFrom() {
        return userIdFrom;
    }

    public LongFilter userIdFrom() {
        if (userIdFrom == null) {
            userIdFrom = new LongFilter();
        }
        return userIdFrom;
    }

    public void setUserIdFrom(LongFilter userIdFrom) {
        this.userIdFrom = userIdFrom;
    }

    public LongFilter getUserIdTo() {
        return userIdTo;
    }

    public LongFilter userIdTo() {
        if (userIdTo == null) {
            userIdTo = new LongFilter();
        }
        return userIdTo;
    }

    public void setUserIdTo(LongFilter userIdTo) {
        this.userIdTo = userIdTo;
    }

    public BooleanFilter getReaded() {
        return readed;
    }

    public BooleanFilter readed() {
        if (readed == null) {
            readed = new BooleanFilter();
        }
        return readed;
    }

    public void setReaded(BooleanFilter readed) {
        this.readed = readed;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MessageCriteria that = (MessageCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dateMessage, that.dateMessage) &&
            Objects.equals(message, that.message) &&
            Objects.equals(userIdFrom, that.userIdFrom) &&
            Objects.equals(userIdTo, that.userIdTo) &&
            Objects.equals(readed, that.readed) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateMessage, message, userIdFrom, userIdTo, readed, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MessageCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dateMessage != null ? "dateMessage=" + dateMessage + ", " : "") +
            (message != null ? "message=" + message + ", " : "") +
            (userIdFrom != null ? "userIdFrom=" + userIdFrom + ", " : "") +
            (userIdTo != null ? "userIdTo=" + userIdTo + ", " : "") +
            (readed != null ? "readed=" + readed + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
