package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.FicheSymptome} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.FicheSymptomeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fiche-symptomes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FicheSymptomeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter observation;

    private LongFilter ficheConsultationId;

    private LongFilter symptomeId;

    private Boolean distinct;

    public FicheSymptomeCriteria() {}

    public FicheSymptomeCriteria(FicheSymptomeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.observation = other.observation == null ? null : other.observation.copy();
        this.ficheConsultationId = other.ficheConsultationId == null ? null : other.ficheConsultationId.copy();
        this.symptomeId = other.symptomeId == null ? null : other.symptomeId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public FicheSymptomeCriteria copy() {
        return new FicheSymptomeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getObservation() {
        return observation;
    }

    public StringFilter observation() {
        if (observation == null) {
            observation = new StringFilter();
        }
        return observation;
    }

    public void setObservation(StringFilter observation) {
        this.observation = observation;
    }

    public LongFilter getFicheConsultationId() {
        return ficheConsultationId;
    }

    public LongFilter ficheConsultationId() {
        if (ficheConsultationId == null) {
            ficheConsultationId = new LongFilter();
        }
        return ficheConsultationId;
    }

    public void setFicheConsultationId(LongFilter ficheConsultationId) {
        this.ficheConsultationId = ficheConsultationId;
    }

    public LongFilter getSymptomeId() {
        return symptomeId;
    }

    public LongFilter symptomeId() {
        if (symptomeId == null) {
            symptomeId = new LongFilter();
        }
        return symptomeId;
    }

    public void setSymptomeId(LongFilter symptomeId) {
        this.symptomeId = symptomeId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FicheSymptomeCriteria that = (FicheSymptomeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(observation, that.observation) &&
            Objects.equals(ficheConsultationId, that.ficheConsultationId) &&
            Objects.equals(symptomeId, that.symptomeId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, observation, ficheConsultationId, symptomeId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FicheSymptomeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (observation != null ? "observation=" + observation + ", " : "") +
            (ficheConsultationId != null ? "ficheConsultationId=" + ficheConsultationId + ", " : "") +
            (symptomeId != null ? "symptomeId=" + symptomeId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
