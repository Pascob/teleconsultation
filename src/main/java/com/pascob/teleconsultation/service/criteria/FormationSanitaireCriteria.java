package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.FormationSanitaire} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.FormationSanitaireResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /formation-sanitaires?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FormationSanitaireCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter designation;

    private StringFilter sigle;

    private StringFilter telephone;

    private StringFilter fixe;

    private StringFilter bp;

    private StringFilter email;

    private LongFilter medecinId;

    private Boolean distinct;

    public FormationSanitaireCriteria() {}

    public FormationSanitaireCriteria(FormationSanitaireCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.designation = other.designation == null ? null : other.designation.copy();
        this.sigle = other.sigle == null ? null : other.sigle.copy();
        this.telephone = other.telephone == null ? null : other.telephone.copy();
        this.fixe = other.fixe == null ? null : other.fixe.copy();
        this.bp = other.bp == null ? null : other.bp.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.medecinId = other.medecinId == null ? null : other.medecinId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public FormationSanitaireCriteria copy() {
        return new FormationSanitaireCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDesignation() {
        return designation;
    }

    public StringFilter designation() {
        if (designation == null) {
            designation = new StringFilter();
        }
        return designation;
    }

    public void setDesignation(StringFilter designation) {
        this.designation = designation;
    }

    public StringFilter getSigle() {
        return sigle;
    }

    public StringFilter sigle() {
        if (sigle == null) {
            sigle = new StringFilter();
        }
        return sigle;
    }

    public void setSigle(StringFilter sigle) {
        this.sigle = sigle;
    }

    public StringFilter getTelephone() {
        return telephone;
    }

    public StringFilter telephone() {
        if (telephone == null) {
            telephone = new StringFilter();
        }
        return telephone;
    }

    public void setTelephone(StringFilter telephone) {
        this.telephone = telephone;
    }

    public StringFilter getFixe() {
        return fixe;
    }

    public StringFilter fixe() {
        if (fixe == null) {
            fixe = new StringFilter();
        }
        return fixe;
    }

    public void setFixe(StringFilter fixe) {
        this.fixe = fixe;
    }

    public StringFilter getBp() {
        return bp;
    }

    public StringFilter bp() {
        if (bp == null) {
            bp = new StringFilter();
        }
        return bp;
    }

    public void setBp(StringFilter bp) {
        this.bp = bp;
    }

    public StringFilter getEmail() {
        return email;
    }

    public StringFilter email() {
        if (email == null) {
            email = new StringFilter();
        }
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public LongFilter getMedecinId() {
        return medecinId;
    }

    public LongFilter medecinId() {
        if (medecinId == null) {
            medecinId = new LongFilter();
        }
        return medecinId;
    }

    public void setMedecinId(LongFilter medecinId) {
        this.medecinId = medecinId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FormationSanitaireCriteria that = (FormationSanitaireCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(designation, that.designation) &&
            Objects.equals(sigle, that.sigle) &&
            Objects.equals(telephone, that.telephone) &&
            Objects.equals(fixe, that.fixe) &&
            Objects.equals(bp, that.bp) &&
            Objects.equals(email, that.email) &&
            Objects.equals(medecinId, that.medecinId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, designation, sigle, telephone, fixe, bp, email, medecinId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FormationSanitaireCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (designation != null ? "designation=" + designation + ", " : "") +
            (sigle != null ? "sigle=" + sigle + ", " : "") +
            (telephone != null ? "telephone=" + telephone + ", " : "") +
            (fixe != null ? "fixe=" + fixe + ", " : "") +
            (bp != null ? "bp=" + bp + ", " : "") +
            (email != null ? "email=" + email + ", " : "") +
            (medecinId != null ? "medecinId=" + medecinId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
