package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.PrescriptionExamen} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.PrescriptionExamenResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /prescription-examen?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PrescriptionExamenCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter description;

    private StringFilter resultat;

    private LongFilter bulletinId;

    private LongFilter examenMedicalId;

    private Boolean distinct;

    public PrescriptionExamenCriteria() {}

    public PrescriptionExamenCriteria(PrescriptionExamenCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.resultat = other.resultat == null ? null : other.resultat.copy();
        this.bulletinId = other.bulletinId == null ? null : other.bulletinId.copy();
        this.examenMedicalId = other.examenMedicalId == null ? null : other.examenMedicalId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PrescriptionExamenCriteria copy() {
        return new PrescriptionExamenCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public StringFilter description() {
        if (description == null) {
            description = new StringFilter();
        }
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getResultat() {
        return resultat;
    }

    public StringFilter resultat() {
        if (resultat == null) {
            resultat = new StringFilter();
        }
        return resultat;
    }

    public void setResultat(StringFilter resultat) {
        this.resultat = resultat;
    }

    public LongFilter getBulletinId() {
        return bulletinId;
    }

    public LongFilter bulletinId() {
        if (bulletinId == null) {
            bulletinId = new LongFilter();
        }
        return bulletinId;
    }

    public void setBulletinId(LongFilter bulletinId) {
        this.bulletinId = bulletinId;
    }

    public LongFilter getExamenMedicalId() {
        return examenMedicalId;
    }

    public LongFilter examenMedicalId() {
        if (examenMedicalId == null) {
            examenMedicalId = new LongFilter();
        }
        return examenMedicalId;
    }

    public void setExamenMedicalId(LongFilter examenMedicalId) {
        this.examenMedicalId = examenMedicalId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PrescriptionExamenCriteria that = (PrescriptionExamenCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(description, that.description) &&
            Objects.equals(resultat, that.resultat) &&
            Objects.equals(bulletinId, that.bulletinId) &&
            Objects.equals(examenMedicalId, that.examenMedicalId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, resultat, bulletinId, examenMedicalId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PrescriptionExamenCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (resultat != null ? "resultat=" + resultat + ", " : "") +
            (bulletinId != null ? "bulletinId=" + bulletinId + ", " : "") +
            (examenMedicalId != null ? "examenMedicalId=" + examenMedicalId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
