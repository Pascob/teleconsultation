package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Consultation} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.ConsultationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /consultations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ConsultationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter poids;

    private FloatFilter temperature;

    private IntegerFilter pouls;

    private IntegerFilter bpm;

    private FloatFilter age;

    private IntegerFilter taille;

    private ZonedDateTimeFilter dateDebutConsulation;

    private BooleanFilter isSuivi;

    private BooleanFilter isClose;

    private LongFilter ficheConsultationId;

    private LongFilter medecinId;

    private LongFilter demandeConsultationId;

    private Boolean distinct;

    public ConsultationCriteria() {}

    public ConsultationCriteria(ConsultationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.poids = other.poids == null ? null : other.poids.copy();
        this.temperature = other.temperature == null ? null : other.temperature.copy();
        this.pouls = other.pouls == null ? null : other.pouls.copy();
        this.bpm = other.bpm == null ? null : other.bpm.copy();
        this.age = other.age == null ? null : other.age.copy();
        this.taille = other.taille == null ? null : other.taille.copy();
        this.dateDebutConsulation = other.dateDebutConsulation == null ? null : other.dateDebutConsulation.copy();
        this.isSuivi = other.isSuivi == null ? null : other.isSuivi.copy();
        this.isClose = other.isClose == null ? null : other.isClose.copy();
        this.ficheConsultationId = other.ficheConsultationId == null ? null : other.ficheConsultationId.copy();
        this.medecinId = other.medecinId == null ? null : other.medecinId.copy();
        this.demandeConsultationId = other.demandeConsultationId == null ? null : other.demandeConsultationId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ConsultationCriteria copy() {
        return new ConsultationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getPoids() {
        return poids;
    }

    public IntegerFilter poids() {
        if (poids == null) {
            poids = new IntegerFilter();
        }
        return poids;
    }

    public void setPoids(IntegerFilter poids) {
        this.poids = poids;
    }

    public FloatFilter getTemperature() {
        return temperature;
    }

    public FloatFilter temperature() {
        if (temperature == null) {
            temperature = new FloatFilter();
        }
        return temperature;
    }

    public void setTemperature(FloatFilter temperature) {
        this.temperature = temperature;
    }

    public IntegerFilter getPouls() {
        return pouls;
    }

    public IntegerFilter pouls() {
        if (pouls == null) {
            pouls = new IntegerFilter();
        }
        return pouls;
    }

    public void setPouls(IntegerFilter pouls) {
        this.pouls = pouls;
    }

    public IntegerFilter getBpm() {
        return bpm;
    }

    public IntegerFilter bpm() {
        if (bpm == null) {
            bpm = new IntegerFilter();
        }
        return bpm;
    }

    public void setBpm(IntegerFilter bpm) {
        this.bpm = bpm;
    }

    public FloatFilter getAge() {
        return age;
    }

    public FloatFilter age() {
        if (age == null) {
            age = new FloatFilter();
        }
        return age;
    }

    public void setAge(FloatFilter age) {
        this.age = age;
    }

    public IntegerFilter getTaille() {
        return taille;
    }

    public IntegerFilter taille() {
        if (taille == null) {
            taille = new IntegerFilter();
        }
        return taille;
    }

    public void setTaille(IntegerFilter taille) {
        this.taille = taille;
    }

    public ZonedDateTimeFilter getDateDebutConsulation() {
        return dateDebutConsulation;
    }

    public ZonedDateTimeFilter dateDebutConsulation() {
        if (dateDebutConsulation == null) {
            dateDebutConsulation = new ZonedDateTimeFilter();
        }
        return dateDebutConsulation;
    }

    public void setDateDebutConsulation(ZonedDateTimeFilter dateDebutConsulation) {
        this.dateDebutConsulation = dateDebutConsulation;
    }

    public BooleanFilter getIsSuivi() {
        return isSuivi;
    }

    public BooleanFilter isSuivi() {
        if (isSuivi == null) {
            isSuivi = new BooleanFilter();
        }
        return isSuivi;
    }

    public void setIsSuivi(BooleanFilter isSuivi) {
        this.isSuivi = isSuivi;
    }

    public BooleanFilter getIsClose() {
        return isClose;
    }

    public BooleanFilter isClose() {
        if (isClose == null) {
            isClose = new BooleanFilter();
        }
        return isClose;
    }

    public void setIsClose(BooleanFilter isClose) {
        this.isClose = isClose;
    }

    public LongFilter getFicheConsultationId() {
        return ficheConsultationId;
    }

    public LongFilter ficheConsultationId() {
        if (ficheConsultationId == null) {
            ficheConsultationId = new LongFilter();
        }
        return ficheConsultationId;
    }

    public void setFicheConsultationId(LongFilter ficheConsultationId) {
        this.ficheConsultationId = ficheConsultationId;
    }

    public LongFilter getMedecinId() {
        return medecinId;
    }

    public LongFilter medecinId() {
        if (medecinId == null) {
            medecinId = new LongFilter();
        }
        return medecinId;
    }

    public void setMedecinId(LongFilter medecinId) {
        this.medecinId = medecinId;
    }

    public LongFilter getDemandeConsultationId() {
        return demandeConsultationId;
    }

    public LongFilter demandeConsultationId() {
        if (demandeConsultationId == null) {
            demandeConsultationId = new LongFilter();
        }
        return demandeConsultationId;
    }

    public void setDemandeConsultationId(LongFilter demandeConsultationId) {
        this.demandeConsultationId = demandeConsultationId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ConsultationCriteria that = (ConsultationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(poids, that.poids) &&
            Objects.equals(temperature, that.temperature) &&
            Objects.equals(pouls, that.pouls) &&
            Objects.equals(bpm, that.bpm) &&
            Objects.equals(age, that.age) &&
            Objects.equals(taille, that.taille) &&
            Objects.equals(dateDebutConsulation, that.dateDebutConsulation) &&
            Objects.equals(isSuivi, that.isSuivi) &&
            Objects.equals(isClose, that.isClose) &&
            Objects.equals(ficheConsultationId, that.ficheConsultationId) &&
            Objects.equals(medecinId, that.medecinId) &&
            Objects.equals(demandeConsultationId, that.demandeConsultationId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            poids,
            temperature,
            pouls,
            bpm,
            age,
            taille,
            dateDebutConsulation,
            isSuivi,
            isClose,
            ficheConsultationId,
            medecinId,
            demandeConsultationId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ConsultationCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (poids != null ? "poids=" + poids + ", " : "") +
            (temperature != null ? "temperature=" + temperature + ", " : "") +
            (pouls != null ? "pouls=" + pouls + ", " : "") +
            (bpm != null ? "bpm=" + bpm + ", " : "") +
            (age != null ? "age=" + age + ", " : "") +
            (taille != null ? "taille=" + taille + ", " : "") +
            (dateDebutConsulation != null ? "dateDebutConsulation=" + dateDebutConsulation + ", " : "") +
            (isSuivi != null ? "isSuivi=" + isSuivi + ", " : "") +
            (isClose != null ? "isClose=" + isClose + ", " : "") +
            (ficheConsultationId != null ? "ficheConsultationId=" + ficheConsultationId + ", " : "") +
            (medecinId != null ? "medecinId=" + medecinId + ", " : "") +
            (demandeConsultationId != null ? "demandeConsultationId=" + demandeConsultationId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
