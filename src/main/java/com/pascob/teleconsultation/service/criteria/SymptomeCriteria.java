package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Symptome} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.SymptomeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /symptomes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SymptomeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter designation;

    private StringFilter code;

    private LongFilter ficheSymptomeId;

    private Boolean distinct;

    public SymptomeCriteria() {}

    public SymptomeCriteria(SymptomeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.designation = other.designation == null ? null : other.designation.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.ficheSymptomeId = other.ficheSymptomeId == null ? null : other.ficheSymptomeId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public SymptomeCriteria copy() {
        return new SymptomeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDesignation() {
        return designation;
    }

    public StringFilter designation() {
        if (designation == null) {
            designation = new StringFilter();
        }
        return designation;
    }

    public void setDesignation(StringFilter designation) {
        this.designation = designation;
    }

    public StringFilter getCode() {
        return code;
    }

    public StringFilter code() {
        if (code == null) {
            code = new StringFilter();
        }
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public LongFilter getFicheSymptomeId() {
        return ficheSymptomeId;
    }

    public LongFilter ficheSymptomeId() {
        if (ficheSymptomeId == null) {
            ficheSymptomeId = new LongFilter();
        }
        return ficheSymptomeId;
    }

    public void setFicheSymptomeId(LongFilter ficheSymptomeId) {
        this.ficheSymptomeId = ficheSymptomeId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SymptomeCriteria that = (SymptomeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(designation, that.designation) &&
            Objects.equals(code, that.code) &&
            Objects.equals(ficheSymptomeId, that.ficheSymptomeId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, designation, code, ficheSymptomeId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SymptomeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (designation != null ? "designation=" + designation + ", " : "") +
            (code != null ? "code=" + code + ", " : "") +
            (ficheSymptomeId != null ? "ficheSymptomeId=" + ficheSymptomeId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
