package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.PrescriptionProduit} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.PrescriptionProduitResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /prescription-produits?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PrescriptionProduitCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter posologie;

    private StringFilter observation;

    private LongFilter produitId;

    private LongFilter ordonnanceId;

    private Boolean distinct;

    public PrescriptionProduitCriteria() {}

    public PrescriptionProduitCriteria(PrescriptionProduitCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.posologie = other.posologie == null ? null : other.posologie.copy();
        this.observation = other.observation == null ? null : other.observation.copy();
        this.produitId = other.produitId == null ? null : other.produitId.copy();
        this.ordonnanceId = other.ordonnanceId == null ? null : other.ordonnanceId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PrescriptionProduitCriteria copy() {
        return new PrescriptionProduitCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPosologie() {
        return posologie;
    }

    public StringFilter posologie() {
        if (posologie == null) {
            posologie = new StringFilter();
        }
        return posologie;
    }

    public void setPosologie(StringFilter posologie) {
        this.posologie = posologie;
    }

    public StringFilter getObservation() {
        return observation;
    }

    public StringFilter observation() {
        if (observation == null) {
            observation = new StringFilter();
        }
        return observation;
    }

    public void setObservation(StringFilter observation) {
        this.observation = observation;
    }

    public LongFilter getProduitId() {
        return produitId;
    }

    public LongFilter produitId() {
        if (produitId == null) {
            produitId = new LongFilter();
        }
        return produitId;
    }

    public void setProduitId(LongFilter produitId) {
        this.produitId = produitId;
    }

    public LongFilter getOrdonnanceId() {
        return ordonnanceId;
    }

    public LongFilter ordonnanceId() {
        if (ordonnanceId == null) {
            ordonnanceId = new LongFilter();
        }
        return ordonnanceId;
    }

    public void setOrdonnanceId(LongFilter ordonnanceId) {
        this.ordonnanceId = ordonnanceId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PrescriptionProduitCriteria that = (PrescriptionProduitCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(posologie, that.posologie) &&
            Objects.equals(observation, that.observation) &&
            Objects.equals(produitId, that.produitId) &&
            Objects.equals(ordonnanceId, that.ordonnanceId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, posologie, observation, produitId, ordonnanceId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PrescriptionProduitCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (posologie != null ? "posologie=" + posologie + ", " : "") +
            (observation != null ? "observation=" + observation + ", " : "") +
            (produitId != null ? "produitId=" + produitId + ", " : "") +
            (ordonnanceId != null ? "ordonnanceId=" + ordonnanceId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
