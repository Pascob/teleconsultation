package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.ExamenMedical} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.ExamenMedicalResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /examen-medicals?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExamenMedicalCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter designation;

    private StringFilter code;

    private LongFilter prescriptionExamenId;

    private Boolean distinct;

    public ExamenMedicalCriteria() {}

    public ExamenMedicalCriteria(ExamenMedicalCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.designation = other.designation == null ? null : other.designation.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.prescriptionExamenId = other.prescriptionExamenId == null ? null : other.prescriptionExamenId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ExamenMedicalCriteria copy() {
        return new ExamenMedicalCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDesignation() {
        return designation;
    }

    public StringFilter designation() {
        if (designation == null) {
            designation = new StringFilter();
        }
        return designation;
    }

    public void setDesignation(StringFilter designation) {
        this.designation = designation;
    }

    public StringFilter getCode() {
        return code;
    }

    public StringFilter code() {
        if (code == null) {
            code = new StringFilter();
        }
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public LongFilter getPrescriptionExamenId() {
        return prescriptionExamenId;
    }

    public LongFilter prescriptionExamenId() {
        if (prescriptionExamenId == null) {
            prescriptionExamenId = new LongFilter();
        }
        return prescriptionExamenId;
    }

    public void setPrescriptionExamenId(LongFilter prescriptionExamenId) {
        this.prescriptionExamenId = prescriptionExamenId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ExamenMedicalCriteria that = (ExamenMedicalCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(designation, that.designation) &&
            Objects.equals(code, that.code) &&
            Objects.equals(prescriptionExamenId, that.prescriptionExamenId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, designation, code, prescriptionExamenId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExamenMedicalCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (designation != null ? "designation=" + designation + ", " : "") +
            (code != null ? "code=" + code + ", " : "") +
            (prescriptionExamenId != null ? "prescriptionExamenId=" + prescriptionExamenId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
