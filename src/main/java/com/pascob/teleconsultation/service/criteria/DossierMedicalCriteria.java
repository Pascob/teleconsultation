package com.pascob.teleconsultation.service.criteria;

import com.pascob.teleconsultation.domain.enumeration.GroupeSanguin;
import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.DossierMedical} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.DossierMedicalResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dossier-medicals?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DossierMedicalCriteria implements Serializable, Criteria {

    /**
     * Class for filtering GroupeSanguin
     */
    public static class GroupeSanguinFilter extends Filter<GroupeSanguin> {

        public GroupeSanguinFilter() {}

        public GroupeSanguinFilter(GroupeSanguinFilter filter) {
            super(filter);
        }

        @Override
        public GroupeSanguinFilter copy() {
            return new GroupeSanguinFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter numero;

    private GroupeSanguinFilter groupeSanguin;

    private LongFilter demandeConsultationId;

    private LongFilter patientId;

    private LongFilter medecinId;

    private Boolean distinct;

    public DossierMedicalCriteria() {}

    public DossierMedicalCriteria(DossierMedicalCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.numero = other.numero == null ? null : other.numero.copy();
        this.groupeSanguin = other.groupeSanguin == null ? null : other.groupeSanguin.copy();
        this.demandeConsultationId = other.demandeConsultationId == null ? null : other.demandeConsultationId.copy();
        this.patientId = other.patientId == null ? null : other.patientId.copy();
        this.medecinId = other.medecinId == null ? null : other.medecinId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public DossierMedicalCriteria copy() {
        return new DossierMedicalCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNumero() {
        return numero;
    }

    public StringFilter numero() {
        if (numero == null) {
            numero = new StringFilter();
        }
        return numero;
    }

    public void setNumero(StringFilter numero) {
        this.numero = numero;
    }

    public GroupeSanguinFilter getGroupeSanguin() {
        return groupeSanguin;
    }

    public GroupeSanguinFilter groupeSanguin() {
        if (groupeSanguin == null) {
            groupeSanguin = new GroupeSanguinFilter();
        }
        return groupeSanguin;
    }

    public void setGroupeSanguin(GroupeSanguinFilter groupeSanguin) {
        this.groupeSanguin = groupeSanguin;
    }

    public LongFilter getDemandeConsultationId() {
        return demandeConsultationId;
    }

    public LongFilter demandeConsultationId() {
        if (demandeConsultationId == null) {
            demandeConsultationId = new LongFilter();
        }
        return demandeConsultationId;
    }

    public void setDemandeConsultationId(LongFilter demandeConsultationId) {
        this.demandeConsultationId = demandeConsultationId;
    }

    public LongFilter getPatientId() {
        return patientId;
    }

    public LongFilter patientId() {
        if (patientId == null) {
            patientId = new LongFilter();
        }
        return patientId;
    }

    public void setPatientId(LongFilter patientId) {
        this.patientId = patientId;
    }

    public LongFilter getMedecinId() {
        return medecinId;
    }

    public LongFilter medecinId() {
        if (medecinId == null) {
            medecinId = new LongFilter();
        }
        return medecinId;
    }

    public void setMedecinId(LongFilter medecinId) {
        this.medecinId = medecinId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DossierMedicalCriteria that = (DossierMedicalCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(numero, that.numero) &&
            Objects.equals(groupeSanguin, that.groupeSanguin) &&
            Objects.equals(demandeConsultationId, that.demandeConsultationId) &&
            Objects.equals(patientId, that.patientId) &&
            Objects.equals(medecinId, that.medecinId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numero, groupeSanguin, demandeConsultationId, patientId, medecinId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DossierMedicalCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (numero != null ? "numero=" + numero + ", " : "") +
            (groupeSanguin != null ? "groupeSanguin=" + groupeSanguin + ", " : "") +
            (demandeConsultationId != null ? "demandeConsultationId=" + demandeConsultationId + ", " : "") +
            (patientId != null ? "patientId=" + patientId + ", " : "") +
            (medecinId != null ? "medecinId=" + medecinId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
