package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.FicheConsultation} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.FicheConsultationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /fiche-consultations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FicheConsultationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter dateFiche;

    private StringFilter numero;

    private StringFilter observation;

    private StringFilter diagnostic;

    private LongFilter ordonnanceId;

    private LongFilter bulletinId;

    private LongFilter ficheSymptomeId;

    private LongFilter medecinId;

    private LongFilter consultationId;

    private Boolean distinct;

    public FicheConsultationCriteria() {}

    public FicheConsultationCriteria(FicheConsultationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateFiche = other.dateFiche == null ? null : other.dateFiche.copy();
        this.numero = other.numero == null ? null : other.numero.copy();
        this.observation = other.observation == null ? null : other.observation.copy();
        this.diagnostic = other.diagnostic == null ? null : other.diagnostic.copy();
        this.ordonnanceId = other.ordonnanceId == null ? null : other.ordonnanceId.copy();
        this.bulletinId = other.bulletinId == null ? null : other.bulletinId.copy();
        this.ficheSymptomeId = other.ficheSymptomeId == null ? null : other.ficheSymptomeId.copy();
        this.medecinId = other.medecinId == null ? null : other.medecinId.copy();
        this.consultationId = other.consultationId == null ? null : other.consultationId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public FicheConsultationCriteria copy() {
        return new FicheConsultationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getDateFiche() {
        return dateFiche;
    }

    public LocalDateFilter dateFiche() {
        if (dateFiche == null) {
            dateFiche = new LocalDateFilter();
        }
        return dateFiche;
    }

    public void setDateFiche(LocalDateFilter dateFiche) {
        this.dateFiche = dateFiche;
    }

    public StringFilter getNumero() {
        return numero;
    }

    public StringFilter numero() {
        if (numero == null) {
            numero = new StringFilter();
        }
        return numero;
    }

    public void setNumero(StringFilter numero) {
        this.numero = numero;
    }

    public StringFilter getObservation() {
        return observation;
    }

    public StringFilter observation() {
        if (observation == null) {
            observation = new StringFilter();
        }
        return observation;
    }

    public void setObservation(StringFilter observation) {
        this.observation = observation;
    }

    public StringFilter getDiagnostic() {
        return diagnostic;
    }

    public StringFilter diagnostic() {
        if (diagnostic == null) {
            diagnostic = new StringFilter();
        }
        return diagnostic;
    }

    public void setDiagnostic(StringFilter diagnostic) {
        this.diagnostic = diagnostic;
    }

    public LongFilter getOrdonnanceId() {
        return ordonnanceId;
    }

    public LongFilter ordonnanceId() {
        if (ordonnanceId == null) {
            ordonnanceId = new LongFilter();
        }
        return ordonnanceId;
    }

    public void setOrdonnanceId(LongFilter ordonnanceId) {
        this.ordonnanceId = ordonnanceId;
    }

    public LongFilter getBulletinId() {
        return bulletinId;
    }

    public LongFilter bulletinId() {
        if (bulletinId == null) {
            bulletinId = new LongFilter();
        }
        return bulletinId;
    }

    public void setBulletinId(LongFilter bulletinId) {
        this.bulletinId = bulletinId;
    }

    public LongFilter getFicheSymptomeId() {
        return ficheSymptomeId;
    }

    public LongFilter ficheSymptomeId() {
        if (ficheSymptomeId == null) {
            ficheSymptomeId = new LongFilter();
        }
        return ficheSymptomeId;
    }

    public void setFicheSymptomeId(LongFilter ficheSymptomeId) {
        this.ficheSymptomeId = ficheSymptomeId;
    }

    public LongFilter getMedecinId() {
        return medecinId;
    }

    public LongFilter medecinId() {
        if (medecinId == null) {
            medecinId = new LongFilter();
        }
        return medecinId;
    }

    public void setMedecinId(LongFilter medecinId) {
        this.medecinId = medecinId;
    }

    public LongFilter getConsultationId() {
        return consultationId;
    }

    public LongFilter consultationId() {
        if (consultationId == null) {
            consultationId = new LongFilter();
        }
        return consultationId;
    }

    public void setConsultationId(LongFilter consultationId) {
        this.consultationId = consultationId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FicheConsultationCriteria that = (FicheConsultationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dateFiche, that.dateFiche) &&
            Objects.equals(numero, that.numero) &&
            Objects.equals(observation, that.observation) &&
            Objects.equals(diagnostic, that.diagnostic) &&
            Objects.equals(ordonnanceId, that.ordonnanceId) &&
            Objects.equals(bulletinId, that.bulletinId) &&
            Objects.equals(ficheSymptomeId, that.ficheSymptomeId) &&
            Objects.equals(medecinId, that.medecinId) &&
            Objects.equals(consultationId, that.consultationId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            dateFiche,
            numero,
            observation,
            diagnostic,
            ordonnanceId,
            bulletinId,
            ficheSymptomeId,
            medecinId,
            consultationId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FicheConsultationCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dateFiche != null ? "dateFiche=" + dateFiche + ", " : "") +
            (numero != null ? "numero=" + numero + ", " : "") +
            (observation != null ? "observation=" + observation + ", " : "") +
            (diagnostic != null ? "diagnostic=" + diagnostic + ", " : "") +
            (ordonnanceId != null ? "ordonnanceId=" + ordonnanceId + ", " : "") +
            (bulletinId != null ? "bulletinId=" + bulletinId + ", " : "") +
            (ficheSymptomeId != null ? "ficheSymptomeId=" + ficheSymptomeId + ", " : "") +
            (medecinId != null ? "medecinId=" + medecinId + ", " : "") +
            (consultationId != null ? "consultationId=" + consultationId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
