package com.pascob.teleconsultation.service.criteria;

import com.pascob.teleconsultation.domain.enumeration.TypeNotification;
import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Notification} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.NotificationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /notifications?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NotificationCriteria implements Serializable, Criteria {

    /**
     * Class for filtering TypeNotification
     */
    public static class TypeNotificationFilter extends Filter<TypeNotification> {

        public TypeNotificationFilter() {}

        public TypeNotificationFilter(TypeNotificationFilter filter) {
            super(filter);
        }

        @Override
        public TypeNotificationFilter copy() {
            return new TypeNotificationFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter dateNotification;

    private StringFilter objet;

    private StringFilter corpus;

    private StringFilter sender;

    private StringFilter receiver;

    private TypeNotificationFilter typeNotification;

    private Boolean distinct;

    public NotificationCriteria() {}

    public NotificationCriteria(NotificationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateNotification = other.dateNotification == null ? null : other.dateNotification.copy();
        this.objet = other.objet == null ? null : other.objet.copy();
        this.corpus = other.corpus == null ? null : other.corpus.copy();
        this.sender = other.sender == null ? null : other.sender.copy();
        this.receiver = other.receiver == null ? null : other.receiver.copy();
        this.typeNotification = other.typeNotification == null ? null : other.typeNotification.copy();
        this.distinct = other.distinct;
    }

    @Override
    public NotificationCriteria copy() {
        return new NotificationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getDateNotification() {
        return dateNotification;
    }

    public ZonedDateTimeFilter dateNotification() {
        if (dateNotification == null) {
            dateNotification = new ZonedDateTimeFilter();
        }
        return dateNotification;
    }

    public void setDateNotification(ZonedDateTimeFilter dateNotification) {
        this.dateNotification = dateNotification;
    }

    public StringFilter getObjet() {
        return objet;
    }

    public StringFilter objet() {
        if (objet == null) {
            objet = new StringFilter();
        }
        return objet;
    }

    public void setObjet(StringFilter objet) {
        this.objet = objet;
    }

    public StringFilter getCorpus() {
        return corpus;
    }

    public StringFilter corpus() {
        if (corpus == null) {
            corpus = new StringFilter();
        }
        return corpus;
    }

    public void setCorpus(StringFilter corpus) {
        this.corpus = corpus;
    }

    public StringFilter getSender() {
        return sender;
    }

    public StringFilter sender() {
        if (sender == null) {
            sender = new StringFilter();
        }
        return sender;
    }

    public void setSender(StringFilter sender) {
        this.sender = sender;
    }

    public StringFilter getReceiver() {
        return receiver;
    }

    public StringFilter receiver() {
        if (receiver == null) {
            receiver = new StringFilter();
        }
        return receiver;
    }

    public void setReceiver(StringFilter receiver) {
        this.receiver = receiver;
    }

    public TypeNotificationFilter getTypeNotification() {
        return typeNotification;
    }

    public TypeNotificationFilter typeNotification() {
        if (typeNotification == null) {
            typeNotification = new TypeNotificationFilter();
        }
        return typeNotification;
    }

    public void setTypeNotification(TypeNotificationFilter typeNotification) {
        this.typeNotification = typeNotification;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NotificationCriteria that = (NotificationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dateNotification, that.dateNotification) &&
            Objects.equals(objet, that.objet) &&
            Objects.equals(corpus, that.corpus) &&
            Objects.equals(sender, that.sender) &&
            Objects.equals(receiver, that.receiver) &&
            Objects.equals(typeNotification, that.typeNotification) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateNotification, objet, corpus, sender, receiver, typeNotification, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NotificationCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dateNotification != null ? "dateNotification=" + dateNotification + ", " : "") +
            (objet != null ? "objet=" + objet + ", " : "") +
            (corpus != null ? "corpus=" + corpus + ", " : "") +
            (sender != null ? "sender=" + sender + ", " : "") +
            (receiver != null ? "receiver=" + receiver + ", " : "") +
            (typeNotification != null ? "typeNotification=" + typeNotification + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
