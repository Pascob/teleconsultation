package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Bulletin} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.BulletinResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bulletins?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class BulletinCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter dateBulletin;

    private StringFilter numero;

    private LongFilter prescriptionExamenId;

    private LongFilter ficheConsultationId;

    private Boolean distinct;

    public BulletinCriteria() {}

    public BulletinCriteria(BulletinCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateBulletin = other.dateBulletin == null ? null : other.dateBulletin.copy();
        this.numero = other.numero == null ? null : other.numero.copy();
        this.prescriptionExamenId = other.prescriptionExamenId == null ? null : other.prescriptionExamenId.copy();
        this.ficheConsultationId = other.ficheConsultationId == null ? null : other.ficheConsultationId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public BulletinCriteria copy() {
        return new BulletinCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getDateBulletin() {
        return dateBulletin;
    }

    public LocalDateFilter dateBulletin() {
        if (dateBulletin == null) {
            dateBulletin = new LocalDateFilter();
        }
        return dateBulletin;
    }

    public void setDateBulletin(LocalDateFilter dateBulletin) {
        this.dateBulletin = dateBulletin;
    }

    public StringFilter getNumero() {
        return numero;
    }

    public StringFilter numero() {
        if (numero == null) {
            numero = new StringFilter();
        }
        return numero;
    }

    public void setNumero(StringFilter numero) {
        this.numero = numero;
    }

    public LongFilter getPrescriptionExamenId() {
        return prescriptionExamenId;
    }

    public LongFilter prescriptionExamenId() {
        if (prescriptionExamenId == null) {
            prescriptionExamenId = new LongFilter();
        }
        return prescriptionExamenId;
    }

    public void setPrescriptionExamenId(LongFilter prescriptionExamenId) {
        this.prescriptionExamenId = prescriptionExamenId;
    }

    public LongFilter getFicheConsultationId() {
        return ficheConsultationId;
    }

    public LongFilter ficheConsultationId() {
        if (ficheConsultationId == null) {
            ficheConsultationId = new LongFilter();
        }
        return ficheConsultationId;
    }

    public void setFicheConsultationId(LongFilter ficheConsultationId) {
        this.ficheConsultationId = ficheConsultationId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BulletinCriteria that = (BulletinCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dateBulletin, that.dateBulletin) &&
            Objects.equals(numero, that.numero) &&
            Objects.equals(prescriptionExamenId, that.prescriptionExamenId) &&
            Objects.equals(ficheConsultationId, that.ficheConsultationId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateBulletin, numero, prescriptionExamenId, ficheConsultationId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BulletinCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dateBulletin != null ? "dateBulletin=" + dateBulletin + ", " : "") +
            (numero != null ? "numero=" + numero + ", " : "") +
            (prescriptionExamenId != null ? "prescriptionExamenId=" + prescriptionExamenId + ", " : "") +
            (ficheConsultationId != null ? "ficheConsultationId=" + ficheConsultationId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
