package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Specialite} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.SpecialiteResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /specialites?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SpecialiteCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter designation;

    private DoubleFilter frais;

    private LongFilter medecinId;

    private LongFilter demandeConsultationId;

    private Boolean distinct;

    public SpecialiteCriteria() {}

    public SpecialiteCriteria(SpecialiteCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.designation = other.designation == null ? null : other.designation.copy();
        this.frais = other.frais == null ? null : other.frais.copy();
        this.medecinId = other.medecinId == null ? null : other.medecinId.copy();
        this.demandeConsultationId = other.demandeConsultationId == null ? null : other.demandeConsultationId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public SpecialiteCriteria copy() {
        return new SpecialiteCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDesignation() {
        return designation;
    }

    public StringFilter designation() {
        if (designation == null) {
            designation = new StringFilter();
        }
        return designation;
    }

    public void setDesignation(StringFilter designation) {
        this.designation = designation;
    }

    public DoubleFilter getFrais() {
        return frais;
    }

    public DoubleFilter frais() {
        if (frais == null) {
            frais = new DoubleFilter();
        }
        return frais;
    }

    public void setFrais(DoubleFilter frais) {
        this.frais = frais;
    }

    public LongFilter getMedecinId() {
        return medecinId;
    }

    public LongFilter medecinId() {
        if (medecinId == null) {
            medecinId = new LongFilter();
        }
        return medecinId;
    }

    public void setMedecinId(LongFilter medecinId) {
        this.medecinId = medecinId;
    }

    public LongFilter getDemandeConsultationId() {
        return demandeConsultationId;
    }

    public LongFilter demandeConsultationId() {
        if (demandeConsultationId == null) {
            demandeConsultationId = new LongFilter();
        }
        return demandeConsultationId;
    }

    public void setDemandeConsultationId(LongFilter demandeConsultationId) {
        this.demandeConsultationId = demandeConsultationId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpecialiteCriteria that = (SpecialiteCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(designation, that.designation) &&
            Objects.equals(frais, that.frais) &&
            Objects.equals(medecinId, that.medecinId) &&
            Objects.equals(demandeConsultationId, that.demandeConsultationId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, designation, frais, medecinId, demandeConsultationId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SpecialiteCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (designation != null ? "designation=" + designation + ", " : "") +
            (frais != null ? "frais=" + frais + ", " : "") +
            (medecinId != null ? "medecinId=" + medecinId + ", " : "") +
            (demandeConsultationId != null ? "demandeConsultationId=" + demandeConsultationId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
