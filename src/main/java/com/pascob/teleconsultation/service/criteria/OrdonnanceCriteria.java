package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Ordonnance} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.OrdonnanceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ordonnances?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class OrdonnanceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LocalDateFilter dateOrdonnance;

    private StringFilter numero;

    private LongFilter prescriptionProduitId;

    private LongFilter ficheConsultationId;

    private Boolean distinct;

    public OrdonnanceCriteria() {}

    public OrdonnanceCriteria(OrdonnanceCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateOrdonnance = other.dateOrdonnance == null ? null : other.dateOrdonnance.copy();
        this.numero = other.numero == null ? null : other.numero.copy();
        this.prescriptionProduitId = other.prescriptionProduitId == null ? null : other.prescriptionProduitId.copy();
        this.ficheConsultationId = other.ficheConsultationId == null ? null : other.ficheConsultationId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public OrdonnanceCriteria copy() {
        return new OrdonnanceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LocalDateFilter getDateOrdonnance() {
        return dateOrdonnance;
    }

    public LocalDateFilter dateOrdonnance() {
        if (dateOrdonnance == null) {
            dateOrdonnance = new LocalDateFilter();
        }
        return dateOrdonnance;
    }

    public void setDateOrdonnance(LocalDateFilter dateOrdonnance) {
        this.dateOrdonnance = dateOrdonnance;
    }

    public StringFilter getNumero() {
        return numero;
    }

    public StringFilter numero() {
        if (numero == null) {
            numero = new StringFilter();
        }
        return numero;
    }

    public void setNumero(StringFilter numero) {
        this.numero = numero;
    }

    public LongFilter getPrescriptionProduitId() {
        return prescriptionProduitId;
    }

    public LongFilter prescriptionProduitId() {
        if (prescriptionProduitId == null) {
            prescriptionProduitId = new LongFilter();
        }
        return prescriptionProduitId;
    }

    public void setPrescriptionProduitId(LongFilter prescriptionProduitId) {
        this.prescriptionProduitId = prescriptionProduitId;
    }

    public LongFilter getFicheConsultationId() {
        return ficheConsultationId;
    }

    public LongFilter ficheConsultationId() {
        if (ficheConsultationId == null) {
            ficheConsultationId = new LongFilter();
        }
        return ficheConsultationId;
    }

    public void setFicheConsultationId(LongFilter ficheConsultationId) {
        this.ficheConsultationId = ficheConsultationId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrdonnanceCriteria that = (OrdonnanceCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dateOrdonnance, that.dateOrdonnance) &&
            Objects.equals(numero, that.numero) &&
            Objects.equals(prescriptionProduitId, that.prescriptionProduitId) &&
            Objects.equals(ficheConsultationId, that.ficheConsultationId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateOrdonnance, numero, prescriptionProduitId, ficheConsultationId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdonnanceCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dateOrdonnance != null ? "dateOrdonnance=" + dateOrdonnance + ", " : "") +
            (numero != null ? "numero=" + numero + ", " : "") +
            (prescriptionProduitId != null ? "prescriptionProduitId=" + prescriptionProduitId + ", " : "") +
            (ficheConsultationId != null ? "ficheConsultationId=" + ficheConsultationId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
