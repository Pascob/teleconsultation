package com.pascob.teleconsultation.service.criteria;

import com.pascob.teleconsultation.domain.enumeration.StatutDemandeConsultation;
import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.DemandeConsultation} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.DemandeConsultationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /demande-consultations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DemandeConsultationCriteria implements Serializable, Criteria {

    /**
     * Class for filtering StatutDemandeConsultation
     */
    public static class StatutDemandeConsultationFilter extends Filter<StatutDemandeConsultation> {

        public StatutDemandeConsultationFilter() {}

        public StatutDemandeConsultationFilter(StatutDemandeConsultationFilter filter) {
            super(filter);
        }

        @Override
        public StatutDemandeConsultationFilter copy() {
            return new StatutDemandeConsultationFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter dateHeureDemande;

    private IntegerFilter ordre;

    private StatutDemandeConsultationFilter statut;

    private StringFilter codeOtp;

    private StringFilter telephonePaiement;

    private DoubleFilter montant;

    private LongFilter consultationId;

    private LongFilter specialiteId;

    private LongFilter dossierMedicalId;

    private Boolean distinct;

    public DemandeConsultationCriteria() {}

    public DemandeConsultationCriteria(DemandeConsultationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateHeureDemande = other.dateHeureDemande == null ? null : other.dateHeureDemande.copy();
        this.ordre = other.ordre == null ? null : other.ordre.copy();
        this.statut = other.statut == null ? null : other.statut.copy();
        this.codeOtp = other.codeOtp == null ? null : other.codeOtp.copy();
        this.telephonePaiement = other.telephonePaiement == null ? null : other.telephonePaiement.copy();
        this.montant = other.montant == null ? null : other.montant.copy();
        this.consultationId = other.consultationId == null ? null : other.consultationId.copy();
        this.specialiteId = other.specialiteId == null ? null : other.specialiteId.copy();
        this.dossierMedicalId = other.dossierMedicalId == null ? null : other.dossierMedicalId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public DemandeConsultationCriteria copy() {
        return new DemandeConsultationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getDateHeureDemande() {
        return dateHeureDemande;
    }

    public ZonedDateTimeFilter dateHeureDemande() {
        if (dateHeureDemande == null) {
            dateHeureDemande = new ZonedDateTimeFilter();
        }
        return dateHeureDemande;
    }

    public void setDateHeureDemande(ZonedDateTimeFilter dateHeureDemande) {
        this.dateHeureDemande = dateHeureDemande;
    }

    public IntegerFilter getOrdre() {
        return ordre;
    }

    public IntegerFilter ordre() {
        if (ordre == null) {
            ordre = new IntegerFilter();
        }
        return ordre;
    }

    public void setOrdre(IntegerFilter ordre) {
        this.ordre = ordre;
    }

    public StatutDemandeConsultationFilter getStatut() {
        return statut;
    }

    public StatutDemandeConsultationFilter statut() {
        if (statut == null) {
            statut = new StatutDemandeConsultationFilter();
        }
        return statut;
    }

    public void setStatut(StatutDemandeConsultationFilter statut) {
        this.statut = statut;
    }

    public StringFilter getCodeOtp() {
        return codeOtp;
    }

    public StringFilter codeOtp() {
        if (codeOtp == null) {
            codeOtp = new StringFilter();
        }
        return codeOtp;
    }

    public void setCodeOtp(StringFilter codeOtp) {
        this.codeOtp = codeOtp;
    }

    public StringFilter getTelephonePaiement() {
        return telephonePaiement;
    }

    public StringFilter telephonePaiement() {
        if (telephonePaiement == null) {
            telephonePaiement = new StringFilter();
        }
        return telephonePaiement;
    }

    public void setTelephonePaiement(StringFilter telephonePaiement) {
        this.telephonePaiement = telephonePaiement;
    }

    public DoubleFilter getMontant() {
        return montant;
    }

    public DoubleFilter montant() {
        if (montant == null) {
            montant = new DoubleFilter();
        }
        return montant;
    }

    public void setMontant(DoubleFilter montant) {
        this.montant = montant;
    }

    public LongFilter getConsultationId() {
        return consultationId;
    }

    public LongFilter consultationId() {
        if (consultationId == null) {
            consultationId = new LongFilter();
        }
        return consultationId;
    }

    public void setConsultationId(LongFilter consultationId) {
        this.consultationId = consultationId;
    }

    public LongFilter getSpecialiteId() {
        return specialiteId;
    }

    public LongFilter specialiteId() {
        if (specialiteId == null) {
            specialiteId = new LongFilter();
        }
        return specialiteId;
    }

    public void setSpecialiteId(LongFilter specialiteId) {
        this.specialiteId = specialiteId;
    }

    public LongFilter getDossierMedicalId() {
        return dossierMedicalId;
    }

    public LongFilter dossierMedicalId() {
        if (dossierMedicalId == null) {
            dossierMedicalId = new LongFilter();
        }
        return dossierMedicalId;
    }

    public void setDossierMedicalId(LongFilter dossierMedicalId) {
        this.dossierMedicalId = dossierMedicalId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DemandeConsultationCriteria that = (DemandeConsultationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dateHeureDemande, that.dateHeureDemande) &&
            Objects.equals(ordre, that.ordre) &&
            Objects.equals(statut, that.statut) &&
            Objects.equals(codeOtp, that.codeOtp) &&
            Objects.equals(telephonePaiement, that.telephonePaiement) &&
            Objects.equals(montant, that.montant) &&
            Objects.equals(consultationId, that.consultationId) &&
            Objects.equals(specialiteId, that.specialiteId) &&
            Objects.equals(dossierMedicalId, that.dossierMedicalId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            dateHeureDemande,
            ordre,
            statut,
            codeOtp,
            telephonePaiement,
            montant,
            consultationId,
            specialiteId,
            dossierMedicalId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DemandeConsultationCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dateHeureDemande != null ? "dateHeureDemande=" + dateHeureDemande + ", " : "") +
            (ordre != null ? "ordre=" + ordre + ", " : "") +
            (statut != null ? "statut=" + statut + ", " : "") +
            (codeOtp != null ? "codeOtp=" + codeOtp + ", " : "") +
            (telephonePaiement != null ? "telephonePaiement=" + telephonePaiement + ", " : "") +
            (montant != null ? "montant=" + montant + ", " : "") +
            (consultationId != null ? "consultationId=" + consultationId + ", " : "") +
            (specialiteId != null ? "specialiteId=" + specialiteId + ", " : "") +
            (dossierMedicalId != null ? "dossierMedicalId=" + dossierMedicalId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
