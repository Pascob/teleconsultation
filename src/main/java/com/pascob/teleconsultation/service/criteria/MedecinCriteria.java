package com.pascob.teleconsultation.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.pascob.teleconsultation.domain.Medecin} entity. This class is used
 * in {@link com.pascob.teleconsultation.web.rest.MedecinResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /medecins?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MedecinCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter userId;

    private StringFilter matricule;

    private LongFilter dossierMedicalId;

    private LongFilter consultationId;

    private LongFilter ficheConsultationId;

    private LongFilter formationSanitaireId;

    private LongFilter specialiteId;

    private Boolean distinct;

    public MedecinCriteria() {}

    public MedecinCriteria(MedecinCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.matricule = other.matricule == null ? null : other.matricule.copy();
        this.dossierMedicalId = other.dossierMedicalId == null ? null : other.dossierMedicalId.copy();
        this.consultationId = other.consultationId == null ? null : other.consultationId.copy();
        this.ficheConsultationId = other.ficheConsultationId == null ? null : other.ficheConsultationId.copy();
        this.formationSanitaireId = other.formationSanitaireId == null ? null : other.formationSanitaireId.copy();
        this.specialiteId = other.specialiteId == null ? null : other.specialiteId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MedecinCriteria copy() {
        return new MedecinCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public StringFilter getMatricule() {
        return matricule;
    }

    public StringFilter matricule() {
        if (matricule == null) {
            matricule = new StringFilter();
        }
        return matricule;
    }

    public void setMatricule(StringFilter matricule) {
        this.matricule = matricule;
    }

    public LongFilter getDossierMedicalId() {
        return dossierMedicalId;
    }

    public LongFilter dossierMedicalId() {
        if (dossierMedicalId == null) {
            dossierMedicalId = new LongFilter();
        }
        return dossierMedicalId;
    }

    public void setDossierMedicalId(LongFilter dossierMedicalId) {
        this.dossierMedicalId = dossierMedicalId;
    }

    public LongFilter getConsultationId() {
        return consultationId;
    }

    public LongFilter consultationId() {
        if (consultationId == null) {
            consultationId = new LongFilter();
        }
        return consultationId;
    }

    public void setConsultationId(LongFilter consultationId) {
        this.consultationId = consultationId;
    }

    public LongFilter getFicheConsultationId() {
        return ficheConsultationId;
    }

    public LongFilter ficheConsultationId() {
        if (ficheConsultationId == null) {
            ficheConsultationId = new LongFilter();
        }
        return ficheConsultationId;
    }

    public void setFicheConsultationId(LongFilter ficheConsultationId) {
        this.ficheConsultationId = ficheConsultationId;
    }

    public LongFilter getFormationSanitaireId() {
        return formationSanitaireId;
    }

    public LongFilter formationSanitaireId() {
        if (formationSanitaireId == null) {
            formationSanitaireId = new LongFilter();
        }
        return formationSanitaireId;
    }

    public void setFormationSanitaireId(LongFilter formationSanitaireId) {
        this.formationSanitaireId = formationSanitaireId;
    }

    public LongFilter getSpecialiteId() {
        return specialiteId;
    }

    public LongFilter specialiteId() {
        if (specialiteId == null) {
            specialiteId = new LongFilter();
        }
        return specialiteId;
    }

    public void setSpecialiteId(LongFilter specialiteId) {
        this.specialiteId = specialiteId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MedecinCriteria that = (MedecinCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(matricule, that.matricule) &&
            Objects.equals(dossierMedicalId, that.dossierMedicalId) &&
            Objects.equals(consultationId, that.consultationId) &&
            Objects.equals(ficheConsultationId, that.ficheConsultationId) &&
            Objects.equals(formationSanitaireId, that.formationSanitaireId) &&
            Objects.equals(specialiteId, that.specialiteId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            userId,
            matricule,
            dossierMedicalId,
            consultationId,
            ficheConsultationId,
            formationSanitaireId,
            specialiteId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MedecinCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (matricule != null ? "matricule=" + matricule + ", " : "") +
            (dossierMedicalId != null ? "dossierMedicalId=" + dossierMedicalId + ", " : "") +
            (consultationId != null ? "consultationId=" + consultationId + ", " : "") +
            (ficheConsultationId != null ? "ficheConsultationId=" + ficheConsultationId + ", " : "") +
            (formationSanitaireId != null ? "formationSanitaireId=" + formationSanitaireId + ", " : "") +
            (specialiteId != null ? "specialiteId=" + specialiteId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
