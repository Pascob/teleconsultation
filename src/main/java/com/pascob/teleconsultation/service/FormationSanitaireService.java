package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.FormationSanitaire;
import com.pascob.teleconsultation.repository.FormationSanitaireRepository;
import com.pascob.teleconsultation.service.dto.FormationSanitaireDTO;
import com.pascob.teleconsultation.service.mapper.FormationSanitaireMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FormationSanitaire}.
 */
@Service
@Transactional
public class FormationSanitaireService {

    private final Logger log = LoggerFactory.getLogger(FormationSanitaireService.class);

    private final FormationSanitaireRepository formationSanitaireRepository;

    private final FormationSanitaireMapper formationSanitaireMapper;

    public FormationSanitaireService(
        FormationSanitaireRepository formationSanitaireRepository,
        FormationSanitaireMapper formationSanitaireMapper
    ) {
        this.formationSanitaireRepository = formationSanitaireRepository;
        this.formationSanitaireMapper = formationSanitaireMapper;
    }

    /**
     * Save a formationSanitaire.
     *
     * @param formationSanitaireDTO the entity to save.
     * @return the persisted entity.
     */
    public FormationSanitaireDTO save(FormationSanitaireDTO formationSanitaireDTO) {
        log.debug("Request to save FormationSanitaire : {}", formationSanitaireDTO);
        FormationSanitaire formationSanitaire = formationSanitaireMapper.toEntity(formationSanitaireDTO);
        formationSanitaire = formationSanitaireRepository.save(formationSanitaire);
        return formationSanitaireMapper.toDto(formationSanitaire);
    }

    /**
     * Update a formationSanitaire.
     *
     * @param formationSanitaireDTO the entity to save.
     * @return the persisted entity.
     */
    public FormationSanitaireDTO update(FormationSanitaireDTO formationSanitaireDTO) {
        log.debug("Request to update FormationSanitaire : {}", formationSanitaireDTO);
        FormationSanitaire formationSanitaire = formationSanitaireMapper.toEntity(formationSanitaireDTO);
        formationSanitaire = formationSanitaireRepository.save(formationSanitaire);
        return formationSanitaireMapper.toDto(formationSanitaire);
    }

    /**
     * Partially update a formationSanitaire.
     *
     * @param formationSanitaireDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<FormationSanitaireDTO> partialUpdate(FormationSanitaireDTO formationSanitaireDTO) {
        log.debug("Request to partially update FormationSanitaire : {}", formationSanitaireDTO);

        return formationSanitaireRepository
            .findById(formationSanitaireDTO.getId())
            .map(existingFormationSanitaire -> {
                formationSanitaireMapper.partialUpdate(existingFormationSanitaire, formationSanitaireDTO);

                return existingFormationSanitaire;
            })
            .map(formationSanitaireRepository::save)
            .map(formationSanitaireMapper::toDto);
    }

    /**
     * Get all the formationSanitaires.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FormationSanitaireDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FormationSanitaires");
        return formationSanitaireRepository.findAll(pageable).map(formationSanitaireMapper::toDto);
    }

    /**
     * Get one formationSanitaire by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FormationSanitaireDTO> findOne(Long id) {
        log.debug("Request to get FormationSanitaire : {}", id);
        return formationSanitaireRepository.findById(id).map(formationSanitaireMapper::toDto);
    }

    /**
     * Delete the formationSanitaire by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FormationSanitaire : {}", id);
        formationSanitaireRepository.deleteById(id);
    }
}
