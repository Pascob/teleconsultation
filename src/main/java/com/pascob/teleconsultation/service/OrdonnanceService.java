package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.Ordonnance;
import com.pascob.teleconsultation.repository.OrdonnanceRepository;
import com.pascob.teleconsultation.service.dto.OrdonnanceDTO;
import com.pascob.teleconsultation.service.mapper.OrdonnanceMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Ordonnance}.
 */
@Service
@Transactional
public class OrdonnanceService {

    private final Logger log = LoggerFactory.getLogger(OrdonnanceService.class);

    private final OrdonnanceRepository ordonnanceRepository;

    private final OrdonnanceMapper ordonnanceMapper;

    public OrdonnanceService(OrdonnanceRepository ordonnanceRepository, OrdonnanceMapper ordonnanceMapper) {
        this.ordonnanceRepository = ordonnanceRepository;
        this.ordonnanceMapper = ordonnanceMapper;
    }

    /**
     * Save a ordonnance.
     *
     * @param ordonnanceDTO the entity to save.
     * @return the persisted entity.
     */
    public OrdonnanceDTO save(OrdonnanceDTO ordonnanceDTO) {
        log.debug("Request to save Ordonnance : {}", ordonnanceDTO);
        Ordonnance ordonnance = ordonnanceMapper.toEntity(ordonnanceDTO);
        ordonnance = ordonnanceRepository.save(ordonnance);
        return ordonnanceMapper.toDto(ordonnance);
    }

    /**
     * Update a ordonnance.
     *
     * @param ordonnanceDTO the entity to save.
     * @return the persisted entity.
     */
    public OrdonnanceDTO update(OrdonnanceDTO ordonnanceDTO) {
        log.debug("Request to update Ordonnance : {}", ordonnanceDTO);
        Ordonnance ordonnance = ordonnanceMapper.toEntity(ordonnanceDTO);
        ordonnance = ordonnanceRepository.save(ordonnance);
        return ordonnanceMapper.toDto(ordonnance);
    }

    /**
     * Partially update a ordonnance.
     *
     * @param ordonnanceDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OrdonnanceDTO> partialUpdate(OrdonnanceDTO ordonnanceDTO) {
        log.debug("Request to partially update Ordonnance : {}", ordonnanceDTO);

        return ordonnanceRepository
            .findById(ordonnanceDTO.getId())
            .map(existingOrdonnance -> {
                ordonnanceMapper.partialUpdate(existingOrdonnance, ordonnanceDTO);

                return existingOrdonnance;
            })
            .map(ordonnanceRepository::save)
            .map(ordonnanceMapper::toDto);
    }

    /**
     * Get all the ordonnances.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrdonnanceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Ordonnances");
        return ordonnanceRepository.findAll(pageable).map(ordonnanceMapper::toDto);
    }

    /**
     * Get one ordonnance by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrdonnanceDTO> findOne(Long id) {
        log.debug("Request to get Ordonnance : {}", id);
        return ordonnanceRepository.findById(id).map(ordonnanceMapper::toDto);
    }

    /**
     * Delete the ordonnance by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Ordonnance : {}", id);
        ordonnanceRepository.deleteById(id);
    }
}
