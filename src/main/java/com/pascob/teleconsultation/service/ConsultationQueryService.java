package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.Consultation;
import com.pascob.teleconsultation.repository.ConsultationRepository;
import com.pascob.teleconsultation.service.criteria.ConsultationCriteria;
import com.pascob.teleconsultation.service.dto.ConsultationDTO;
import com.pascob.teleconsultation.service.mapper.ConsultationMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Consultation} entities in the database.
 * The main input is a {@link ConsultationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ConsultationDTO} or a {@link Page} of {@link ConsultationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ConsultationQueryService extends QueryService<Consultation> {

    private final Logger log = LoggerFactory.getLogger(ConsultationQueryService.class);

    private final ConsultationRepository consultationRepository;

    private final ConsultationMapper consultationMapper;

    public ConsultationQueryService(ConsultationRepository consultationRepository, ConsultationMapper consultationMapper) {
        this.consultationRepository = consultationRepository;
        this.consultationMapper = consultationMapper;
    }

    /**
     * Return a {@link List} of {@link ConsultationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConsultationDTO> findByCriteria(ConsultationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Consultation> specification = createSpecification(criteria);
        return consultationMapper.toDto(consultationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ConsultationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConsultationDTO> findByCriteria(ConsultationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Consultation> specification = createSpecification(criteria);
        return consultationRepository.findAll(specification, page).map(consultationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ConsultationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Consultation> specification = createSpecification(criteria);
        return consultationRepository.count(specification);
    }

    /**
     * Function to convert {@link ConsultationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Consultation> createSpecification(ConsultationCriteria criteria) {
        Specification<Consultation> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Consultation_.id));
            }
            if (criteria.getPoids() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPoids(), Consultation_.poids));
            }
            if (criteria.getTemperature() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTemperature(), Consultation_.temperature));
            }
            if (criteria.getPouls() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPouls(), Consultation_.pouls));
            }
            if (criteria.getBpm() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBpm(), Consultation_.bpm));
            }
            if (criteria.getAge() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAge(), Consultation_.age));
            }
            if (criteria.getTaille() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTaille(), Consultation_.taille));
            }
            if (criteria.getDateDebutConsulation() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getDateDebutConsulation(), Consultation_.dateDebutConsulation));
            }
            if (criteria.getIsSuivi() != null) {
                specification = specification.and(buildSpecification(criteria.getIsSuivi(), Consultation_.isSuivi));
            }
            if (criteria.getIsClose() != null) {
                specification = specification.and(buildSpecification(criteria.getIsClose(), Consultation_.isClose));
            }
            if (criteria.getFicheConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFicheConsultationId(),
                            root -> root.join(Consultation_.ficheConsultations, JoinType.LEFT).get(FicheConsultation_.id)
                        )
                    );
            }
            if (criteria.getMedecinId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMedecinId(),
                            root -> root.join(Consultation_.medecin, JoinType.LEFT).get(Medecin_.id)
                        )
                    );
            }
            if (criteria.getDemandeConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDemandeConsultationId(),
                            root -> root.join(Consultation_.demandeConsultation, JoinType.LEFT).get(DemandeConsultation_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
