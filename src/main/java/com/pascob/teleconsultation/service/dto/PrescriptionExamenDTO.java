package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.PrescriptionExamen} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PrescriptionExamenDTO implements Serializable {

    private Long id;

    private String description;

    private String resultat;

    private BulletinDTO bulletin;

    private ExamenMedicalDTO examenMedical;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResultat() {
        return resultat;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public BulletinDTO getBulletin() {
        return bulletin;
    }

    public void setBulletin(BulletinDTO bulletin) {
        this.bulletin = bulletin;
    }

    public ExamenMedicalDTO getExamenMedical() {
        return examenMedical;
    }

    public void setExamenMedical(ExamenMedicalDTO examenMedical) {
        this.examenMedical = examenMedical;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PrescriptionExamenDTO)) {
            return false;
        }

        PrescriptionExamenDTO prescriptionExamenDTO = (PrescriptionExamenDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, prescriptionExamenDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PrescriptionExamenDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", resultat='" + getResultat() + "'" +
            ", bulletin=" + getBulletin() +
            ", examenMedical=" + getExamenMedical() +
            "}";
    }
}
