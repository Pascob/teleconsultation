package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.Specialite} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SpecialiteDTO implements Serializable {

    private Long id;

    @NotNull
    private String designation;

    @DecimalMin(value = "0")
    private Double frais;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Double getFrais() {
        return frais;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpecialiteDTO)) {
            return false;
        }

        SpecialiteDTO specialiteDTO = (SpecialiteDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, specialiteDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SpecialiteDTO{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", frais=" + getFrais() +
            "}";
    }
}
