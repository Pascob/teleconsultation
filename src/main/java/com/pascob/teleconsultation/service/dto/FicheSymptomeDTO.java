package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.FicheSymptome} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FicheSymptomeDTO implements Serializable {

    private Long id;

    private String observation;

    private FicheConsultationDTO ficheConsultation;

    private SymptomeDTO symptome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public FicheConsultationDTO getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultationDTO ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }

    public SymptomeDTO getSymptome() {
        return symptome;
    }

    public void setSymptome(SymptomeDTO symptome) {
        this.symptome = symptome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FicheSymptomeDTO)) {
            return false;
        }

        FicheSymptomeDTO ficheSymptomeDTO = (FicheSymptomeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, ficheSymptomeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FicheSymptomeDTO{" +
            "id=" + getId() +
            ", observation='" + getObservation() + "'" +
            ", ficheConsultation=" + getFicheConsultation() +
            ", symptome=" + getSymptome() +
            "}";
    }
}
