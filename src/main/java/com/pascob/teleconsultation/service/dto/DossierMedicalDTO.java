package com.pascob.teleconsultation.service.dto;

import com.pascob.teleconsultation.domain.enumeration.GroupeSanguin;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.DossierMedical} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DossierMedicalDTO implements Serializable {

    private Long id;

    @NotNull
    private String numero;

    private GroupeSanguin groupeSanguin;

    private MedecinDTO medecin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public GroupeSanguin getGroupeSanguin() {
        return groupeSanguin;
    }

    public void setGroupeSanguin(GroupeSanguin groupeSanguin) {
        this.groupeSanguin = groupeSanguin;
    }

    public MedecinDTO getMedecin() {
        return medecin;
    }

    public void setMedecin(MedecinDTO medecin) {
        this.medecin = medecin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DossierMedicalDTO)) {
            return false;
        }

        DossierMedicalDTO dossierMedicalDTO = (DossierMedicalDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, dossierMedicalDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DossierMedicalDTO{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", groupeSanguin='" + getGroupeSanguin() + "'" +
            ", medecin=" + getMedecin() +
            "}";
    }
}
