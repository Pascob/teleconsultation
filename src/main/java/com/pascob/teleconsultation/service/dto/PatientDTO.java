package com.pascob.teleconsultation.service.dto;

import com.pascob.teleconsultation.domain.enumeration.Genre;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.Patient} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PatientDTO implements Serializable {

    private Long id;

    @NotNull
    private Long userId;

    private LocalDate dateNaissance;

    private Genre genre;

    private DossierMedicalDTO dossierMedical;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public DossierMedicalDTO getDossierMedical() {
        return dossierMedical;
    }

    public void setDossierMedical(DossierMedicalDTO dossierMedical) {
        this.dossierMedical = dossierMedical;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientDTO)) {
            return false;
        }

        PatientDTO patientDTO = (PatientDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, patientDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public String toString() {
        return (
            "PatientDTO{" +
            "id=" +
            getId() +
            ", userId=" +
            getUserId() +
            ", dateNaissance='" +
            getDateNaissance() +
            "'" +
            ", genre='" +
            getGenre() +
            "'" +
            ", dossierMedical=" +
            getDossierMedical() +
            "}"
        );
    }
}
