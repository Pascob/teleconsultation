package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.Consultation} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ConsultationDTO implements Serializable {

    private Long id;

    @Min(value = 0)
    private Integer poids;

    @DecimalMin(value = "0")
    private Float temperature;

    @Min(value = 0)
    private Integer pouls;

    @Min(value = 0)
    private Integer bpm;

    @DecimalMin(value = "0")
    private Float age;

    @Min(value = 0)
    private Integer taille;

    private ZonedDateTime dateDebutConsulation;

    private Boolean isSuivi;

    private Boolean isClose;

    private MedecinDTO medecin;

    private DemandeConsultationDTO demandeConsultation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPoids() {
        return poids;
    }

    public void setPoids(Integer poids) {
        this.poids = poids;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Integer getPouls() {
        return pouls;
    }

    public void setPouls(Integer pouls) {
        this.pouls = pouls;
    }

    public Integer getBpm() {
        return bpm;
    }

    public void setBpm(Integer bpm) {
        this.bpm = bpm;
    }

    public Float getAge() {
        return age;
    }

    public void setAge(Float age) {
        this.age = age;
    }

    public Integer getTaille() {
        return taille;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public ZonedDateTime getDateDebutConsulation() {
        return dateDebutConsulation;
    }

    public void setDateDebutConsulation(ZonedDateTime dateDebutConsulation) {
        this.dateDebutConsulation = dateDebutConsulation;
    }

    public Boolean getIsSuivi() {
        return isSuivi;
    }

    public void setIsSuivi(Boolean isSuivi) {
        this.isSuivi = isSuivi;
    }

    public Boolean getIsClose() {
        return isClose;
    }

    public void setIsClose(Boolean isClose) {
        this.isClose = isClose;
    }

    public MedecinDTO getMedecin() {
        return medecin;
    }

    public void setMedecin(MedecinDTO medecin) {
        this.medecin = medecin;
    }

    public DemandeConsultationDTO getDemandeConsultation() {
        return demandeConsultation;
    }

    public void setDemandeConsultation(DemandeConsultationDTO demandeConsultation) {
        this.demandeConsultation = demandeConsultation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ConsultationDTO)) {
            return false;
        }

        ConsultationDTO consultationDTO = (ConsultationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, consultationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ConsultationDTO{" +
            "id=" + getId() +
            ", poids=" + getPoids() +
            ", temperature=" + getTemperature() +
            ", pouls=" + getPouls() +
            ", bpm=" + getBpm() +
            ", age=" + getAge() +
            ", taille=" + getTaille() +
            ", dateDebutConsulation='" + getDateDebutConsulation() + "'" +
            ", isSuivi='" + getIsSuivi() + "'" +
            ", isClose='" + getIsClose() + "'" +
            ", medecin=" + getMedecin() +
            ", demandeConsultation=" + getDemandeConsultation() +
            "}";
    }
}
