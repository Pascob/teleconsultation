package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.Medecin} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MedecinDTO implements Serializable {

    private Long id;

    @NotNull
    private Long userId;

    private String matricule;

    @Lob
    private byte[] signature;

    private String signatureContentType;
    private FormationSanitaireDTO formationSanitaire;

    private SpecialiteDTO specialite;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public String getSignatureContentType() {
        return signatureContentType;
    }

    public void setSignatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
    }

    public FormationSanitaireDTO getFormationSanitaire() {
        return formationSanitaire;
    }

    public void setFormationSanitaire(FormationSanitaireDTO formationSanitaire) {
        this.formationSanitaire = formationSanitaire;
    }

    public SpecialiteDTO getSpecialite() {
        return specialite;
    }

    public void setSpecialite(SpecialiteDTO specialite) {
        this.specialite = specialite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MedecinDTO)) {
            return false;
        }

        MedecinDTO medecinDTO = (MedecinDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, medecinDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MedecinDTO{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", matricule='" + getMatricule() + "'" +
            ", signature='" + getSignature() + "'" +
            ", formationSanitaire=" + getFormationSanitaire() +
            ", specialite=" + getSpecialite() +
            "}";
    }
}
