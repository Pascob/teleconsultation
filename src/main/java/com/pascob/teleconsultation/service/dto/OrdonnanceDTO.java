package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.Ordonnance} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class OrdonnanceDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate dateOrdonnance;

    private String numero;

    private FicheConsultationDTO ficheConsultation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateOrdonnance() {
        return dateOrdonnance;
    }

    public void setDateOrdonnance(LocalDate dateOrdonnance) {
        this.dateOrdonnance = dateOrdonnance;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public FicheConsultationDTO getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultationDTO ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrdonnanceDTO)) {
            return false;
        }

        OrdonnanceDTO ordonnanceDTO = (OrdonnanceDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, ordonnanceDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdonnanceDTO{" +
            "id=" + getId() +
            ", dateOrdonnance='" + getDateOrdonnance() + "'" +
            ", numero='" + getNumero() + "'" +
            ", ficheConsultation=" + getFicheConsultation() +
            "}";
    }
}
