package com.pascob.teleconsultation.service.dto;

import com.pascob.teleconsultation.domain.enumeration.StatutDemandeConsultation;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.DemandeConsultation} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DemandeConsultationDTO implements Serializable {

    private Long id;

    private ZonedDateTime dateHeureDemande;

    private Integer ordre;

    private StatutDemandeConsultation statut;

    @NotNull
    private String codeOtp;

    @NotNull
    private String telephonePaiement;

    @DecimalMin(value = "0")
    private Double montant;

    @NotNull
    private SpecialiteDTO specialite;

    private DossierMedicalDTO dossierMedical;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateHeureDemande() {
        return dateHeureDemande;
    }

    public void setDateHeureDemande(ZonedDateTime dateHeureDemande) {
        this.dateHeureDemande = dateHeureDemande;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public StatutDemandeConsultation getStatut() {
        return statut;
    }

    public void setStatut(StatutDemandeConsultation statut) {
        this.statut = statut;
    }

    public String getCodeOtp() {
        return codeOtp;
    }

    public void setCodeOtp(String codeOtp) {
        this.codeOtp = codeOtp;
    }

    public String getTelephonePaiement() {
        return telephonePaiement;
    }

    public void setTelephonePaiement(String telephonePaiement) {
        this.telephonePaiement = telephonePaiement;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public SpecialiteDTO getSpecialite() {
        return specialite;
    }

    public void setSpecialite(SpecialiteDTO specialite) {
        this.specialite = specialite;
    }

    public DossierMedicalDTO getDossierMedical() {
        return dossierMedical;
    }

    public void setDossierMedical(DossierMedicalDTO dossierMedical) {
        this.dossierMedical = dossierMedical;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DemandeConsultationDTO)) {
            return false;
        }

        DemandeConsultationDTO demandeConsultationDTO = (DemandeConsultationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, demandeConsultationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DemandeConsultationDTO{" +
            "id=" + getId() +
            ", dateHeureDemande='" + getDateHeureDemande() + "'" +
            ", ordre=" + getOrdre() +
            ", statut='" + getStatut() + "'" +
            ", codeOtp='" + getCodeOtp() + "'" +
            ", telephonePaiement='" + getTelephonePaiement() + "'" +
            ", montant=" + getMontant() +
            ", specialite=" + getSpecialite() +
            ", dossierMedical=" + getDossierMedical() +
            "}";
    }
}
