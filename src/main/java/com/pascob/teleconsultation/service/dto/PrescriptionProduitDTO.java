package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.PrescriptionProduit} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PrescriptionProduitDTO implements Serializable {

    private Long id;

    private String posologie;

    private String observation;

    private ProduitDTO produit;

    private OrdonnanceDTO ordonnance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPosologie() {
        return posologie;
    }

    public void setPosologie(String posologie) {
        this.posologie = posologie;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public ProduitDTO getProduit() {
        return produit;
    }

    public void setProduit(ProduitDTO produit) {
        this.produit = produit;
    }

    public OrdonnanceDTO getOrdonnance() {
        return ordonnance;
    }

    public void setOrdonnance(OrdonnanceDTO ordonnance) {
        this.ordonnance = ordonnance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PrescriptionProduitDTO)) {
            return false;
        }

        PrescriptionProduitDTO prescriptionProduitDTO = (PrescriptionProduitDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, prescriptionProduitDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PrescriptionProduitDTO{" +
            "id=" + getId() +
            ", posologie='" + getPosologie() + "'" +
            ", observation='" + getObservation() + "'" +
            ", produit=" + getProduit() +
            ", ordonnance=" + getOrdonnance() +
            "}";
    }
}
