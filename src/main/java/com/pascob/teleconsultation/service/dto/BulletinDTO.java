package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.Bulletin} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class BulletinDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate dateBulletin;

    private String numero;

    private FicheConsultationDTO ficheConsultation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateBulletin() {
        return dateBulletin;
    }

    public void setDateBulletin(LocalDate dateBulletin) {
        this.dateBulletin = dateBulletin;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public FicheConsultationDTO getFicheConsultation() {
        return ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultationDTO ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BulletinDTO)) {
            return false;
        }

        BulletinDTO bulletinDTO = (BulletinDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, bulletinDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BulletinDTO{" +
            "id=" + getId() +
            ", dateBulletin='" + getDateBulletin() + "'" +
            ", numero='" + getNumero() + "'" +
            ", ficheConsultation=" + getFicheConsultation() +
            "}";
    }
}
