package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.ExamenMedical} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExamenMedicalDTO implements Serializable {

    private Long id;

    @NotNull
    private String designation;

    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExamenMedicalDTO)) {
            return false;
        }

        ExamenMedicalDTO examenMedicalDTO = (ExamenMedicalDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, examenMedicalDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExamenMedicalDTO{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
