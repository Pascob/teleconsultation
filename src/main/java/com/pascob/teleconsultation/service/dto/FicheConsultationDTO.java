package com.pascob.teleconsultation.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.pascob.teleconsultation.domain.FicheConsultation} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FicheConsultationDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate dateFiche;

    private String numero;

    private String observation;

    private String diagnostic;

    private MedecinDTO medecin;

    private ConsultationDTO consultation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateFiche() {
        return dateFiche;
    }

    public void setDateFiche(LocalDate dateFiche) {
        this.dateFiche = dateFiche;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public MedecinDTO getMedecin() {
        return medecin;
    }

    public void setMedecin(MedecinDTO medecin) {
        this.medecin = medecin;
    }

    public ConsultationDTO getConsultation() {
        return consultation;
    }

    public void setConsultation(ConsultationDTO consultation) {
        this.consultation = consultation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FicheConsultationDTO)) {
            return false;
        }

        FicheConsultationDTO ficheConsultationDTO = (FicheConsultationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, ficheConsultationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FicheConsultationDTO{" +
            "id=" + getId() +
            ", dateFiche='" + getDateFiche() + "'" +
            ", numero='" + getNumero() + "'" +
            ", observation='" + getObservation() + "'" +
            ", diagnostic='" + getDiagnostic() + "'" +
            ", medecin=" + getMedecin() +
            ", consultation=" + getConsultation() +
            "}";
    }
}
