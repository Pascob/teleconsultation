package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.PrescriptionProduit;
import com.pascob.teleconsultation.repository.PrescriptionProduitRepository;
import com.pascob.teleconsultation.service.criteria.PrescriptionProduitCriteria;
import com.pascob.teleconsultation.service.dto.PrescriptionProduitDTO;
import com.pascob.teleconsultation.service.mapper.PrescriptionProduitMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PrescriptionProduit} entities in the database.
 * The main input is a {@link PrescriptionProduitCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PrescriptionProduitDTO} or a {@link Page} of {@link PrescriptionProduitDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PrescriptionProduitQueryService extends QueryService<PrescriptionProduit> {

    private final Logger log = LoggerFactory.getLogger(PrescriptionProduitQueryService.class);

    private final PrescriptionProduitRepository prescriptionProduitRepository;

    private final PrescriptionProduitMapper prescriptionProduitMapper;

    public PrescriptionProduitQueryService(
        PrescriptionProduitRepository prescriptionProduitRepository,
        PrescriptionProduitMapper prescriptionProduitMapper
    ) {
        this.prescriptionProduitRepository = prescriptionProduitRepository;
        this.prescriptionProduitMapper = prescriptionProduitMapper;
    }

    /**
     * Return a {@link List} of {@link PrescriptionProduitDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PrescriptionProduitDTO> findByCriteria(PrescriptionProduitCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PrescriptionProduit> specification = createSpecification(criteria);
        return prescriptionProduitMapper.toDto(prescriptionProduitRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PrescriptionProduitDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PrescriptionProduitDTO> findByCriteria(PrescriptionProduitCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PrescriptionProduit> specification = createSpecification(criteria);
        return prescriptionProduitRepository.findAll(specification, page).map(prescriptionProduitMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PrescriptionProduitCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PrescriptionProduit> specification = createSpecification(criteria);
        return prescriptionProduitRepository.count(specification);
    }

    /**
     * Function to convert {@link PrescriptionProduitCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PrescriptionProduit> createSpecification(PrescriptionProduitCriteria criteria) {
        Specification<PrescriptionProduit> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PrescriptionProduit_.id));
            }
            if (criteria.getPosologie() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPosologie(), PrescriptionProduit_.posologie));
            }
            if (criteria.getObservation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getObservation(), PrescriptionProduit_.observation));
            }
            if (criteria.getProduitId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getProduitId(),
                            root -> root.join(PrescriptionProduit_.produit, JoinType.LEFT).get(Produit_.id)
                        )
                    );
            }
            if (criteria.getOrdonnanceId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getOrdonnanceId(),
                            root -> root.join(PrescriptionProduit_.ordonnance, JoinType.LEFT).get(Ordonnance_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
