package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.Symptome;
import com.pascob.teleconsultation.repository.SymptomeRepository;
import com.pascob.teleconsultation.service.dto.SymptomeDTO;
import com.pascob.teleconsultation.service.mapper.SymptomeMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Symptome}.
 */
@Service
@Transactional
public class SymptomeService {

    private final Logger log = LoggerFactory.getLogger(SymptomeService.class);

    private final SymptomeRepository symptomeRepository;

    private final SymptomeMapper symptomeMapper;

    public SymptomeService(SymptomeRepository symptomeRepository, SymptomeMapper symptomeMapper) {
        this.symptomeRepository = symptomeRepository;
        this.symptomeMapper = symptomeMapper;
    }

    /**
     * Save a symptome.
     *
     * @param symptomeDTO the entity to save.
     * @return the persisted entity.
     */
    public SymptomeDTO save(SymptomeDTO symptomeDTO) {
        log.debug("Request to save Symptome : {}", symptomeDTO);
        Symptome symptome = symptomeMapper.toEntity(symptomeDTO);
        symptome = symptomeRepository.save(symptome);
        return symptomeMapper.toDto(symptome);
    }

    /**
     * Update a symptome.
     *
     * @param symptomeDTO the entity to save.
     * @return the persisted entity.
     */
    public SymptomeDTO update(SymptomeDTO symptomeDTO) {
        log.debug("Request to update Symptome : {}", symptomeDTO);
        Symptome symptome = symptomeMapper.toEntity(symptomeDTO);
        symptome = symptomeRepository.save(symptome);
        return symptomeMapper.toDto(symptome);
    }

    /**
     * Partially update a symptome.
     *
     * @param symptomeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SymptomeDTO> partialUpdate(SymptomeDTO symptomeDTO) {
        log.debug("Request to partially update Symptome : {}", symptomeDTO);

        return symptomeRepository
            .findById(symptomeDTO.getId())
            .map(existingSymptome -> {
                symptomeMapper.partialUpdate(existingSymptome, symptomeDTO);

                return existingSymptome;
            })
            .map(symptomeRepository::save)
            .map(symptomeMapper::toDto);
    }

    /**
     * Get all the symptomes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SymptomeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Symptomes");
        return symptomeRepository.findAll(pageable).map(symptomeMapper::toDto);
    }

    /**
     * Get one symptome by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SymptomeDTO> findOne(Long id) {
        log.debug("Request to get Symptome : {}", id);
        return symptomeRepository.findById(id).map(symptomeMapper::toDto);
    }

    /**
     * Delete the symptome by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Symptome : {}", id);
        symptomeRepository.deleteById(id);
    }
}
