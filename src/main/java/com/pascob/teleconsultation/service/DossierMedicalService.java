package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.repository.DossierMedicalRepository;
import com.pascob.teleconsultation.service.dto.DossierMedicalDTO;
import com.pascob.teleconsultation.service.mapper.DossierMedicalMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DossierMedical}.
 */
@Service
@Transactional
public class DossierMedicalService {

    private final Logger log = LoggerFactory.getLogger(DossierMedicalService.class);

    private final DossierMedicalRepository dossierMedicalRepository;

    private final DossierMedicalMapper dossierMedicalMapper;

    public DossierMedicalService(DossierMedicalRepository dossierMedicalRepository, DossierMedicalMapper dossierMedicalMapper) {
        this.dossierMedicalRepository = dossierMedicalRepository;
        this.dossierMedicalMapper = dossierMedicalMapper;
    }

    /**
     * Save a dossierMedical.
     *
     * @param dossierMedicalDTO the entity to save.
     * @return the persisted entity.
     */
    public DossierMedicalDTO save(DossierMedicalDTO dossierMedicalDTO) {
        log.debug("Request to save DossierMedical : {}", dossierMedicalDTO);
        DossierMedical dossierMedical = dossierMedicalMapper.toEntity(dossierMedicalDTO);
        dossierMedical = dossierMedicalRepository.save(dossierMedical);
        return dossierMedicalMapper.toDto(dossierMedical);
    }

    /**
     * Update a dossierMedical.
     *
     * @param dossierMedicalDTO the entity to save.
     * @return the persisted entity.
     */
    public DossierMedicalDTO update(DossierMedicalDTO dossierMedicalDTO) {
        log.debug("Request to update DossierMedical : {}", dossierMedicalDTO);
        DossierMedical dossierMedical = dossierMedicalMapper.toEntity(dossierMedicalDTO);
        dossierMedical = dossierMedicalRepository.save(dossierMedical);
        return dossierMedicalMapper.toDto(dossierMedical);
    }

    /**
     * Partially update a dossierMedical.
     *
     * @param dossierMedicalDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DossierMedicalDTO> partialUpdate(DossierMedicalDTO dossierMedicalDTO) {
        log.debug("Request to partially update DossierMedical : {}", dossierMedicalDTO);

        return dossierMedicalRepository
            .findById(dossierMedicalDTO.getId())
            .map(existingDossierMedical -> {
                dossierMedicalMapper.partialUpdate(existingDossierMedical, dossierMedicalDTO);

                return existingDossierMedical;
            })
            .map(dossierMedicalRepository::save)
            .map(dossierMedicalMapper::toDto);
    }

    /**
     * Get all the dossierMedicals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DossierMedicalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DossierMedicals");
        return dossierMedicalRepository.findAll(pageable).map(dossierMedicalMapper::toDto);
    }

    /**
     *  Get all the dossierMedicals where Patient is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DossierMedicalDTO> findAllWherePatientIsNull() {
        log.debug("Request to get all dossierMedicals where Patient is null");
        return StreamSupport
            .stream(dossierMedicalRepository.findAll().spliterator(), false)
            .filter(dossierMedical -> dossierMedical.getPatient() == null)
            .map(dossierMedicalMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one dossierMedical by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DossierMedicalDTO> findOne(Long id) {
        log.debug("Request to get DossierMedical : {}", id);
        return dossierMedicalRepository.findById(id).map(dossierMedicalMapper::toDto);
    }

    /**
     * Delete the dossierMedical by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DossierMedical : {}", id);
        dossierMedicalRepository.deleteById(id);
    }
}
