package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.FicheSymptome;
import com.pascob.teleconsultation.repository.FicheSymptomeRepository;
import com.pascob.teleconsultation.service.dto.FicheSymptomeDTO;
import com.pascob.teleconsultation.service.mapper.FicheSymptomeMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FicheSymptome}.
 */
@Service
@Transactional
public class FicheSymptomeService {

    private final Logger log = LoggerFactory.getLogger(FicheSymptomeService.class);

    private final FicheSymptomeRepository ficheSymptomeRepository;

    private final FicheSymptomeMapper ficheSymptomeMapper;

    public FicheSymptomeService(FicheSymptomeRepository ficheSymptomeRepository, FicheSymptomeMapper ficheSymptomeMapper) {
        this.ficheSymptomeRepository = ficheSymptomeRepository;
        this.ficheSymptomeMapper = ficheSymptomeMapper;
    }

    /**
     * Save a ficheSymptome.
     *
     * @param ficheSymptomeDTO the entity to save.
     * @return the persisted entity.
     */
    public FicheSymptomeDTO save(FicheSymptomeDTO ficheSymptomeDTO) {
        log.debug("Request to save FicheSymptome : {}", ficheSymptomeDTO);
        FicheSymptome ficheSymptome = ficheSymptomeMapper.toEntity(ficheSymptomeDTO);
        ficheSymptome = ficheSymptomeRepository.save(ficheSymptome);
        return ficheSymptomeMapper.toDto(ficheSymptome);
    }

    /**
     * Update a ficheSymptome.
     *
     * @param ficheSymptomeDTO the entity to save.
     * @return the persisted entity.
     */
    public FicheSymptomeDTO update(FicheSymptomeDTO ficheSymptomeDTO) {
        log.debug("Request to update FicheSymptome : {}", ficheSymptomeDTO);
        FicheSymptome ficheSymptome = ficheSymptomeMapper.toEntity(ficheSymptomeDTO);
        ficheSymptome = ficheSymptomeRepository.save(ficheSymptome);
        return ficheSymptomeMapper.toDto(ficheSymptome);
    }

    /**
     * Partially update a ficheSymptome.
     *
     * @param ficheSymptomeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<FicheSymptomeDTO> partialUpdate(FicheSymptomeDTO ficheSymptomeDTO) {
        log.debug("Request to partially update FicheSymptome : {}", ficheSymptomeDTO);

        return ficheSymptomeRepository
            .findById(ficheSymptomeDTO.getId())
            .map(existingFicheSymptome -> {
                ficheSymptomeMapper.partialUpdate(existingFicheSymptome, ficheSymptomeDTO);

                return existingFicheSymptome;
            })
            .map(ficheSymptomeRepository::save)
            .map(ficheSymptomeMapper::toDto);
    }

    /**
     * Get all the ficheSymptomes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FicheSymptomeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all FicheSymptomes");
        return ficheSymptomeRepository.findAll(pageable).map(ficheSymptomeMapper::toDto);
    }

    /**
     * Get one ficheSymptome by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FicheSymptomeDTO> findOne(Long id) {
        log.debug("Request to get FicheSymptome : {}", id);
        return ficheSymptomeRepository.findById(id).map(ficheSymptomeMapper::toDto);
    }

    /**
     * Delete the ficheSymptome by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FicheSymptome : {}", id);
        ficheSymptomeRepository.deleteById(id);
    }
}
