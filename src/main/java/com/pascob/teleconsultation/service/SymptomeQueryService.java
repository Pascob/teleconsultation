package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.Symptome;
import com.pascob.teleconsultation.repository.SymptomeRepository;
import com.pascob.teleconsultation.service.criteria.SymptomeCriteria;
import com.pascob.teleconsultation.service.dto.SymptomeDTO;
import com.pascob.teleconsultation.service.mapper.SymptomeMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Symptome} entities in the database.
 * The main input is a {@link SymptomeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SymptomeDTO} or a {@link Page} of {@link SymptomeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SymptomeQueryService extends QueryService<Symptome> {

    private final Logger log = LoggerFactory.getLogger(SymptomeQueryService.class);

    private final SymptomeRepository symptomeRepository;

    private final SymptomeMapper symptomeMapper;

    public SymptomeQueryService(SymptomeRepository symptomeRepository, SymptomeMapper symptomeMapper) {
        this.symptomeRepository = symptomeRepository;
        this.symptomeMapper = symptomeMapper;
    }

    /**
     * Return a {@link List} of {@link SymptomeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SymptomeDTO> findByCriteria(SymptomeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Symptome> specification = createSpecification(criteria);
        return symptomeMapper.toDto(symptomeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SymptomeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SymptomeDTO> findByCriteria(SymptomeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Symptome> specification = createSpecification(criteria);
        return symptomeRepository.findAll(specification, page).map(symptomeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SymptomeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Symptome> specification = createSpecification(criteria);
        return symptomeRepository.count(specification);
    }

    /**
     * Function to convert {@link SymptomeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Symptome> createSpecification(SymptomeCriteria criteria) {
        Specification<Symptome> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Symptome_.id));
            }
            if (criteria.getDesignation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesignation(), Symptome_.designation));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Symptome_.code));
            }
            if (criteria.getFicheSymptomeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFicheSymptomeId(),
                            root -> root.join(Symptome_.ficheSymptomes, JoinType.LEFT).get(FicheSymptome_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
