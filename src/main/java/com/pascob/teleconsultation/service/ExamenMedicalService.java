package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.ExamenMedical;
import com.pascob.teleconsultation.repository.ExamenMedicalRepository;
import com.pascob.teleconsultation.service.dto.ExamenMedicalDTO;
import com.pascob.teleconsultation.service.mapper.ExamenMedicalMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ExamenMedical}.
 */
@Service
@Transactional
public class ExamenMedicalService {

    private final Logger log = LoggerFactory.getLogger(ExamenMedicalService.class);

    private final ExamenMedicalRepository examenMedicalRepository;

    private final ExamenMedicalMapper examenMedicalMapper;

    public ExamenMedicalService(ExamenMedicalRepository examenMedicalRepository, ExamenMedicalMapper examenMedicalMapper) {
        this.examenMedicalRepository = examenMedicalRepository;
        this.examenMedicalMapper = examenMedicalMapper;
    }

    /**
     * Save a examenMedical.
     *
     * @param examenMedicalDTO the entity to save.
     * @return the persisted entity.
     */
    public ExamenMedicalDTO save(ExamenMedicalDTO examenMedicalDTO) {
        log.debug("Request to save ExamenMedical : {}", examenMedicalDTO);
        ExamenMedical examenMedical = examenMedicalMapper.toEntity(examenMedicalDTO);
        examenMedical = examenMedicalRepository.save(examenMedical);
        return examenMedicalMapper.toDto(examenMedical);
    }

    /**
     * Update a examenMedical.
     *
     * @param examenMedicalDTO the entity to save.
     * @return the persisted entity.
     */
    public ExamenMedicalDTO update(ExamenMedicalDTO examenMedicalDTO) {
        log.debug("Request to update ExamenMedical : {}", examenMedicalDTO);
        ExamenMedical examenMedical = examenMedicalMapper.toEntity(examenMedicalDTO);
        examenMedical = examenMedicalRepository.save(examenMedical);
        return examenMedicalMapper.toDto(examenMedical);
    }

    /**
     * Partially update a examenMedical.
     *
     * @param examenMedicalDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ExamenMedicalDTO> partialUpdate(ExamenMedicalDTO examenMedicalDTO) {
        log.debug("Request to partially update ExamenMedical : {}", examenMedicalDTO);

        return examenMedicalRepository
            .findById(examenMedicalDTO.getId())
            .map(existingExamenMedical -> {
                examenMedicalMapper.partialUpdate(existingExamenMedical, examenMedicalDTO);

                return existingExamenMedical;
            })
            .map(examenMedicalRepository::save)
            .map(examenMedicalMapper::toDto);
    }

    /**
     * Get all the examenMedicals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExamenMedicalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExamenMedicals");
        return examenMedicalRepository.findAll(pageable).map(examenMedicalMapper::toDto);
    }

    /**
     * Get one examenMedical by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExamenMedicalDTO> findOne(Long id) {
        log.debug("Request to get ExamenMedical : {}", id);
        return examenMedicalRepository.findById(id).map(examenMedicalMapper::toDto);
    }

    /**
     * Delete the examenMedical by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExamenMedical : {}", id);
        examenMedicalRepository.deleteById(id);
    }
}
