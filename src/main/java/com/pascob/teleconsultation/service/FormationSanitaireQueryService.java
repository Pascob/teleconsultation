package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.FormationSanitaire;
import com.pascob.teleconsultation.repository.FormationSanitaireRepository;
import com.pascob.teleconsultation.service.criteria.FormationSanitaireCriteria;
import com.pascob.teleconsultation.service.dto.FormationSanitaireDTO;
import com.pascob.teleconsultation.service.mapper.FormationSanitaireMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link FormationSanitaire} entities in the database.
 * The main input is a {@link FormationSanitaireCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FormationSanitaireDTO} or a {@link Page} of {@link FormationSanitaireDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FormationSanitaireQueryService extends QueryService<FormationSanitaire> {

    private final Logger log = LoggerFactory.getLogger(FormationSanitaireQueryService.class);

    private final FormationSanitaireRepository formationSanitaireRepository;

    private final FormationSanitaireMapper formationSanitaireMapper;

    public FormationSanitaireQueryService(
        FormationSanitaireRepository formationSanitaireRepository,
        FormationSanitaireMapper formationSanitaireMapper
    ) {
        this.formationSanitaireRepository = formationSanitaireRepository;
        this.formationSanitaireMapper = formationSanitaireMapper;
    }

    /**
     * Return a {@link List} of {@link FormationSanitaireDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FormationSanitaireDTO> findByCriteria(FormationSanitaireCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FormationSanitaire> specification = createSpecification(criteria);
        return formationSanitaireMapper.toDto(formationSanitaireRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link FormationSanitaireDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FormationSanitaireDTO> findByCriteria(FormationSanitaireCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FormationSanitaire> specification = createSpecification(criteria);
        return formationSanitaireRepository.findAll(specification, page).map(formationSanitaireMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FormationSanitaireCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FormationSanitaire> specification = createSpecification(criteria);
        return formationSanitaireRepository.count(specification);
    }

    /**
     * Function to convert {@link FormationSanitaireCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FormationSanitaire> createSpecification(FormationSanitaireCriteria criteria) {
        Specification<FormationSanitaire> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), FormationSanitaire_.id));
            }
            if (criteria.getDesignation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesignation(), FormationSanitaire_.designation));
            }
            if (criteria.getSigle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSigle(), FormationSanitaire_.sigle));
            }
            if (criteria.getTelephone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelephone(), FormationSanitaire_.telephone));
            }
            if (criteria.getFixe() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFixe(), FormationSanitaire_.fixe));
            }
            if (criteria.getBp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBp(), FormationSanitaire_.bp));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), FormationSanitaire_.email));
            }
            if (criteria.getMedecinId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMedecinId(),
                            root -> root.join(FormationSanitaire_.medecins, JoinType.LEFT).get(Medecin_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
