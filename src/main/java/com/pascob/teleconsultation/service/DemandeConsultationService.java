package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.config.Constants;
import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.domain.User;
import com.pascob.teleconsultation.domain.enumeration.StatutDemandeConsultation;
import com.pascob.teleconsultation.repository.DemandeConsultationRepository;
import com.pascob.teleconsultation.repository.DossierMedicalRepository;
import com.pascob.teleconsultation.service.dto.DemandeConsultationDTO;
import com.pascob.teleconsultation.service.mapper.DemandeConsultationMapper;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DemandeConsultation}.
 */
@Service
@Transactional
public class DemandeConsultationService {

    private final Logger log = LoggerFactory.getLogger(DemandeConsultationService.class);

    private final DemandeConsultationRepository demandeConsultationRepository;

    private final DemandeConsultationMapper demandeConsultationMapper;

    private final DossierMedicalRepository dossierMedicalRepository;

    private final UserService userService;

    public DemandeConsultationService(
        DemandeConsultationRepository demandeConsultationRepository,
        DemandeConsultationMapper demandeConsultationMapper,
        DossierMedicalRepository dossierMedicalRepository,
        UserService userService
    ) {
        this.demandeConsultationRepository = demandeConsultationRepository;
        this.demandeConsultationMapper = demandeConsultationMapper;
        this.dossierMedicalRepository = dossierMedicalRepository;
        this.userService = userService;
    }

    /**
     * Save a demandeConsultation.
     *
     * @param demandeConsultationDTO the entity to save.
     * @return the persisted entity.
     */
    public DemandeConsultationDTO save(DemandeConsultationDTO demandeConsultationDTO) throws Exception {
        log.debug("Request to save DemandeConsultation : {}", demandeConsultationDTO);
        if (demandeConsultationDTO.getId() == null) {
            // Creaction
            demandeConsultationDTO.setDateHeureDemande(ZonedDateTime.now(Constants.GMT_ZONE));
            demandeConsultationDTO.setOrdre(
                demandeConsultationRepository.countAllByDateHeureDemandeBetween(
                    Constants.getDebutJournee(true),
                    Constants.getDebutJournee(false)
                ) +
                1
            );
            demandeConsultationDTO.setStatut(StatutDemandeConsultation.EN_COURS);
        }
        Optional<User> user = userService.getCurrentUser();
        if (user.isEmpty()) {
            throw new Exception("Veuillez vous connecter");
        }
        //        DossierMedical dossierMedical = dossierMedicalRepository.findByPatientUserId()
        log.error("DOSSIER {}", user.get().getPatient().getDossierMedical());
        DemandeConsultation demandeConsultation = demandeConsultationMapper.toEntity(demandeConsultationDTO);
        demandeConsultation.setDossierMedical(user.get().getPatient().getDossierMedical());
        demandeConsultation = demandeConsultationRepository.save(demandeConsultation);
        return demandeConsultationMapper.toDto(demandeConsultation);
    }

    /**
     * Update a demandeConsultation.
     *
     * @param demandeConsultationDTO the entity to save.
     * @return the persisted entity.
     */
    public DemandeConsultationDTO update(DemandeConsultationDTO demandeConsultationDTO) {
        log.debug("Request to update DemandeConsultation : {}", demandeConsultationDTO);
        DemandeConsultation demandeConsultation = demandeConsultationMapper.toEntity(demandeConsultationDTO);
        demandeConsultation = demandeConsultationRepository.save(demandeConsultation);
        return demandeConsultationMapper.toDto(demandeConsultation);
    }

    /**
     * Partially update a demandeConsultation.
     *
     * @param demandeConsultationDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DemandeConsultationDTO> partialUpdate(DemandeConsultationDTO demandeConsultationDTO) {
        log.debug("Request to partially update DemandeConsultation : {}", demandeConsultationDTO);

        return demandeConsultationRepository
            .findById(demandeConsultationDTO.getId())
            .map(existingDemandeConsultation -> {
                demandeConsultationMapper.partialUpdate(existingDemandeConsultation, demandeConsultationDTO);

                return existingDemandeConsultation;
            })
            .map(demandeConsultationRepository::save)
            .map(demandeConsultationMapper::toDto);
    }

    /**
     * Get all the demandeConsultations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DemandeConsultationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DemandeConsultations");
        return demandeConsultationRepository.findAll(pageable).map(demandeConsultationMapper::toDto);
    }

    /**
     * Get one demandeConsultation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DemandeConsultationDTO> findOne(Long id) {
        log.debug("Request to get DemandeConsultation : {}", id);
        return demandeConsultationRepository.findById(id).map(demandeConsultationMapper::toDto);
    }

    /**
     * Delete the demandeConsultation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DemandeConsultation : {}", id);
        demandeConsultationRepository.deleteById(id);
    }
}
