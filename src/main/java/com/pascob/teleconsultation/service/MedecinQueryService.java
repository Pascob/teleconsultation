package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.repository.MedecinRepository;
import com.pascob.teleconsultation.service.criteria.MedecinCriteria;
import com.pascob.teleconsultation.service.dto.MedecinDTO;
import com.pascob.teleconsultation.service.mapper.MedecinMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Medecin} entities in the database.
 * The main input is a {@link MedecinCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MedecinDTO} or a {@link Page} of {@link MedecinDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MedecinQueryService extends QueryService<Medecin> {

    private final Logger log = LoggerFactory.getLogger(MedecinQueryService.class);

    private final MedecinRepository medecinRepository;

    private final MedecinMapper medecinMapper;

    public MedecinQueryService(MedecinRepository medecinRepository, MedecinMapper medecinMapper) {
        this.medecinRepository = medecinRepository;
        this.medecinMapper = medecinMapper;
    }

    /**
     * Return a {@link List} of {@link MedecinDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MedecinDTO> findByCriteria(MedecinCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Medecin> specification = createSpecification(criteria);
        return medecinMapper.toDto(medecinRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MedecinDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MedecinDTO> findByCriteria(MedecinCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Medecin> specification = createSpecification(criteria);
        return medecinRepository.findAll(specification, page).map(medecinMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MedecinCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Medecin> specification = createSpecification(criteria);
        return medecinRepository.count(specification);
    }

    /**
     * Function to convert {@link MedecinCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Medecin> createSpecification(MedecinCriteria criteria) {
        Specification<Medecin> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Medecin_.id));
            }
            /* if (criteria.getUserId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserId(), Medecin_.userId));
            }*/
            if (criteria.getMatricule() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMatricule(), Medecin_.matricule));
            }
            if (criteria.getDossierMedicalId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDossierMedicalId(),
                            root -> root.join(Medecin_.dossierMedicals, JoinType.LEFT).get(DossierMedical_.id)
                        )
                    );
            }
            if (criteria.getConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getConsultationId(),
                            root -> root.join(Medecin_.consultations, JoinType.LEFT).get(Consultation_.id)
                        )
                    );
            }
            if (criteria.getFicheConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFicheConsultationId(),
                            root -> root.join(Medecin_.ficheConsultations, JoinType.LEFT).get(FicheConsultation_.id)
                        )
                    );
            }
            if (criteria.getFormationSanitaireId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFormationSanitaireId(),
                            root -> root.join(Medecin_.formationSanitaire, JoinType.LEFT).get(FormationSanitaire_.id)
                        )
                    );
            }
            if (criteria.getSpecialiteId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getSpecialiteId(),
                            root -> root.join(Medecin_.specialite, JoinType.LEFT).get(Specialite_.id)
                        )
                    );
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(Medecin_.user, JoinType.LEFT).get(User_.id))
                    );
            }
        }
        return specification;
    }
}
