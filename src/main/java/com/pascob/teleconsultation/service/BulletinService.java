package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.Bulletin;
import com.pascob.teleconsultation.repository.BulletinRepository;
import com.pascob.teleconsultation.service.dto.BulletinDTO;
import com.pascob.teleconsultation.service.mapper.BulletinMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Bulletin}.
 */
@Service
@Transactional
public class BulletinService {

    private final Logger log = LoggerFactory.getLogger(BulletinService.class);

    private final BulletinRepository bulletinRepository;

    private final BulletinMapper bulletinMapper;

    public BulletinService(BulletinRepository bulletinRepository, BulletinMapper bulletinMapper) {
        this.bulletinRepository = bulletinRepository;
        this.bulletinMapper = bulletinMapper;
    }

    /**
     * Save a bulletin.
     *
     * @param bulletinDTO the entity to save.
     * @return the persisted entity.
     */
    public BulletinDTO save(BulletinDTO bulletinDTO) {
        log.debug("Request to save Bulletin : {}", bulletinDTO);
        Bulletin bulletin = bulletinMapper.toEntity(bulletinDTO);
        bulletin = bulletinRepository.save(bulletin);
        return bulletinMapper.toDto(bulletin);
    }

    /**
     * Update a bulletin.
     *
     * @param bulletinDTO the entity to save.
     * @return the persisted entity.
     */
    public BulletinDTO update(BulletinDTO bulletinDTO) {
        log.debug("Request to update Bulletin : {}", bulletinDTO);
        Bulletin bulletin = bulletinMapper.toEntity(bulletinDTO);
        bulletin = bulletinRepository.save(bulletin);
        return bulletinMapper.toDto(bulletin);
    }

    /**
     * Partially update a bulletin.
     *
     * @param bulletinDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<BulletinDTO> partialUpdate(BulletinDTO bulletinDTO) {
        log.debug("Request to partially update Bulletin : {}", bulletinDTO);

        return bulletinRepository
            .findById(bulletinDTO.getId())
            .map(existingBulletin -> {
                bulletinMapper.partialUpdate(existingBulletin, bulletinDTO);

                return existingBulletin;
            })
            .map(bulletinRepository::save)
            .map(bulletinMapper::toDto);
    }

    /**
     * Get all the bulletins.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BulletinDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Bulletins");
        return bulletinRepository.findAll(pageable).map(bulletinMapper::toDto);
    }

    /**
     * Get one bulletin by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BulletinDTO> findOne(Long id) {
        log.debug("Request to get Bulletin : {}", id);
        return bulletinRepository.findById(id).map(bulletinMapper::toDto);
    }

    /**
     * Delete the bulletin by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Bulletin : {}", id);
        bulletinRepository.deleteById(id);
    }
}
