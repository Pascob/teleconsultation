package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.FicheSymptome;
import com.pascob.teleconsultation.repository.FicheSymptomeRepository;
import com.pascob.teleconsultation.service.criteria.FicheSymptomeCriteria;
import com.pascob.teleconsultation.service.dto.FicheSymptomeDTO;
import com.pascob.teleconsultation.service.mapper.FicheSymptomeMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link FicheSymptome} entities in the database.
 * The main input is a {@link FicheSymptomeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FicheSymptomeDTO} or a {@link Page} of {@link FicheSymptomeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FicheSymptomeQueryService extends QueryService<FicheSymptome> {

    private final Logger log = LoggerFactory.getLogger(FicheSymptomeQueryService.class);

    private final FicheSymptomeRepository ficheSymptomeRepository;

    private final FicheSymptomeMapper ficheSymptomeMapper;

    public FicheSymptomeQueryService(FicheSymptomeRepository ficheSymptomeRepository, FicheSymptomeMapper ficheSymptomeMapper) {
        this.ficheSymptomeRepository = ficheSymptomeRepository;
        this.ficheSymptomeMapper = ficheSymptomeMapper;
    }

    /**
     * Return a {@link List} of {@link FicheSymptomeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FicheSymptomeDTO> findByCriteria(FicheSymptomeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FicheSymptome> specification = createSpecification(criteria);
        return ficheSymptomeMapper.toDto(ficheSymptomeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link FicheSymptomeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FicheSymptomeDTO> findByCriteria(FicheSymptomeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FicheSymptome> specification = createSpecification(criteria);
        return ficheSymptomeRepository.findAll(specification, page).map(ficheSymptomeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FicheSymptomeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FicheSymptome> specification = createSpecification(criteria);
        return ficheSymptomeRepository.count(specification);
    }

    /**
     * Function to convert {@link FicheSymptomeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FicheSymptome> createSpecification(FicheSymptomeCriteria criteria) {
        Specification<FicheSymptome> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), FicheSymptome_.id));
            }
            if (criteria.getObservation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getObservation(), FicheSymptome_.observation));
            }
            if (criteria.getFicheConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFicheConsultationId(),
                            root -> root.join(FicheSymptome_.ficheConsultation, JoinType.LEFT).get(FicheConsultation_.id)
                        )
                    );
            }
            if (criteria.getSymptomeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getSymptomeId(),
                            root -> root.join(FicheSymptome_.symptome, JoinType.LEFT).get(Symptome_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
