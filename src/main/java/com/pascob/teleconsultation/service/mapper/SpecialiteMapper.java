package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Specialite;
import com.pascob.teleconsultation.service.dto.SpecialiteDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Specialite} and its DTO {@link SpecialiteDTO}.
 */
@Mapper(componentModel = "spring")
public interface SpecialiteMapper extends EntityMapper<SpecialiteDTO, Specialite> {}
