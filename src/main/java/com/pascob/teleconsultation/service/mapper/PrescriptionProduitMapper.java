package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Ordonnance;
import com.pascob.teleconsultation.domain.PrescriptionProduit;
import com.pascob.teleconsultation.domain.Produit;
import com.pascob.teleconsultation.service.dto.OrdonnanceDTO;
import com.pascob.teleconsultation.service.dto.PrescriptionProduitDTO;
import com.pascob.teleconsultation.service.dto.ProduitDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PrescriptionProduit} and its DTO {@link PrescriptionProduitDTO}.
 */
@Mapper(componentModel = "spring")
public interface PrescriptionProduitMapper extends EntityMapper<PrescriptionProduitDTO, PrescriptionProduit> {
    @Mapping(target = "produit", source = "produit", qualifiedByName = "produitId")
    @Mapping(target = "ordonnance", source = "ordonnance", qualifiedByName = "ordonnanceId")
    PrescriptionProduitDTO toDto(PrescriptionProduit s);

    @Named("produitId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProduitDTO toDtoProduitId(Produit produit);

    @Named("ordonnanceId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OrdonnanceDTO toDtoOrdonnanceId(Ordonnance ordonnance);
}
