package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Consultation;
import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.service.dto.ConsultationDTO;
import com.pascob.teleconsultation.service.dto.DemandeConsultationDTO;
import com.pascob.teleconsultation.service.dto.MedecinDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Consultation} and its DTO {@link ConsultationDTO}.
 */
@Mapper(componentModel = "spring")
public interface ConsultationMapper extends EntityMapper<ConsultationDTO, Consultation> {
    @Mapping(target = "medecin", source = "medecin", qualifiedByName = "medecinId")
    @Mapping(target = "demandeConsultation", source = "demandeConsultation", qualifiedByName = "demandeConsultationId")
    ConsultationDTO toDto(Consultation s);

    @Named("medecinId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MedecinDTO toDtoMedecinId(Medecin medecin);

    @Named("demandeConsultationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DemandeConsultationDTO toDtoDemandeConsultationId(DemandeConsultation demandeConsultation);
}
