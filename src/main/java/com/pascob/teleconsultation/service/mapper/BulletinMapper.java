package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Bulletin;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.service.dto.BulletinDTO;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Bulletin} and its DTO {@link BulletinDTO}.
 */
@Mapper(componentModel = "spring")
public interface BulletinMapper extends EntityMapper<BulletinDTO, Bulletin> {
    @Mapping(target = "ficheConsultation", source = "ficheConsultation", qualifiedByName = "ficheConsultationId")
    BulletinDTO toDto(Bulletin s);

    @Named("ficheConsultationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    FicheConsultationDTO toDtoFicheConsultationId(FicheConsultation ficheConsultation);
}
