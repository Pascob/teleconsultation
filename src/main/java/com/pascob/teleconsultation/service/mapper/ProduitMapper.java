package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Produit;
import com.pascob.teleconsultation.service.dto.ProduitDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Produit} and its DTO {@link ProduitDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProduitMapper extends EntityMapper<ProduitDTO, Produit> {}
