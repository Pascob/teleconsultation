package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.domain.Specialite;
import com.pascob.teleconsultation.service.dto.DemandeConsultationDTO;
import com.pascob.teleconsultation.service.dto.DossierMedicalDTO;
import com.pascob.teleconsultation.service.dto.SpecialiteDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DemandeConsultation} and its DTO {@link DemandeConsultationDTO}.
 */
@Mapper(componentModel = "spring")
public interface DemandeConsultationMapper extends EntityMapper<DemandeConsultationDTO, DemandeConsultation> {
    @Mapping(target = "specialite", source = "specialite", qualifiedByName = "specialiteId")
    @Mapping(target = "dossierMedical", source = "dossierMedical", qualifiedByName = "dossierMedicalId")
    DemandeConsultationDTO toDto(DemandeConsultation s);

    @Named("specialiteId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SpecialiteDTO toDtoSpecialiteId(Specialite specialite);

    @Named("dossierMedicalId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DossierMedicalDTO toDtoDossierMedicalId(DossierMedical dossierMedical);
}
