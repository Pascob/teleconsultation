package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.FormationSanitaire;
import com.pascob.teleconsultation.service.dto.FormationSanitaireDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link FormationSanitaire} and its DTO {@link FormationSanitaireDTO}.
 */
@Mapper(componentModel = "spring")
public interface FormationSanitaireMapper extends EntityMapper<FormationSanitaireDTO, FormationSanitaire> {}
