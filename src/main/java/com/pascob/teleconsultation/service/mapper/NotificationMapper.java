package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Notification;
import com.pascob.teleconsultation.service.dto.NotificationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Notification} and its DTO {@link NotificationDTO}.
 */
@Mapper(componentModel = "spring")
public interface NotificationMapper extends EntityMapper<NotificationDTO, Notification> {}
