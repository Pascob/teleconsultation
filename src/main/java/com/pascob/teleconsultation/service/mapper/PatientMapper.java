package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.domain.Patient;
import com.pascob.teleconsultation.domain.User;
import com.pascob.teleconsultation.service.dto.AdminUserDTO;
import com.pascob.teleconsultation.service.dto.DossierMedicalDTO;
import com.pascob.teleconsultation.service.dto.PatientDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Patient} and its DTO {@link PatientDTO}.
 */
@Mapper(componentModel = "spring")
public interface PatientMapper extends EntityMapper<PatientDTO, Patient> {
    @Mapping(target = "dossierMedical", source = "dossierMedical", qualifiedByName = "dossierMedicalId")
    PatientDTO toDto(Patient s);

    @Named("dossierMedicalId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DossierMedicalDTO toDtoDossierMedicalId(DossierMedical dossierMedical);
}
