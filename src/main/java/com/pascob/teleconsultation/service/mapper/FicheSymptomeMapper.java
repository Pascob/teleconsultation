package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.FicheSymptome;
import com.pascob.teleconsultation.domain.Symptome;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import com.pascob.teleconsultation.service.dto.FicheSymptomeDTO;
import com.pascob.teleconsultation.service.dto.SymptomeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link FicheSymptome} and its DTO {@link FicheSymptomeDTO}.
 */
@Mapper(componentModel = "spring")
public interface FicheSymptomeMapper extends EntityMapper<FicheSymptomeDTO, FicheSymptome> {
    @Mapping(target = "ficheConsultation", source = "ficheConsultation", qualifiedByName = "ficheConsultationId")
    @Mapping(target = "symptome", source = "symptome", qualifiedByName = "symptomeId")
    FicheSymptomeDTO toDto(FicheSymptome s);

    @Named("ficheConsultationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    FicheConsultationDTO toDtoFicheConsultationId(FicheConsultation ficheConsultation);

    @Named("symptomeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SymptomeDTO toDtoSymptomeId(Symptome symptome);
}
