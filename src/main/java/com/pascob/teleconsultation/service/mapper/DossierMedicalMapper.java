package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.service.dto.DossierMedicalDTO;
import com.pascob.teleconsultation.service.dto.MedecinDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DossierMedical} and its DTO {@link DossierMedicalDTO}.
 */
@Mapper(componentModel = "spring")
public interface DossierMedicalMapper extends EntityMapper<DossierMedicalDTO, DossierMedical> {
    @Mapping(target = "medecin", source = "medecin", qualifiedByName = "medecinId")
    DossierMedicalDTO toDto(DossierMedical s);

    @Named("medecinId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MedecinDTO toDtoMedecinId(Medecin medecin);
}
