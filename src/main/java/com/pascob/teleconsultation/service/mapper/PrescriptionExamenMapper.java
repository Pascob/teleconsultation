package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Bulletin;
import com.pascob.teleconsultation.domain.ExamenMedical;
import com.pascob.teleconsultation.domain.PrescriptionExamen;
import com.pascob.teleconsultation.service.dto.BulletinDTO;
import com.pascob.teleconsultation.service.dto.ExamenMedicalDTO;
import com.pascob.teleconsultation.service.dto.PrescriptionExamenDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PrescriptionExamen} and its DTO {@link PrescriptionExamenDTO}.
 */
@Mapper(componentModel = "spring")
public interface PrescriptionExamenMapper extends EntityMapper<PrescriptionExamenDTO, PrescriptionExamen> {
    @Mapping(target = "bulletin", source = "bulletin", qualifiedByName = "bulletinId")
    @Mapping(target = "examenMedical", source = "examenMedical", qualifiedByName = "examenMedicalId")
    PrescriptionExamenDTO toDto(PrescriptionExamen s);

    @Named("bulletinId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    BulletinDTO toDtoBulletinId(Bulletin bulletin);

    @Named("examenMedicalId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ExamenMedicalDTO toDtoExamenMedicalId(ExamenMedical examenMedical);
}
