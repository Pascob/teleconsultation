package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.ExamenMedical;
import com.pascob.teleconsultation.service.dto.ExamenMedicalDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExamenMedical} and its DTO {@link ExamenMedicalDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExamenMedicalMapper extends EntityMapper<ExamenMedicalDTO, ExamenMedical> {}
