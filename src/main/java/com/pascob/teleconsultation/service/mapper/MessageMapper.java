package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Message;
import com.pascob.teleconsultation.service.dto.MessageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Message} and its DTO {@link MessageDTO}.
 */
@Mapper(componentModel = "spring")
public interface MessageMapper extends EntityMapper<MessageDTO, Message> {}
