package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Symptome;
import com.pascob.teleconsultation.service.dto.SymptomeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Symptome} and its DTO {@link SymptomeDTO}.
 */
@Mapper(componentModel = "spring")
public interface SymptomeMapper extends EntityMapper<SymptomeDTO, Symptome> {}
