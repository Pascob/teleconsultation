package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.Ordonnance;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import com.pascob.teleconsultation.service.dto.OrdonnanceDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Ordonnance} and its DTO {@link OrdonnanceDTO}.
 */
@Mapper(componentModel = "spring")
public interface OrdonnanceMapper extends EntityMapper<OrdonnanceDTO, Ordonnance> {
    @Mapping(target = "ficheConsultation", source = "ficheConsultation", qualifiedByName = "ficheConsultationId")
    OrdonnanceDTO toDto(Ordonnance s);

    @Named("ficheConsultationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    FicheConsultationDTO toDtoFicheConsultationId(FicheConsultation ficheConsultation);
}
