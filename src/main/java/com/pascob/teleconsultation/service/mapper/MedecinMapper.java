package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.FormationSanitaire;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.domain.Specialite;
import com.pascob.teleconsultation.service.dto.FormationSanitaireDTO;
import com.pascob.teleconsultation.service.dto.MedecinDTO;
import com.pascob.teleconsultation.service.dto.SpecialiteDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Medecin} and its DTO {@link MedecinDTO}.
 */
@Mapper(componentModel = "spring")
public interface MedecinMapper extends EntityMapper<MedecinDTO, Medecin> {
    @Mapping(target = "formationSanitaire", source = "formationSanitaire", qualifiedByName = "formationSanitaireId")
    @Mapping(target = "specialite", source = "specialite", qualifiedByName = "specialiteId")
    MedecinDTO toDto(Medecin s);

    @Named("formationSanitaireId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    FormationSanitaireDTO toDtoFormationSanitaireId(FormationSanitaire formationSanitaire);

    @Named("specialiteId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SpecialiteDTO toDtoSpecialiteId(Specialite specialite);
}
