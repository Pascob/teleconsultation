package com.pascob.teleconsultation.service.mapper;

import com.pascob.teleconsultation.domain.Consultation;
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.domain.Medecin;
import com.pascob.teleconsultation.service.dto.ConsultationDTO;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import com.pascob.teleconsultation.service.dto.MedecinDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link FicheConsultation} and its DTO {@link FicheConsultationDTO}.
 */
@Mapper(componentModel = "spring")
public interface FicheConsultationMapper extends EntityMapper<FicheConsultationDTO, FicheConsultation> {
    @Mapping(target = "medecin", source = "medecin", qualifiedByName = "medecinId")
    @Mapping(target = "consultation", source = "consultation", qualifiedByName = "consultationId")
    FicheConsultationDTO toDto(FicheConsultation s);

    @Named("medecinId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MedecinDTO toDtoMedecinId(Medecin medecin);

    @Named("consultationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ConsultationDTO toDtoConsultationId(Consultation consultation);
}
