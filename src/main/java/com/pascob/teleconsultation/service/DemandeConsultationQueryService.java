package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.DemandeConsultation;
import com.pascob.teleconsultation.repository.DemandeConsultationRepository;
import com.pascob.teleconsultation.service.criteria.DemandeConsultationCriteria;
import com.pascob.teleconsultation.service.dto.DemandeConsultationDTO;
import com.pascob.teleconsultation.service.mapper.DemandeConsultationMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link DemandeConsultation} entities in the database.
 * The main input is a {@link DemandeConsultationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DemandeConsultationDTO} or a {@link Page} of {@link DemandeConsultationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DemandeConsultationQueryService extends QueryService<DemandeConsultation> {

    private final Logger log = LoggerFactory.getLogger(DemandeConsultationQueryService.class);

    private final DemandeConsultationRepository demandeConsultationRepository;

    private final DemandeConsultationMapper demandeConsultationMapper;

    public DemandeConsultationQueryService(
        DemandeConsultationRepository demandeConsultationRepository,
        DemandeConsultationMapper demandeConsultationMapper
    ) {
        this.demandeConsultationRepository = demandeConsultationRepository;
        this.demandeConsultationMapper = demandeConsultationMapper;
    }

    /**
     * Return a {@link List} of {@link DemandeConsultationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DemandeConsultationDTO> findByCriteria(DemandeConsultationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DemandeConsultation> specification = createSpecification(criteria);
        return demandeConsultationMapper.toDto(demandeConsultationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DemandeConsultationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DemandeConsultationDTO> findByCriteria(DemandeConsultationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DemandeConsultation> specification = createSpecification(criteria);
        return demandeConsultationRepository.findAll(specification, page).map(demandeConsultationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DemandeConsultationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DemandeConsultation> specification = createSpecification(criteria);
        return demandeConsultationRepository.count(specification);
    }

    /**
     * Function to convert {@link DemandeConsultationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DemandeConsultation> createSpecification(DemandeConsultationCriteria criteria) {
        Specification<DemandeConsultation> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DemandeConsultation_.id));
            }
            if (criteria.getDateHeureDemande() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getDateHeureDemande(), DemandeConsultation_.dateHeureDemande));
            }
            if (criteria.getOrdre() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrdre(), DemandeConsultation_.ordre));
            }
            if (criteria.getStatut() != null) {
                specification = specification.and(buildSpecification(criteria.getStatut(), DemandeConsultation_.statut));
            }
            if (criteria.getCodeOtp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCodeOtp(), DemandeConsultation_.codeOtp));
            }
            if (criteria.getTelephonePaiement() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getTelephonePaiement(), DemandeConsultation_.telephonePaiement));
            }
            if (criteria.getMontant() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMontant(), DemandeConsultation_.montant));
            }
            if (criteria.getConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getConsultationId(),
                            root -> root.join(DemandeConsultation_.consultations, JoinType.LEFT).get(Consultation_.id)
                        )
                    );
            }
            if (criteria.getSpecialiteId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getSpecialiteId(),
                            root -> root.join(DemandeConsultation_.specialite, JoinType.LEFT).get(Specialite_.id)
                        )
                    );
            }
            if (criteria.getDossierMedicalId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDossierMedicalId(),
                            root -> root.join(DemandeConsultation_.dossierMedical, JoinType.LEFT).get(DossierMedical_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
