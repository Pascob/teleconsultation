package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.FicheConsultation;
import com.pascob.teleconsultation.repository.FicheConsultationRepository;
import com.pascob.teleconsultation.service.criteria.FicheConsultationCriteria;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import com.pascob.teleconsultation.service.mapper.FicheConsultationMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link FicheConsultation} entities in the database.
 * The main input is a {@link FicheConsultationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FicheConsultationDTO} or a {@link Page} of {@link FicheConsultationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FicheConsultationQueryService extends QueryService<FicheConsultation> {

    private final Logger log = LoggerFactory.getLogger(FicheConsultationQueryService.class);

    private final FicheConsultationRepository ficheConsultationRepository;

    private final FicheConsultationMapper ficheConsultationMapper;

    public FicheConsultationQueryService(
        FicheConsultationRepository ficheConsultationRepository,
        FicheConsultationMapper ficheConsultationMapper
    ) {
        this.ficheConsultationRepository = ficheConsultationRepository;
        this.ficheConsultationMapper = ficheConsultationMapper;
    }

    /**
     * Return a {@link List} of {@link FicheConsultationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FicheConsultationDTO> findByCriteria(FicheConsultationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FicheConsultation> specification = createSpecification(criteria);
        return ficheConsultationMapper.toDto(ficheConsultationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link FicheConsultationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FicheConsultationDTO> findByCriteria(FicheConsultationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FicheConsultation> specification = createSpecification(criteria);
        return ficheConsultationRepository.findAll(specification, page).map(ficheConsultationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FicheConsultationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FicheConsultation> specification = createSpecification(criteria);
        return ficheConsultationRepository.count(specification);
    }

    /**
     * Function to convert {@link FicheConsultationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FicheConsultation> createSpecification(FicheConsultationCriteria criteria) {
        Specification<FicheConsultation> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), FicheConsultation_.id));
            }
            if (criteria.getDateFiche() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateFiche(), FicheConsultation_.dateFiche));
            }
            if (criteria.getNumero() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumero(), FicheConsultation_.numero));
            }
            if (criteria.getObservation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getObservation(), FicheConsultation_.observation));
            }
            if (criteria.getDiagnostic() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDiagnostic(), FicheConsultation_.diagnostic));
            }
            if (criteria.getOrdonnanceId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getOrdonnanceId(),
                            root -> root.join(FicheConsultation_.ordonnances, JoinType.LEFT).get(Ordonnance_.id)
                        )
                    );
            }
            if (criteria.getBulletinId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getBulletinId(),
                            root -> root.join(FicheConsultation_.bulletins, JoinType.LEFT).get(Bulletin_.id)
                        )
                    );
            }
            if (criteria.getFicheSymptomeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFicheSymptomeId(),
                            root -> root.join(FicheConsultation_.ficheSymptomes, JoinType.LEFT).get(FicheSymptome_.id)
                        )
                    );
            }
            if (criteria.getMedecinId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMedecinId(),
                            root -> root.join(FicheConsultation_.medecin, JoinType.LEFT).get(Medecin_.id)
                        )
                    );
            }
            if (criteria.getConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getConsultationId(),
                            root -> root.join(FicheConsultation_.consultation, JoinType.LEFT).get(Consultation_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
