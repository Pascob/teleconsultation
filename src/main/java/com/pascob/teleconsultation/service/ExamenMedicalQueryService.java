package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.ExamenMedical;
import com.pascob.teleconsultation.repository.ExamenMedicalRepository;
import com.pascob.teleconsultation.service.criteria.ExamenMedicalCriteria;
import com.pascob.teleconsultation.service.dto.ExamenMedicalDTO;
import com.pascob.teleconsultation.service.mapper.ExamenMedicalMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ExamenMedical} entities in the database.
 * The main input is a {@link ExamenMedicalCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ExamenMedicalDTO} or a {@link Page} of {@link ExamenMedicalDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ExamenMedicalQueryService extends QueryService<ExamenMedical> {

    private final Logger log = LoggerFactory.getLogger(ExamenMedicalQueryService.class);

    private final ExamenMedicalRepository examenMedicalRepository;

    private final ExamenMedicalMapper examenMedicalMapper;

    public ExamenMedicalQueryService(ExamenMedicalRepository examenMedicalRepository, ExamenMedicalMapper examenMedicalMapper) {
        this.examenMedicalRepository = examenMedicalRepository;
        this.examenMedicalMapper = examenMedicalMapper;
    }

    /**
     * Return a {@link List} of {@link ExamenMedicalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ExamenMedicalDTO> findByCriteria(ExamenMedicalCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ExamenMedical> specification = createSpecification(criteria);
        return examenMedicalMapper.toDto(examenMedicalRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ExamenMedicalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ExamenMedicalDTO> findByCriteria(ExamenMedicalCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ExamenMedical> specification = createSpecification(criteria);
        return examenMedicalRepository.findAll(specification, page).map(examenMedicalMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ExamenMedicalCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ExamenMedical> specification = createSpecification(criteria);
        return examenMedicalRepository.count(specification);
    }

    /**
     * Function to convert {@link ExamenMedicalCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ExamenMedical> createSpecification(ExamenMedicalCriteria criteria) {
        Specification<ExamenMedical> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ExamenMedical_.id));
            }
            if (criteria.getDesignation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesignation(), ExamenMedical_.designation));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), ExamenMedical_.code));
            }
            if (criteria.getPrescriptionExamenId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPrescriptionExamenId(),
                            root -> root.join(ExamenMedical_.prescriptionExamen, JoinType.LEFT).get(PrescriptionExamen_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
