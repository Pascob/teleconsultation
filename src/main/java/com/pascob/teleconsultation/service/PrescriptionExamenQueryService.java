package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.PrescriptionExamen;
import com.pascob.teleconsultation.repository.PrescriptionExamenRepository;
import com.pascob.teleconsultation.service.criteria.PrescriptionExamenCriteria;
import com.pascob.teleconsultation.service.dto.PrescriptionExamenDTO;
import com.pascob.teleconsultation.service.mapper.PrescriptionExamenMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PrescriptionExamen} entities in the database.
 * The main input is a {@link PrescriptionExamenCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PrescriptionExamenDTO} or a {@link Page} of {@link PrescriptionExamenDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PrescriptionExamenQueryService extends QueryService<PrescriptionExamen> {

    private final Logger log = LoggerFactory.getLogger(PrescriptionExamenQueryService.class);

    private final PrescriptionExamenRepository prescriptionExamenRepository;

    private final PrescriptionExamenMapper prescriptionExamenMapper;

    public PrescriptionExamenQueryService(
        PrescriptionExamenRepository prescriptionExamenRepository,
        PrescriptionExamenMapper prescriptionExamenMapper
    ) {
        this.prescriptionExamenRepository = prescriptionExamenRepository;
        this.prescriptionExamenMapper = prescriptionExamenMapper;
    }

    /**
     * Return a {@link List} of {@link PrescriptionExamenDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PrescriptionExamenDTO> findByCriteria(PrescriptionExamenCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PrescriptionExamen> specification = createSpecification(criteria);
        return prescriptionExamenMapper.toDto(prescriptionExamenRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PrescriptionExamenDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PrescriptionExamenDTO> findByCriteria(PrescriptionExamenCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PrescriptionExamen> specification = createSpecification(criteria);
        return prescriptionExamenRepository.findAll(specification, page).map(prescriptionExamenMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PrescriptionExamenCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PrescriptionExamen> specification = createSpecification(criteria);
        return prescriptionExamenRepository.count(specification);
    }

    /**
     * Function to convert {@link PrescriptionExamenCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PrescriptionExamen> createSpecification(PrescriptionExamenCriteria criteria) {
        Specification<PrescriptionExamen> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PrescriptionExamen_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), PrescriptionExamen_.description));
            }
            if (criteria.getResultat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getResultat(), PrescriptionExamen_.resultat));
            }
            if (criteria.getBulletinId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getBulletinId(),
                            root -> root.join(PrescriptionExamen_.bulletin, JoinType.LEFT).get(Bulletin_.id)
                        )
                    );
            }
            if (criteria.getExamenMedicalId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getExamenMedicalId(),
                            root -> root.join(PrescriptionExamen_.examenMedical, JoinType.LEFT).get(ExamenMedical_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
