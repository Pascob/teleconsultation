package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.Bulletin;
import com.pascob.teleconsultation.repository.BulletinRepository;
import com.pascob.teleconsultation.service.criteria.BulletinCriteria;
import com.pascob.teleconsultation.service.dto.BulletinDTO;
import com.pascob.teleconsultation.service.mapper.BulletinMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Bulletin} entities in the database.
 * The main input is a {@link BulletinCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BulletinDTO} or a {@link Page} of {@link BulletinDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BulletinQueryService extends QueryService<Bulletin> {

    private final Logger log = LoggerFactory.getLogger(BulletinQueryService.class);

    private final BulletinRepository bulletinRepository;

    private final BulletinMapper bulletinMapper;

    public BulletinQueryService(BulletinRepository bulletinRepository, BulletinMapper bulletinMapper) {
        this.bulletinRepository = bulletinRepository;
        this.bulletinMapper = bulletinMapper;
    }

    /**
     * Return a {@link List} of {@link BulletinDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BulletinDTO> findByCriteria(BulletinCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Bulletin> specification = createSpecification(criteria);
        return bulletinMapper.toDto(bulletinRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BulletinDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BulletinDTO> findByCriteria(BulletinCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Bulletin> specification = createSpecification(criteria);
        return bulletinRepository.findAll(specification, page).map(bulletinMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BulletinCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Bulletin> specification = createSpecification(criteria);
        return bulletinRepository.count(specification);
    }

    /**
     * Function to convert {@link BulletinCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Bulletin> createSpecification(BulletinCriteria criteria) {
        Specification<Bulletin> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Bulletin_.id));
            }
            if (criteria.getDateBulletin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateBulletin(), Bulletin_.dateBulletin));
            }
            if (criteria.getNumero() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumero(), Bulletin_.numero));
            }
            if (criteria.getPrescriptionExamenId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPrescriptionExamenId(),
                            root -> root.join(Bulletin_.prescriptionExamen, JoinType.LEFT).get(PrescriptionExamen_.id)
                        )
                    );
            }
            if (criteria.getFicheConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getFicheConsultationId(),
                            root -> root.join(Bulletin_.ficheConsultation, JoinType.LEFT).get(FicheConsultation_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
