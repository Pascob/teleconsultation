package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.PrescriptionExamen;
import com.pascob.teleconsultation.repository.PrescriptionExamenRepository;
import com.pascob.teleconsultation.service.dto.PrescriptionExamenDTO;
import com.pascob.teleconsultation.service.mapper.PrescriptionExamenMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PrescriptionExamen}.
 */
@Service
@Transactional
public class PrescriptionExamenService {

    private final Logger log = LoggerFactory.getLogger(PrescriptionExamenService.class);

    private final PrescriptionExamenRepository prescriptionExamenRepository;

    private final PrescriptionExamenMapper prescriptionExamenMapper;

    public PrescriptionExamenService(
        PrescriptionExamenRepository prescriptionExamenRepository,
        PrescriptionExamenMapper prescriptionExamenMapper
    ) {
        this.prescriptionExamenRepository = prescriptionExamenRepository;
        this.prescriptionExamenMapper = prescriptionExamenMapper;
    }

    /**
     * Save a prescriptionExamen.
     *
     * @param prescriptionExamenDTO the entity to save.
     * @return the persisted entity.
     */
    public PrescriptionExamenDTO save(PrescriptionExamenDTO prescriptionExamenDTO) {
        log.debug("Request to save PrescriptionExamen : {}", prescriptionExamenDTO);
        PrescriptionExamen prescriptionExamen = prescriptionExamenMapper.toEntity(prescriptionExamenDTO);
        prescriptionExamen = prescriptionExamenRepository.save(prescriptionExamen);
        return prescriptionExamenMapper.toDto(prescriptionExamen);
    }

    /**
     * Update a prescriptionExamen.
     *
     * @param prescriptionExamenDTO the entity to save.
     * @return the persisted entity.
     */
    public PrescriptionExamenDTO update(PrescriptionExamenDTO prescriptionExamenDTO) {
        log.debug("Request to update PrescriptionExamen : {}", prescriptionExamenDTO);
        PrescriptionExamen prescriptionExamen = prescriptionExamenMapper.toEntity(prescriptionExamenDTO);
        prescriptionExamen = prescriptionExamenRepository.save(prescriptionExamen);
        return prescriptionExamenMapper.toDto(prescriptionExamen);
    }

    /**
     * Partially update a prescriptionExamen.
     *
     * @param prescriptionExamenDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PrescriptionExamenDTO> partialUpdate(PrescriptionExamenDTO prescriptionExamenDTO) {
        log.debug("Request to partially update PrescriptionExamen : {}", prescriptionExamenDTO);

        return prescriptionExamenRepository
            .findById(prescriptionExamenDTO.getId())
            .map(existingPrescriptionExamen -> {
                prescriptionExamenMapper.partialUpdate(existingPrescriptionExamen, prescriptionExamenDTO);

                return existingPrescriptionExamen;
            })
            .map(prescriptionExamenRepository::save)
            .map(prescriptionExamenMapper::toDto);
    }

    /**
     * Get all the prescriptionExamen.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PrescriptionExamenDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PrescriptionExamen");
        return prescriptionExamenRepository.findAll(pageable).map(prescriptionExamenMapper::toDto);
    }

    /**
     * Get one prescriptionExamen by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PrescriptionExamenDTO> findOne(Long id) {
        log.debug("Request to get PrescriptionExamen : {}", id);
        return prescriptionExamenRepository.findById(id).map(prescriptionExamenMapper::toDto);
    }

    /**
     * Delete the prescriptionExamen by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PrescriptionExamen : {}", id);
        prescriptionExamenRepository.deleteById(id);
    }
}
