package com.pascob.teleconsultation.service;

import com.pascob.teleconsultation.domain.*; // for static metamodels
import com.pascob.teleconsultation.domain.DossierMedical;
import com.pascob.teleconsultation.repository.DossierMedicalRepository;
import com.pascob.teleconsultation.service.criteria.DossierMedicalCriteria;
import com.pascob.teleconsultation.service.dto.DossierMedicalDTO;
import com.pascob.teleconsultation.service.mapper.DossierMedicalMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link DossierMedical} entities in the database.
 * The main input is a {@link DossierMedicalCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DossierMedicalDTO} or a {@link Page} of {@link DossierMedicalDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DossierMedicalQueryService extends QueryService<DossierMedical> {

    private final Logger log = LoggerFactory.getLogger(DossierMedicalQueryService.class);

    private final DossierMedicalRepository dossierMedicalRepository;

    private final DossierMedicalMapper dossierMedicalMapper;

    public DossierMedicalQueryService(DossierMedicalRepository dossierMedicalRepository, DossierMedicalMapper dossierMedicalMapper) {
        this.dossierMedicalRepository = dossierMedicalRepository;
        this.dossierMedicalMapper = dossierMedicalMapper;
    }

    /**
     * Return a {@link List} of {@link DossierMedicalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DossierMedicalDTO> findByCriteria(DossierMedicalCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DossierMedical> specification = createSpecification(criteria);
        return dossierMedicalMapper.toDto(dossierMedicalRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DossierMedicalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DossierMedicalDTO> findByCriteria(DossierMedicalCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DossierMedical> specification = createSpecification(criteria);
        return dossierMedicalRepository.findAll(specification, page).map(dossierMedicalMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DossierMedicalCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DossierMedical> specification = createSpecification(criteria);
        return dossierMedicalRepository.count(specification);
    }

    /**
     * Function to convert {@link DossierMedicalCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DossierMedical> createSpecification(DossierMedicalCriteria criteria) {
        Specification<DossierMedical> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DossierMedical_.id));
            }
            if (criteria.getNumero() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNumero(), DossierMedical_.numero));
            }
            if (criteria.getGroupeSanguin() != null) {
                specification = specification.and(buildSpecification(criteria.getGroupeSanguin(), DossierMedical_.groupeSanguin));
            }
            if (criteria.getDemandeConsultationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDemandeConsultationId(),
                            root -> root.join(DossierMedical_.demandeConsultations, JoinType.LEFT).get(DemandeConsultation_.id)
                        )
                    );
            }
            if (criteria.getPatientId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPatientId(),
                            root -> root.join(DossierMedical_.patient, JoinType.LEFT).get(Patient_.id)
                        )
                    );
            }
            if (criteria.getMedecinId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMedecinId(),
                            root -> root.join(DossierMedical_.medecin, JoinType.LEFT).get(Medecin_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
