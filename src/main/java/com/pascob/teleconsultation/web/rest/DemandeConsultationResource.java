package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.DemandeConsultationRepository;
import com.pascob.teleconsultation.service.DemandeConsultationQueryService;
import com.pascob.teleconsultation.service.DemandeConsultationService;
import com.pascob.teleconsultation.service.criteria.DemandeConsultationCriteria;
import com.pascob.teleconsultation.service.dto.DemandeConsultationDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.DemandeConsultation}.
 */
@RestController
@RequestMapping("/api")
public class DemandeConsultationResource {

    private final Logger log = LoggerFactory.getLogger(DemandeConsultationResource.class);

    private static final String ENTITY_NAME = "demandeConsultation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DemandeConsultationService demandeConsultationService;

    private final DemandeConsultationRepository demandeConsultationRepository;

    private final DemandeConsultationQueryService demandeConsultationQueryService;

    public DemandeConsultationResource(
        DemandeConsultationService demandeConsultationService,
        DemandeConsultationRepository demandeConsultationRepository,
        DemandeConsultationQueryService demandeConsultationQueryService
    ) {
        this.demandeConsultationService = demandeConsultationService;
        this.demandeConsultationRepository = demandeConsultationRepository;
        this.demandeConsultationQueryService = demandeConsultationQueryService;
    }

    /**
     * {@code POST  /demande-consultations} : Create a new demandeConsultation.
     *
     * @param demandeConsultationDTO the demandeConsultationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new demandeConsultationDTO, or with status {@code 400 (Bad Request)} if the demandeConsultation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/demande-consultations")
    public ResponseEntity<DemandeConsultationDTO> createDemandeConsultation(
        @Valid @RequestBody DemandeConsultationDTO demandeConsultationDTO
    ) throws Exception {
        log.debug("REST request to save DemandeConsultation : {}", demandeConsultationDTO);
        if (demandeConsultationDTO.getId() != null) {
            throw new BadRequestAlertException("A new demandeConsultation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DemandeConsultationDTO result = demandeConsultationService.save(demandeConsultationDTO);
        return ResponseEntity
            .created(new URI("/api/demande-consultations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /demande-consultations/:id} : Updates an existing demandeConsultation.
     *
     * @param id the id of the demandeConsultationDTO to save.
     * @param demandeConsultationDTO the demandeConsultationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated demandeConsultationDTO,
     * or with status {@code 400 (Bad Request)} if the demandeConsultationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the demandeConsultationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/demande-consultations/{id}")
    public ResponseEntity<DemandeConsultationDTO> updateDemandeConsultation(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DemandeConsultationDTO demandeConsultationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DemandeConsultation : {}, {}", id, demandeConsultationDTO);
        if (demandeConsultationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, demandeConsultationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!demandeConsultationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DemandeConsultationDTO result = demandeConsultationService.update(demandeConsultationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, demandeConsultationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /demande-consultations/:id} : Partial updates given fields of an existing demandeConsultation, field will ignore if it is null
     *
     * @param id the id of the demandeConsultationDTO to save.
     * @param demandeConsultationDTO the demandeConsultationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated demandeConsultationDTO,
     * or with status {@code 400 (Bad Request)} if the demandeConsultationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the demandeConsultationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the demandeConsultationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/demande-consultations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DemandeConsultationDTO> partialUpdateDemandeConsultation(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DemandeConsultationDTO demandeConsultationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DemandeConsultation partially : {}, {}", id, demandeConsultationDTO);
        if (demandeConsultationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, demandeConsultationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!demandeConsultationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DemandeConsultationDTO> result = demandeConsultationService.partialUpdate(demandeConsultationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, demandeConsultationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /demande-consultations} : get all the demandeConsultations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of demandeConsultations in body.
     */
    @GetMapping("/demande-consultations")
    public ResponseEntity<List<DemandeConsultationDTO>> getAllDemandeConsultations(
        DemandeConsultationCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get DemandeConsultations by criteria: {}", criteria);
        Page<DemandeConsultationDTO> page = demandeConsultationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /demande-consultations/count} : count all the demandeConsultations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/demande-consultations/count")
    public ResponseEntity<Long> countDemandeConsultations(DemandeConsultationCriteria criteria) {
        log.debug("REST request to count DemandeConsultations by criteria: {}", criteria);
        return ResponseEntity.ok().body(demandeConsultationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /demande-consultations/:id} : get the "id" demandeConsultation.
     *
     * @param id the id of the demandeConsultationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the demandeConsultationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/demande-consultations/{id}")
    public ResponseEntity<DemandeConsultationDTO> getDemandeConsultation(@PathVariable Long id) {
        log.debug("REST request to get DemandeConsultation : {}", id);
        Optional<DemandeConsultationDTO> demandeConsultationDTO = demandeConsultationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(demandeConsultationDTO);
    }

    /**
     * {@code DELETE  /demande-consultations/:id} : delete the "id" demandeConsultation.
     *
     * @param id the id of the demandeConsultationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/demande-consultations/{id}")
    public ResponseEntity<Void> deleteDemandeConsultation(@PathVariable Long id) {
        log.debug("REST request to delete DemandeConsultation : {}", id);
        demandeConsultationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
