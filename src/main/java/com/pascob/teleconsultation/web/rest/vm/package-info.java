/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pascob.teleconsultation.web.rest.vm;
