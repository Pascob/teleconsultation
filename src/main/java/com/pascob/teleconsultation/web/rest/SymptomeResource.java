package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.SymptomeRepository;
import com.pascob.teleconsultation.service.SymptomeQueryService;
import com.pascob.teleconsultation.service.SymptomeService;
import com.pascob.teleconsultation.service.criteria.SymptomeCriteria;
import com.pascob.teleconsultation.service.dto.SymptomeDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.Symptome}.
 */
@RestController
@RequestMapping("/api")
public class SymptomeResource {

    private final Logger log = LoggerFactory.getLogger(SymptomeResource.class);

    private static final String ENTITY_NAME = "symptome";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SymptomeService symptomeService;

    private final SymptomeRepository symptomeRepository;

    private final SymptomeQueryService symptomeQueryService;

    public SymptomeResource(
        SymptomeService symptomeService,
        SymptomeRepository symptomeRepository,
        SymptomeQueryService symptomeQueryService
    ) {
        this.symptomeService = symptomeService;
        this.symptomeRepository = symptomeRepository;
        this.symptomeQueryService = symptomeQueryService;
    }

    /**
     * {@code POST  /symptomes} : Create a new symptome.
     *
     * @param symptomeDTO the symptomeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new symptomeDTO, or with status {@code 400 (Bad Request)} if the symptome has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/symptomes")
    public ResponseEntity<SymptomeDTO> createSymptome(@Valid @RequestBody SymptomeDTO symptomeDTO) throws URISyntaxException {
        log.debug("REST request to save Symptome : {}", symptomeDTO);
        if (symptomeDTO.getId() != null) {
            throw new BadRequestAlertException("A new symptome cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SymptomeDTO result = symptomeService.save(symptomeDTO);
        return ResponseEntity
            .created(new URI("/api/symptomes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /symptomes/:id} : Updates an existing symptome.
     *
     * @param id the id of the symptomeDTO to save.
     * @param symptomeDTO the symptomeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated symptomeDTO,
     * or with status {@code 400 (Bad Request)} if the symptomeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the symptomeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/symptomes/{id}")
    public ResponseEntity<SymptomeDTO> updateSymptome(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody SymptomeDTO symptomeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Symptome : {}, {}", id, symptomeDTO);
        if (symptomeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, symptomeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!symptomeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SymptomeDTO result = symptomeService.update(symptomeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, symptomeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /symptomes/:id} : Partial updates given fields of an existing symptome, field will ignore if it is null
     *
     * @param id the id of the symptomeDTO to save.
     * @param symptomeDTO the symptomeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated symptomeDTO,
     * or with status {@code 400 (Bad Request)} if the symptomeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the symptomeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the symptomeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/symptomes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SymptomeDTO> partialUpdateSymptome(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody SymptomeDTO symptomeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Symptome partially : {}, {}", id, symptomeDTO);
        if (symptomeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, symptomeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!symptomeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SymptomeDTO> result = symptomeService.partialUpdate(symptomeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, symptomeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /symptomes} : get all the symptomes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of symptomes in body.
     */
    @GetMapping("/symptomes")
    public ResponseEntity<List<SymptomeDTO>> getAllSymptomes(
        SymptomeCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Symptomes by criteria: {}", criteria);
        Page<SymptomeDTO> page = symptomeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /symptomes/count} : count all the symptomes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/symptomes/count")
    public ResponseEntity<Long> countSymptomes(SymptomeCriteria criteria) {
        log.debug("REST request to count Symptomes by criteria: {}", criteria);
        return ResponseEntity.ok().body(symptomeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /symptomes/:id} : get the "id" symptome.
     *
     * @param id the id of the symptomeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the symptomeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/symptomes/{id}")
    public ResponseEntity<SymptomeDTO> getSymptome(@PathVariable Long id) {
        log.debug("REST request to get Symptome : {}", id);
        Optional<SymptomeDTO> symptomeDTO = symptomeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(symptomeDTO);
    }

    /**
     * {@code DELETE  /symptomes/:id} : delete the "id" symptome.
     *
     * @param id the id of the symptomeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/symptomes/{id}")
    public ResponseEntity<Void> deleteSymptome(@PathVariable Long id) {
        log.debug("REST request to delete Symptome : {}", id);
        symptomeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
