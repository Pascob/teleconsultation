package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.FicheConsultationRepository;
import com.pascob.teleconsultation.service.FicheConsultationQueryService;
import com.pascob.teleconsultation.service.FicheConsultationService;
import com.pascob.teleconsultation.service.criteria.FicheConsultationCriteria;
import com.pascob.teleconsultation.service.dto.FicheConsultationDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.FicheConsultation}.
 */
@RestController
@RequestMapping("/api")
public class FicheConsultationResource {

    private final Logger log = LoggerFactory.getLogger(FicheConsultationResource.class);

    private static final String ENTITY_NAME = "ficheConsultation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FicheConsultationService ficheConsultationService;

    private final FicheConsultationRepository ficheConsultationRepository;

    private final FicheConsultationQueryService ficheConsultationQueryService;

    public FicheConsultationResource(
        FicheConsultationService ficheConsultationService,
        FicheConsultationRepository ficheConsultationRepository,
        FicheConsultationQueryService ficheConsultationQueryService
    ) {
        this.ficheConsultationService = ficheConsultationService;
        this.ficheConsultationRepository = ficheConsultationRepository;
        this.ficheConsultationQueryService = ficheConsultationQueryService;
    }

    /**
     * {@code POST  /fiche-consultations} : Create a new ficheConsultation.
     *
     * @param ficheConsultationDTO the ficheConsultationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ficheConsultationDTO, or with status {@code 400 (Bad Request)} if the ficheConsultation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fiche-consultations")
    public ResponseEntity<FicheConsultationDTO> createFicheConsultation(@Valid @RequestBody FicheConsultationDTO ficheConsultationDTO)
        throws URISyntaxException {
        log.debug("REST request to save FicheConsultation : {}", ficheConsultationDTO);
        if (ficheConsultationDTO.getId() != null) {
            throw new BadRequestAlertException("A new ficheConsultation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FicheConsultationDTO result = ficheConsultationService.save(ficheConsultationDTO);
        return ResponseEntity
            .created(new URI("/api/fiche-consultations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fiche-consultations/:id} : Updates an existing ficheConsultation.
     *
     * @param id the id of the ficheConsultationDTO to save.
     * @param ficheConsultationDTO the ficheConsultationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ficheConsultationDTO,
     * or with status {@code 400 (Bad Request)} if the ficheConsultationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ficheConsultationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fiche-consultations/{id}")
    public ResponseEntity<FicheConsultationDTO> updateFicheConsultation(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody FicheConsultationDTO ficheConsultationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update FicheConsultation : {}, {}", id, ficheConsultationDTO);
        if (ficheConsultationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ficheConsultationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ficheConsultationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FicheConsultationDTO result = ficheConsultationService.update(ficheConsultationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ficheConsultationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /fiche-consultations/:id} : Partial updates given fields of an existing ficheConsultation, field will ignore if it is null
     *
     * @param id the id of the ficheConsultationDTO to save.
     * @param ficheConsultationDTO the ficheConsultationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ficheConsultationDTO,
     * or with status {@code 400 (Bad Request)} if the ficheConsultationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the ficheConsultationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the ficheConsultationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fiche-consultations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FicheConsultationDTO> partialUpdateFicheConsultation(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody FicheConsultationDTO ficheConsultationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update FicheConsultation partially : {}, {}", id, ficheConsultationDTO);
        if (ficheConsultationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ficheConsultationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ficheConsultationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FicheConsultationDTO> result = ficheConsultationService.partialUpdate(ficheConsultationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ficheConsultationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /fiche-consultations} : get all the ficheConsultations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ficheConsultations in body.
     */
    @GetMapping("/fiche-consultations")
    public ResponseEntity<List<FicheConsultationDTO>> getAllFicheConsultations(
        FicheConsultationCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get FicheConsultations by criteria: {}", criteria);
        Page<FicheConsultationDTO> page = ficheConsultationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /fiche-consultations/count} : count all the ficheConsultations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/fiche-consultations/count")
    public ResponseEntity<Long> countFicheConsultations(FicheConsultationCriteria criteria) {
        log.debug("REST request to count FicheConsultations by criteria: {}", criteria);
        return ResponseEntity.ok().body(ficheConsultationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fiche-consultations/:id} : get the "id" ficheConsultation.
     *
     * @param id the id of the ficheConsultationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ficheConsultationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fiche-consultations/{id}")
    public ResponseEntity<FicheConsultationDTO> getFicheConsultation(@PathVariable Long id) {
        log.debug("REST request to get FicheConsultation : {}", id);
        Optional<FicheConsultationDTO> ficheConsultationDTO = ficheConsultationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ficheConsultationDTO);
    }

    /**
     * {@code DELETE  /fiche-consultations/:id} : delete the "id" ficheConsultation.
     *
     * @param id the id of the ficheConsultationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fiche-consultations/{id}")
    public ResponseEntity<Void> deleteFicheConsultation(@PathVariable Long id) {
        log.debug("REST request to delete FicheConsultation : {}", id);
        ficheConsultationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
