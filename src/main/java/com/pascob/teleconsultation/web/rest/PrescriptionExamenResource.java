package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.PrescriptionExamenRepository;
import com.pascob.teleconsultation.service.PrescriptionExamenQueryService;
import com.pascob.teleconsultation.service.PrescriptionExamenService;
import com.pascob.teleconsultation.service.criteria.PrescriptionExamenCriteria;
import com.pascob.teleconsultation.service.dto.PrescriptionExamenDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.PrescriptionExamen}.
 */
@RestController
@RequestMapping("/api")
public class PrescriptionExamenResource {

    private final Logger log = LoggerFactory.getLogger(PrescriptionExamenResource.class);

    private static final String ENTITY_NAME = "prescriptionExamen";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PrescriptionExamenService prescriptionExamenService;

    private final PrescriptionExamenRepository prescriptionExamenRepository;

    private final PrescriptionExamenQueryService prescriptionExamenQueryService;

    public PrescriptionExamenResource(
        PrescriptionExamenService prescriptionExamenService,
        PrescriptionExamenRepository prescriptionExamenRepository,
        PrescriptionExamenQueryService prescriptionExamenQueryService
    ) {
        this.prescriptionExamenService = prescriptionExamenService;
        this.prescriptionExamenRepository = prescriptionExamenRepository;
        this.prescriptionExamenQueryService = prescriptionExamenQueryService;
    }

    /**
     * {@code POST  /prescription-examen} : Create a new prescriptionExamen.
     *
     * @param prescriptionExamenDTO the prescriptionExamenDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new prescriptionExamenDTO, or with status {@code 400 (Bad Request)} if the prescriptionExamen has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/prescription-examen")
    public ResponseEntity<PrescriptionExamenDTO> createPrescriptionExamen(@RequestBody PrescriptionExamenDTO prescriptionExamenDTO)
        throws URISyntaxException {
        log.debug("REST request to save PrescriptionExamen : {}", prescriptionExamenDTO);
        if (prescriptionExamenDTO.getId() != null) {
            throw new BadRequestAlertException("A new prescriptionExamen cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PrescriptionExamenDTO result = prescriptionExamenService.save(prescriptionExamenDTO);
        return ResponseEntity
            .created(new URI("/api/prescription-examen/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /prescription-examen/:id} : Updates an existing prescriptionExamen.
     *
     * @param id the id of the prescriptionExamenDTO to save.
     * @param prescriptionExamenDTO the prescriptionExamenDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prescriptionExamenDTO,
     * or with status {@code 400 (Bad Request)} if the prescriptionExamenDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the prescriptionExamenDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/prescription-examen/{id}")
    public ResponseEntity<PrescriptionExamenDTO> updatePrescriptionExamen(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PrescriptionExamenDTO prescriptionExamenDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PrescriptionExamen : {}, {}", id, prescriptionExamenDTO);
        if (prescriptionExamenDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prescriptionExamenDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prescriptionExamenRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PrescriptionExamenDTO result = prescriptionExamenService.update(prescriptionExamenDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prescriptionExamenDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /prescription-examen/:id} : Partial updates given fields of an existing prescriptionExamen, field will ignore if it is null
     *
     * @param id the id of the prescriptionExamenDTO to save.
     * @param prescriptionExamenDTO the prescriptionExamenDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prescriptionExamenDTO,
     * or with status {@code 400 (Bad Request)} if the prescriptionExamenDTO is not valid,
     * or with status {@code 404 (Not Found)} if the prescriptionExamenDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the prescriptionExamenDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/prescription-examen/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PrescriptionExamenDTO> partialUpdatePrescriptionExamen(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PrescriptionExamenDTO prescriptionExamenDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PrescriptionExamen partially : {}, {}", id, prescriptionExamenDTO);
        if (prescriptionExamenDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prescriptionExamenDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prescriptionExamenRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PrescriptionExamenDTO> result = prescriptionExamenService.partialUpdate(prescriptionExamenDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prescriptionExamenDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /prescription-examen} : get all the prescriptionExamen.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of prescriptionExamen in body.
     */
    @GetMapping("/prescription-examen")
    public ResponseEntity<List<PrescriptionExamenDTO>> getAllPrescriptionExamen(
        PrescriptionExamenCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get PrescriptionExamen by criteria: {}", criteria);
        Page<PrescriptionExamenDTO> page = prescriptionExamenQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /prescription-examen/count} : count all the prescriptionExamen.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/prescription-examen/count")
    public ResponseEntity<Long> countPrescriptionExamen(PrescriptionExamenCriteria criteria) {
        log.debug("REST request to count PrescriptionExamen by criteria: {}", criteria);
        return ResponseEntity.ok().body(prescriptionExamenQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /prescription-examen/:id} : get the "id" prescriptionExamen.
     *
     * @param id the id of the prescriptionExamenDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the prescriptionExamenDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/prescription-examen/{id}")
    public ResponseEntity<PrescriptionExamenDTO> getPrescriptionExamen(@PathVariable Long id) {
        log.debug("REST request to get PrescriptionExamen : {}", id);
        Optional<PrescriptionExamenDTO> prescriptionExamenDTO = prescriptionExamenService.findOne(id);
        return ResponseUtil.wrapOrNotFound(prescriptionExamenDTO);
    }

    /**
     * {@code DELETE  /prescription-examen/:id} : delete the "id" prescriptionExamen.
     *
     * @param id the id of the prescriptionExamenDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/prescription-examen/{id}")
    public ResponseEntity<Void> deletePrescriptionExamen(@PathVariable Long id) {
        log.debug("REST request to delete PrescriptionExamen : {}", id);
        prescriptionExamenService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
