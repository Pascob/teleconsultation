package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.PrescriptionProduitRepository;
import com.pascob.teleconsultation.service.PrescriptionProduitQueryService;
import com.pascob.teleconsultation.service.PrescriptionProduitService;
import com.pascob.teleconsultation.service.criteria.PrescriptionProduitCriteria;
import com.pascob.teleconsultation.service.dto.PrescriptionProduitDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.PrescriptionProduit}.
 */
@RestController
@RequestMapping("/api")
public class PrescriptionProduitResource {

    private final Logger log = LoggerFactory.getLogger(PrescriptionProduitResource.class);

    private static final String ENTITY_NAME = "prescriptionProduit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PrescriptionProduitService prescriptionProduitService;

    private final PrescriptionProduitRepository prescriptionProduitRepository;

    private final PrescriptionProduitQueryService prescriptionProduitQueryService;

    public PrescriptionProduitResource(
        PrescriptionProduitService prescriptionProduitService,
        PrescriptionProduitRepository prescriptionProduitRepository,
        PrescriptionProduitQueryService prescriptionProduitQueryService
    ) {
        this.prescriptionProduitService = prescriptionProduitService;
        this.prescriptionProduitRepository = prescriptionProduitRepository;
        this.prescriptionProduitQueryService = prescriptionProduitQueryService;
    }

    /**
     * {@code POST  /prescription-produits} : Create a new prescriptionProduit.
     *
     * @param prescriptionProduitDTO the prescriptionProduitDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new prescriptionProduitDTO, or with status {@code 400 (Bad Request)} if the prescriptionProduit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/prescription-produits")
    public ResponseEntity<PrescriptionProduitDTO> createPrescriptionProduit(@RequestBody PrescriptionProduitDTO prescriptionProduitDTO)
        throws URISyntaxException {
        log.debug("REST request to save PrescriptionProduit : {}", prescriptionProduitDTO);
        if (prescriptionProduitDTO.getId() != null) {
            throw new BadRequestAlertException("A new prescriptionProduit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PrescriptionProduitDTO result = prescriptionProduitService.save(prescriptionProduitDTO);
        return ResponseEntity
            .created(new URI("/api/prescription-produits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /prescription-produits/:id} : Updates an existing prescriptionProduit.
     *
     * @param id the id of the prescriptionProduitDTO to save.
     * @param prescriptionProduitDTO the prescriptionProduitDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prescriptionProduitDTO,
     * or with status {@code 400 (Bad Request)} if the prescriptionProduitDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the prescriptionProduitDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/prescription-produits/{id}")
    public ResponseEntity<PrescriptionProduitDTO> updatePrescriptionProduit(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PrescriptionProduitDTO prescriptionProduitDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PrescriptionProduit : {}, {}", id, prescriptionProduitDTO);
        if (prescriptionProduitDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prescriptionProduitDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prescriptionProduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PrescriptionProduitDTO result = prescriptionProduitService.update(prescriptionProduitDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prescriptionProduitDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /prescription-produits/:id} : Partial updates given fields of an existing prescriptionProduit, field will ignore if it is null
     *
     * @param id the id of the prescriptionProduitDTO to save.
     * @param prescriptionProduitDTO the prescriptionProduitDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated prescriptionProduitDTO,
     * or with status {@code 400 (Bad Request)} if the prescriptionProduitDTO is not valid,
     * or with status {@code 404 (Not Found)} if the prescriptionProduitDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the prescriptionProduitDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/prescription-produits/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PrescriptionProduitDTO> partialUpdatePrescriptionProduit(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PrescriptionProduitDTO prescriptionProduitDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PrescriptionProduit partially : {}, {}", id, prescriptionProduitDTO);
        if (prescriptionProduitDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, prescriptionProduitDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!prescriptionProduitRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PrescriptionProduitDTO> result = prescriptionProduitService.partialUpdate(prescriptionProduitDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, prescriptionProduitDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /prescription-produits} : get all the prescriptionProduits.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of prescriptionProduits in body.
     */
    @GetMapping("/prescription-produits")
    public ResponseEntity<List<PrescriptionProduitDTO>> getAllPrescriptionProduits(
        PrescriptionProduitCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get PrescriptionProduits by criteria: {}", criteria);
        Page<PrescriptionProduitDTO> page = prescriptionProduitQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /prescription-produits/count} : count all the prescriptionProduits.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/prescription-produits/count")
    public ResponseEntity<Long> countPrescriptionProduits(PrescriptionProduitCriteria criteria) {
        log.debug("REST request to count PrescriptionProduits by criteria: {}", criteria);
        return ResponseEntity.ok().body(prescriptionProduitQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /prescription-produits/:id} : get the "id" prescriptionProduit.
     *
     * @param id the id of the prescriptionProduitDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the prescriptionProduitDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/prescription-produits/{id}")
    public ResponseEntity<PrescriptionProduitDTO> getPrescriptionProduit(@PathVariable Long id) {
        log.debug("REST request to get PrescriptionProduit : {}", id);
        Optional<PrescriptionProduitDTO> prescriptionProduitDTO = prescriptionProduitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(prescriptionProduitDTO);
    }

    /**
     * {@code DELETE  /prescription-produits/:id} : delete the "id" prescriptionProduit.
     *
     * @param id the id of the prescriptionProduitDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/prescription-produits/{id}")
    public ResponseEntity<Void> deletePrescriptionProduit(@PathVariable Long id) {
        log.debug("REST request to delete PrescriptionProduit : {}", id);
        prescriptionProduitService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
