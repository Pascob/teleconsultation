package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.FormationSanitaireRepository;
import com.pascob.teleconsultation.service.FormationSanitaireQueryService;
import com.pascob.teleconsultation.service.FormationSanitaireService;
import com.pascob.teleconsultation.service.criteria.FormationSanitaireCriteria;
import com.pascob.teleconsultation.service.dto.FormationSanitaireDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.FormationSanitaire}.
 */
@RestController
@RequestMapping("/api")
public class FormationSanitaireResource {

    private final Logger log = LoggerFactory.getLogger(FormationSanitaireResource.class);

    private static final String ENTITY_NAME = "formationSanitaire";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FormationSanitaireService formationSanitaireService;

    private final FormationSanitaireRepository formationSanitaireRepository;

    private final FormationSanitaireQueryService formationSanitaireQueryService;

    public FormationSanitaireResource(
        FormationSanitaireService formationSanitaireService,
        FormationSanitaireRepository formationSanitaireRepository,
        FormationSanitaireQueryService formationSanitaireQueryService
    ) {
        this.formationSanitaireService = formationSanitaireService;
        this.formationSanitaireRepository = formationSanitaireRepository;
        this.formationSanitaireQueryService = formationSanitaireQueryService;
    }

    /**
     * {@code POST  /formation-sanitaires} : Create a new formationSanitaire.
     *
     * @param formationSanitaireDTO the formationSanitaireDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new formationSanitaireDTO, or with status {@code 400 (Bad Request)} if the formationSanitaire has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/formation-sanitaires")
    public ResponseEntity<FormationSanitaireDTO> createFormationSanitaire(@Valid @RequestBody FormationSanitaireDTO formationSanitaireDTO)
        throws URISyntaxException {
        log.debug("REST request to save FormationSanitaire : {}", formationSanitaireDTO);
        if (formationSanitaireDTO.getId() != null) {
            throw new BadRequestAlertException("A new formationSanitaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FormationSanitaireDTO result = formationSanitaireService.save(formationSanitaireDTO);
        return ResponseEntity
            .created(new URI("/api/formation-sanitaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /formation-sanitaires/:id} : Updates an existing formationSanitaire.
     *
     * @param id the id of the formationSanitaireDTO to save.
     * @param formationSanitaireDTO the formationSanitaireDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated formationSanitaireDTO,
     * or with status {@code 400 (Bad Request)} if the formationSanitaireDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the formationSanitaireDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/formation-sanitaires/{id}")
    public ResponseEntity<FormationSanitaireDTO> updateFormationSanitaire(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody FormationSanitaireDTO formationSanitaireDTO
    ) throws URISyntaxException {
        log.debug("REST request to update FormationSanitaire : {}, {}", id, formationSanitaireDTO);
        if (formationSanitaireDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, formationSanitaireDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!formationSanitaireRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FormationSanitaireDTO result = formationSanitaireService.update(formationSanitaireDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, formationSanitaireDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /formation-sanitaires/:id} : Partial updates given fields of an existing formationSanitaire, field will ignore if it is null
     *
     * @param id the id of the formationSanitaireDTO to save.
     * @param formationSanitaireDTO the formationSanitaireDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated formationSanitaireDTO,
     * or with status {@code 400 (Bad Request)} if the formationSanitaireDTO is not valid,
     * or with status {@code 404 (Not Found)} if the formationSanitaireDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the formationSanitaireDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/formation-sanitaires/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FormationSanitaireDTO> partialUpdateFormationSanitaire(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody FormationSanitaireDTO formationSanitaireDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update FormationSanitaire partially : {}, {}", id, formationSanitaireDTO);
        if (formationSanitaireDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, formationSanitaireDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!formationSanitaireRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FormationSanitaireDTO> result = formationSanitaireService.partialUpdate(formationSanitaireDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, formationSanitaireDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /formation-sanitaires} : get all the formationSanitaires.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of formationSanitaires in body.
     */
    @GetMapping("/formation-sanitaires")
    public ResponseEntity<List<FormationSanitaireDTO>> getAllFormationSanitaires(
        FormationSanitaireCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get FormationSanitaires by criteria: {}", criteria);
        Page<FormationSanitaireDTO> page = formationSanitaireQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /formation-sanitaires/count} : count all the formationSanitaires.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/formation-sanitaires/count")
    public ResponseEntity<Long> countFormationSanitaires(FormationSanitaireCriteria criteria) {
        log.debug("REST request to count FormationSanitaires by criteria: {}", criteria);
        return ResponseEntity.ok().body(formationSanitaireQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /formation-sanitaires/:id} : get the "id" formationSanitaire.
     *
     * @param id the id of the formationSanitaireDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the formationSanitaireDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/formation-sanitaires/{id}")
    public ResponseEntity<FormationSanitaireDTO> getFormationSanitaire(@PathVariable Long id) {
        log.debug("REST request to get FormationSanitaire : {}", id);
        Optional<FormationSanitaireDTO> formationSanitaireDTO = formationSanitaireService.findOne(id);
        return ResponseUtil.wrapOrNotFound(formationSanitaireDTO);
    }

    /**
     * {@code DELETE  /formation-sanitaires/:id} : delete the "id" formationSanitaire.
     *
     * @param id the id of the formationSanitaireDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/formation-sanitaires/{id}")
    public ResponseEntity<Void> deleteFormationSanitaire(@PathVariable Long id) {
        log.debug("REST request to delete FormationSanitaire : {}", id);
        formationSanitaireService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
