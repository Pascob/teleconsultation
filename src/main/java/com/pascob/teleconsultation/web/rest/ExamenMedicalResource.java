package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.ExamenMedicalRepository;
import com.pascob.teleconsultation.service.ExamenMedicalQueryService;
import com.pascob.teleconsultation.service.ExamenMedicalService;
import com.pascob.teleconsultation.service.criteria.ExamenMedicalCriteria;
import com.pascob.teleconsultation.service.dto.ExamenMedicalDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.ExamenMedical}.
 */
@RestController
@RequestMapping("/api")
public class ExamenMedicalResource {

    private final Logger log = LoggerFactory.getLogger(ExamenMedicalResource.class);

    private static final String ENTITY_NAME = "examenMedical";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExamenMedicalService examenMedicalService;

    private final ExamenMedicalRepository examenMedicalRepository;

    private final ExamenMedicalQueryService examenMedicalQueryService;

    public ExamenMedicalResource(
        ExamenMedicalService examenMedicalService,
        ExamenMedicalRepository examenMedicalRepository,
        ExamenMedicalQueryService examenMedicalQueryService
    ) {
        this.examenMedicalService = examenMedicalService;
        this.examenMedicalRepository = examenMedicalRepository;
        this.examenMedicalQueryService = examenMedicalQueryService;
    }

    /**
     * {@code POST  /examen-medicals} : Create a new examenMedical.
     *
     * @param examenMedicalDTO the examenMedicalDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new examenMedicalDTO, or with status {@code 400 (Bad Request)} if the examenMedical has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/examen-medicals")
    public ResponseEntity<ExamenMedicalDTO> createExamenMedical(@Valid @RequestBody ExamenMedicalDTO examenMedicalDTO)
        throws URISyntaxException {
        log.debug("REST request to save ExamenMedical : {}", examenMedicalDTO);
        if (examenMedicalDTO.getId() != null) {
            throw new BadRequestAlertException("A new examenMedical cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExamenMedicalDTO result = examenMedicalService.save(examenMedicalDTO);
        return ResponseEntity
            .created(new URI("/api/examen-medicals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /examen-medicals/:id} : Updates an existing examenMedical.
     *
     * @param id the id of the examenMedicalDTO to save.
     * @param examenMedicalDTO the examenMedicalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated examenMedicalDTO,
     * or with status {@code 400 (Bad Request)} if the examenMedicalDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the examenMedicalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/examen-medicals/{id}")
    public ResponseEntity<ExamenMedicalDTO> updateExamenMedical(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ExamenMedicalDTO examenMedicalDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ExamenMedical : {}, {}", id, examenMedicalDTO);
        if (examenMedicalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, examenMedicalDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!examenMedicalRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExamenMedicalDTO result = examenMedicalService.update(examenMedicalDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, examenMedicalDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /examen-medicals/:id} : Partial updates given fields of an existing examenMedical, field will ignore if it is null
     *
     * @param id the id of the examenMedicalDTO to save.
     * @param examenMedicalDTO the examenMedicalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated examenMedicalDTO,
     * or with status {@code 400 (Bad Request)} if the examenMedicalDTO is not valid,
     * or with status {@code 404 (Not Found)} if the examenMedicalDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the examenMedicalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/examen-medicals/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ExamenMedicalDTO> partialUpdateExamenMedical(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ExamenMedicalDTO examenMedicalDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExamenMedical partially : {}, {}", id, examenMedicalDTO);
        if (examenMedicalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, examenMedicalDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!examenMedicalRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExamenMedicalDTO> result = examenMedicalService.partialUpdate(examenMedicalDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, examenMedicalDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /examen-medicals} : get all the examenMedicals.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of examenMedicals in body.
     */
    @GetMapping("/examen-medicals")
    public ResponseEntity<List<ExamenMedicalDTO>> getAllExamenMedicals(
        ExamenMedicalCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get ExamenMedicals by criteria: {}", criteria);
        Page<ExamenMedicalDTO> page = examenMedicalQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /examen-medicals/count} : count all the examenMedicals.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/examen-medicals/count")
    public ResponseEntity<Long> countExamenMedicals(ExamenMedicalCriteria criteria) {
        log.debug("REST request to count ExamenMedicals by criteria: {}", criteria);
        return ResponseEntity.ok().body(examenMedicalQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /examen-medicals/:id} : get the "id" examenMedical.
     *
     * @param id the id of the examenMedicalDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the examenMedicalDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/examen-medicals/{id}")
    public ResponseEntity<ExamenMedicalDTO> getExamenMedical(@PathVariable Long id) {
        log.debug("REST request to get ExamenMedical : {}", id);
        Optional<ExamenMedicalDTO> examenMedicalDTO = examenMedicalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(examenMedicalDTO);
    }

    /**
     * {@code DELETE  /examen-medicals/:id} : delete the "id" examenMedical.
     *
     * @param id the id of the examenMedicalDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/examen-medicals/{id}")
    public ResponseEntity<Void> deleteExamenMedical(@PathVariable Long id) {
        log.debug("REST request to delete ExamenMedical : {}", id);
        examenMedicalService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
