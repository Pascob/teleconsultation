package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.OrdonnanceRepository;
import com.pascob.teleconsultation.service.OrdonnanceQueryService;
import com.pascob.teleconsultation.service.OrdonnanceService;
import com.pascob.teleconsultation.service.criteria.OrdonnanceCriteria;
import com.pascob.teleconsultation.service.dto.OrdonnanceDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.Ordonnance}.
 */
@RestController
@RequestMapping("/api")
public class OrdonnanceResource {

    private final Logger log = LoggerFactory.getLogger(OrdonnanceResource.class);

    private static final String ENTITY_NAME = "ordonnance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrdonnanceService ordonnanceService;

    private final OrdonnanceRepository ordonnanceRepository;

    private final OrdonnanceQueryService ordonnanceQueryService;

    public OrdonnanceResource(
        OrdonnanceService ordonnanceService,
        OrdonnanceRepository ordonnanceRepository,
        OrdonnanceQueryService ordonnanceQueryService
    ) {
        this.ordonnanceService = ordonnanceService;
        this.ordonnanceRepository = ordonnanceRepository;
        this.ordonnanceQueryService = ordonnanceQueryService;
    }

    /**
     * {@code POST  /ordonnances} : Create a new ordonnance.
     *
     * @param ordonnanceDTO the ordonnanceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ordonnanceDTO, or with status {@code 400 (Bad Request)} if the ordonnance has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ordonnances")
    public ResponseEntity<OrdonnanceDTO> createOrdonnance(@Valid @RequestBody OrdonnanceDTO ordonnanceDTO) throws URISyntaxException {
        log.debug("REST request to save Ordonnance : {}", ordonnanceDTO);
        if (ordonnanceDTO.getId() != null) {
            throw new BadRequestAlertException("A new ordonnance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrdonnanceDTO result = ordonnanceService.save(ordonnanceDTO);
        return ResponseEntity
            .created(new URI("/api/ordonnances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ordonnances/:id} : Updates an existing ordonnance.
     *
     * @param id the id of the ordonnanceDTO to save.
     * @param ordonnanceDTO the ordonnanceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ordonnanceDTO,
     * or with status {@code 400 (Bad Request)} if the ordonnanceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ordonnanceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ordonnances/{id}")
    public ResponseEntity<OrdonnanceDTO> updateOrdonnance(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OrdonnanceDTO ordonnanceDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Ordonnance : {}, {}", id, ordonnanceDTO);
        if (ordonnanceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ordonnanceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ordonnanceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OrdonnanceDTO result = ordonnanceService.update(ordonnanceDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ordonnanceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ordonnances/:id} : Partial updates given fields of an existing ordonnance, field will ignore if it is null
     *
     * @param id the id of the ordonnanceDTO to save.
     * @param ordonnanceDTO the ordonnanceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ordonnanceDTO,
     * or with status {@code 400 (Bad Request)} if the ordonnanceDTO is not valid,
     * or with status {@code 404 (Not Found)} if the ordonnanceDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the ordonnanceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ordonnances/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OrdonnanceDTO> partialUpdateOrdonnance(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody OrdonnanceDTO ordonnanceDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Ordonnance partially : {}, {}", id, ordonnanceDTO);
        if (ordonnanceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ordonnanceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ordonnanceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OrdonnanceDTO> result = ordonnanceService.partialUpdate(ordonnanceDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ordonnanceDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /ordonnances} : get all the ordonnances.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ordonnances in body.
     */
    @GetMapping("/ordonnances")
    public ResponseEntity<List<OrdonnanceDTO>> getAllOrdonnances(
        OrdonnanceCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Ordonnances by criteria: {}", criteria);
        Page<OrdonnanceDTO> page = ordonnanceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ordonnances/count} : count all the ordonnances.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/ordonnances/count")
    public ResponseEntity<Long> countOrdonnances(OrdonnanceCriteria criteria) {
        log.debug("REST request to count Ordonnances by criteria: {}", criteria);
        return ResponseEntity.ok().body(ordonnanceQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ordonnances/:id} : get the "id" ordonnance.
     *
     * @param id the id of the ordonnanceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ordonnanceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ordonnances/{id}")
    public ResponseEntity<OrdonnanceDTO> getOrdonnance(@PathVariable Long id) {
        log.debug("REST request to get Ordonnance : {}", id);
        Optional<OrdonnanceDTO> ordonnanceDTO = ordonnanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ordonnanceDTO);
    }

    /**
     * {@code DELETE  /ordonnances/:id} : delete the "id" ordonnance.
     *
     * @param id the id of the ordonnanceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ordonnances/{id}")
    public ResponseEntity<Void> deleteOrdonnance(@PathVariable Long id) {
        log.debug("REST request to delete Ordonnance : {}", id);
        ordonnanceService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
