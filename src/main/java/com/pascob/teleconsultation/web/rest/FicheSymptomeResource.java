package com.pascob.teleconsultation.web.rest;

import com.pascob.teleconsultation.repository.FicheSymptomeRepository;
import com.pascob.teleconsultation.service.FicheSymptomeQueryService;
import com.pascob.teleconsultation.service.FicheSymptomeService;
import com.pascob.teleconsultation.service.criteria.FicheSymptomeCriteria;
import com.pascob.teleconsultation.service.dto.FicheSymptomeDTO;
import com.pascob.teleconsultation.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.pascob.teleconsultation.domain.FicheSymptome}.
 */
@RestController
@RequestMapping("/api")
public class FicheSymptomeResource {

    private final Logger log = LoggerFactory.getLogger(FicheSymptomeResource.class);

    private static final String ENTITY_NAME = "ficheSymptome";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FicheSymptomeService ficheSymptomeService;

    private final FicheSymptomeRepository ficheSymptomeRepository;

    private final FicheSymptomeQueryService ficheSymptomeQueryService;

    public FicheSymptomeResource(
        FicheSymptomeService ficheSymptomeService,
        FicheSymptomeRepository ficheSymptomeRepository,
        FicheSymptomeQueryService ficheSymptomeQueryService
    ) {
        this.ficheSymptomeService = ficheSymptomeService;
        this.ficheSymptomeRepository = ficheSymptomeRepository;
        this.ficheSymptomeQueryService = ficheSymptomeQueryService;
    }

    /**
     * {@code POST  /fiche-symptomes} : Create a new ficheSymptome.
     *
     * @param ficheSymptomeDTO the ficheSymptomeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ficheSymptomeDTO, or with status {@code 400 (Bad Request)} if the ficheSymptome has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fiche-symptomes")
    public ResponseEntity<FicheSymptomeDTO> createFicheSymptome(@RequestBody FicheSymptomeDTO ficheSymptomeDTO) throws URISyntaxException {
        log.debug("REST request to save FicheSymptome : {}", ficheSymptomeDTO);
        if (ficheSymptomeDTO.getId() != null) {
            throw new BadRequestAlertException("A new ficheSymptome cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FicheSymptomeDTO result = ficheSymptomeService.save(ficheSymptomeDTO);
        return ResponseEntity
            .created(new URI("/api/fiche-symptomes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fiche-symptomes/:id} : Updates an existing ficheSymptome.
     *
     * @param id the id of the ficheSymptomeDTO to save.
     * @param ficheSymptomeDTO the ficheSymptomeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ficheSymptomeDTO,
     * or with status {@code 400 (Bad Request)} if the ficheSymptomeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ficheSymptomeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fiche-symptomes/{id}")
    public ResponseEntity<FicheSymptomeDTO> updateFicheSymptome(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FicheSymptomeDTO ficheSymptomeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update FicheSymptome : {}, {}", id, ficheSymptomeDTO);
        if (ficheSymptomeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ficheSymptomeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ficheSymptomeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FicheSymptomeDTO result = ficheSymptomeService.update(ficheSymptomeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ficheSymptomeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /fiche-symptomes/:id} : Partial updates given fields of an existing ficheSymptome, field will ignore if it is null
     *
     * @param id the id of the ficheSymptomeDTO to save.
     * @param ficheSymptomeDTO the ficheSymptomeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ficheSymptomeDTO,
     * or with status {@code 400 (Bad Request)} if the ficheSymptomeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the ficheSymptomeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the ficheSymptomeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/fiche-symptomes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FicheSymptomeDTO> partialUpdateFicheSymptome(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody FicheSymptomeDTO ficheSymptomeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update FicheSymptome partially : {}, {}", id, ficheSymptomeDTO);
        if (ficheSymptomeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ficheSymptomeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ficheSymptomeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FicheSymptomeDTO> result = ficheSymptomeService.partialUpdate(ficheSymptomeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ficheSymptomeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /fiche-symptomes} : get all the ficheSymptomes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ficheSymptomes in body.
     */
    @GetMapping("/fiche-symptomes")
    public ResponseEntity<List<FicheSymptomeDTO>> getAllFicheSymptomes(
        FicheSymptomeCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get FicheSymptomes by criteria: {}", criteria);
        Page<FicheSymptomeDTO> page = ficheSymptomeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /fiche-symptomes/count} : count all the ficheSymptomes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/fiche-symptomes/count")
    public ResponseEntity<Long> countFicheSymptomes(FicheSymptomeCriteria criteria) {
        log.debug("REST request to count FicheSymptomes by criteria: {}", criteria);
        return ResponseEntity.ok().body(ficheSymptomeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /fiche-symptomes/:id} : get the "id" ficheSymptome.
     *
     * @param id the id of the ficheSymptomeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ficheSymptomeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fiche-symptomes/{id}")
    public ResponseEntity<FicheSymptomeDTO> getFicheSymptome(@PathVariable Long id) {
        log.debug("REST request to get FicheSymptome : {}", id);
        Optional<FicheSymptomeDTO> ficheSymptomeDTO = ficheSymptomeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ficheSymptomeDTO);
    }

    /**
     * {@code DELETE  /fiche-symptomes/:id} : delete the "id" ficheSymptome.
     *
     * @param id the id of the ficheSymptomeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fiche-symptomes/{id}")
    public ResponseEntity<Void> deleteFicheSymptome(@PathVariable Long id) {
        log.debug("REST request to delete FicheSymptome : {}", id);
        ficheSymptomeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
