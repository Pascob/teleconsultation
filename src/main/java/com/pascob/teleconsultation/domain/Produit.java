package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "designation", nullable = false)
    private String designation;

    @OneToMany(mappedBy = "produit")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produit", "ordonnance" }, allowSetters = true)
    private Set<PrescriptionProduit> prescriptionProduits = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Produit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return this.designation;
    }

    public Produit designation(String designation) {
        this.setDesignation(designation);
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Set<PrescriptionProduit> getPrescriptionProduits() {
        return this.prescriptionProduits;
    }

    public void setPrescriptionProduits(Set<PrescriptionProduit> prescriptionProduits) {
        if (this.prescriptionProduits != null) {
            this.prescriptionProduits.forEach(i -> i.setProduit(null));
        }
        if (prescriptionProduits != null) {
            prescriptionProduits.forEach(i -> i.setProduit(this));
        }
        this.prescriptionProduits = prescriptionProduits;
    }

    public Produit prescriptionProduits(Set<PrescriptionProduit> prescriptionProduits) {
        this.setPrescriptionProduits(prescriptionProduits);
        return this;
    }

    public Produit addPrescriptionProduit(PrescriptionProduit prescriptionProduit) {
        this.prescriptionProduits.add(prescriptionProduit);
        prescriptionProduit.setProduit(this);
        return this;
    }

    public Produit removePrescriptionProduit(PrescriptionProduit prescriptionProduit) {
        this.prescriptionProduits.remove(prescriptionProduit);
        prescriptionProduit.setProduit(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Produit{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            "}";
    }
}
