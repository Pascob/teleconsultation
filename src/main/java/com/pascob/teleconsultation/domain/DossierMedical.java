package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pascob.teleconsultation.domain.enumeration.GroupeSanguin;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DossierMedical.
 */
@Entity
@Table(name = "dossier_medical")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DossierMedical implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @Enumerated(EnumType.STRING)
    @Column(name = "groupe_sanguin")
    private GroupeSanguin groupeSanguin;

    @OneToMany(mappedBy = "dossierMedical")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "consultations", "specialite", "dossierMedical" }, allowSetters = true)
    private Set<DemandeConsultation> demandeConsultations = new HashSet<>();

    @JsonIgnoreProperties(value = { "dossierMedical" }, allowSetters = true)
    @OneToOne(mappedBy = "dossierMedical")
    private Patient patient;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "dossierMedicals", "consultations", "ficheConsultations", "formationSanitaire", "specialite" },
        allowSetters = true
    )
    private Medecin medecin;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public DossierMedical id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return this.numero;
    }

    public DossierMedical numero(String numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public GroupeSanguin getGroupeSanguin() {
        return this.groupeSanguin;
    }

    public DossierMedical groupeSanguin(GroupeSanguin groupeSanguin) {
        this.setGroupeSanguin(groupeSanguin);
        return this;
    }

    public void setGroupeSanguin(GroupeSanguin groupeSanguin) {
        this.groupeSanguin = groupeSanguin;
    }

    public Set<DemandeConsultation> getDemandeConsultations() {
        return this.demandeConsultations;
    }

    public void setDemandeConsultations(Set<DemandeConsultation> demandeConsultations) {
        if (this.demandeConsultations != null) {
            this.demandeConsultations.forEach(i -> i.setDossierMedical(null));
        }
        if (demandeConsultations != null) {
            demandeConsultations.forEach(i -> i.setDossierMedical(this));
        }
        this.demandeConsultations = demandeConsultations;
    }

    public DossierMedical demandeConsultations(Set<DemandeConsultation> demandeConsultations) {
        this.setDemandeConsultations(demandeConsultations);
        return this;
    }

    public DossierMedical addDemandeConsultation(DemandeConsultation demandeConsultation) {
        this.demandeConsultations.add(demandeConsultation);
        demandeConsultation.setDossierMedical(this);
        return this;
    }

    public DossierMedical removeDemandeConsultation(DemandeConsultation demandeConsultation) {
        this.demandeConsultations.remove(demandeConsultation);
        demandeConsultation.setDossierMedical(null);
        return this;
    }

    public Patient getPatient() {
        return this.patient;
    }

    public void setPatient(Patient patient) {
        if (this.patient != null) {
            this.patient.setDossierMedical(null);
        }
        if (patient != null) {
            patient.setDossierMedical(this);
        }
        this.patient = patient;
    }

    public DossierMedical patient(Patient patient) {
        this.setPatient(patient);
        return this;
    }

    public Medecin getMedecin() {
        return this.medecin;
    }

    public void setMedecin(Medecin medecin) {
        this.medecin = medecin;
    }

    public DossierMedical medecin(Medecin medecin) {
        this.setMedecin(medecin);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DossierMedical)) {
            return false;
        }
        return id != null && id.equals(((DossierMedical) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DossierMedical{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", groupeSanguin='" + getGroupeSanguin() + "'" +
            "}";
    }
}
