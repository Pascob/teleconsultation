package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A FormationSanitaire.
 */
@Entity
@Table(name = "formation_sanitaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FormationSanitaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "designation", nullable = false)
    private String designation;

    @Column(name = "sigle")
    private String sigle;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "fixe")
    private String fixe;

    @Column(name = "bp")
    private String bp;

    @NotNull
    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    @Column(name = "email", nullable = false)
    private String email;

    @Lob
    @Column(name = "logo")
    private byte[] logo;

    @Column(name = "logo_content_type")
    private String logoContentType;

    @OneToMany(mappedBy = "formationSanitaire")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "dossierMedicals", "consultations", "ficheConsultations", "formationSanitaire", "specialite" },
        allowSetters = true
    )
    private Set<Medecin> medecins = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FormationSanitaire id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return this.designation;
    }

    public FormationSanitaire designation(String designation) {
        this.setDesignation(designation);
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getSigle() {
        return this.sigle;
    }

    public FormationSanitaire sigle(String sigle) {
        this.setSigle(sigle);
        return this;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public FormationSanitaire telephone(String telephone) {
        this.setTelephone(telephone);
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFixe() {
        return this.fixe;
    }

    public FormationSanitaire fixe(String fixe) {
        this.setFixe(fixe);
        return this;
    }

    public void setFixe(String fixe) {
        this.fixe = fixe;
    }

    public String getBp() {
        return this.bp;
    }

    public FormationSanitaire bp(String bp) {
        this.setBp(bp);
        return this;
    }

    public void setBp(String bp) {
        this.bp = bp;
    }

    public String getEmail() {
        return this.email;
    }

    public FormationSanitaire email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getLogo() {
        return this.logo;
    }

    public FormationSanitaire logo(byte[] logo) {
        this.setLogo(logo);
        return this;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return this.logoContentType;
    }

    public FormationSanitaire logoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
        return this;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public Set<Medecin> getMedecins() {
        return this.medecins;
    }

    public void setMedecins(Set<Medecin> medecins) {
        if (this.medecins != null) {
            this.medecins.forEach(i -> i.setFormationSanitaire(null));
        }
        if (medecins != null) {
            medecins.forEach(i -> i.setFormationSanitaire(this));
        }
        this.medecins = medecins;
    }

    public FormationSanitaire medecins(Set<Medecin> medecins) {
        this.setMedecins(medecins);
        return this;
    }

    public FormationSanitaire addMedecin(Medecin medecin) {
        this.medecins.add(medecin);
        medecin.setFormationSanitaire(this);
        return this;
    }

    public FormationSanitaire removeMedecin(Medecin medecin) {
        this.medecins.remove(medecin);
        medecin.setFormationSanitaire(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FormationSanitaire)) {
            return false;
        }
        return id != null && id.equals(((FormationSanitaire) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FormationSanitaire{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", sigle='" + getSigle() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", fixe='" + getFixe() + "'" +
            ", bp='" + getBp() + "'" +
            ", email='" + getEmail() + "'" +
            ", logo='" + getLogo() + "'" +
            ", logoContentType='" + getLogoContentType() + "'" +
            "}";
    }
}
