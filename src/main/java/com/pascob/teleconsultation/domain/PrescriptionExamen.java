package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PrescriptionExamen.
 */
@Entity
@Table(name = "prescription_examen")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PrescriptionExamen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "resultat")
    private String resultat;

    @ManyToOne
    @JsonIgnoreProperties(value = { "prescriptionExamen", "ficheConsultation" }, allowSetters = true)
    private Bulletin bulletin;

    @ManyToOne
    @JsonIgnoreProperties(value = { "prescriptionExamen" }, allowSetters = true)
    private ExamenMedical examenMedical;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PrescriptionExamen id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public PrescriptionExamen description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResultat() {
        return this.resultat;
    }

    public PrescriptionExamen resultat(String resultat) {
        this.setResultat(resultat);
        return this;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public Bulletin getBulletin() {
        return this.bulletin;
    }

    public void setBulletin(Bulletin bulletin) {
        this.bulletin = bulletin;
    }

    public PrescriptionExamen bulletin(Bulletin bulletin) {
        this.setBulletin(bulletin);
        return this;
    }

    public ExamenMedical getExamenMedical() {
        return this.examenMedical;
    }

    public void setExamenMedical(ExamenMedical examenMedical) {
        this.examenMedical = examenMedical;
    }

    public PrescriptionExamen examenMedical(ExamenMedical examenMedical) {
        this.setExamenMedical(examenMedical);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PrescriptionExamen)) {
            return false;
        }
        return id != null && id.equals(((PrescriptionExamen) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PrescriptionExamen{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", resultat='" + getResultat() + "'" +
            "}";
    }
}
