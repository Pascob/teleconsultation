package com.pascob.teleconsultation.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Message.
 */
@Entity
@Table(name = "message")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_message")
    private ZonedDateTime dateMessage;

    @Column(name = "message")
    private String message;

    @Column(name = "user_id_from")
    private Long userIdFrom;

    @Column(name = "user_id_to")
    private Long userIdTo;

    @Column(name = "readed")
    private Boolean readed;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Message id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateMessage() {
        return this.dateMessage;
    }

    public Message dateMessage(ZonedDateTime dateMessage) {
        this.setDateMessage(dateMessage);
        return this;
    }

    public void setDateMessage(ZonedDateTime dateMessage) {
        this.dateMessage = dateMessage;
    }

    public String getMessage() {
        return this.message;
    }

    public Message message(String message) {
        this.setMessage(message);
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getUserIdFrom() {
        return this.userIdFrom;
    }

    public Message userIdFrom(Long userIdFrom) {
        this.setUserIdFrom(userIdFrom);
        return this;
    }

    public void setUserIdFrom(Long userIdFrom) {
        this.userIdFrom = userIdFrom;
    }

    public Long getUserIdTo() {
        return this.userIdTo;
    }

    public Message userIdTo(Long userIdTo) {
        this.setUserIdTo(userIdTo);
        return this;
    }

    public void setUserIdTo(Long userIdTo) {
        this.userIdTo = userIdTo;
    }

    public Boolean getReaded() {
        return this.readed;
    }

    public Message readed(Boolean readed) {
        this.setReaded(readed);
        return this;
    }

    public void setReaded(Boolean readed) {
        this.readed = readed;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Message)) {
            return false;
        }
        return id != null && id.equals(((Message) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Message{" +
            "id=" + getId() +
            ", dateMessage='" + getDateMessage() + "'" +
            ", message='" + getMessage() + "'" +
            ", userIdFrom=" + getUserIdFrom() +
            ", userIdTo=" + getUserIdTo() +
            ", readed='" + getReaded() + "'" +
            "}";
    }
}
