package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Ordonnance.
 */
@Entity
@Table(name = "ordonnance")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Ordonnance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "date_ordonnance", nullable = false)
    private LocalDate dateOrdonnance;

    @Column(name = "numero")
    private String numero;

    @OneToMany(mappedBy = "ordonnance")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produit", "ordonnance" }, allowSetters = true)
    private Set<PrescriptionProduit> prescriptionProduits = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "ordonnances", "bulletins", "ficheSymptomes", "medecin", "consultation" }, allowSetters = true)
    private FicheConsultation ficheConsultation;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Ordonnance id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateOrdonnance() {
        return this.dateOrdonnance;
    }

    public Ordonnance dateOrdonnance(LocalDate dateOrdonnance) {
        this.setDateOrdonnance(dateOrdonnance);
        return this;
    }

    public void setDateOrdonnance(LocalDate dateOrdonnance) {
        this.dateOrdonnance = dateOrdonnance;
    }

    public String getNumero() {
        return this.numero;
    }

    public Ordonnance numero(String numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Set<PrescriptionProduit> getPrescriptionProduits() {
        return this.prescriptionProduits;
    }

    public void setPrescriptionProduits(Set<PrescriptionProduit> prescriptionProduits) {
        if (this.prescriptionProduits != null) {
            this.prescriptionProduits.forEach(i -> i.setOrdonnance(null));
        }
        if (prescriptionProduits != null) {
            prescriptionProduits.forEach(i -> i.setOrdonnance(this));
        }
        this.prescriptionProduits = prescriptionProduits;
    }

    public Ordonnance prescriptionProduits(Set<PrescriptionProduit> prescriptionProduits) {
        this.setPrescriptionProduits(prescriptionProduits);
        return this;
    }

    public Ordonnance addPrescriptionProduit(PrescriptionProduit prescriptionProduit) {
        this.prescriptionProduits.add(prescriptionProduit);
        prescriptionProduit.setOrdonnance(this);
        return this;
    }

    public Ordonnance removePrescriptionProduit(PrescriptionProduit prescriptionProduit) {
        this.prescriptionProduits.remove(prescriptionProduit);
        prescriptionProduit.setOrdonnance(null);
        return this;
    }

    public FicheConsultation getFicheConsultation() {
        return this.ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }

    public Ordonnance ficheConsultation(FicheConsultation ficheConsultation) {
        this.setFicheConsultation(ficheConsultation);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ordonnance)) {
            return false;
        }
        return id != null && id.equals(((Ordonnance) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ordonnance{" +
            "id=" + getId() +
            ", dateOrdonnance='" + getDateOrdonnance() + "'" +
            ", numero='" + getNumero() + "'" +
            "}";
    }
}
