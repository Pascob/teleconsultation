package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pascob.teleconsultation.domain.enumeration.StatutDemandeConsultation;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DemandeConsultation.
 */
@Entity
@Table(name = "demande_consultation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DemandeConsultation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_heure_demande")
    private ZonedDateTime dateHeureDemande;

    @Column(name = "ordre")
    private Integer ordre;

    @Enumerated(EnumType.STRING)
    @Column(name = "statut")
    private StatutDemandeConsultation statut;

    @Column(name = "moyen_paiement")
    private String moyenPaiement;

    @Column(name = "code_otp")
    private String codeOtp;

    @Column(name = "telephone_paiement")
    private String telephonePaiement;

    @DecimalMin(value = "0")
    @Column(name = "montant")
    private Double montant;

    @OneToMany(mappedBy = "demandeConsultation")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ficheConsultations", "medecin", "demandeConsultation" }, allowSetters = true)
    private Set<Consultation> consultations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "medecins", "demandeConsultations" }, allowSetters = true)
    private Specialite specialite;

    @ManyToOne
    @JsonIgnoreProperties(value = { "demandeConsultations", "patient", "medecin" }, allowSetters = true)
    private DossierMedical dossierMedical;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public DemandeConsultation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateHeureDemande() {
        return this.dateHeureDemande;
    }

    public DemandeConsultation dateHeureDemande(ZonedDateTime dateHeureDemande) {
        this.setDateHeureDemande(dateHeureDemande);
        return this;
    }

    public void setDateHeureDemande(ZonedDateTime dateHeureDemande) {
        this.dateHeureDemande = dateHeureDemande;
    }

    public Integer getOrdre() {
        return this.ordre;
    }

    public DemandeConsultation ordre(Integer ordre) {
        this.setOrdre(ordre);
        return this;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public StatutDemandeConsultation getStatut() {
        return this.statut;
    }

    public DemandeConsultation statut(StatutDemandeConsultation statut) {
        this.setStatut(statut);
        return this;
    }

    public void setStatut(StatutDemandeConsultation statut) {
        this.statut = statut;
    }

    public String getCodeOtp() {
        return this.codeOtp;
    }

    public DemandeConsultation codeOtp(String codeOtp) {
        this.setCodeOtp(codeOtp);
        return this;
    }

    public void setCodeOtp(String codeOtp) {
        this.codeOtp = codeOtp;
    }

    public String getTelephonePaiement() {
        return this.telephonePaiement;
    }

    public DemandeConsultation telephonePaiement(String telephonePaiement) {
        this.setTelephonePaiement(telephonePaiement);
        return this;
    }

    public void setTelephonePaiement(String telephonePaiement) {
        this.telephonePaiement = telephonePaiement;
    }

    public Double getMontant() {
        return this.montant;
    }

    public DemandeConsultation montant(Double montant) {
        this.setMontant(montant);
        return this;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Set<Consultation> getConsultations() {
        return this.consultations;
    }

    public void setConsultations(Set<Consultation> consultations) {
        if (this.consultations != null) {
            this.consultations.forEach(i -> i.setDemandeConsultation(null));
        }
        if (consultations != null) {
            consultations.forEach(i -> i.setDemandeConsultation(this));
        }
        this.consultations = consultations;
    }

    public DemandeConsultation consultations(Set<Consultation> consultations) {
        this.setConsultations(consultations);
        return this;
    }

    public DemandeConsultation addConsultation(Consultation consultation) {
        this.consultations.add(consultation);
        consultation.setDemandeConsultation(this);
        return this;
    }

    public DemandeConsultation removeConsultation(Consultation consultation) {
        this.consultations.remove(consultation);
        consultation.setDemandeConsultation(null);
        return this;
    }

    public Specialite getSpecialite() {
        return this.specialite;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }

    public DemandeConsultation specialite(Specialite specialite) {
        this.setSpecialite(specialite);
        return this;
    }

    public DossierMedical getDossierMedical() {
        return this.dossierMedical;
    }

    public void setDossierMedical(DossierMedical dossierMedical) {
        this.dossierMedical = dossierMedical;
    }

    public DemandeConsultation dossierMedical(DossierMedical dossierMedical) {
        this.setDossierMedical(dossierMedical);
        return this;
    }

    public String getMoyenPaiement() {
        return moyenPaiement;
    }

    public void setMoyenPaiement(String moyenPaiement) {
        this.moyenPaiement = moyenPaiement;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DemandeConsultation)) {
            return false;
        }
        return id != null && id.equals(((DemandeConsultation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DemandeConsultation{" +
            "id=" + getId() +
            ", dateHeureDemande='" + getDateHeureDemande() + "'" +
            ", ordre=" + getOrdre() +
            ", statut='" + getStatut() + "'" +
            ", codeOtp='" + getCodeOtp() + "'" +
            ", telephonePaiement='" + getTelephonePaiement() + "'" +
            ", montant=" + getMontant() +
            "}";
    }
}
