package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Consultation.
 */
@Entity
@Table(name = "consultation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Consultation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Min(value = 0)
    @Column(name = "poids")
    private Integer poids;

    @DecimalMin(value = "0")
    @Column(name = "temperature")
    private Float temperature;

    @Min(value = 0)
    @Column(name = "pouls")
    private Integer pouls;

    @Min(value = 0)
    @Column(name = "bpm")
    private Integer bpm;

    @DecimalMin(value = "0")
    @Column(name = "age")
    private Float age;

    @Min(value = 0)
    @Column(name = "taille")
    private Integer taille;

    @Column(name = "date_debut_consulation")
    private ZonedDateTime dateDebutConsulation;

    @Column(name = "is_suivi")
    private Boolean isSuivi;

    @Column(name = "is_close")
    private Boolean isClose;

    @OneToMany(mappedBy = "consultation")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ordonnances", "bulletins", "ficheSymptomes", "medecin", "consultation" }, allowSetters = true)
    private Set<FicheConsultation> ficheConsultations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "dossierMedicals", "consultations", "ficheConsultations", "formationSanitaire", "specialite" },
        allowSetters = true
    )
    private Medecin medecin;

    @ManyToOne
    @JsonIgnoreProperties(value = { "consultations", "specialite", "dossierMedical" }, allowSetters = true)
    private DemandeConsultation demandeConsultation;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Consultation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPoids() {
        return this.poids;
    }

    public Consultation poids(Integer poids) {
        this.setPoids(poids);
        return this;
    }

    public void setPoids(Integer poids) {
        this.poids = poids;
    }

    public Float getTemperature() {
        return this.temperature;
    }

    public Consultation temperature(Float temperature) {
        this.setTemperature(temperature);
        return this;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Integer getPouls() {
        return this.pouls;
    }

    public Consultation pouls(Integer pouls) {
        this.setPouls(pouls);
        return this;
    }

    public void setPouls(Integer pouls) {
        this.pouls = pouls;
    }

    public Integer getBpm() {
        return this.bpm;
    }

    public Consultation bpm(Integer bpm) {
        this.setBpm(bpm);
        return this;
    }

    public void setBpm(Integer bpm) {
        this.bpm = bpm;
    }

    public Float getAge() {
        return this.age;
    }

    public Consultation age(Float age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Float age) {
        this.age = age;
    }

    public Integer getTaille() {
        return this.taille;
    }

    public Consultation taille(Integer taille) {
        this.setTaille(taille);
        return this;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public ZonedDateTime getDateDebutConsulation() {
        return this.dateDebutConsulation;
    }

    public Consultation dateDebutConsulation(ZonedDateTime dateDebutConsulation) {
        this.setDateDebutConsulation(dateDebutConsulation);
        return this;
    }

    public void setDateDebutConsulation(ZonedDateTime dateDebutConsulation) {
        this.dateDebutConsulation = dateDebutConsulation;
    }

    public Boolean getIsSuivi() {
        return this.isSuivi;
    }

    public Consultation isSuivi(Boolean isSuivi) {
        this.setIsSuivi(isSuivi);
        return this;
    }

    public void setIsSuivi(Boolean isSuivi) {
        this.isSuivi = isSuivi;
    }

    public Boolean getIsClose() {
        return this.isClose;
    }

    public Consultation isClose(Boolean isClose) {
        this.setIsClose(isClose);
        return this;
    }

    public void setIsClose(Boolean isClose) {
        this.isClose = isClose;
    }

    public Set<FicheConsultation> getFicheConsultations() {
        return this.ficheConsultations;
    }

    public void setFicheConsultations(Set<FicheConsultation> ficheConsultations) {
        if (this.ficheConsultations != null) {
            this.ficheConsultations.forEach(i -> i.setConsultation(null));
        }
        if (ficheConsultations != null) {
            ficheConsultations.forEach(i -> i.setConsultation(this));
        }
        this.ficheConsultations = ficheConsultations;
    }

    public Consultation ficheConsultations(Set<FicheConsultation> ficheConsultations) {
        this.setFicheConsultations(ficheConsultations);
        return this;
    }

    public Consultation addFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultations.add(ficheConsultation);
        ficheConsultation.setConsultation(this);
        return this;
    }

    public Consultation removeFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultations.remove(ficheConsultation);
        ficheConsultation.setConsultation(null);
        return this;
    }

    public Medecin getMedecin() {
        return this.medecin;
    }

    public void setMedecin(Medecin medecin) {
        this.medecin = medecin;
    }

    public Consultation medecin(Medecin medecin) {
        this.setMedecin(medecin);
        return this;
    }

    public DemandeConsultation getDemandeConsultation() {
        return this.demandeConsultation;
    }

    public void setDemandeConsultation(DemandeConsultation demandeConsultation) {
        this.demandeConsultation = demandeConsultation;
    }

    public Consultation demandeConsultation(DemandeConsultation demandeConsultation) {
        this.setDemandeConsultation(demandeConsultation);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Consultation)) {
            return false;
        }
        return id != null && id.equals(((Consultation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Consultation{" +
            "id=" + getId() +
            ", poids=" + getPoids() +
            ", temperature=" + getTemperature() +
            ", pouls=" + getPouls() +
            ", bpm=" + getBpm() +
            ", age=" + getAge() +
            ", taille=" + getTaille() +
            ", dateDebutConsulation='" + getDateDebutConsulation() + "'" +
            ", isSuivi='" + getIsSuivi() + "'" +
            ", isClose='" + getIsClose() + "'" +
            "}";
    }
}
