package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A FicheConsultation.
 */
@Entity
@Table(name = "fiche_consultation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FicheConsultation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "date_fiche", nullable = false)
    private LocalDate dateFiche;

    @Column(name = "numero")
    private String numero;

    @Column(name = "observation")
    private String observation;

    @Column(name = "diagnostic")
    private String diagnostic;

    @OneToMany(mappedBy = "ficheConsultation")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "prescriptionProduits", "ficheConsultation" }, allowSetters = true)
    private Set<Ordonnance> ordonnances = new HashSet<>();

    @OneToMany(mappedBy = "ficheConsultation")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "prescriptionExamen", "ficheConsultation" }, allowSetters = true)
    private Set<Bulletin> bulletins = new HashSet<>();

    @OneToMany(mappedBy = "ficheConsultation")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ficheConsultation", "symptome" }, allowSetters = true)
    private Set<FicheSymptome> ficheSymptomes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "dossierMedicals", "consultations", "ficheConsultations", "formationSanitaire", "specialite" },
        allowSetters = true
    )
    private Medecin medecin;

    @ManyToOne
    @JsonIgnoreProperties(value = { "ficheConsultations", "medecin", "demandeConsultation" }, allowSetters = true)
    private Consultation consultation;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FicheConsultation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateFiche() {
        return this.dateFiche;
    }

    public FicheConsultation dateFiche(LocalDate dateFiche) {
        this.setDateFiche(dateFiche);
        return this;
    }

    public void setDateFiche(LocalDate dateFiche) {
        this.dateFiche = dateFiche;
    }

    public String getNumero() {
        return this.numero;
    }

    public FicheConsultation numero(String numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObservation() {
        return this.observation;
    }

    public FicheConsultation observation(String observation) {
        this.setObservation(observation);
        return this;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDiagnostic() {
        return this.diagnostic;
    }

    public FicheConsultation diagnostic(String diagnostic) {
        this.setDiagnostic(diagnostic);
        return this;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public Set<Ordonnance> getOrdonnances() {
        return this.ordonnances;
    }

    public void setOrdonnances(Set<Ordonnance> ordonnances) {
        if (this.ordonnances != null) {
            this.ordonnances.forEach(i -> i.setFicheConsultation(null));
        }
        if (ordonnances != null) {
            ordonnances.forEach(i -> i.setFicheConsultation(this));
        }
        this.ordonnances = ordonnances;
    }

    public FicheConsultation ordonnances(Set<Ordonnance> ordonnances) {
        this.setOrdonnances(ordonnances);
        return this;
    }

    public FicheConsultation addOrdonnance(Ordonnance ordonnance) {
        this.ordonnances.add(ordonnance);
        ordonnance.setFicheConsultation(this);
        return this;
    }

    public FicheConsultation removeOrdonnance(Ordonnance ordonnance) {
        this.ordonnances.remove(ordonnance);
        ordonnance.setFicheConsultation(null);
        return this;
    }

    public Set<Bulletin> getBulletins() {
        return this.bulletins;
    }

    public void setBulletins(Set<Bulletin> bulletins) {
        if (this.bulletins != null) {
            this.bulletins.forEach(i -> i.setFicheConsultation(null));
        }
        if (bulletins != null) {
            bulletins.forEach(i -> i.setFicheConsultation(this));
        }
        this.bulletins = bulletins;
    }

    public FicheConsultation bulletins(Set<Bulletin> bulletins) {
        this.setBulletins(bulletins);
        return this;
    }

    public FicheConsultation addBulletin(Bulletin bulletin) {
        this.bulletins.add(bulletin);
        bulletin.setFicheConsultation(this);
        return this;
    }

    public FicheConsultation removeBulletin(Bulletin bulletin) {
        this.bulletins.remove(bulletin);
        bulletin.setFicheConsultation(null);
        return this;
    }

    public Set<FicheSymptome> getFicheSymptomes() {
        return this.ficheSymptomes;
    }

    public void setFicheSymptomes(Set<FicheSymptome> ficheSymptomes) {
        if (this.ficheSymptomes != null) {
            this.ficheSymptomes.forEach(i -> i.setFicheConsultation(null));
        }
        if (ficheSymptomes != null) {
            ficheSymptomes.forEach(i -> i.setFicheConsultation(this));
        }
        this.ficheSymptomes = ficheSymptomes;
    }

    public FicheConsultation ficheSymptomes(Set<FicheSymptome> ficheSymptomes) {
        this.setFicheSymptomes(ficheSymptomes);
        return this;
    }

    public FicheConsultation addFicheSymptome(FicheSymptome ficheSymptome) {
        this.ficheSymptomes.add(ficheSymptome);
        ficheSymptome.setFicheConsultation(this);
        return this;
    }

    public FicheConsultation removeFicheSymptome(FicheSymptome ficheSymptome) {
        this.ficheSymptomes.remove(ficheSymptome);
        ficheSymptome.setFicheConsultation(null);
        return this;
    }

    public Medecin getMedecin() {
        return this.medecin;
    }

    public void setMedecin(Medecin medecin) {
        this.medecin = medecin;
    }

    public FicheConsultation medecin(Medecin medecin) {
        this.setMedecin(medecin);
        return this;
    }

    public Consultation getConsultation() {
        return this.consultation;
    }

    public void setConsultation(Consultation consultation) {
        this.consultation = consultation;
    }

    public FicheConsultation consultation(Consultation consultation) {
        this.setConsultation(consultation);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FicheConsultation)) {
            return false;
        }
        return id != null && id.equals(((FicheConsultation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FicheConsultation{" +
            "id=" + getId() +
            ", dateFiche='" + getDateFiche() + "'" +
            ", numero='" + getNumero() + "'" +
            ", observation='" + getObservation() + "'" +
            ", diagnostic='" + getDiagnostic() + "'" +
            "}";
    }
}
