package com.pascob.teleconsultation.domain.enumeration;

/**
 * The Genre enumeration.
 */
public enum Genre {
    M,
    F,
}
