package com.pascob.teleconsultation.domain.enumeration;

/**
 * The GroupeSanguin enumeration.
 */
public enum GroupeSanguin {
    O_PLUS,
    AB_PLUS,
    A_PLUS,
    B_PLUS,
    O_MOINS,
    AB_MOINS,
    A_MOINS,
    B_MOINS,
}
