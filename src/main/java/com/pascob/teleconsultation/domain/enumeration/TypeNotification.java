package com.pascob.teleconsultation.domain.enumeration;

/**
 * The TypeNotification enumeration.
 */
public enum TypeNotification {
    EMAIL,
    ALERT,
    SMS,
}
