package com.pascob.teleconsultation.domain.enumeration;

/**
 * The StatutDemandeConsultation enumeration.
 */
public enum StatutDemandeConsultation {
    EN_COURS,
    PAYE,
    EN_CONSULTATION,
    DELAIS_EXPIRE,
    CONSULTE,
}
