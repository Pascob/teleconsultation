package com.pascob.teleconsultation.domain;

import com.pascob.teleconsultation.domain.enumeration.TypeNotification;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Notification.
 */
@Entity
@Table(name = "notification")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_notification")
    private ZonedDateTime dateNotification;

    @Column(name = "objet")
    private String objet;

    @Column(name = "corpus")
    private String corpus;

    @Column(name = "sender")
    private String sender;

    @Column(name = "receiver")
    private String receiver;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_notification")
    private TypeNotification typeNotification;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Notification id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateNotification() {
        return this.dateNotification;
    }

    public Notification dateNotification(ZonedDateTime dateNotification) {
        this.setDateNotification(dateNotification);
        return this;
    }

    public void setDateNotification(ZonedDateTime dateNotification) {
        this.dateNotification = dateNotification;
    }

    public String getObjet() {
        return this.objet;
    }

    public Notification objet(String objet) {
        this.setObjet(objet);
        return this;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getCorpus() {
        return this.corpus;
    }

    public Notification corpus(String corpus) {
        this.setCorpus(corpus);
        return this;
    }

    public void setCorpus(String corpus) {
        this.corpus = corpus;
    }

    public String getSender() {
        return this.sender;
    }

    public Notification sender(String sender) {
        this.setSender(sender);
        return this;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return this.receiver;
    }

    public Notification receiver(String receiver) {
        this.setReceiver(receiver);
        return this;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public TypeNotification getTypeNotification() {
        return this.typeNotification;
    }

    public Notification typeNotification(TypeNotification typeNotification) {
        this.setTypeNotification(typeNotification);
        return this;
    }

    public void setTypeNotification(TypeNotification typeNotification) {
        this.typeNotification = typeNotification;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notification)) {
            return false;
        }
        return id != null && id.equals(((Notification) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Notification{" +
            "id=" + getId() +
            ", dateNotification='" + getDateNotification() + "'" +
            ", objet='" + getObjet() + "'" +
            ", corpus='" + getCorpus() + "'" +
            ", sender='" + getSender() + "'" +
            ", receiver='" + getReceiver() + "'" +
            ", typeNotification='" + getTypeNotification() + "'" +
            "}";
    }
}
