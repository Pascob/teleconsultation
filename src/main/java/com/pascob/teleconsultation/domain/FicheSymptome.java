package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A FicheSymptome.
 */
@Entity
@Table(name = "fiche_symptome")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class FicheSymptome implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "observation")
    private String observation;

    @ManyToOne
    @JsonIgnoreProperties(value = { "ordonnances", "bulletins", "ficheSymptomes", "medecin", "consultation" }, allowSetters = true)
    private FicheConsultation ficheConsultation;

    @ManyToOne
    @JsonIgnoreProperties(value = { "ficheSymptomes" }, allowSetters = true)
    private Symptome symptome;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FicheSymptome id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObservation() {
        return this.observation;
    }

    public FicheSymptome observation(String observation) {
        this.setObservation(observation);
        return this;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public FicheConsultation getFicheConsultation() {
        return this.ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }

    public FicheSymptome ficheConsultation(FicheConsultation ficheConsultation) {
        this.setFicheConsultation(ficheConsultation);
        return this;
    }

    public Symptome getSymptome() {
        return this.symptome;
    }

    public void setSymptome(Symptome symptome) {
        this.symptome = symptome;
    }

    public FicheSymptome symptome(Symptome symptome) {
        this.setSymptome(symptome);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FicheSymptome)) {
            return false;
        }
        return id != null && id.equals(((FicheSymptome) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FicheSymptome{" +
            "id=" + getId() +
            ", observation='" + getObservation() + "'" +
            "}";
    }
}
