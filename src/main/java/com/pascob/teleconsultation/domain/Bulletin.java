package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Bulletin.
 */
@Entity
@Table(name = "bulletin")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Bulletin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "date_bulletin", nullable = false)
    private LocalDate dateBulletin;

    @Column(name = "numero")
    private String numero;

    @OneToMany(mappedBy = "bulletin")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "bulletin", "examenMedical" }, allowSetters = true)
    private Set<PrescriptionExamen> prescriptionExamen = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "ordonnances", "bulletins", "ficheSymptomes", "medecin", "consultation" }, allowSetters = true)
    private FicheConsultation ficheConsultation;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Bulletin id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateBulletin() {
        return this.dateBulletin;
    }

    public Bulletin dateBulletin(LocalDate dateBulletin) {
        this.setDateBulletin(dateBulletin);
        return this;
    }

    public void setDateBulletin(LocalDate dateBulletin) {
        this.dateBulletin = dateBulletin;
    }

    public String getNumero() {
        return this.numero;
    }

    public Bulletin numero(String numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Set<PrescriptionExamen> getPrescriptionExamen() {
        return this.prescriptionExamen;
    }

    public void setPrescriptionExamen(Set<PrescriptionExamen> prescriptionExamen) {
        if (this.prescriptionExamen != null) {
            this.prescriptionExamen.forEach(i -> i.setBulletin(null));
        }
        if (prescriptionExamen != null) {
            prescriptionExamen.forEach(i -> i.setBulletin(this));
        }
        this.prescriptionExamen = prescriptionExamen;
    }

    public Bulletin prescriptionExamen(Set<PrescriptionExamen> prescriptionExamen) {
        this.setPrescriptionExamen(prescriptionExamen);
        return this;
    }

    public Bulletin addPrescriptionExamen(PrescriptionExamen prescriptionExamen) {
        this.prescriptionExamen.add(prescriptionExamen);
        prescriptionExamen.setBulletin(this);
        return this;
    }

    public Bulletin removePrescriptionExamen(PrescriptionExamen prescriptionExamen) {
        this.prescriptionExamen.remove(prescriptionExamen);
        prescriptionExamen.setBulletin(null);
        return this;
    }

    public FicheConsultation getFicheConsultation() {
        return this.ficheConsultation;
    }

    public void setFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultation = ficheConsultation;
    }

    public Bulletin ficheConsultation(FicheConsultation ficheConsultation) {
        this.setFicheConsultation(ficheConsultation);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Bulletin)) {
            return false;
        }
        return id != null && id.equals(((Bulletin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Bulletin{" +
            "id=" + getId() +
            ", dateBulletin='" + getDateBulletin() + "'" +
            ", numero='" + getNumero() + "'" +
            "}";
    }
}
