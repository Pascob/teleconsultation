package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ExamenMedical.
 */
@Entity
@Table(name = "examen_medical")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExamenMedical implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "designation", nullable = false)
    private String designation;

    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "examenMedical")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "bulletin", "examenMedical" }, allowSetters = true)
    private Set<PrescriptionExamen> prescriptionExamen = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ExamenMedical id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return this.designation;
    }

    public ExamenMedical designation(String designation) {
        this.setDesignation(designation);
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCode() {
        return this.code;
    }

    public ExamenMedical code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<PrescriptionExamen> getPrescriptionExamen() {
        return this.prescriptionExamen;
    }

    public void setPrescriptionExamen(Set<PrescriptionExamen> prescriptionExamen) {
        if (this.prescriptionExamen != null) {
            this.prescriptionExamen.forEach(i -> i.setExamenMedical(null));
        }
        if (prescriptionExamen != null) {
            prescriptionExamen.forEach(i -> i.setExamenMedical(this));
        }
        this.prescriptionExamen = prescriptionExamen;
    }

    public ExamenMedical prescriptionExamen(Set<PrescriptionExamen> prescriptionExamen) {
        this.setPrescriptionExamen(prescriptionExamen);
        return this;
    }

    public ExamenMedical addPrescriptionExamen(PrescriptionExamen prescriptionExamen) {
        this.prescriptionExamen.add(prescriptionExamen);
        prescriptionExamen.setExamenMedical(this);
        return this;
    }

    public ExamenMedical removePrescriptionExamen(PrescriptionExamen prescriptionExamen) {
        this.prescriptionExamen.remove(prescriptionExamen);
        prescriptionExamen.setExamenMedical(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExamenMedical)) {
            return false;
        }
        return id != null && id.equals(((ExamenMedical) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExamenMedical{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
