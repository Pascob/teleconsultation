package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Symptome.
 */
@Entity
@Table(name = "symptome")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Symptome implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "designation", nullable = false)
    private String designation;

    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "symptome")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ficheConsultation", "symptome" }, allowSetters = true)
    private Set<FicheSymptome> ficheSymptomes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Symptome id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return this.designation;
    }

    public Symptome designation(String designation) {
        this.setDesignation(designation);
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCode() {
        return this.code;
    }

    public Symptome code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<FicheSymptome> getFicheSymptomes() {
        return this.ficheSymptomes;
    }

    public void setFicheSymptomes(Set<FicheSymptome> ficheSymptomes) {
        if (this.ficheSymptomes != null) {
            this.ficheSymptomes.forEach(i -> i.setSymptome(null));
        }
        if (ficheSymptomes != null) {
            ficheSymptomes.forEach(i -> i.setSymptome(this));
        }
        this.ficheSymptomes = ficheSymptomes;
    }

    public Symptome ficheSymptomes(Set<FicheSymptome> ficheSymptomes) {
        this.setFicheSymptomes(ficheSymptomes);
        return this;
    }

    public Symptome addFicheSymptome(FicheSymptome ficheSymptome) {
        this.ficheSymptomes.add(ficheSymptome);
        ficheSymptome.setSymptome(this);
        return this;
    }

    public Symptome removeFicheSymptome(FicheSymptome ficheSymptome) {
        this.ficheSymptomes.remove(ficheSymptome);
        ficheSymptome.setSymptome(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Symptome)) {
            return false;
        }
        return id != null && id.equals(((Symptome) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Symptome{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
