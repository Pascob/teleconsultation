package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PrescriptionProduit.
 */
@Entity
@Table(name = "prescription_produit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PrescriptionProduit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "posologie")
    private String posologie;

    @Column(name = "observation")
    private String observation;

    @ManyToOne
    @JsonIgnoreProperties(value = { "prescriptionProduits" }, allowSetters = true)
    private Produit produit;

    @ManyToOne
    @JsonIgnoreProperties(value = { "prescriptionProduits", "ficheConsultation" }, allowSetters = true)
    private Ordonnance ordonnance;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PrescriptionProduit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPosologie() {
        return this.posologie;
    }

    public PrescriptionProduit posologie(String posologie) {
        this.setPosologie(posologie);
        return this;
    }

    public void setPosologie(String posologie) {
        this.posologie = posologie;
    }

    public String getObservation() {
        return this.observation;
    }

    public PrescriptionProduit observation(String observation) {
        this.setObservation(observation);
        return this;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Produit getProduit() {
        return this.produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public PrescriptionProduit produit(Produit produit) {
        this.setProduit(produit);
        return this;
    }

    public Ordonnance getOrdonnance() {
        return this.ordonnance;
    }

    public void setOrdonnance(Ordonnance ordonnance) {
        this.ordonnance = ordonnance;
    }

    public PrescriptionProduit ordonnance(Ordonnance ordonnance) {
        this.setOrdonnance(ordonnance);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PrescriptionProduit)) {
            return false;
        }
        return id != null && id.equals(((PrescriptionProduit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PrescriptionProduit{" +
            "id=" + getId() +
            ", posologie='" + getPosologie() + "'" +
            ", observation='" + getObservation() + "'" +
            "}";
    }
}
