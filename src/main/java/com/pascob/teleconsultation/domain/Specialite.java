package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Specialite.
 */
@Entity
@Table(name = "specialite")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Specialite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "designation", nullable = false)
    private String designation;

    @DecimalMin(value = "0")
    @Column(name = "frais")
    private Double frais;

    @OneToMany(mappedBy = "specialite")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "dossierMedicals", "consultations", "ficheConsultations", "formationSanitaire", "specialite" },
        allowSetters = true
    )
    private Set<Medecin> medecins = new HashSet<>();

    @OneToMany(mappedBy = "specialite")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "consultations", "specialite", "dossierMedical" }, allowSetters = true)
    private Set<DemandeConsultation> demandeConsultations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Specialite id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return this.designation;
    }

    public Specialite designation(String designation) {
        this.setDesignation(designation);
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Double getFrais() {
        return this.frais;
    }

    public Specialite frais(Double frais) {
        this.setFrais(frais);
        return this;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }

    public Set<Medecin> getMedecins() {
        return this.medecins;
    }

    public void setMedecins(Set<Medecin> medecins) {
        if (this.medecins != null) {
            this.medecins.forEach(i -> i.setSpecialite(null));
        }
        if (medecins != null) {
            medecins.forEach(i -> i.setSpecialite(this));
        }
        this.medecins = medecins;
    }

    public Specialite medecins(Set<Medecin> medecins) {
        this.setMedecins(medecins);
        return this;
    }

    public Specialite addMedecin(Medecin medecin) {
        this.medecins.add(medecin);
        medecin.setSpecialite(this);
        return this;
    }

    public Specialite removeMedecin(Medecin medecin) {
        this.medecins.remove(medecin);
        medecin.setSpecialite(null);
        return this;
    }

    public Set<DemandeConsultation> getDemandeConsultations() {
        return this.demandeConsultations;
    }

    public void setDemandeConsultations(Set<DemandeConsultation> demandeConsultations) {
        if (this.demandeConsultations != null) {
            this.demandeConsultations.forEach(i -> i.setSpecialite(null));
        }
        if (demandeConsultations != null) {
            demandeConsultations.forEach(i -> i.setSpecialite(this));
        }
        this.demandeConsultations = demandeConsultations;
    }

    public Specialite demandeConsultations(Set<DemandeConsultation> demandeConsultations) {
        this.setDemandeConsultations(demandeConsultations);
        return this;
    }

    public Specialite addDemandeConsultation(DemandeConsultation demandeConsultation) {
        this.demandeConsultations.add(demandeConsultation);
        demandeConsultation.setSpecialite(this);
        return this;
    }

    public Specialite removeDemandeConsultation(DemandeConsultation demandeConsultation) {
        this.demandeConsultations.remove(demandeConsultation);
        demandeConsultation.setSpecialite(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Specialite)) {
            return false;
        }
        return id != null && id.equals(((Specialite) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Specialite{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", frais=" + getFrais() +
            "}";
    }
}
