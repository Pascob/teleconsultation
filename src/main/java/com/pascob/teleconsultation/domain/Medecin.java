package com.pascob.teleconsultation.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Medecin.
 */
@Entity
@Table(name = "medecin")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Medecin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /*@NotNull
    @Column(name = "user_id", nullable = false)
    private Long userId;*/

    @Column(name = "matricule")
    private String matricule;

    @Lob
    @Column(name = "signature")
    private byte[] signature;

    @Column(name = "signature_content_type")
    private String signatureContentType;

    @OneToMany(mappedBy = "medecin")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "demandeConsultations", "patient", "medecin" }, allowSetters = true)
    private Set<DossierMedical> dossierMedicals = new HashSet<>();

    @OneToMany(mappedBy = "medecin")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ficheConsultations", "medecin", "demandeConsultation" }, allowSetters = true)
    private Set<Consultation> consultations = new HashSet<>();

    @OneToMany(mappedBy = "medecin")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ordonnances", "bulletins", "ficheSymptomes", "medecin", "consultation" }, allowSetters = true)
    private Set<FicheConsultation> ficheConsultations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "medecins" }, allowSetters = true)
    private FormationSanitaire formationSanitaire;

    @ManyToOne
    @JsonIgnoreProperties(value = { "medecins", "demandeConsultations" }, allowSetters = true)
    private Specialite specialite;

    @JsonIgnoreProperties(value = { "medecin" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Medecin id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //    public Long getUserId() {
    //        return this.userId;
    //    }

    public Medecin user(User user) {
        this.setUser(user);
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    //    public void setUserId(Long userId) {
    //        this.userId = userId;
    //    }

    public String getMatricule() {
        return this.matricule;
    }

    public Medecin matricule(String matricule) {
        this.setMatricule(matricule);
        return this;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public byte[] getSignature() {
        return this.signature;
    }

    public Medecin signature(byte[] signature) {
        this.setSignature(signature);
        return this;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public String getSignatureContentType() {
        return this.signatureContentType;
    }

    public Medecin signatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
        return this;
    }

    public void setSignatureContentType(String signatureContentType) {
        this.signatureContentType = signatureContentType;
    }

    public Set<DossierMedical> getDossierMedicals() {
        return this.dossierMedicals;
    }

    public void setDossierMedicals(Set<DossierMedical> dossierMedicals) {
        if (this.dossierMedicals != null) {
            this.dossierMedicals.forEach(i -> i.setMedecin(null));
        }
        if (dossierMedicals != null) {
            dossierMedicals.forEach(i -> i.setMedecin(this));
        }
        this.dossierMedicals = dossierMedicals;
    }

    public Medecin dossierMedicals(Set<DossierMedical> dossierMedicals) {
        this.setDossierMedicals(dossierMedicals);
        return this;
    }

    public Medecin addDossierMedical(DossierMedical dossierMedical) {
        this.dossierMedicals.add(dossierMedical);
        dossierMedical.setMedecin(this);
        return this;
    }

    public Medecin removeDossierMedical(DossierMedical dossierMedical) {
        this.dossierMedicals.remove(dossierMedical);
        dossierMedical.setMedecin(null);
        return this;
    }

    public Set<Consultation> getConsultations() {
        return this.consultations;
    }

    public void setConsultations(Set<Consultation> consultations) {
        if (this.consultations != null) {
            this.consultations.forEach(i -> i.setMedecin(null));
        }
        if (consultations != null) {
            consultations.forEach(i -> i.setMedecin(this));
        }
        this.consultations = consultations;
    }

    public Medecin consultations(Set<Consultation> consultations) {
        this.setConsultations(consultations);
        return this;
    }

    public Medecin addConsultation(Consultation consultation) {
        this.consultations.add(consultation);
        consultation.setMedecin(this);
        return this;
    }

    public Medecin removeConsultation(Consultation consultation) {
        this.consultations.remove(consultation);
        consultation.setMedecin(null);
        return this;
    }

    public Set<FicheConsultation> getFicheConsultations() {
        return this.ficheConsultations;
    }

    public void setFicheConsultations(Set<FicheConsultation> ficheConsultations) {
        if (this.ficheConsultations != null) {
            this.ficheConsultations.forEach(i -> i.setMedecin(null));
        }
        if (ficheConsultations != null) {
            ficheConsultations.forEach(i -> i.setMedecin(this));
        }
        this.ficheConsultations = ficheConsultations;
    }

    public Medecin ficheConsultations(Set<FicheConsultation> ficheConsultations) {
        this.setFicheConsultations(ficheConsultations);
        return this;
    }

    public Medecin addFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultations.add(ficheConsultation);
        ficheConsultation.setMedecin(this);
        return this;
    }

    public Medecin removeFicheConsultation(FicheConsultation ficheConsultation) {
        this.ficheConsultations.remove(ficheConsultation);
        ficheConsultation.setMedecin(null);
        return this;
    }

    public FormationSanitaire getFormationSanitaire() {
        return this.formationSanitaire;
    }

    public void setFormationSanitaire(FormationSanitaire formationSanitaire) {
        this.formationSanitaire = formationSanitaire;
    }

    public Medecin formationSanitaire(FormationSanitaire formationSanitaire) {
        this.setFormationSanitaire(formationSanitaire);
        return this;
    }

    public Specialite getSpecialite() {
        return this.specialite;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }

    public Medecin specialite(Specialite specialite) {
        this.setSpecialite(specialite);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Medecin)) {
            return false;
        }
        return id != null && id.equals(((Medecin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Medecin{" +
            "id=" + getId() +
            ", user=" + getUser() +
            ", matricule='" + getMatricule() + "'" +
            ", signature='" + getSignature() + "'" +
            ", signatureContentType='" + getSignatureContentType() + "'" +
            "}";
    }
}
